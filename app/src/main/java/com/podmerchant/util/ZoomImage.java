package com.podmerchant.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.podmerchant.R;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;


public class ZoomImage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoom_image);

        ImageView imgClose = findViewById(R.id.imgClose);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String img1 = extras.getString("img1");
        String img2 = extras.getString("img2");
        String img3 = extras.getString("img3");
        String img4 = extras.getString("img4");
        String img5 = extras.getString("img5");

        String strimg1 = "https://simg.apneareamein.com/" + img1;
        String strimg2 = "https://simg.apneareamein.com/" + img2;
        String strimg3 = "https://simg.apneareamein.com/" + img3;
        String strimg4 = "https://simg.apneareamein.com/" + img4;
        String strimg5 = "https://simg.apneareamein.com/" + img5;

        ArrayList<String> itemsimg = new ArrayList<>();
        itemsimg.add(strimg1);

        if (!(img2 != null && img2.equals(""))) {
            itemsimg.add(strimg2);
        }
        if (!(img3 != null && img3.equals(""))) {
            itemsimg.add(strimg3);
        }
        if (!(img4 != null && img4.equals(""))) {
            itemsimg.add(strimg4);
        }
        if (!(img5 != null && img5.equals(""))) {
            itemsimg.add(strimg5);
        }


        ViewPager viewPager = findViewById(R.id.view_pager);
        CirclePageIndicator circlePageIndicator = findViewById(R.id.circleIndicator);
        ImageAdapter adapter = new ImageAdapter(this, itemsimg);
        viewPager.setAdapter(adapter);
        circlePageIndicator.setViewPager(viewPager);

        float density = getResources().getDisplayMetrics().density;
        circlePageIndicator.setRadius(3 * density);
    }

    private class ImageAdapter extends PagerAdapter {

        private ArrayList<String> IMAGES;
        private Context context;

        ImageAdapter(Context context, ArrayList<String> productImage) {
            this.context = context;
            this.IMAGES = productImage;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return IMAGES.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup viewGroup, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.swipe_image_layout, viewGroup, false);

            ImageView imageView = view.findViewById(R.id.image);

            //TODO here product images setup to glide
            /*Glide.with(context).load(IMAGES.get(position))
                    .thumbnail(Glide.with(context).load(R.drawable.loading))
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);*/

            Picasso.with(ZoomImage.this).load(IMAGES.get(position))
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(imageView);

            viewGroup.addView(view, 0);

            return view;
        }
    }

}
