package com.podmerchant.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {

    private Context _context;
    SharedPreferences prefs;

    public SharedPreferencesUtils(Context _context) {
        this._context = _context;
        try {
            prefs = this._context.getSharedPreferences("application", Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void logout()
    {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }
    public void setLoginFlag(boolean isLogin) {

        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("loginFlag", isLogin);
        editor.commit();
    }

    public boolean getLoginFlag() {
        if (prefs.contains("loginFlag"))
            return prefs.getBoolean("loginFlag", false);
        else
            return false;
    }

    public void setLoginusers(String loginusers)
    {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("loginusers", loginusers);
        editor.commit();
    }
    public String getLoginusers() {
        if (prefs.contains("loginusers"))
            return prefs.getString("loginusers", "");
        else
            return "";
    }

    //email id
    public void setEmailId(String EmailId) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("EmailId", EmailId);
        editor.commit();
    }
    public String getEmailId() {
        if (prefs.contains("EmailId"))
            return prefs.getString("EmailId", "");
        else
            return "";
    }

    //set sop admin type for delivery boy
    public void setShopAdminType(String ShopAdminType){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ShopAdminType",ShopAdminType);
        editor.commit();
    }
    public String getShopAdminType(){
        if(prefs.contains("ShopAdminType"))
            return prefs.getString("ShopAdminType","");
        else
            return "";
    }

    //set shop name
    public void setShopName(String ShopName){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ShopName",ShopName);
        editor.commit();
    }
    public String getShopName(){
        if(prefs.contains("ShopName")){
            return  prefs.getString("ShopName","PICODEL Partner");
        }
        return "";
    }

    //user id
    public void setDelId(String DelID) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("DelID", DelID);
        editor.commit();
    }
    public String getDelId() {
        if (prefs.contains("DelID"))
            return prefs.getString("DelID", "");
        else
            return "";
    }


    //manager id
    public void setManagerId(String ManagerID) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ManagerID", ManagerID);
        editor.commit();
    }
    public String getManagerId() {
        if (prefs.contains("ManagerID"))
            return prefs.getString("ManagerID", "");
        else
            return "";
    }


    //token
    public int setToken(String Token) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Token", Token);
        editor.commit();
        return 0;
    }
    public String getToken() {
        if (prefs.contains("Token"))
            return prefs.getString("Token", "");
        else
            return "";
    }
    //first_name
    public int setShopStatus(String ShopStatus) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ShopStatus", ShopStatus);
        editor.commit();
        return 0;
    }
    public String getShopStatus() {
        if (prefs.contains("ShopStatus"))
            return prefs.getString("ShopStatus", "");
        else
            return "";
    }
    //last_name
    public int setAdminType(String AdminType) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("AdminType", AdminType);
        editor.commit();
        return 0;
    }
    public String getAdminType() {
        if (prefs.contains("AdminType"))
            return prefs.getString("AdminType", "");
        else
            return "";
    }
    //username
    public int setUserName(String UserName) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("UserName", UserName);
        editor.commit();
        return 0;
    }
    public String getUserName() {
        if (prefs.contains("UserName"))
            return prefs.getString("UserName", "");
        else
            return "";
    }
    //phonenumber

    public int setPhoneNumber(String PhoneNumber) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("PhoneNumber", PhoneNumber);
        editor.commit();
        return 0;
    }
    public String getPhoneNumber() {
        if (prefs.contains("PhoneNumber"))
            return prefs.getString("PhoneNumber", "");
        else
            return "";
    }

    //available_balance
    public int setAvailableBalance(String LoginType) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("AvailableBalance", LoginType);
        editor.commit();
        return 0;
    }
    public String getAvailableBalance() {
        if (prefs.contains("AvailableBalance"))
            return prefs.getString("AvailableBalance", "");
        else
            return "";
    }
    //Sessionid
    public int setSessionId(String Sessionid) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Sessionid", Sessionid);
        editor.commit();
        return 0;
    }
    public String getSessionId() {
        if (prefs.contains("Sessionid"))
            return prefs.getString("Sessionid", "");
        else
            return "";
    }


    public void setuser_profile(String profile)
    {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Profile", profile);
        editor.commit();
    }
    public String get_Profile(){
        if (prefs.contains("Profile"))
        {
            return prefs.getString("Profile","");
        }
        else return "";
    }


    public int setImageUrl(String ImageUrl) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ImageUrl", ImageUrl);
        editor.commit();
        return 0;
    }
    public String getImageUrl() {
        if (prefs.contains("ImageUrl"))
            return prefs.getString("ImageUrl", "");
        else
            return "";
    }

    public int setMenuResponce(String MenuResponce) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("MenuResponce", MenuResponce);
        editor.commit();
        return 0;
    }
    public String getMenuResponce() {
        if (prefs.contains("MenuResponce"))
            return prefs.getString("MenuResponce", "");
        else
            return "";
    }

    public int setCatCode(String CatCode) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("CatCode", CatCode);
        editor.commit();
        return 0;
    }
    public String getCatCode() {
        if (prefs.contains("CatCode"))
            return prefs.getString("CatCode", "");
        else
            return "";
    }

    public int setDistancekm(String distancekm) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("distancekm", distancekm);
        editor.commit();
        return 0;
    }
    public String getDistancekm() {
        if (prefs.contains("distancekm"))
            return prefs.getString("distancekm", "");
        else
            return "";
    }

    public int setOrder_code(String Order_code) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Order_code", Order_code);
        editor.commit();
        return 0;
    }
    public String getOrder_code() {
        if (prefs.contains("Order_code"))
            return prefs.getString("Order_code", "");
        else
            return "";
    }

    public int setPosition(int Position) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Position", String.valueOf(Position));
        editor.commit();
        return 0;
    }
    public String getPosition() {
        if (prefs.contains("Position"))
            return prefs.getString("Position", "");
        else
            return "";
    }

    public int setnewStatus(String newStatus) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("newStatus", newStatus);
        editor.commit();
        return 0;
    }
    public String getnewStatus() {
        if (prefs.contains("newStatus"))
            return prefs.getString("newStatus", "");
        else
            return "";
    }

    public int setUserID(String UserID) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("UserID", UserID);
        editor.commit();
        return 0;
    }
    public String getUserID() {
        if (prefs.contains("UserID"))
            return prefs.getString("UserID", "");
        else
            return "";
    }

    public int setShopID(String ShopID) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ShopID", ShopID);
        editor.commit();
        return 0;
    }
    public String getShopID() {
        if (prefs.contains("ShopID"))
            return prefs.getString("ShopID", "");
        else
            return "";
    }


    public int setAdminID(String Admin_id) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Admin_id", Admin_id);
        editor.commit();
        return 0;
    }
    public String getAdminID() {
        if (prefs.contains("Admin_id"))
            return prefs.getString("Admin_id", "");
        else
            return "";
    }

    public int setLatitude(String latitude){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Latitude",latitude);
        editor.commit();
        return 0;
    }

    public String getLatitude(){
        if(prefs.contains("Latitude"))
            return prefs.getString("Latitude","");
        return "";
    }


    public int setLongitude(String longitude){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Longitude",longitude);
        editor.commit();
        return 0;
    }

    public String getLongitude(){
        if(prefs.contains("Longitude"))
            return prefs.getString("Longitude","");
        return "";
    }

    //set local language
    public int setLocal(String localKey){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("localKey",localKey);
        editor.commit();
        return 0;
    }

    public String getLocal(){
        if(prefs.contains("localKey"))
             return prefs.getString("localKey","en_US");
        return "";
    }


    //store registration data in  session.
    public void setRPageno(String rPageNo){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rPageNo",rPageNo);
        editor.commit();
    }
    public String getRPageno(){
        if(prefs.contains("rPageNo"))
            return prefs.getString("rPageNo","");
        return "";
    }
    public void setROwnerGender(String rOwnerGender){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rOwnerGender",rOwnerGender);
        editor.commit();
    }
    public String getROwnerGender(){
        if(prefs.contains("rOwnerGender"))
            return prefs.getString("rOwnerGender","");
        return "";
    }
    public void setROwnerName(String rOwnerName){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rOwnerName",rOwnerName);
        editor.commit();
    }
    public String getROwnerName(){
        if(prefs.contains("rOwnerName"))
            return prefs.getString("rOwnerName","");
        return "";
    }
    public void setRShopName(String rShopName){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rShopName",rShopName);
        editor.commit();
    }
    public String getRShopName(){
        if(prefs.contains("rShopName"))
            return prefs.getString("rShopName","");
        return "";
    }
    public void setROwnerContact(String rOwnerContact){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rOwnerContact",rOwnerContact);
        editor.commit();
    }
    public String getROwnerContact(){
        if(prefs.contains("rOwnerContact"))
            return prefs.getString("rOwnerContact","");
        return "";
    }
    public void setRShopContact(String rShopContact){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rShopContact",rShopContact);
        editor.commit();
    }
    public String getRShopContact(){
        if(prefs.contains("rShopContact"))
            return prefs.getString("rShopContact","");
        return "";
    }
    public void setRShopEmail(String rShopEmail){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rShopEmail",rShopEmail);
        editor.commit();
    }
    public String getRShopEmail(){
        if(prefs.contains("rShopEmail"))
            return prefs.getString("rShopEmail","");
        return "";
    }
    //sp_category
    public void setRShopCatType(String rShopCatType){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rShopCatType",rShopCatType);
        editor.commit();
    }
    public String getRShopCatType(){
        if(prefs.contains("rShopCatType"))
            return prefs.getString("rShopCatType","");
        return "";
    }
    ///sp_shoptype
    public void setRShopType(String rShopType){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rShopType",rShopType);
        editor.commit();
    }
    public String getRShopType(){
        if(prefs.contains("rShopType"))
            return prefs.getString("rShopType","");
        return "";
    }
    //sp_shopadmintype
    public void setRShopAdmintype(String rShopAdminType){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rShopAdminType",rShopAdminType);
        editor.commit();
    }
    public String getRShoppAdmintype(){
        if(prefs.contains("rShopAdminType"))
            return prefs.getString("rShopAdminType","");
        return "";
    }
    //sp_turnover
    public void setRTurnover(String rTurnover){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rTurnover",rTurnover);
        editor.commit();
    }
    public String getRTurnover(){
        if(prefs.contains("rTurnover"))
            return prefs.getString("rTurnover","");
        return "";
    }
    //et_how_may_products
    public void setRHowManyProd(String rHowManyProd){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rHowManyProd",rHowManyProd);
        editor.commit();
    }
    public String getRHowManyProd(){
        if(prefs.contains("rHowManyProd"))
            return prefs.getString("rHowManyProd","");
        return "";
    }
    //et_lowest_cost
    public void setRLowestCost(String rLowestCost){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rLowestCost",rLowestCost);
        editor.commit();
    }
    public String getRLowestCost(){
        if(prefs.contains("rLowestCost"))
            return prefs.getString("rLowestCost","");
        return "";
    }
    //et_highest_cost
    public void setRHighestCost(String rHighestCost){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rHighestCost",rHighestCost);
        editor.commit();
    }
    public String getRHighestCost(){
        if(prefs.contains("rHighestCost"))
            return prefs.getString("rHighestCost","");
        return "";
    }
    //cb_required_licence
    public void setRCheckLice(boolean rCheckLice){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("rCheckLice", rCheckLice);
        editor.commit();
    }
    public boolean getRCheckLice(){
        if(prefs.contains("rCheckLice"))
            return prefs.getBoolean("rCheckLice",true);
        return true;
    }
    //cb_quility_product_img
    public void setRCheckQualityImg(boolean rCheckQualityImg){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("rCheckQualityImg", rCheckQualityImg);
        editor.commit();
    }
    public boolean getRCheckQualityImg(){
        if(prefs.contains("rCheckQualityImg"))
            return prefs.getBoolean("rCheckQualityImg",true);
        return true;
    }
    //et_managername
    public void setRManagerName(String rManagerName){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rManagerName",rManagerName);
        editor.commit();
    }
    public String getRManagerName(){
        if(prefs.contains("rManagerName"))
            return prefs.getString("rManagerName","");
        return "";
    }
    //et_managercontact
    public void setRManagerContact(String rManagerContact){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rManagerContact",rManagerContact);
        editor.commit();
    }
    public String getRManagerContact(){
        if(prefs.contains("rManagerContact"))
            return prefs.getString("rManagerContact","");
        return "";
    }
    //sp_document
    public void setRDocument(String rDocument){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rDocument",rDocument);
        editor.commit();
    }
    public String getRDocument(){
        if(prefs.contains("rDocument"))
            return prefs.getString("rDocument","");
        return "";
    }
    //reg_other_document
    public void setROtherDocument(String rOtherDocument){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rOtherDocument",rOtherDocument);
        editor.commit();
    }
    public String getROtherDocument(){
        if(prefs.contains("rOtherDocument"))
            return prefs.getString("rOtherDocument","");
        return "";
    }
    //idproofBitmap
    public void setRIdProofBitmap(String rIdProofBitmap){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rIdProofBitmap", rIdProofBitmap);
        editor.commit();
    }
    public String getRIdProofBitmap(){
        if(prefs.contains("rIdProofBitmap"))
            return prefs.getString("rIdProofBitmap", "");
        return  "";
    }
    //et_reg_shopgstno
    public void setRGstNo(String rGstNo){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rGstNo",rGstNo);
        editor.commit();
    }
    public String getRGstNo(){
        if(prefs.contains("rGstNo"))
            return prefs.getString("rGstNo","");
        return "";
    }
    //sp_deliveryboy
    public void setRDeliveryboy(String rDeliveryboy){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rDeliveryboy",rDeliveryboy);
        editor.commit();
    }
    public String getDeliveryboy(){
        if(prefs.contains("rDeliveryboy"))
            return prefs.getString("rDeliveryboy","");
        return "";
    }
    //et_no_delivery_boy
    public void setRNoDeliveryboy(String rNoDeliveryboy){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rNoDeliveryboy",rNoDeliveryboy);
        editor.commit();
    }
    public String getNoDeliveryboy(){
        if(prefs.contains("rNoDeliveryboy"))
            return prefs.getString("rNoDeliveryboy","");
        return "";
    }
    //sp_knowledgetech
    public void setRKnowledgeTech(String rKnowledgeTech){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rKnowledgeTech",rKnowledgeTech);
        editor.commit();
    }
    public String getRKnowledgeTech(){
        if(prefs.contains("rKnowledgeTech"))
            return prefs.getString("rKnowledgeTech","");
        return "";
    }
    //sp_payschedule
    public void setRPaySchedule(String rPaySchedule){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rPaySchedule",rPaySchedule);
        editor.commit();
    }
    public String getRPaySchedule(){
        if(prefs.contains("rPaySchedule"))
            return prefs.getString("rPaySchedule","");
        return "";
    }
    //sp_idproof
    public void setRIdProof(String rIdProof){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rIdProof",rIdProof);
        editor.commit();
    }
    public String getRIdProof(){
        if(prefs.contains("rIdProof"))
            return prefs.getString("rIdProof","");
        return "";
    }
    //img_licence id_proof image
    public void setRIdProofImg(String rIdProofImg){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rIdProofImg",rIdProofImg);
        editor.commit();
    }
    public String getRIdProofImg(){
        if(prefs.contains("rIdProofImg"))
            return prefs.getString("rIdProofImg","");
        return "";
    }
    //shopbitmap
    public void setRShopImg(String rShopImg){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rShopImg",rShopImg);
        editor.commit();
    }
    public String getRShopImg(){
        if(prefs.contains("rShopImg"))
            return prefs.getString("rShopImg","");
        return "";
    }
    //bar_area_km
    public void setRSeekBar(int rSeekBar){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("rSeekBar",rSeekBar);
        editor.commit();
    }
    public int getRSeekBar(){
        if(prefs.contains("rSeekBar"))
            return prefs.getInt("rSeekBar",1);
        return 1;
    }
    //et_address1
    public void setRAddress1(String rAddress1){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rAddress1",rAddress1);
        editor.commit();
    }
    public String getRAddress1(){
        if(prefs.contains("rAddress1"))
            return prefs.getString("rAddress1","");
        return "";
    }
    //et_shopaddress2
    public void setRAddress2(String rAddress2){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rAddress2",rAddress2);
        editor.commit();
    }
    public String getRAddress2(){
        if(prefs.contains("rAddress2"))
            return prefs.getString("rAddress2","");
        return "";
    }
    //et_shopaddress3
    public void setRAddress3(String rAddress3){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rAddress3",rAddress3);
        editor.commit();
    }
    public String getRAddress3(){
        if(prefs.contains("rAddress3"))
            return prefs.getString("rAddress3","");
        return "";
    }
    //et_shopaddress4
    public void setRAddress4(String rAddress4){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rAddress4",rAddress4);
        editor.commit();
    }
    public String getRAddress4(){
        if(prefs.contains("rAddress4"))
            return prefs.getString("rAddress4","");
        return "";
    }
    //et_shopaddress5
    public void setRAddress5(String rAddress5){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rAddress5",rAddress5);
        editor.commit();
    }
    public String getRAddress5(){
        if(prefs.contains("rAddress5"))
            return prefs.getString("rAddress5","");
        return "";
    }
    //et_shopaddress6
    public void setRAddress6(String rAddress6){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rAddress6",rAddress6);
        editor.commit();
    }
    public String getRAddress6(){
        if(prefs.contains("rAddress6"))
            return prefs.getString("rAddress6","");
        return "";
    }
    //et_shopcity
    public void setRCity(String rCity){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("rCity",rCity);
        editor.commit();
    }
    public String getRCity(){
        if(prefs.contains("rCity"))
            return prefs.getString("rCity","");
        return "";
    }

    //notification on off status
    public void setNotification(String notOnOff){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("notOnOff",notOnOff);
        editor.commit();
    }
    public String getNotification(){
        if(prefs.contains("notOnOff"))
            return prefs.getString("notOnOff","0");
        return "";
    }
    //notification end hour
    public void setNotificationHour(int notOnOffEnd){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("notOnOffEnd",notOnOffEnd);
        editor.commit();
    }
    public int getNotificationHour(){
        if(prefs.contains("notOnOffEnd"))
            return prefs.getInt("notOnOffEnd",0);
        return 0;
    }
    //notification end Min
    public void setNotificationMin(int notOnOffMin){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("notOnOffMin",notOnOffMin);
        editor.commit();
    }
    public int getNotificationMin(){
        if(prefs.contains("notOnOffMin"))
            return prefs.getInt("notOnOffMin",0);
        return 0;
    }

    //set refer status
    public void setReferStatus(String setrefer){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("setrefer",setrefer);
        editor.commit();
    }
    public String getReferStatus(){
        if(prefs.contains("setrefer"))
            return prefs.getString("setrefer","no");
        return "";
    }

    //set refer code
    public void setReferCode(String referCode){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("referCode",referCode);
        editor.commit();
    }
    public String getReferCode(){
        if(prefs.contains("referCode"))
            return prefs.getString("referCode","POD78");
        return "";
    }

    //set shop Area
     public void setShopArea(String area){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("area",area);
        editor.commit();
    }
    public String getShopArea(){
        if(prefs.contains("area"))
            return prefs.getString("area","NA");
        return "";
    }

    //set price restrict
    public void setPriceRestrict(String area){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("price_restrict",area);
        editor.commit();
    }
    public String getPriceRestrict(){
        if(prefs.contains("price_restrict"))
            return prefs.getString("price_restrict","NA");
        return "";
    }

    //set paw switch mode option
    public void setPawMode(String pawMode){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pawmode",pawMode);
        editor.apply();
    }
    public String getPawMode(){
        if(prefs.contains("pawmode"))
            return prefs.getString("pawmode","NA");
        return "";
    }
}

