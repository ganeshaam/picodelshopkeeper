package com.podmerchant.util;

import android.content.Context;
import android.content.SharedPreferences;

class PrefsManager {

    private static final int SEQUENCE_NEVER_STARTED = 0;
    private static final int SEQUENCE_FINISHED = -1;
    private static final String PREFS_NAME = "material_showcaseview_prefs";
    private static final String STATUS = "status_";
    private String showcaseID = null;
    private Context context;

    public PrefsManager(Context context, String showcaseID) {
        this.context = context;
        this.showcaseID = showcaseID;
    }

    boolean hasFired() {
        int status = this.getSequenceStatus();
        return status == SEQUENCE_FINISHED;
    }

    void setFired() {
        this.setSequenceStatus(SEQUENCE_FINISHED);
    }

    private int getSequenceStatus() {
        return this.context.getSharedPreferences("material_showcaseview_prefs", 0).getInt("status_" + this.showcaseID, SEQUENCE_NEVER_STARTED);
    }

    private void setSequenceStatus(int status) {
        SharedPreferences internal = this.context.getSharedPreferences("material_showcaseview_prefs", 0);
        internal.edit().putInt("status_" + this.showcaseID, status).apply();
    }

    public void resetShowcase() {
        resetShowcase(this.context, this.showcaseID);
    }

    private static void resetShowcase(Context context, String showcaseID) {
        SharedPreferences internal = context.getSharedPreferences("material_showcaseview_prefs", 0);
        internal.edit().putInt("status_" + showcaseID, SEQUENCE_NEVER_STARTED).apply();
    }

    public static void resetAll(Context context) {
        SharedPreferences internal = context.getSharedPreferences("material_showcaseview_prefs", 0);
        internal.edit().clear().apply();
    }

    public void close() {
        this.context = null;
    }
}
