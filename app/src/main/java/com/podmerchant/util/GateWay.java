package com.podmerchant.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.rw.loadingdialog.LoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashSet;

@SuppressLint("Registered")
public class GateWay extends AppCompatActivity {


    private final Context context;
    private int CartCount;
    private LoadingView loadingView;
    String userContact, userEmailId;
    private LinkedHashSet<ReviewPOJO> review = new LinkedHashSet<>();
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
   // SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(GateWay.this);

    public GateWay(Context context) {
        this.context = context;
    }

    public void cartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Your Cart seems to be empty, SHOP NOW.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void wishListAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("No items selected as Favourites !\n" +
                "Pls add products by clicking on Heart in Product List. ")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void ErrorHandlingMethod(VolleyError error) {
        if (error instanceof ServerError) {
            String title = "Server Error";
            String message = "Sorry for the inconvenience, the web server is not responding. Please try again after some time.";
            String buttonText = "Ok";

            GateWay gateWay = new GateWay(context);
            gateWay.showAlertDialog(title, message, buttonText);
        } else if (error instanceof NoConnectionError) {
            String title = "No Connection Error";
            String message = "Communication Error! Please try again after some time.";
            String buttonText = "Ok";

            GateWay gateWay = new GateWay(context);
            gateWay.showAlertDialog(title, message, buttonText);
        } else if (error instanceof TimeoutError) {
            String title = "Timeout Error";
            String message = "Connection TimeOut! Please check your internet connection.";
            String buttonText = "Ok";

            GateWay gateWay = new GateWay(context);
            gateWay.showAlertDialog(title, message, buttonText);
        } else if (error instanceof ParseError) {
            String title = "Parse Error";
            String message = "Parsing error! Please try again after some time.";
            String buttonText = "Ok";

            GateWay gateWay = new GateWay(context);
            gateWay.showAlertDialog(title, message, buttonText);
        }
    }

    private void showAlertDialog(String title, String message, String buttonText) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setIcon(R.drawable.appicon); //Setting Dialog icon Image
        alertDialog.setTitle(title);  // Setting Dialog Title
        alertDialog.setMessage(message); // Setting Dialog Message
        alertDialog.setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { // Setting OK Button
                finish();
            }
        });
        AlertDialog dialog = alertDialog.show(); // Showing Alert Message
        TextView messageText = dialog.findViewById(android.R.id.message);
        assert messageText != null;
        messageText.setGravity(Gravity.CENTER);
        dialog.show();
    }

    public void progressDialogStart() {
        loadingView = new LoadingView.Builder(context)
                .setProgressColorResource(R.color.colorAccent)
                .setProgressStyle(LoadingView.ProgressStyle.CYCLIC)
                .attachTo((Activity) context);

        if (!((Activity) context).isFinishing()) {
            loadingView.show();
        }

    }

    public void progressDialogStop() {
        loadingView.hide();
    }

    public void displaySnackBar(View view) {
        Snackbar snackbar = Snackbar
                .make(view, "No internet connection, please try again.", Snackbar.LENGTH_LONG)
                .setAction("", null);
        snackbar.setActionTextColor(Color.WHITE); // Changing message text color
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(Color.parseColor("#bf360c"));
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    /*public String getCity() {
        DBHelper db = new DBHelper(context);
        HashMap userInfoHashMap;
        userInfoHashMap = db.getLocalityDetails();
        return (String) userInfoHashMap.get("l_state");
    }

    public String getArea() {
        DBHelper db = new DBHelper(context);
        HashMap userInfoHashMap;
        userInfoHashMap = db.getLocalityDetails();
        return (String) userInfoHashMap.get("l_city");
    }

    public String getContact() {
        DBHelper db = new DBHelper(context);
        HashMap userInfoHashMap;
        userInfoHashMap = db.getUserDetails();
        return (String) userInfoHashMap.get("contact");
    }

    public String getUserName() {
        DBHelper db = new DBHelper(context);
        HashMap userInfoHashMap;
        userInfoHashMap = db.getUserDetails();
        return (String) userInfoHashMap.get("name");
    }

    public String getUserEmail() {
        DBHelper db = new DBHelper(context);
        HashMap userInfoHashMap;
        userInfoHashMap = db.getUserDetails();
        return (String) userInfoHashMap.get("email");
    }
*/
    /**
     * @return review - LinkedHashSet use for showing rating information.
     */
    public LinkedHashSet fetchReviewData() {
        ReviewPOJO reviewZero = new ReviewPOJO("0.0", "", 0, 0);
        ReviewPOJO reviewOne = new ReviewPOJO("1.0", "Hated it", R.drawable.ic_sentiment_very_dissatisfied, R.color.green);
        ReviewPOJO reviewTwo = new ReviewPOJO("2.0", "Disliked it", R.drawable.ic_sentiment_dissatisfied, R.color.green1);
        ReviewPOJO reviewThree = new ReviewPOJO("3.0", "It's OK", R.drawable.ic_sentiment_neutral, R.color.green2);
        ReviewPOJO reviewFour = new ReviewPOJO("4.0", "Liked it", R.drawable.ic_sentiment_satisfied, R.color.green3);
        ReviewPOJO reviewFive = new ReviewPOJO("5.0", "Loved it", R.drawable.ic_sentiment_very_satisfied, R.color.green4);
        review.add(reviewZero);
        review.add(reviewOne);
        review.add(reviewTwo);
        review.add(reviewThree);
        review.add(reviewFour);
        review.add(reviewFive);

        return review;
    }

    /**
     * This is POJO class with these values;
     * rating
     * reason
     * drawableImage
     * color
     */
    public class ReviewPOJO {

        private String rating;
        private String reason;
        private int drawableImage;
        private int color;

        ReviewPOJO(String rating, String reason, int drawableImage, int color) {
            this.rating = rating;
            this.reason = reason;
            this.drawableImage = drawableImage;
            this.color = color;
        }

        public String getRating() {
            return rating;
        }

        public String getReason() {
            return reason;
        }

        public int getDrawableImage() {
            return drawableImage;
        }

        public int getColor() {
            return color;
        }
    }

    /**
     * This method have two parameters contact & email.
     * To fetch the normal_cart_count and set to this updateAddToCartCount() method @BaseActivity.
     * url - http://www.apneareamein.com/And/shopping/version_1.6.4/GetCartCount.php
     */
    public void SyncData() { //TODO server method
        JSONObject params = new JSONObject();

        userContact = "8862025200";//sharedPreferencesUtils.getPhoneNumber();//getContact();
        userEmailId = "ganeshb.aam@gmail.com";//sharedPreferencesUtils.getEmailId();//getUserEmail();
        try {
            params.put("contact", userContact);
            params.put("email", userEmailId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetCartCount, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    CartCount = response.getInt("normal_cart_count");
                    //((BaseActivity) context).updateAddToCartCount(CartCount);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }
/*
    public void hide() {
        BaseActivity.bottomNavigationView.getMenu().findItem(R.id.navigation_home).setCheckable(false);
        BaseActivity.bottomNavigationView.getMenu().findItem(R.id.navigation_profile).setCheckable(false);
        BaseActivity.bottomNavigationView.getMenu().findItem(R.id.navigation_cart).setCheckable(false);
        BaseActivity.bottomNavigationView.getMenu().findItem(R.id.navigation_services).setCheckable(false);
    }*/

    // slide the view from its current position to top itself
    public static void slideUp(final View view) {
        if (view.getVisibility() == View.GONE) {
            TranslateAnimation animate = new TranslateAnimation(
                    0,                 // fromXDelta
                    0,                 // toXDelta
                    view.getHeight(),  // fromYDelta
                    0);
            animate.setDuration(500);
            animate.setFillAfter(true);
            view.startAnimation(animate);

            animate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    // slide the view from its current position to below itself
    public static void slideDown(final View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(1000);
        animate.setFillAfter(true);
        view.startAnimation(animate);

        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi";//"Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile";//"Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "No";//"Not connected to Internet";
        }
        return status;
    }

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

}
