package com.podmerchant.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class InstallReferrerReceiver extends BroadcastReceiver {

    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    public void onReceive(Context context, Intent intent) {

        String referrerCode = intent.getStringExtra("referrer");
        Log.d("podReferid",referrerCode);

        sharedPreferencesUtils = new SharedPreferencesUtils(context);
        sharedPreferencesUtils.setReferStatus("yes");
        sharedPreferencesUtils.setReferCode(referrerCode);
     }
}
