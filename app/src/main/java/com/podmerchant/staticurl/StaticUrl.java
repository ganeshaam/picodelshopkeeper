package com.podmerchant.staticurl;
import com.podmerchant.BuildConfig;

public class StaticUrl {

    public static String baseUrl2 = "https://www.picodel.com/seller/shoptestapi";
    public static String baseUrl3 = "https://www.picodel.com/seller/supperadmin";
    public static String baseUrl4 = "https://www.picodel.com/seller/franchisee";
    public static String baseUrl = "https://www.picodel.com/seller/shopapi"; //Live
    public static String SHOPPING_INITIAL_URL1 = "https://www.picodel.com/And/shopping/RESTAPI/";
    public static String SHOPPING_INITIAL_URL = "https://www.picodel.com/And/shopping/AppAPI/";
    public static String SHOPPING_INITIAL_URL2 = "https://www.picodel.com/And/shopping/";

    public static String versionName = BuildConfig.VERSION_NAME;

    public static String IMAGE_URL = "https://simg.picodel.com/";

    public static String verifyMobile = baseUrl+"/checkmono";
    public static String verifydeliveryBoyMobile = baseUrl+"/checkdeliveryboynumber";
    public static String androidkey = baseUrl+"/androidkey";
    public static String verifymanagerMobile = baseUrl+"/checkmanagernumber";
    public static String getlatlong = baseUrl+"/getlatlong";
    //public static String sendotp = baseUrl+"/sendotp";
    public static String sendotp = baseUrl2+"/sendotp";
    public static String orderlist = baseUrl+"/getorders";// Live Order List Working the Shopkeeper
    //public static String orderlist = baseUrl4+"/getorders"; // for the Kolkata City price restriction
    public static String orderlistAreaWise = baseUrl2+"/areawiseorders";
    //public static String cartlist = baseUrl+"/cartitem"; //Live API  for All shop and manufacturer
    //public static String cartlist = baseUrl2+"/cartitemsareawise"; //Live API  for All shop and manufacturer
    public static String cartlist = baseUrl4+"/cartitemsareawise"; //Live API  for All shop price restrict
    public static String Brandcartitem = baseUrl2+"/brandcartitem";
    public static String getUseraddress = baseUrl+"/getaddress";
    //public static String shopregistration= baseUrl+"/createshop"; // Live API
    public static String shopregistration= baseUrl2+"/createshop"; // Added refercode
    public static String shopadminref= baseUrl+"/shopadminref";
    public static String shop_address_update = baseUrl+"/addressupdate";
    //public static String shop_profile_address_update = baseUrl2+"/profileupdate"; Live API
    public static String shop_profile_address_update = baseUrl4+"/profileupdate"; // API to get extra delivery boys
    public static String acceptorder = baseUrl+"/acceptorder";
    public static String cancelcartitem = baseUrl+"/cancelcartitem";
    public static String removecartitem = baseUrl+"/removecartitem";
    public static String chkorderstatus  = baseUrl+"/chkorderstatus";
    public static String orderstatus  = baseUrl+"/orderstatus";
    public static String cancelorder = baseUrl+"/cancelorder";
    public static String checkintrestedbut = baseUrl+"/checkintrestedbut";
    //public static String cancelorder = baseUrl+"/cancelorder?order_id=%s&assign_shop_id=%s&assign_delboy_id=%s&reason_of_rejection=%s";
    public static String deliverdorder =   baseUrl+"/deliverorder?order_id=order_id";
    public static String adddeliveryboy = baseUrl+"/adddeliveryboy";
    public static String addShopmanager = baseUrl+"/shopmanager";

    public static String getDeliveryboys = baseUrl+"/getdeliveryboy";
    public static String getmanagers = baseUrl+"/getmanagers";
    public static String checkmanager = baseUrl+"/checkmanager";
    public static String assignorder = baseUrl+"/assignorder";
    //public static String getPaymentMode = baseUrl+"/paymentmode";
    public static String getPaymentMode = baseUrl2+"/paymentmodelist";
    public static String setdeliverstatus = baseUrl+"/deliverstatus";
    //public static String assignorder = baseUrl+"/assignorder";
    public static String getDeliveryboy = baseUrl+"/deliveryboy";
    public static String histroycount  = baseUrl+"/histroycount";
    public static String deliveryreports = baseUrl+"/deliveryreports";
    public static String rejectionreports = baseUrl+"/rejectionreports";
    public static String checkstatus = baseUrl+"/checkstatus";
    public static String freelancerArea = baseUrl+"/freelancerarea";
    public static String updatetoken = baseUrl+"/updatetoken";
    public static String updateManagertoken = baseUrl+"/updatetokenm";

    //public static String registration = baseUrl+"create?area=%s&nearest_areas=%s";

    //start------ Wishlist
    public static String urlGetMyWishListItems = SHOPPING_INITIAL_URL1 + "wishListProducts.php";
    public static String urlAddMyWishListItems = SHOPPING_INITIAL_URL + "AddWishListDetails.php";
    public static String urlDeleteMyWishListItems = SHOPPING_INITIAL_URL + "deleteWishListItems.php";
    public static String urlGetWishListItem = SHOPPING_INITIAL_URL + "fetchWishlistID.php";//sync with local
    public static String urlAddCartItemToWishlist = SHOPPING_INITIAL_URL + "addCarttoWishlist.php";//sync with local

    //start----- commonly used urls
    public static String urlGetuserdata = SHOPPING_INITIAL_URL + "PkfetchUserDataWithAddress.php"; //fetchUserDataWithAddress
    public static String urlSimilarResult = SHOPPING_INITIAL_URL + "similarProductResult.php";
    public static String urlReviewResult = SHOPPING_INITIAL_URL + "rating_review.php";
    public static String urlReviewAll = SHOPPING_INITIAL_URL + "picodel_review.php";

    //start----- FilterLayout
    public static String urlGetDependentBrand = SHOPPING_INITIAL_URL + "getDependentBrandData.php";
    public static String urlGetDependentSize = SHOPPING_INITIAL_URL + "getDependentSizeData.php";
    //end----- FilterLayout
    //start----- SearchProducts
    public static String urlNewGetProducts = SHOPPING_INITIAL_URL + "searchProducts11.php";
    public static String urlGetAllFilterData = SHOPPING_INITIAL_URL + "get_all_filter.php";
    public static String urlGetSubCategories = SHOPPING_INITIAL_URL + "new_getSubCategories.php";
    public static String urlGetCategories = SHOPPING_INITIAL_URL + "getCategories.php";
    public static String urlCheckCouponCode = SHOPPING_INITIAL_URL + "checkCouponCode.php";
    //end----- SearchProducts

    //start----- ProductInformationFragment
    public static String urlGetProductReviewResult = SHOPPING_INITIAL_URL + "get_Product_Review.php";
    public static String urlGetSingleProductInformation = SHOPPING_INITIAL_URL + "single_product_information.php";
    //end----- ProductInformationFragment
    //start----- AddToCart
    public static String urlGetCartCount = SHOPPING_INITIAL_URL + "PkGetCartCount.php";//sync with local GetCartCount
    public static String urlCheckQtyProductWise = SHOPPING_INITIAL_URL + "PkcheckQtyProductWise.php"; //checkQtyProductWise
    public static String urlDeleteCartResult = SHOPPING_INITIAL_URL + "PkdeleteCartItems.php";//For Normal & Combo Cart deleteCartItems
    public static String urlAdd_UpdateCartDetails = SHOPPING_INITIAL_URL + "PkNewAddUpdateCartDetails.php";//For Normal Cart    newAddUpdateCartDetails
    //public static String urlPawAdd_UpdateCartDetails = SHOPPING_INITIAL_URL + "pawNewAddUpdateCartDetails.php";//For Normal Cart    newAddUpdateCartDetails

    public static String urlComboCartResult = SHOPPING_INITIAL_URL + "updateComboCartDetails.php";//For Combo Cart
    public static String urlGetCartItem = SHOPPING_INITIAL_URL + "fetchCartID.php";
    public static String urlGetMyCartItems = SHOPPING_INITIAL_URL1 + "PkcartProducts.php";//For Normal & Combo Cartpublic static cartProducts
    public static String urlGetMyCartOutOfStockItems = SHOPPING_INITIAL_URL + "PkGetMyCartOutOfStockItems.php";//For Normal & Combo Cart GetMyCartOutOfStockItems
    //end----- AddToCart

    public static String urlStopOrder = SHOPPING_INITIAL_URL + "stopOrder.php";

    //start----- QuickLinks
    public static String about_us = SHOPPING_INITIAL_URL2 + "aboutUs.html";
    public static String terms = SHOPPING_INITIAL_URL2 + "termsConditions.html";
    public static String cash_back = SHOPPING_INITIAL_URL2 + "cashback.html";
    public static String help = SHOPPING_INITIAL_URL2 + "help.html";
    public static String blank_page = SHOPPING_INITIAL_URL2 + "blank_page.html";
    //end----- QuickLinks

    //start----- MyOrder
    public static String urlMyOrderForUser = SHOPPING_INITIAL_URL + "PkuserMyOrdersHome.php";   // userMyOrdersHome.php
     public static String urlOrderRemark = SHOPPING_INITIAL_URL + "PkuserOrderRemark.php";   //userOrderRemark.php
    public static String urlCancelOrderSpinnerData = SHOPPING_INITIAL_URL + "cancelOrderSpinner.php";
    public static String urlCancelOrder = SHOPPING_INITIAL_URL + "PkuserCancelOrder.php";  //userCancelOrder.php
    public static String urlOrderDetails = SHOPPING_INITIAL_URL + "PkDeliveryType_v1.7.php"; //DeliveryType_v1.7
    //end----- MyOrder

    //start----- NotificationsHome
   public static String urlGetNotification = SHOPPING_INITIAL_URL + "PkgetNotifications.php";   //getNotifications.php
    public static String urlDeactiveNotification = SHOPPING_INITIAL_URL + "deactiveNotification.php";

    //start----- MyOrderDetails
    public static String urlProductDetails = SHOPPING_INITIAL_URL + "PkuserMyOrderDetails.php";   //userMyOrderDetails.php
    public static String urlmyLastOrder = SHOPPING_INITIAL_URL + "userMyLastOrder.php";
    public static String urlUserRepeatOrder = SHOPPING_INITIAL_URL + "userRepeatOrder.php";
    //end----- MyOrderDetails

    //start----- CashBackOffer
    public static String urlUserCashBackCategory = SHOPPING_INITIAL_URL + "getRedeemCategory.php";
    //end----- CashBackOffer

    //start----- OrderDetails
    public static String urlGetPriceDetails = SHOPPING_INITIAL_URL + "PknewAddToCartCalculation_v1.7.php"; //newAddToCartCalculation_v1.7
    public static String urlCashOnDelivery = SHOPPING_INITIAL_URL + "test_Pknew_place_order_v1.7.php";   //test_Pknew_place_order_v1.7.php //Pknew_place_order_v1.7.php
    //Above is Live API FOR FINAL ORDER PLACE

    public static String urlProblematicDeleteCondition = SHOPPING_INITIAL_URL + "problematicDeleteCartItems.php";//
    public static String urlOrderConfirmationNotification = SHOPPING_INITIAL_URL + "PkorderConfirmationNotification.php";
    public static String urlCouponsDetails = SHOPPING_INITIAL_URL + "couponDetails.php";
    public static String urlNewScheduleDates = SHOPPING_INITIAL_URL + "PknewScheduleDatesSlots_v1.7.php";
    //end----- OrderDetails

    //start----applayCouponCode
    public static String urlsendcouponCode = SHOPPING_INITIAL_URL + "sendcouponCode.php";
    public static String urlgetNewScheduledDatesForCoupon = SHOPPING_INITIAL_URL + "getNewScheduledDatesForCoupon.php";
    public static String urlverifyTRN = SHOPPING_INITIAL_URL + "verifyTRN.php";
    //end-----

    //shop keeper get recharge points and amount
    public static String placerecharge = baseUrl+"/placerecharge";
    public static String getrechargehistroy = baseUrl+"/getrechargehistroy";
    public static String getrecharge = baseUrl+"/getrecharge";
    public static String getslider = baseUrl+"/getslider";
    public static String getrechargeplan = baseUrl+"/getrechargeplan";
    //public static String stafflist  = baseUrl+"/getstafflist";//Live
    public static String stafflist  = baseUrl2+"/getstafflist";
    public static String invoicedetails = baseUrl+"/invoicedetails"; //Live API


    public static String termscond  = baseUrl+"/terms";
    public static String helpmerchant = baseUrl+"/helpmerchant";

    public static String brandlist = baseUrl+"/getbrandlist";
    public static String setpartnerkit = baseUrl+"/partnerkit";
    public static String getUserName = baseUrl+"/getuser";
    public static String sendcancelmail = baseUrl+"/sendcancelmail"; //LIVE
    //public static String sendcancelmail = baseUrl2+"/sendcancelmail";

    public static String signupSugestedBy = baseUrl+"/signupsugestedby";
    public static String checksugestedrefer = baseUrl+"/checksugestedrefer";
    public static String partnerkitlist = baseUrl+"/partnerkitlist";
    public static String getpicodelpoints = baseUrl+"/picodelpoints";
    public static String insertviewbyshop = baseUrl+"/insertviewbyshop";

    public static String newScheduleforShopkeeper = SHOPPING_INITIAL_URL +"newScheduleforShopkeeper.php";
    public static String actionTestOrder  = baseUrl+"/testorder";
    public static String actionIsregister  = baseUrl+"/isregister";
    public static String cancellationpolicy = baseUrl+"/cancellationpolicy";
    public static String shopcancelstatus = SHOPPING_INITIAL_URL+"/shopcancelstatus.php";
    public static String getshopcategory = baseUrl+"/getshopcategory";
    public static String invoice_manufacturer = SHOPPING_INITIAL_URL+"/invoice_manufacturer.php";
    public static String Receipt_Distributor = SHOPPING_INITIAL_URL+"/Receipt_Distributor.php";
    public static String accept_distributor = baseUrl2+"/accept_distributor"; //check status if the manufacture Accepted order or Not
    public static String urlAccept_by_distributor = SHOPPING_INITIAL_URL +"Accept_by_distributor.php"; // Brand Accept Order and the Email Invoice  to Admin and  manufacturer
    public static String urlgetAccountDetails = SHOPPING_INITIAL_URL +"getAccountDetails.php"; //Get Manufacturer ACCOUNT Detailos
    public static String paymentdeliverypdate = baseUrl2+"/paymentdeliverypdate"; //Update Distributor delivery payment status with image
    public static String paymentdetails = baseUrl2+"/paymentdetails";//not working now
    public static String Getdeliveredproducts = baseUrl2+"/getdeliveredproducts";  ////Get Distributor order payments status Details  with Images
    public static String Getpickuplocation = baseUrl2+"/getpickuplocation"; //Show pickup location on the google map activity

    public static String urlSimilarResult11 = SHOPPING_INITIAL_URL + "similarProductResult11.php";
    public static String urlnewAddUpdateByShop = SHOPPING_INITIAL_URL + "newAddUpdateByShop.php";//For Normal Cart    newAddUpdateCartDetails
    public static String urlsend_UpdatedCart_Email = SHOPPING_INITIAL_URL + "send_UpdatedCart_Email.php"; //"send_email_test.php";   //
    public static String urlsend_AddedCart_Email = SHOPPING_INITIAL_URL + "send_AddedCart_Email.php"; // "Afrer Added cart By the Shopkeeper";   //
    public static String urlremoveCartItemByShop = SHOPPING_INITIAL_URL + "removeCartItemByShop.php";
    public static String urlgetShopWiseProductCount = SHOPPING_INITIAL_URL + "getShopWiseProductCount.php";
    public static String urlactiveCityForshop = SHOPPING_INITIAL_URL + "activeCityForAdmin.php";
    public static String urladdToCartbyShopkeeper = SHOPPING_INITIAL_URL + "addToCartbyShopkeeper.php"; //"send_email_test.php";   //

    public static String userTermsCondition  = baseUrl2+"/singeitem";
    public static String checkadmin  = baseUrl3+"/checkadmin";
    public static String urldeliverydetails = baseUrl2+"/deliverydetails";
    public static String urlAcceptdeliveryrequest = baseUrl4+"/acceptdeliveryrequest";

    //paw product list
    public static String urlPawProductList = SHOPPING_INITIAL_URL+"/pawProductList.php";
    public static String urlPawNewAddUpdateCartDetails = SHOPPING_INITIAL_URL+"/pawNewAddUpdateCartDetails.php";
    public static String urlGetMyCartPawItems = SHOPPING_INITIAL_URL1 + "PawcartProducts.php";//For Normal & Combo Cartpublic static cartProducts
    public static String urlNewPlaceOrder = SHOPPING_INITIAL_URL+"/pawNewPlaceOrder.php";

    public static String urlPawUserList = SHOPPING_INITIAL_URL+"/pawUserList.php";
    public static String urlPawAddNewUser = SHOPPING_INITIAL_URL+"/pawAddNewUser.php";
    public static String urlPawOrderHistroy = SHOPPING_INITIAL_URL+"/pawOrderHistroy.php";
    public static String urladdPawProduct = SHOPPING_INITIAL_URL + "/pawAddNewProducts.php"; //"send_email_test.php";   //pawAddNewProduct.php
    public static String urlStateList = SHOPPING_INITIAL_URL + "activeStateConfiguration.php";
    public static String urlCityList = SHOPPING_INITIAL_URL + "UpdateUserCity.php";
    public static String urlCityConfiguration = SHOPPING_INITIAL_URL + "activeCityandState.php";
    public static String urlgetDeliveryRequestDetails = SHOPPING_INITIAL_URL + "getDeliveryRequestDetails.php";

}
