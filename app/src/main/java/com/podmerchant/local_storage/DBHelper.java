package com.podmerchant.local_storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "apnemein_shopping.db";

    //this is for first table
    private static final String TABLE_NAME = "user_info";
    private static final String SHOPADMIN_COLUMN_ID = "id";
    private static final String USER_NAME = "name";
    private static final String USER_EMAIL = "email";
    private static final String USER_ADDRESS = "address";
    private static final String USER_CONTACT = "contact";

    private static final String LOCALITY_TABLE = "locality_info";
    private static final String L_ID = "l_id";
    private static final String L_CITY = "l_state";
    private static final String L_AREA = "l_city";

    private static final String PRODUCT_ID = "pId";

    private static final String WISHLIST_TABLE = "my_wish_list";
    private static final String WISHLIST_ID = "id";
    private static final String WISHLIST_PRODUCT_ID = "pId";

    private static final String NEW_CART_TABLE = "new_my_cart";
    private static final String NEW_ID = "new_id";
    private static final String NEW_PID = "new_pid";

    private Cursor cursor;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 6);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_SHOP_ADMIN_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + SHOPADMIN_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + USER_EMAIL + " TEXT,"
                + USER_NAME + " TEXT,"
                + USER_ADDRESS + " TEXT,"
                + USER_CONTACT + " TEXT" + ")";
        db.execSQL(CREATE_SHOP_ADMIN_TABLE);

        String CREATE_LOCALITY_TABLE = "CREATE TABLE IF NOT EXISTS " + LOCALITY_TABLE + "("
                + L_ID + " INTEGER PRIMARY KEY,"
                + L_CITY + " TEXT ,"
                + L_AREA + " TEXT " + ")";
        db.execSQL(CREATE_LOCALITY_TABLE);

        String CREATE_WISH_LIST_TABLE = "CREATE TABLE IF NOT EXISTS " + WISHLIST_TABLE + "("
                + WISHLIST_ID + " INTEGER PRIMARY KEY,"
                + WISHLIST_PRODUCT_ID + " TEXT " + ")";
        db.execSQL(CREATE_WISH_LIST_TABLE);

        String CREATE_NEW_CART_TABLE = "CREATE TABLE IF NOT EXISTS " + NEW_CART_TABLE + "( " + NEW_ID + " INTEGER PRIMARY KEY," + NEW_PID + " TEXT NOT NULL UNIQUE" + ")";
        db.execSQL(CREATE_NEW_CART_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NEW_CART_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + WISHLIST_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + LOCALITY_TABLE);
        // Create tables again
        onCreate(db);
    }

    public void deleteAll2() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.delete(WISHLIST_TABLE, null, null);
        db.delete(NEW_CART_TABLE, null, null);
        db.delete(LOCALITY_TABLE, null, null);
        db.close();
    }

    //Start of UserInfo
    public boolean insertLocalityInfo(String city, String area) {
        deleteOnlyLocalityInfoTable();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(L_CITY, city);
        contentValues.put(L_AREA, area);
        // Inserting Row
        db.insert(LOCALITY_TABLE, null, contentValues);
        db.close(); // Closing database connection
        return true;
    }

    public void updateUserInfoAgain(String email, String strName, String address, String contact) {
        deleteOnlyUserInfoTable();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_NAME, strName);
        contentValues.put(USER_EMAIL, email);
        contentValues.put(USER_ADDRESS, address);
        contentValues.put(USER_CONTACT, contact);
        // inserting Row
        db.insert(TABLE_NAME, null, contentValues);
        db.close(); // Closing database connection
    }

    public HashMap getLocalityDetails() {
        HashMap info = new HashMap();
        String selectQuery = "SELECT  * FROM " + LOCALITY_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        try {
            cursor = db.rawQuery(selectQuery, null);
            try {
                if (cursor.moveToFirst()) {
                    if (cursor.getCount() > 0) {
                        info.put("l_state", cursor.getString(1));
                        info.put("l_city", cursor.getString(2));
                    }
                }
            } finally {
                try {
                    if (cursor != null)
                        cursor.close();
                } catch (Exception ignore) {
                }
            }

        } finally {
            try {
                if (cursor != null)
                    cursor.close();
                db.close();
            } catch (Exception ignore) {
            }
        }
        // return city & area
        return info;
    }

    public HashMap getUserDetails() {
        HashMap info = new HashMap();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        try {
            cursor = db.rawQuery(selectQuery, null);
            try {
                if (cursor.moveToFirst()) {
                    if (cursor.getCount() > 0) {
                        info.put("email", cursor.getString(1));
                        info.put("name", cursor.getString(2));
                        info.put("address", cursor.getString(3));
                        info.put("contact", cursor.getString(4));
                    }
                }
            } finally {
                try {
                    if (cursor != null)
                        cursor.close();
                } catch (Exception ignore) {
                }
            }

        } finally {
            try {
                if (cursor != null)
                    cursor.close();
                db.close();
            } catch (Exception ignore) {
            }
        }
        // return city & area
        return info;
    }
    //End  of UserInfo

    // Start of WishList
    public HashMap getWishListDetails(String pId) {
        HashMap info = new HashMap();
        String selectQuery = "SELECT  * FROM " + WISHLIST_TABLE + " where pId = '" + pId + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            info.put("pId", cursor.getString(1));
        }
        cursor.close();
        db.close();
        // return product name
        return info;
    }


    public int deleteWishListProductItem(String pId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int deleteItem = db.delete(WISHLIST_TABLE, PRODUCT_ID + "=?", new String[]{pId});
        db.close(); // Closing database connection
        return deleteItem;
    }

    public void deleteOnlyWishListTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(WISHLIST_TABLE, null, null);
        db.close();
    }

    public long fetchWishListCount() {
        String sql = "SELECT COUNT(*) FROM " + WISHLIST_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        long count = statement.simpleQueryForLong();
        db.close();
        return count;
    }

    public void insertProductIntoWishList(String pId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRODUCT_ID, pId);
        // Inserting Row
        db.insert(WISHLIST_TABLE, null, contentValues);
        db.close(); // Closing database connection
    }
    //End of WishList

    // Start of Normal Cart
    public void insertCount(String pId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NEW_PID, pId);
        // Inserting Row
        db.insert(NEW_CART_TABLE, null, contentValues);
        db.close(); // Closing database connection
    }

    public HashMap getCartDetails(String pId) {
        HashMap info = new HashMap();
        String selectQuery = "SELECT  * FROM " + NEW_CART_TABLE + " where new_pid = '" + pId + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            info.put("new_pid", cursor.getString(1));
        }
        cursor.close();
        db.close();
        // return product id
        return info;
    }

    public long fetchAddToCartCount() {
        String sql = "SELECT COUNT(*) FROM " + NEW_CART_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        long count = statement.simpleQueryForLong();
        db.close();
        return count;
    }

    public void deleteOnlyCartTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(NEW_CART_TABLE, null, null);
        db.execSQL("delete from "+ NEW_CART_TABLE);
        db.close();
    }

    public void deleteProductItem(String pId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int deleteItem = db.delete(NEW_CART_TABLE, NEW_PID + "=?", new String[]{pId});
        Log.e("DeletedItemFromLocalDB:",""+deleteItem);
        db.close(); // Closing database connection
    }

    public void NormalCartDeleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(NEW_CART_TABLE, null, null);
        db.close();
    }
    // End of Normal Cart

    private void deleteOnlyUserInfoTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

    private void deleteOnlyLocalityInfoTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(LOCALITY_TABLE, null, null);
        db.close();
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }

    public class Movie {

        private double price;
        private String qty;
        private String tag;
        String title;
        String message;
        String img_url;
        int n_id;

        public String getN_tag() {
            return n_tag;
        }

        public String getImg_url() {
            return img_url;
        }

        public String getMessage() {
            return message;
        }

        public String getTitle() {
            return title;
        }

        String n_tag;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        private String address;

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }


        public void setTitle(String title) {
            this.title = title;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setImg_url(String img_url) {
            this.img_url = img_url;
        }

        public void setN_tag(String n_tag) {
            this.n_tag = n_tag;
        }

        public int getN_id() {
            return n_id;
        }

        public void setN_id(int n_id) {
            this.n_id = n_id;
        }
    }

}
