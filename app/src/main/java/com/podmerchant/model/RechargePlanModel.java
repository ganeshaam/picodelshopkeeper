package com.podmerchant.model;

public class RechargePlanModel {

   private String id,planName,recharge,points,afterGst,payment_mode,staffid,staffname,rdate,bgcolor,staffImgUrl,invoiceId,flagPayment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getRecharge() {
        return recharge;
    }

    public void setRecharge(String recharge) {
        this.recharge = recharge;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public String getRdate() {
        return rdate;
    }

    public void setRdate(String rdate) {
        this.rdate = rdate;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }

    public String getStaffImgUrl() {
        return staffImgUrl;
    }

    public void setStaffImgUrl(String staffImgUrl) {
        this.staffImgUrl = staffImgUrl;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getAfterGst() {
        return afterGst;
    }

    public void setAfterGst(String afterGst) {
        this.afterGst = afterGst;
    }

    public String getFlagPayment() {
        return flagPayment;
    }

    public void setFlagPayment(String flagPayment) {
        this.flagPayment = flagPayment;
    }

    @Override
    public String toString() {
        return "RechargePlanModel{" +
                "id='" + id + '\'' +
                ", planName='" + planName + '\'' +
                ", recharge='" + recharge + '\'' +
                ", points='" + points + '\'' +
                '}';
    }
}
