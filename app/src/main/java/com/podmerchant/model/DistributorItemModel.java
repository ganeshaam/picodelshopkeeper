package com.podmerchant.model;

public class DistributorItemModel {

    public  String product_brand,cart_unique_id,user_id,shop_id,product_Count,id;


    public String getProduct_brand() {
        return product_brand;
    }

    public void setProduct_brand(String product_brand) {
        this.product_brand = product_brand;
    }

    public String getCart_unique_id() {
        return cart_unique_id;
    }

    public void setCart_unique_id(String cart_unique_id) {
        this.cart_unique_id = cart_unique_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getProduct_Count() {
        return product_Count;
    }

    public void setProduct_Count(String product_Count) {
        this.product_Count = product_Count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
