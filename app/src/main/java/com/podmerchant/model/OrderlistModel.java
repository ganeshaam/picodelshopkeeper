package com.podmerchant.model;

public class OrderlistModel {

    private String order_id,new_order_id,user_id, admin_approve,approveBy,assign_shop_id,assign_delboy_id,status,cart_user_id,cart_unique_id,payment_method,
            update_payment_method,payment_type,referrence_transaction_id,total_amount,updated_total_amount,shipping_address,latitude,longitude,nearest_society,
            coupon_save_amt,coupon_used,mrp_save_amt,aam_delivery_charge,updated_shipping_charges,order_date,mac_address,user_pending_bill,pending_bill_status,
            pending_bill_updatedy,pending_bii_date,clothbag_required,sms_sent,distance,address_id,actionTime,address1,address2,address3,address4,address5,
            area,pincode,show_cart,action_time,updated_at,am_delivery_charge,delivery_date,delivery_time,delivery_method,payment_option,color_flag;


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getNew_order_id() {
        return new_order_id;
    }

    public void setNew_order_id(String new_order_id) {
        this.new_order_id = new_order_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAdmin_approve() {
        return admin_approve;
    }

    public void setAdmin_approve(String admin_approve) {
        this.admin_approve = admin_approve;
    }

    public String getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(String approveBy) {
        this.approveBy = approveBy;
    }

    public String getAssign_shop_id() {
        return assign_shop_id;
    }

    public void setAssign_shop_id(String assign_shop_id) {
        this.assign_shop_id = assign_shop_id;
    }

    public String getAssign_delboy_id() {
        return assign_delboy_id;
    }

    public void setAssign_delboy_id(String assign_delboy_id) {
        this.assign_delboy_id = assign_delboy_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCart_user_id() {
        return cart_user_id;
    }

    public void setCart_user_id(String cart_user_id) {
        this.cart_user_id = cart_user_id;
    }

    public String getCart_unique_id() {
        return cart_unique_id;
    }

    public void setCart_unique_id(String cart_unique_id) {
        this.cart_unique_id = cart_unique_id;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getUpdate_payment_method() {
        return update_payment_method;
    }

    public void setUpdate_payment_method(String update_payment_method) {
        this.update_payment_method = update_payment_method;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getReferrence_transaction_id() {
        return referrence_transaction_id;
    }

    public void setReferrence_transaction_id(String referrence_transaction_id) {
        this.referrence_transaction_id = referrence_transaction_id;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getUpdated_total_amount() {
        return updated_total_amount;
    }

    public void setUpdated_total_amount(String updated_total_amount) {
        this.updated_total_amount = updated_total_amount;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(String shipping_address) {
        this.shipping_address = shipping_address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNearest_society() {
        return nearest_society;
    }

    public void setNearest_society(String nearest_society) {
        this.nearest_society = nearest_society;
    }

    public String getCoupon_save_amt() {
        return coupon_save_amt;
    }

    public void setCoupon_save_amt(String coupon_save_amt) {
        this.coupon_save_amt = coupon_save_amt;
    }

    public String getCoupon_used() {
        return coupon_used;
    }

    public void setCoupon_used(String coupon_used) {
        this.coupon_used = coupon_used;
    }

    public String getMrp_save_amt() {
        return mrp_save_amt;
    }

    public void setMrp_save_amt(String mrp_save_amt) {
        this.mrp_save_amt = mrp_save_amt;
    }

    public String getAam_delivery_charge() {
        return aam_delivery_charge;
    }

    public void setAam_delivery_charge(String aam_delivery_charge) {
        this.aam_delivery_charge = aam_delivery_charge;
    }

    public String getUpdated_shipping_charges() {
        return updated_shipping_charges;
    }

    public void setUpdated_shipping_charges(String updated_shipping_charges) {
        this.updated_shipping_charges = updated_shipping_charges;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getMac_address() {
        return mac_address;
    }

    public void setMac_address(String mac_address) {
        this.mac_address = mac_address;
    }

    public String getUser_pending_bill() {
        return user_pending_bill;
    }

    public void setUser_pending_bill(String user_pending_bill) {
        this.user_pending_bill = user_pending_bill;
    }

    public String getPending_bill_status() {
        return pending_bill_status;
    }

    public void setPending_bill_status(String pending_bill_status) {
        this.pending_bill_status = pending_bill_status;
    }

    public String getPending_bill_updatedy() {
        return pending_bill_updatedy;
    }

    public void setPending_bill_updatedy(String pending_bill_updatedy) {
        this.pending_bill_updatedy = pending_bill_updatedy;
    }

    public String getPending_bii_date() {
        return pending_bii_date;
    }

    public void setPending_bii_date(String pending_bii_date) {
        this.pending_bii_date = pending_bii_date;
    }

    public String getClothbag_required() {
        return clothbag_required;
    }

    public void setClothbag_required(String clothbag_required) {
        this.clothbag_required = clothbag_required;
    }

    public String getSms_sent() {
        return sms_sent;
    }

    public void setSms_sent(String sms_sent) {
        this.sms_sent = sms_sent;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getActionTime() {
        return actionTime;
    }

    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getAddress5() {
        return address5;
    }

    public void setAddress5(String address5) {
        this.address5 = address5;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getShow_cart() {
        return show_cart;
    }

    public void setShow_cart(String show_cart) {
        this.show_cart = show_cart;
    }

    public String getAction_time() {
        return action_time;
    }

    public void setAction_time(String action_time) {
        this.action_time = action_time;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAm_delivery_charge() {
        return am_delivery_charge;
    }

    public void setAm_delivery_charge(String am_delivery_charge) {
        this.am_delivery_charge = am_delivery_charge;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getDelivery_method() {
        return delivery_method;
    }

    public void setDelivery_method(String delivery_method) {
        this.delivery_method = delivery_method;
    }

    public String getPayment_option() {
        return payment_option;
    }

    public void setPayment_option(String payment_option) {
        this.payment_option = payment_option;
    }

    public String getColor_flag() {
        return color_flag;
    }

    public void setColor_flag(String color_flag) {
        this.color_flag = color_flag;
    }
}
