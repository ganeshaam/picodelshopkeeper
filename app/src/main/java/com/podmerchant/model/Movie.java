package com.podmerchant.model;

public class Movie {

    private String image;
    private String productName;
    private String id;
    private String sellerName;
    private String mrp;
    private String discount;
    private String price;
    private int count = 1;
    private String shop_id;
    private String shopName;
    private boolean status = false;
    private String tag;
    private String title;
    private String select_type;
    private int type;
    private String redeemCategory, redeemCategoryCount;
    private String redeem_service_image, redeem_service_name, redeem_service_cost;
    private String redeem_shop_name;
    private String redeem_offer_name;
    private String redeem_min_amount;
    private String redeem_offer_amount;
    private String redeem_offer_unit;
    private String redeem_end_date;
    private String redeem_shop_address;
    private String product_size;
    private String p_Name;
    private String addToCartProductId, addToCartProductName, addToCartBrandName, addToCartSellerName,
            addToCartQty, addToCartProductImg, addToCartShopName;
    double addToCartPrice, cPrice;
    private int addToCartCount = 0;
    private String offerCode, pId1, pId2, pId3, pName1, pName2, pName3,
            img1, img2, img3, strShopName, qty, uniqueId;
    public static final int NORMAL_TYPE = 0;
    public static final int COMBO_TYPE = 1;
    private String SearchMainCatShop_id, SearchMainCatShopAvgRating, SearchMainCatShopName, SearchMainCatHomeDelivery, SearchMainCatShopImage,
            SearchMainCatDeliveryTime, SearchMainCatAddress, SearchMainCatDeliveryArea, SearchMainCatProductQuantity,
            SearchMainCatAamPin, SearchMainCatShopCategory, SearchMainCatMinDeliveryAmt, SearchMainCatCouponMsg;
    private String resto_productId;
    private String resto_productName;
    private String resto_sellerName;
    private String resto_qty;
    private String restoShopId;
    private String resto_review_name;
    private String resto_review_rating;
    private String resto_review_review;
    private String resto_review_rdate;
    double resto_price;
    private String shop_name;
    private String shopId;
    private String resto_food_product_id;
    private String resto_food_product_mrp;
    private String resto_food_product_price;
    private String resto_food_product_name;
    private String resto_food_product_discount;
    private String coupon_code;
    private String address_id, strName, strAddress, strContact, strPincode, strCount, strAvailableQuantity, strHindiName;

    public Movie() {
    }

    public Movie(String redeemCategory, String redeemCategoryCount) {
        this.redeemCategory = redeemCategory;
        this.redeemCategoryCount = redeemCategoryCount;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public Movie(String redeem_shop_id, String redeem_shop_name, String redeem_offer_name,
                 String redeem_min_amount, String redeem_offer_amount, String redeem_offer_unit,
                 String redeem_startdate, String redeem_end_date, String redeem_shop_address) {
        this.redeem_shop_name = redeem_shop_name;
        this.redeem_offer_name = redeem_offer_name;
        this.redeem_min_amount = redeem_min_amount;
        this.redeem_offer_amount = redeem_offer_amount;
        this.redeem_offer_unit = redeem_offer_unit;
        this.redeem_end_date = redeem_end_date;
        this.redeem_shop_address = redeem_shop_address;
    }

    public Movie(String redeem_service_image, String redeem_service_name, String redeem_service_cost) {
        this.redeem_service_image = redeem_service_image;
        this.redeem_service_name = redeem_service_name;
        this.redeem_service_cost = redeem_service_cost;
    }


    public Movie(String title) {
        this.title = title;
    }


    public Movie(String resto_review_name, String resto_review_rating, String resto_review_review, String resto_review_rdate) {
        this.resto_review_name = resto_review_name;
        this.resto_review_rating = resto_review_rating;
        this.resto_review_review = resto_review_review;
        this.resto_review_rdate = resto_review_rdate;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public Movie(String shop_name, String shipping_time, String min_amount, String offer_discount, String rating,
                 String shop_time, String shopId, String shop_logo, String shop_banner, String counpon_message, String CouponCode) {
        this.shop_name = shop_name;
        String shipping_time1 = shipping_time;
        String rating1 = rating;
        this.shopId = shopId;
        String shop_banner1 = shop_banner;
        String counpon_message1 = counpon_message;
        this.coupon_code = CouponCode;
    }


    public String getStrHindiName() {
        return strHindiName;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public Movie(String shop_id, String image, String image1, String image2, String image3, String image4, String id, String productName,
                 String brandName, String sellerName, String mrp, String discount, String price,
                 String type, String available_qty, String hindi_name, String size, String p_name) {
        this.shop_id = shop_id;
        this.image = image;
        this.id = id;
        this.productName = productName;
        this.sellerName = sellerName;
        this.mrp = mrp;
        this.discount = discount;
        this.price = price;
        this.select_type = type;
        this.strAvailableQuantity = available_qty;
        this.strHindiName = hindi_name;
        this.product_size = size;
        this.p_Name = p_name;
    }

    public String getP_Name() {
        return p_Name;
    }

    public String getStrAvailableQuantity() {
        return strAvailableQuantity;
    }

    public String getAddress_id() {
        return address_id;
    }

    public String getStrName() {
        return strName;
    }

    public String getStrAddress() {
        return strAddress;
    }

    public String getStrContact() {
        return strContact;
    }

    public String getStrPincode() {
        return strPincode;
    }

    public String getStrCount() {
        return strCount;
    }

    public Movie(String address_id, String name, String address, String contact, String strPincode, String strCount) {
        this.address_id = address_id;
        this.strName = name;
        this.strAddress = address;
        this.strContact = contact;
        this.strPincode = strPincode;
        this.strCount = strCount;
    }

    public String getSelect_type() {
        return select_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }

    public String getRedeemCategory() {
        return redeemCategory;
    }

    public String getRedeemCategoryCount() {
        return redeemCategoryCount;
    }

    public String getRedeem_service_image() {
        return redeem_service_image;
    }

    public String getRedeem_service_name() {
        return redeem_service_name;
    }

    public String getRedeem_service_cost() {
        return redeem_service_cost;
    }

    public String getRedeem_shop_name() {
        return redeem_shop_name;
    }

    public String getRedeem_offer_name() {
        return redeem_offer_name;
    }

    public String getRedeem_min_amount() {
        return redeem_min_amount;
    }

    public String getRedeem_offer_amount() {
        return redeem_offer_amount;
    }

    public String getRedeem_offer_unit() {
        return redeem_offer_unit;
    }

    public String getRedeem_end_date() {
        return redeem_end_date;
    }

    public String getRedeem_shop_address() {
        return redeem_shop_address;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public String getProductName() {
        return productName;
    }

    public String getSellerName() {
        return sellerName;
    }

    public String getMrp() {
        return mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public String getPrice() {
        return price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String setDiscount(String discount) {
        this.discount = discount;
        return discount;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isStatus(boolean check) {
        return status;
    }

    public boolean setStatus(boolean status) {
        this.status = status;
        return status;
    }

    public String getResto_review_rdate() {
        return resto_review_rdate;
    }

    public String getResto_review_review() {
        return resto_review_review;
    }

    public String getResto_review_rating() {
        return resto_review_rating;
    }

    public String getResto_review_name() {
        return resto_review_name;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getResto_food_product_id() {
        return resto_food_product_id;
    }

    public String getResto_food_product_mrp() {
        return resto_food_product_mrp;
    }

    public String getResto_food_product_price() {
        return resto_food_product_price;
    }

    public String getResto_food_product_name() {
        return resto_food_product_name;
    }

    public String getResto_food_product_discount() {
        return resto_food_product_discount;
    }

    public String getProduct_size() {
        return product_size;
    }
}
