package com.podmerchant.model;

public class CityModel {

    String city,city_id,status,state_id,price_restrict;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getPrice_restrict() {
        return price_restrict;
    }

    public void setPrice_restrict(String price_restrict) {
        this.price_restrict = price_restrict;
    }
}
