package com.podmerchant.model;

import java.util.ArrayList;

public class productCodeWithProductList {

    public String code;
    int position;

    public ArrayList<ProductCodeWiseProduct> productCodeWiseProducts;
    public ArrayList<ProductCodeWiseProduct> similarProductCodeWiseProducts;

    public int getPosition() {
        return position;
    }


    public void setPosition(int position) {
        this.position = position;
    }

}
