package com.podmerchant.model;

public class FreelancerModel {

    private String areaName,noOfRejection,locationLat,locationLong;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getNoOfRejection() {
        return noOfRejection;
    }

    public void setNoOfRejection(String noOfRejection) {
        this.noOfRejection = noOfRejection;
    }

    public String getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(String locationLat) {
        this.locationLat = locationLat;
    }

    public String getLocationLong() {
        return locationLong;
    }

    public void setLocationLong(String locationLong) {
        this.locationLong = locationLong;
    }
}
