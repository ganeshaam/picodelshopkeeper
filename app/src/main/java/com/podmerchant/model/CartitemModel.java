package com.podmerchant.model;

public class CartitemModel {

  private String cart_item_id,product_id,product_name,product_mrp,item_price,item_price_change,item_mrp_change,old_price,product_cat,item_id,product_hindi_name,
            product_maincat,product_subcat,product_sub_subcat,product_image,product_price,product_discount,iten_qty,product_size,
          brand_name,user_id,cart_user_id,cart_unique_id,updated_status;

    public String getCart_item_id() {
        return cart_item_id;
    }

    public void setCart_item_id(String cart_item_id) {
        this.cart_item_id = cart_item_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_mrp() {
        return product_mrp;
    }

    public void setProduct_mrp(String product_mrp) {
        this.product_mrp = product_mrp;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getItem_price_change() {
        return item_price_change;
    }

    public void setItem_price_change(String item_price_change) {
        this.item_price_change = item_price_change;
    }

    public String getItem_mrp_change() {
        return item_mrp_change;
    }

    public void setItem_mrp_change(String item_mrp_change) {
        this.item_mrp_change = item_mrp_change;
    }

    public String getOld_price() {
        return old_price;
    }

    public void setOld_price(String old_price) {
        this.old_price = old_price;
    }

    public String getProduct_cat() {
        return product_cat;
    }

    public void setProduct_cat(String product_cat) {
        this.product_cat = product_cat;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getProduct_hindi_name() {
        return product_hindi_name;
    }

    public void setProduct_hindi_name(String product_hindi_name) {
        this.product_hindi_name = product_hindi_name;
    }

    public String getProduct_maincat() {
        return product_maincat;
    }

    public void setProduct_maincat(String product_maincat) {
        this.product_maincat = product_maincat;
    }

    public String getProduct_subcat() {
        return product_subcat;
    }

    public void setProduct_subcat(String product_subcat) {
        this.product_subcat = product_subcat;
    }

    public String getProduct_sub_subcat() {
        return product_sub_subcat;
    }

    public void setProduct_sub_subcat(String product_sub_subcat) {
        this.product_sub_subcat = product_sub_subcat;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_discount() {
        return product_discount;
    }

    public void setProduct_discount(String product_discount) {
        this.product_discount = product_discount;
    }

    public String getIten_qty() {
        return iten_qty;
    }

    public void setIten_qty(String iten_qty) {
        this.iten_qty = iten_qty;
    }

    public String getProduct_size() {
        return product_size;
    }

    public void setProduct_size(String product_size) {
        this.product_size = product_size;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCart_user_id() {
        return cart_user_id;
    }

    public void setCart_user_id(String cart_user_id) {
        this.cart_user_id = cart_user_id;
    }

    public String getCart_unique_id() {
        return cart_unique_id;
    }

    public void setCart_unique_id(String cart_unique_id) {
        this.cart_unique_id = cart_unique_id;
    }

    public String getUpdated_status() {
        return updated_status;
    }

    public void setUpdated_status(String updated_status) {
        this.updated_status = updated_status;
    }
}
