package com.podmerchant.model;

public class DeliveryBoyModel {

   private String delBoyId,name,mobile_number,usertype,shop_id,imgeurl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getImgeurl() {
        return imgeurl;
    }

    public void setImgeurl(String imgeurl) {
        this.imgeurl = imgeurl;
    }

    public String getDelBoyId() {
        return delBoyId;
    }

    public void setDelBoyId(String delBoyId) {
        this.delBoyId = delBoyId;
    }
}
