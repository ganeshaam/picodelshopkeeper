package com.podmerchant.model;

import java.io.Serializable;

public class PartnerKitModel implements Serializable {

    private String kitName,kitAmount,position,id,shopcategory;
    private boolean isChecked;

    public String getKitName() {
        return kitName;
    }

    public void setKitName(String kitName) {
        this.kitName = kitName;
    }

    public String getKitAmount() {
        return kitAmount;
    }

    public void setKitAmount(String kitAmount) {
        this.kitAmount = kitAmount;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopcategory() {
        return shopcategory;
    }

    public void setShopcategory(String shopcategory) {
        this.shopcategory = shopcategory;
    }
}
