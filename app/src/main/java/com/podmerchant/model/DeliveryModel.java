package com.podmerchant.model;

public class DeliveryModel {

    String shop_id, d_from, d_destination,d_no_of_items,d_weight,d_height,d_width,d_cdate,d_amount, d_status;

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getD_from() {
        return d_from;
    }

    public void setD_from(String d_from) {
        this.d_from = d_from;
    }

    public String getD_destination() {
        return d_destination;
    }

    public void setD_destination(String d_destination) {
        this.d_destination = d_destination;
    }

    public String getD_no_of_items() {
        return d_no_of_items;
    }

    public void setD_no_of_items(String d_no_of_items) {
        this.d_no_of_items = d_no_of_items;
    }

    public String getD_weight() {
        return d_weight;
    }

    public void setD_weight(String d_weight) {
        this.d_weight = d_weight;
    }

    public String getD_height() {
        return d_height;
    }

    public void setD_height(String d_height) {
        this.d_height = d_height;
    }

    public String getD_width() {
        return d_width;
    }

    public void setD_width(String d_width) {
        this.d_width = d_width;
    }

    public String getD_cdate() {
        return d_cdate;
    }

    public void setD_cdate(String d_cdate) {
        this.d_cdate = d_cdate;
    }

    public String getD_amount() {
        return d_amount;
    }

    public void setD_amount(String d_amount) {
        this.d_amount = d_amount;
    }

    public String getD_status() {
        return d_status;
    }

    public void setD_status(String d_status) {
        this.d_status = d_status;
    }
}
