package com.podmerchant.model;

public class ProductCodeWiseProduct {
    public String code;
    String shop_id;
    final String shop_name;
    final String shop_category;
    final String product_name;
    final String product_id;
    final String product_brand;
    final String product_image;
    final String product_image1;
    final String product_image2;
    final String product_image3;
    final String product_image4;
    final String product_size;
    final String product_mrp;
    final String product_price;
    String product_discount;
    final String product_description;
    final String similar_product_status;
    final String select_type;
    final String strAvailable_Qty;
    final String strHindiName;
    final String product_maincat;
    final String cart_pstatus;

    boolean status = false;
    private String product_qty;

    public ProductCodeWiseProduct(String code, String shop_id, String shop_name, String shop_category,
                                  String product_name, String product_brand, String product_id, String product_image,
                                  String product_image1, String product_image2, String product_image3,
                                  String product_image4, String product_size, String product_mrp, String product_price,
                                  String product_discount, String product_description, String similar_product_status,
                                  String type, String available_qty, String product_maincat, String hindi_name, String cart_pstatus) {
        this.code = code;
        this.shop_id = shop_id;
        this.shop_name = shop_name;
        this.shop_category = shop_category;
        this.product_name = product_name;
        this.product_brand = product_brand;
        this.product_id = product_id;
        this.product_image = product_image;
        this.product_image1 = product_image1;
        this.product_image2 = product_image2;
        this.product_image3 = product_image3;
        this.product_image4 = product_image4;
        this.product_size = product_size;
        this.product_mrp = product_mrp;
        this.product_price = product_price;
        this.product_discount = product_discount;
        this.product_description = product_description;
        this.similar_product_status = similar_product_status;
        this.select_type = type;
        this.strAvailable_Qty = available_qty;
        this.product_maincat = product_maincat;
        this.strHindiName = hindi_name;
        this.cart_pstatus = cart_pstatus;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public String getShop_category() {
        return shop_category;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getProduct_brand() {
        return product_brand;
    }

    public String getProduct_image() {
        return product_image;
    }

    public String getProduct_image1() {
        return product_image1;
    }

    public String getProduct_image2() {
        return product_image2;
    }

    public String getProduct_image3() {
        return product_image3;
    }

    public String getProduct_image4() {
        return product_image4;
    }

    public String getProduct_size() {
        return product_size;
    }

    public String getProduct_mrp() {
        return product_mrp;
    }

    public String getProduct_price() {
        return product_price;
    }

    public String getProduct_discount() {
        return product_discount;
    }

    public String setProduct_discount(String product_discount) {
        this.product_discount = product_discount;
        return product_discount;
    }

    public String getProduct_description() {
        return product_description;
    }

    public String getSimilar_product_status() {
        return similar_product_status;
    }

    public String getSelect_type() {
        return select_type;
    }

    public String getStrAvailable_Qty() {
        return strAvailable_Qty;
    }

    public String getStrHindiName() {
        return strHindiName;
    }

    public String getProduct_maincat() {
        return product_maincat;
    }

    public String getCart_pstatus() {
        return cart_pstatus;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getProduct_qty() {
        return product_qty;
    }

    public void setProduct_qty(String product_qty) {
        this.product_qty = product_qty;
    }
}
