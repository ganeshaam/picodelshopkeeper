package com.podmerchant.model;

public class PicodelPointsModel {

    private String referto,points,oldpoints,newpoints,refercode,status,createdat,referby;


    public String getReferto() {
        return referto;
    }

    public void setReferto(String referto) {
        this.referto = referto;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getOldpoints() {
        return oldpoints;
    }

    public void setOldpoints(String oldpoints) {
        this.oldpoints = oldpoints;
    }

    public String getNewpoints() {
        return newpoints;
    }

    public void setNewpoints(String newpoints) {
        this.newpoints = newpoints;
    }

    public String getRefercode() {
        return refercode;
    }

    public void setRefercode(String refercode) {
        this.refercode = refercode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getReferby() {
        return referby;
    }

    public void setReferby(String referby) {
        this.referby = referby;
    }
}
