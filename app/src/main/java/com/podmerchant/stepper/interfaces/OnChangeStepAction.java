package com.podmerchant.stepper.interfaces;


import com.podmerchant.stepper.SteppersItem;

public interface OnChangeStepAction {

    void onChangeStep(int position, SteppersItem activeStep);

}
