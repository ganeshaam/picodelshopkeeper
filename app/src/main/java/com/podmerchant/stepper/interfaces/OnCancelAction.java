

package com.podmerchant.stepper.interfaces;

public interface OnCancelAction {

    void onCancel();

}
