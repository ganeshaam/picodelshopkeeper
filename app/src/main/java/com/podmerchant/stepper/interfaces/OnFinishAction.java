
package com.podmerchant.stepper.interfaces;

public interface OnFinishAction {

    void onFinish();

}
