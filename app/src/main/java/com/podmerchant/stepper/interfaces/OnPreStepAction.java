package com.podmerchant.stepper.interfaces;

public interface OnPreStepAction {

    void onSkipStep();

}
