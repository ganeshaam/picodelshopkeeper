
package com.podmerchant.stepper;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.podmerchant.BuildConfig;
import com.podmerchant.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

class SteppersAdapter extends RecyclerView.Adapter<SteppersViewHolder> {
    private static final int VIEW_COLLAPSED = 0;
    private static final int VIEW_EXPANDED = 1;

    private static final String TAG = "SteppersAdapter";
    private final SteppersView steppersView;
    private final Context context;
    private final SteppersView.Config config;
    private List<SteppersItem> items;
    private final FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private final Map<Integer, Integer> frameLayoutIds = new HashMap<>();

    private int beforeStep = -1;
    private int currentStep = 0;

    public SteppersAdapter(SteppersView steppersView, SteppersView.Config config, List<SteppersItem> items) {
        this.steppersView = steppersView;
        this.context = steppersView.getContext();
        this.config = config;
        this.items = items;
        this.fragmentManager = config.getFragmentManager();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == currentStep ? VIEW_EXPANDED : VIEW_COLLAPSED);
    }

    @Override
    public SteppersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        if (viewType == VIEW_COLLAPSED) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.steppers_item, parent, false);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.steppers_item_expanded, parent, false);
        }

        return new SteppersViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SteppersViewHolder holder, int p) {
        final int position = holder.getAdapterPosition();
        final SteppersItem steppersItem = items.get(position);

        holder.setChecked(position < currentStep);
        if (holder.isChecked()) {
            holder.roundedView.setChecked(true);
        } else {
            holder.roundedView.setChecked(false);
            holder.roundedView.setText(position + 1 + "");
        }

        if (position == currentStep || holder.isChecked())
            holder.roundedView.setCircleAccentColor();
        else holder.roundedView.setCircleGrayColor();

        holder.textViewLabel.setText(steppersItem.getLabel());
        holder.textViewSubLabel.setText(steppersItem.getSubLabel());

        holder.linearLayoutContent.setVisibility(position == currentStep || position == beforeStep ? View.VISIBLE : View.GONE);

        holder.buttonContinue.setEnabled(steppersItem.isPositiveButtonEnable());
        steppersItem.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (observable != null) {
                    SteppersItem item = (SteppersItem) observable;
                    holder.buttonContinue.setEnabled(item.isPositiveButtonEnable());
                }
            }
        });

        if (position == getItemCount() - 1) {
            holder.buttonContinue.setVisibility(View.GONE);
            holder.buttonCancel.setVisibility(View.GONE);
        } else {
            holder.buttonContinue.setText(context.getResources().getString(R.string.step_continue));
        }
        holder.buttonPrevious.setVisibility(View.GONE);

        holder.buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == getItemCount() - 1) config.getOnFinishAction().onFinish();
                else {
                    if (steppersItem.getOnClickContinue() != null) {
                        steppersItem.getOnClickContinue().onClick();
                    } else {
                        nextStep();
                    }
                }
            }
        });

        holder.buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preStep();
            }

        });


        if (config.isCancelAvailable()) {
            if (config.getOnCancelAction() != null)
                holder.buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        config.getOnCancelAction().onCancel();
                    }
                });
        } else {
            holder.buttonCancel.setVisibility(View.GONE);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.frame_layout, holder.frameLayout, true);

        if (frameLayoutIds.get(position) == null)
            frameLayoutIds.put(position, findUnusedId(holder.itemView));

        //frameLayout.setId(frameLayoutIds.get(position));

        if (config.getFragmentManager() != null && steppersItem.getFragment() != null) {
            holder.frameLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
            holder.frameLayout.setTag(frameLayoutName());

            if (fragmentTransaction == null) {
                fragmentTransaction = fragmentManager.beginTransaction();
            }

            String name = makeFragmentName(steppersView.getId(), position);
            Fragment fragment = fragmentManager.findFragmentByTag(name);

            if (position < beforeStep) {
                if (fragment != null) {
                    if (BuildConfig.DEBUG)
                        Log.v(TAG, "Removing item #" + position + ": f=" + fragment);
                    fragmentTransaction.detach(fragment);
                }
            } else if (position == beforeStep || position == currentStep) {
                if (fragment != null) {
                    if (BuildConfig.DEBUG)
                        Log.v(TAG, "Attaching item #" + position + ": f=" + fragment + " d=" + fragment.isDetached());
                    fragmentTransaction.attach(fragment);
                } else {
                    fragment = steppersItem.getFragment();
                    if (BuildConfig.DEBUG)
                        Log.v(TAG, "Adding item #" + position + ": f=" + fragment + " n=" + name);
                    fragmentTransaction.add(steppersView.getId(), fragment,
                            name);

                }
            }

            if (fragmentTransaction != null) {
                fragmentTransaction.commitAllowingStateLoss();
                fragmentTransaction = null;
                fragmentManager.executePendingTransactions();
            }

            if (fragmentManager.findFragmentByTag(name) != null &&
                    fragmentManager.findFragmentByTag(name).getView() != null) {

                View fragmentView = fragmentManager.findFragmentByTag(name).getView();

                if (fragmentView.getParent() != null && frameLayoutName() != ((View) fragmentView.getParent()).getTag()) {
                    steppersView.removeViewInLayout(fragmentView);

                    holder.frameLayout.removeAllViews();
                    holder.frameLayout.addView(fragmentView);
                }
            }
        }

        if (beforeStep == position) {
            AnimationUtils.hide(holder.linearLayoutContent);
        }
        if (currentStep == position && !steppersItem.isDisplayed()) {
            steppersItem.setDisplayed();
        }
    }

    void nextStep() {
        changeToStep(currentStep + 1);
    }

    void preStep() {
        changeToStep(currentStep - 1);
    }

    void changeToStep(int position) {
        if (position != currentStep) {
            this.beforeStep = currentStep;
            this.currentStep = position;
            if (beforeStep < currentStep)
                notifyItemRangeChanged(beforeStep, currentStep);
            else
                notifyItemRangeChanged(currentStep, beforeStep);

            if (config.getOnChangeStepAction() != null) {
                SteppersItem steppersItem = items.get(this.currentStep);
                config.getOnChangeStepAction().onChangeStep(this.currentStep, steppersItem);
            }
        } else {
            if (BuildConfig.DEBUG) Log.i(TAG, "This step is currently active");
        }
    }

    protected void setItems(List<SteppersItem> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private int fID = 87352142;

    private int findUnusedId(View view) {
        while (view.findViewById(++fID) != null) ;
        return fID;
    }

    private static String frameLayoutName() {
        return "android:steppers:framelayout";
    }

    private static String makeFragmentName(int viewId, long id) {
        return "android:steppers:" + viewId + ":" + id;
    }
}
