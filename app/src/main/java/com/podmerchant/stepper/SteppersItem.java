package com.podmerchant.stepper;

import androidx.fragment.app.Fragment;

import com.podmerchant.stepper.interfaces.OnClickContinue;
import com.podmerchant.stepper.interfaces.OnPreStepAction;

import java.util.Observable;



public class SteppersItem extends Observable {

    private String label;
    private String subLabel;
    private boolean buttonEnable = true;
    private Fragment fragment;
    private OnClickContinue onClickContinue;
    private boolean skippable = false;
    private OnPreStepAction onSkipStepAction;

    private boolean displayed = false;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSubLabel() {
        return subLabel;
    }

    public void setSubLabel(String subLabel) {
        this.subLabel = subLabel;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public boolean isPositiveButtonEnable() {
        return buttonEnable;
    }

    public void setPositiveButtonEnable(boolean buttonEnable) {
        synchronized (this) {
            this.buttonEnable = buttonEnable;
        }
        setChanged();
        notifyObservers();
    }

    public OnClickContinue getOnClickContinue() {
        return onClickContinue;
    }

    public void setOnClickContinue(OnClickContinue onClickContinue) {
        this.onClickContinue = onClickContinue;
    }


    public boolean isSkippable() {
        return skippable;
    }

    public OnPreStepAction getOnSkipStepAction() {
        return onSkipStepAction;
    }

    public void setOnClickPrevious(OnPreStepAction onClickPrevious) {
        this.onSkipStepAction = onClickPrevious;
    }

    public void setSkippable(boolean skippable, OnPreStepAction onSkipStepAction) {
        this.skippable = skippable;
        this.onSkipStepAction = onSkipStepAction;
    }

    synchronized boolean isDisplayed() {
        return displayed;
    }

    void setDisplayed() {
        this.displayed = true;
    }
}
