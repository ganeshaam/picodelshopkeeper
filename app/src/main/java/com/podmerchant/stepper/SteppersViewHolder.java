package com.podmerchant.stepper;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.podmerchant.R;


public class SteppersViewHolder extends RecyclerView.ViewHolder {

    private boolean isChecked;

    final View itemView;
    final RoundedView roundedView;
    final TextView textViewLabel;
    final TextView textViewSubLabel;
    final LinearLayout linearLayoutContent;
    final FrameLayout frameLayout;
    protected LinearLayout frameLayoutsContainer;
    final Button buttonContinue;
    final Button buttonPrevious;
    final Button buttonCancel;
    private Fragment fragment;

    SteppersViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        this.roundedView = itemView.findViewById(R.id.roundedView);
        this.textViewLabel = itemView.findViewById(R.id.textViewLabel);
        this.textViewSubLabel = itemView.findViewById(R.id.textViewSubLabel);
        this.linearLayoutContent = itemView.findViewById(R.id.linearLayoutContent);
        this.frameLayout = itemView.findViewById(R.id.frameLayout);
        //this.frameLayoutsContainer = (LinearLayout) itemView.findViewById(R.id.frameLayoutsContainer);
        this.buttonContinue = itemView.findViewById(R.id.buttonContinue);
        this.buttonPrevious = itemView.findViewById(R.id.buttonPrevious);
        this.buttonCancel = itemView.findViewById(R.id.buttonCancel);
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public Fragment getFragment() {
        return fragment;
    }

    /**
     * @return true if step is done, false if not
     */
    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
