package com.podmerchant.fragment.filter_fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.podmerchant.R;

import java.util.ArrayList;
import java.util.List;

import com.podmerchant.activites.FilterLayout;
import com.podmerchant.model.Movie;

/**
 * A simple {@link Fragment} subclass.
 */
public class PriceFilter extends Fragment {

    private final String[] priceRangeArray = {"0-100", "100-500", "500-1000", "1000-10000", "10000-20000", "20000 and higher"};
    private final String[] discountRangeArray = {"40% and Higher", "30% and Higher", "20% and Higher", "10% and Higher", "5% and Higher"};
    private PriceCustomListViewAdapter priceAdapter;
    private PriceCustomListViewAdapter discountAdapter;
    private CheckBox priceCheckBox;
    private RadioButton discountRadioButton;
    private static int positionForRadioButton = -1;
    public static final ArrayList<Object> priceRangeSelectedList = new ArrayList<>();
    public static final ArrayList<Object> discountRangeSelectedList = new ArrayList<>();
    public static final ArrayList<String> priceNameList = new ArrayList<>();
    public static final ArrayList<String> discountNameList = new ArrayList<>();
    private static int countForDiscountRadioButton = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_price_filter, container, false);

        ListView priceListView = view.findViewById(R.id.priceRangeList);
        ListView discountListView = view.findViewById(R.id.discountRangeList);

        //for price Range
        List<Movie> priceMovies = new ArrayList<>();
        for (String aPriceRangeArray : priceRangeArray) {
            Movie priceItem = new Movie(aPriceRangeArray);
            priceMovies.add(priceItem);
        }

        //for discount Range
        List<Movie> discountMovies = new ArrayList<>();
        for (String aDiscountRangeArray : discountRangeArray) {
            Movie discountItem = new Movie(aDiscountRangeArray);
            discountMovies.add(discountItem);
        }
        //for priceRange Listview
        priceAdapter = new PriceCustomListViewAdapter(getActivity(), R.layout.price_range_list_filter, priceMovies, 0);
        priceListView.setAdapter(priceAdapter);

        //for discountRange ListView
        discountAdapter = new PriceCustomListViewAdapter(getActivity(), R.layout.discount_range_list_filter, discountMovies, 1);
        discountListView.setAdapter(discountAdapter);

        priceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterLayout.tvPriceFilterCount.setVisibility(View.VISIBLE);
                priceCheckBox = view.findViewById(R.id.checkBox1);
                TextView name = view.findViewById(R.id.checkBoxName);
                if (priceCheckBox.isChecked()) {
                    priceRangeSelectedList.remove((Object) position);
                    priceNameList.remove(name.getText());
                    priceCheckBox.setChecked(false);
                } else {
                    priceRangeSelectedList.add(position);
                    priceNameList.add((String) name.getText());
                    priceCheckBox.setChecked(true);
                }
                if (priceRangeSelectedList.size() == 0 && countForDiscountRadioButton == 0) {
                    FilterLayout.tvPriceFilterCount.setVisibility(View.INVISIBLE);
                }
                FilterLayout.tvPriceFilterCount.setText("" + (PriceFilter.priceRangeSelectedList.size() + countForDiscountRadioButton));
            }
        });

        discountListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterLayout.tvPriceFilterCount.setVisibility(View.VISIBLE);
                discountRadioButton = view.findViewById(R.id.radioButtonDiscount);
                TextView name = view.findViewById(R.id.radioButtonName);
                positionForRadioButton = position;
                if (discountRadioButton.isChecked()) {
                    discountRangeSelectedList.remove((Object) position);
                    discountNameList.remove(name.getText());
                    discountRadioButton.setChecked(false);
                    countForDiscountRadioButton = 0;
                } else {
                    discountNameList.clear();
                    discountRangeSelectedList.clear();
                    discountRangeSelectedList.add(position);
                    discountNameList.add((String) name.getText());
                    countForDiscountRadioButton = 1;
                }
                if (priceRangeSelectedList.size() == 0 && countForDiscountRadioButton == 0) {
                    FilterLayout.tvPriceFilterCount.setVisibility(View.INVISIBLE);
                }
                FilterLayout.tvPriceFilterCount.setText("" + (PriceFilter.priceRangeSelectedList.size() + countForDiscountRadioButton));
                discountAdapter.notifyDataSetChanged();
            }
        });

        ((FilterLayout) getActivity()).setFragmentRefreshListener(new FilterLayout.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                priceAdapter.notifyDataSetChanged();
                discountAdapter.notifyDataSetChanged();
            }
        });

        return view;
    }


    public static class PriceCustomListViewAdapter extends ArrayAdapter<Movie> {
        final Context context;
        private final List<Movie> items;
        private final int stepCount;


        public PriceCustomListViewAdapter(Context context, int resourceId, List<Movie> items, int stepCount) {
            super(context, resourceId, items);
            this.context = context;
            this.items = items;
            this.stepCount = stepCount;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            Movie movie = getItem(position);
            if (stepCount == 0) {
                convertView = null;
                LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(R.layout.price_range_list_filter, null);
                CheckBox cbprice = convertView.findViewById(R.id.checkBox1);
                TextView priceRange = convertView.findViewById(R.id.checkBoxName);

                if (priceRangeSelectedList.contains(position))
                    cbprice.setChecked(true);
                else
                    cbprice.setChecked(false);
                priceRange.setText(movie.getTitle());
            }
            if (stepCount == 1) {
                convertView = null;
                LayoutInflater dInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = dInflater.inflate(R.layout.discount_range_list_filter, null);
                TextView discountRange = convertView.findViewById(R.id.radioButtonName);
                RadioButton rb = convertView.findViewById(R.id.radioButtonDiscount);
                discountRange.setText(movie.getTitle());
                if (positionForRadioButton == position && (discountRangeSelectedList.contains(position)))
                    rb.setChecked(true);
                else
                    rb.setChecked(false);
            }
            return convertView;
        }
    }

}
