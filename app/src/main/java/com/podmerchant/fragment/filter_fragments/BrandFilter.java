package com.podmerchant.fragment.filter_fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import com.podmerchant.R;
import com.podmerchant.activites.FilterLayout;
import com.podmerchant.activites.ProductListActivity;
import com.podmerchant.model.Movie;
import java.util.List;
/**
 * A simple {@link Fragment} subclass.
 */
public class BrandFilter extends Fragment {

    private ListView brandListView;
    private String[] brand;
    private List<Movie> brandMovies;
    private BrandCustomListViewAdapter brandAdapter;
    public static final ArrayList<String> normalBrandNameList = new ArrayList<>();
    public static final ArrayList<String> dependentBrandNameList = new ArrayList<>();
    public static final ArrayList<String> dependentBrandNameListLastResult = new ArrayList<>();
    private static int count = 0;
    private static CheckBox brandCheckBox;
    public static String brandTag = "";
    private TextView tvBrandMsg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment__brand_filter, container, false);
        brandListView = view.findViewById(R.id.brandList);
        tvBrandMsg = view.findViewById(R.id.txtBrandMessage);

        brandMovies = new ArrayList<>();
        brandAdapter = new BrandCustomListViewAdapter(getActivity(), brandMovies); //for brand ListView

        setDataToBrandList();
      /*  try {
            ((FilterLayout) getActivity()).setFragmentRefreshListener(new FilterLayout.FragmentRefreshListener() {
                @Override
                public void onRefresh() {
                    try {
                        int listViewCount = brandAdapter.getCount();
                        if (listViewCount != 0) {
                            if (brandTag.equals("normalTag")) {
                                brandAdapter.notifyDataSetChanged();
                            } else {
                                brandAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "There is no data to clear", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
      */  return view;
    }

    private void setDataToBrandList() {
        try {
            if (DepartmentFilter.departmentNameList.size() == 0) {
                brandTag = "normalTag";
            } else {
                brandTag = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (DepartmentFilter.departmentNameList.size() == 0) {
                if (ProductListActivity.uniqueBrandNames.size() > 0) {
                    brand = ProductListActivity.uniqueBrandNames.toArray(new String[ProductListActivity.uniqueBrandNames.size()]);
                    moderateBrand(brand);
                }
            } else {
                if (FilterLayout.categoryDependentUniqueBrandNames.size() > 0) {
                    brand = FilterLayout.categoryDependentUniqueBrandNames.toArray(new String[FilterLayout.categoryDependentUniqueBrandNames.size()]);
                    count = 0;
                    moderateBrand(brand);
                } else {
                    brand = new String[]{};
                    tvBrandMsg.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            dependentBrandNameListLastResult.clear();
            if (brand.length > 0) {
                for (String item : brand) {
                    if (dependentBrandNameList.contains(item)) {
                        count++;
                        dependentBrandNameListLastResult.add(item);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (brandTag.equals("normalTag")) {
            if (normalBrandNameList.size() > 0) {
                FilterLayout.tvBrandFilterCount.setVisibility(View.VISIBLE);
                FilterLayout.tvBrandFilterCount.setText(String.valueOf(normalBrandNameList.size()));
            } else {
                FilterLayout.tvBrandFilterCount.setVisibility(View.INVISIBLE);
            }
        } else {
            if (count > 0) {
                FilterLayout.tvBrandFilterCount.setVisibility(View.VISIBLE);
                FilterLayout.tvBrandFilterCount.setText(String.valueOf(count));
            } else {
                FilterLayout.tvBrandFilterCount.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void moderateBrand(String[] brand) {
        brandMovies = new ArrayList<>();
        for (String aBrand : brand) {
            Movie brandItem = new Movie(aBrand);
            brandMovies.add(brandItem);
        }

        if (brandMovies.size() > 0) {
            tvBrandMsg.setVisibility(View.GONE);

            //for brand Listview
            brandAdapter = new BrandCustomListViewAdapter(getActivity(), brandMovies);
            brandListView.setAdapter(brandAdapter);

            brandListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    brandCheckBox = view.findViewById(R.id.checkBox1);
                    TextView chkName = view.findViewById(R.id.checkBoxName);
                    if (brandCheckBox.isChecked()) {
                        if (brandTag.equals("normalTag")) {
                            normalBrandNameList.remove(chkName.getText());
                            brandCheckBox.setChecked(false);
                        } else {
                            dependentBrandNameList.remove(chkName.getText());
                            brandCheckBox.setChecked(false);
                            count--;
                            if (dependentBrandNameListLastResult.size() > 0) {
                                dependentBrandNameListLastResult.remove(chkName.getText());
                            }
                        }
                    } else {
                        if (brandTag.equals("normalTag")) {
                            normalBrandNameList.add((String) chkName.getText());
                            brandCheckBox.setChecked(true);
                        } else {
                            dependentBrandNameList.add((String) chkName.getText());
                            brandCheckBox.setChecked(true);
                            count++;
                            if (dependentBrandNameListLastResult.size() > 0) {
                                dependentBrandNameListLastResult.add((String) chkName.getText());
                            }
                        }
                    }

                    if (brandTag.equals("normalTag")) {
                        if (normalBrandNameList.size() == 0) {
                            FilterLayout.tvBrandFilterCount.setVisibility(View.INVISIBLE);
                        } else {
                            FilterLayout.tvBrandFilterCount.setVisibility(View.VISIBLE);
                            FilterLayout.tvBrandFilterCount.setText(String.valueOf(normalBrandNameList.size()));
                        }
                    } else {
                        if (count == 0) {
                            FilterLayout.tvBrandFilterCount.setVisibility(View.INVISIBLE);
                        } else {
                            FilterLayout.tvBrandFilterCount.setVisibility(View.VISIBLE);
                            FilterLayout.tvBrandFilterCount.setText(String.valueOf(count));
                        }
                    }
                }
            });
        } else {
            tvBrandMsg.setVisibility(View.VISIBLE);
        }
    }

    private class BrandCustomListViewAdapter extends ArrayAdapter<Movie> {
        final Context context;
        final List<Movie> items;

        BrandCustomListViewAdapter(Context context, List<Movie> items) {
            super(context, R.layout.price_range_list_filter, items);
            this.context = context;
            this.items = items;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {

            Movie movie = getItem(position);
            convertView = null;
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.price_range_list_filter, null);
                CheckBox cbPrice = convertView.findViewById(R.id.checkBox1);
                final TextView priceRange = convertView.findViewById(R.id.checkBoxName);
                priceRange.setText(movie.getTitle());
                String strCheck = movie.getTitle();
                if (brandTag.equals("normalTag")) {
                    if (normalBrandNameList.contains(strCheck)) {
                        cbPrice.setChecked(true);
                    } else {
                        cbPrice.setChecked(false);
                    }
                } else {
                    if (dependentBrandNameList.contains(strCheck)) {
                        cbPrice.setChecked(true);
                    } else {
                        cbPrice.setChecked(false);
                    }
                }
            }
            return convertView;
        }

        @Override
        public int getCount() {
            return items.size();
        }
    }

}
