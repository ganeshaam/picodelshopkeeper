package com.podmerchant.fragment.filter_fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.podmerchant.R;
import com.podmerchant.activites.FilterLayout;
import com.podmerchant.model.Movie;

/**
 * A simple {@link Fragment} subclass.
 */
public class SortFilter extends Fragment {

    private final String[] sortArray = {"Price:Low to High", "Price:High to Low", "Newest Arrivals"};
    private SortCustomListViewAdapter sortAdapter;
    private RadioButton sortRadioButton;
    public static final ArrayList<String> sortNameList = new ArrayList<>();
    public static final ArrayList<Object> sortRangeSelectedList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_sort_filter, container, false);
        ListView sortListView = view.findViewById(R.id.sortList);

        //for sort Range
        List<Movie> sortMovies = new ArrayList<>();
        for (String aSortArray : sortArray) {
            Movie priceItem = new Movie(aSortArray);
            sortMovies.add(priceItem);
        }

        //for Sort ListView
        sortAdapter = new SortCustomListViewAdapter(getActivity(), sortMovies);
        sortListView.setAdapter(sortAdapter);

        sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterLayout.tvSortFilterCount.setVisibility(View.VISIBLE);
                sortRadioButton = view.findViewById(R.id.radioButtonDiscount);
                TextView sortParameterName = view.findViewById(R.id.radioButtonName);
                if (sortRadioButton.isChecked()) {
                    sortRangeSelectedList.remove((Object) position);
                    sortNameList.remove(sortParameterName.getText());
                    sortRadioButton.setChecked(false);
                } else {
                    sortNameList.clear();
                    sortRangeSelectedList.clear();
                    sortRangeSelectedList.add(position);
                    sortNameList.add((String) sortParameterName.getText());
                }
                if (sortNameList.size() == 0) {
                    FilterLayout.tvSortFilterCount.setVisibility(View.INVISIBLE);
                } else {
                    FilterLayout.tvSortFilterCount.setText("1");
                }
                sortAdapter.notifyDataSetChanged();
            }
        });

        ((FilterLayout) getActivity()).setFragmentRefreshListener(new FilterLayout.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                sortAdapter.notifyDataSetChanged();
            }
        });

        return view;
    }

    public static class SortCustomListViewAdapter extends ArrayAdapter<Movie> {
        final Context context;
        private final List<Movie> items;
        RadioButton rb = null;

        public SortCustomListViewAdapter(Context context, List<Movie> items) {
            super(context, R.layout.discount_range_list_filter, items);
            this.context = context;
            this.items = items;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            Movie movie = getItem(position);
            convertView = null;
            LayoutInflater dInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = dInflater.inflate(R.layout.discount_range_list_filter, null);
            TextView discountRange = convertView.findViewById(R.id.radioButtonName);
            discountRange.setText(movie.getTitle());
            rb = convertView.findViewById(R.id.radioButtonDiscount);
            if (sortRangeSelectedList.contains(position)) {
                rb.setChecked(true);
            } else {
                rb.setChecked(false);
            }
            return convertView;
        }
    }
}
