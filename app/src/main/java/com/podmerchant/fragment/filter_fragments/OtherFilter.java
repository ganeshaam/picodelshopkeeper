package com.podmerchant.fragment.filter_fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.podmerchant.R;
import com.podmerchant.activites.FilterLayout;
import com.podmerchant.activites.ProductListActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class OtherFilter extends Fragment {

    private List<String> groupList;
    private List<String> childList;
    private Map<String, List<String>> laptopCollection = new LinkedHashMap<>();
    private ExpandableListView expListView;
    public static final ArrayList<String> cakeTypeList = new ArrayList<>();
    private String[] hpModels;
    private ExpandableListAdapterForOtherFilterWithCheckBox expListAdapter;
    private int previousItem = -1;
    private String groupName;
    public static String sizeTag = "";
    public static final ArrayList<String> categoryBrandDependentSizeNameList = new ArrayList<>();
    public static final ArrayList<String> brandDependentSizeNameList = new ArrayList<>();
    public static final ArrayList<String> categoryDependentSizeNameList = new ArrayList<>();
    public static final ArrayList<String> directNormalSizeNameList = new ArrayList<>();
    private static int categoryBrandCount = 0;
    private static int brandCount = 0;
    private static int categoryCount = 0;
    private static int directNormalCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_other_filter, container, false);
        expListView = view.findViewById(R.id.otherExpList);

        createOtherFilterTags();
        createGroupList();
        createCollection();

        expListAdapter = new ExpandableListAdapterForOtherFilterWithCheckBox(getActivity(), groupList, laptopCollection);
        expListView.setAdapter(expListAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                groupName = groupList.get(groupPosition);
                return false;
            }
        });

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem) {
                    expListView.collapseGroup(previousItem);
                }
                previousItem = groupPosition;
            }
        });

        /*--------------------------OtherFilterCount in on create start------------------------------*/
        switch (sizeTag) {
            case "bothCategoryAndBrand":
                if (categoryBrandCount == 0) {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                } else {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                    FilterLayout.tvOtherFilterCount.setText(String.valueOf(categoryBrandCount + cakeTypeList.size()));
                }
                break;
            case "onlyNormalBrand":
                if (brandCount == 0) {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                } else {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                    FilterLayout.tvOtherFilterCount.setText(String.valueOf(brandCount + cakeTypeList.size()));
                }
                break;
            case "onlyCategory":
                if (categoryCount == 0) {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                } else {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                    FilterLayout.tvOtherFilterCount.setText(String.valueOf(categoryCount + cakeTypeList.size()));
                }
                break;
            case "normal":
                if (directNormalCount == 0) {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                } else {
                    FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                    FilterLayout.tvOtherFilterCount.setText(String.valueOf(directNormalCount + cakeTypeList.size()));
                }
                break;
        }
        if (categoryBrandCount == 0 && brandCount == 0 && categoryCount == 0 && directNormalCount == 0) {
            if (cakeTypeList.size() > 0) {
                FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                FilterLayout.tvOtherFilterCount.setText(String.valueOf(cakeTypeList.size()));
            } else {
                FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
            }
        }
        /*--------------------------OtherFilterCount in on create end------------------------------*/
        return view;
    }

    private void createOtherFilterTags() {
        try {
            if ((DepartmentFilter.departmentNameList.size() > 0) && (BrandFilter.dependentBrandNameList.size() > 0)) {
                sizeTag = "bothCategoryAndBrand";
            } else if (BrandFilter.brandTag.equals("normalTag") && (BrandFilter.normalBrandNameList.size() > 0)) {
                sizeTag = "onlyNormalBrand";
            } else if (DepartmentFilter.departmentNameList.size() > 0) {
                sizeTag = "onlyCategory";
            } else if (DepartmentFilter.departmentNameList.size() == 0) {
                sizeTag = "normal";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createCollection() {
        try {
            if ((DepartmentFilter.departmentNameList.size() > 0) && (BrandFilter.dependentBrandNameList.size() > 0)) {
                hpModels = FilterLayout.categoryAndBrandDependentUniqueSizeList.toArray(new String[FilterLayout.categoryAndBrandDependentUniqueSizeList.size()]);
                categoryBrandCount = 0;
                for (String item : hpModels) {
                    if (categoryBrandDependentSizeNameList.contains(item)) {
                        categoryBrandCount++;
                    }
                }
            } else if (BrandFilter.brandTag.equals("normalTag") && (BrandFilter.normalBrandNameList.size() > 0)) {
                hpModels = FilterLayout.categoryAndBrandDependentUniqueSizeList.toArray(new String[FilterLayout.categoryAndBrandDependentUniqueSizeList.size()]);
                brandCount = 0;
                for (String item : hpModels) {
                    if (brandDependentSizeNameList.contains(item)) {
                        brandCount++;
                    }
                }
            } else if (DepartmentFilter.departmentNameList.size() > 0) {
                hpModels = FilterLayout.categoryDependentUniqueSizeList.toArray(new String[FilterLayout.categoryDependentUniqueSizeList.size()]);
                categoryCount = 0;
                for (String item : hpModels) {
                    if (categoryDependentSizeNameList.contains(item)) {
                        categoryCount++;
                    }
                }
            } else if (DepartmentFilter.departmentNameList.size() == 0) {
                hpModels = ProductListActivity.uniqueSizeList.toArray(new String[ProductListActivity.uniqueSizeList.size()]);
                directNormalCount = 0;
                for (String item : hpModels) {
                    if (directNormalSizeNameList.contains(item)) {
                        directNormalCount++;
                    }
                }
            }

            String[] cakeTypeModels = {"Cakes", "Eggless Cakes"};
            laptopCollection = new LinkedHashMap<>();

            for (String laptop : groupList) {
                if (laptop.equals("Size")) {
                    loadChild(hpModels);
                } else if (laptop.equals("Cake Type")) {
                    loadChild(cakeTypeModels);
                }
                laptopCollection.put(laptop, childList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadChild(String[] laptopModels) {
        childList = new ArrayList<>();
        if (laptopModels.length > 0) {
            Collections.addAll(childList, laptopModels);
        }
    }

    private void createGroupList() {
        groupList = new ArrayList<>();
        groupList.add("Size");

        if (DepartmentFilter.departmentNameList.size() > 0) {
            if (DepartmentFilter.departmentNameList.contains("Cakes & Pastries")) {
                groupList.add("Cake Type");
            } else {
                cakeTypeList.clear();
            }
        } else {
            if (ProductListActivity.catList.contains("Cakes & Pastries")) {
                groupList.add("Cake Type");
            } else {
            }
        }
    }

    private class ExpandableListAdapterForOtherFilterWithCheckBox extends BaseExpandableListAdapter {

        private final Activity mContext;
        private final Map<String, List<String>> mListDataChild;
        private final List<String> mListDataGroup;
        private ChildViewHolder childViewHolder;
        private GroupViewHolder groupViewHolder;
        private String groupText;
        private String childText;

        public ExpandableListAdapterForOtherFilterWithCheckBox(Activity context, List<String> listDataGroup, Map<String, List<String>> listDataChild) {
            mContext = context;
            mListDataGroup = listDataGroup;
            mListDataChild = listDataChild;
        }

        @Override
        public int getGroupCount() {
            return mListDataGroup.size();
        }

        @Override
        public String getGroup(int groupPosition) {
            return mListDataGroup.get(groupPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            groupText = getGroup(groupPosition);

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.parent_group, null);
                groupViewHolder = new GroupViewHolder();
                groupViewHolder.mGroupText = convertView.findViewById(R.id.txtParent);
                convertView.setTag(groupViewHolder);
            } else {
                groupViewHolder = (GroupViewHolder) convertView.getTag();
            }
            groupViewHolder.mGroupText.setText(groupText);
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mListDataChild.get(mListDataGroup.get(groupPosition)).size();
        }

        @Override
        public String getChild(int groupPosition, int childPosition) {
            return mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            childText = getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.expandable_child, null);
                childViewHolder = new ChildViewHolder();
                childViewHolder.mChildText = convertView.findViewById(R.id.txtChild);
                childViewHolder.mCheckBox = convertView.findViewById(R.id.checkBoxChild);
                convertView.setTag(R.layout.expandable_child, childViewHolder);
            } else {
                childViewHolder = (ChildViewHolder) convertView.getTag(R.layout.expandable_child);
            }
            childViewHolder.mChildText.setText(childText);
            childViewHolder.mCheckBox.setOnCheckedChangeListener(null);

            /*------------------child of expandable list view save logic start--------------------*/
            try {
                if (groupName.equals("Size")) {
                    switch (sizeTag) {
                        case "bothCategoryAndBrand":
                            if (categoryBrandDependentSizeNameList.contains(childText)) {
                                childViewHolder.mCheckBox.setChecked(true);
                            } else {
                                childViewHolder.mCheckBox.setChecked(false);
                            }
                            break;
                        case "onlyNormalBrand":
                            if (brandDependentSizeNameList.contains(childText)) {
                                childViewHolder.mCheckBox.setChecked(true);
                            } else {
                                childViewHolder.mCheckBox.setChecked(false);
                            }
                            break;
                        case "onlyCategory":
                            if (categoryDependentSizeNameList.contains(childText)) {
                                childViewHolder.mCheckBox.setChecked(true);
                            } else {
                                childViewHolder.mCheckBox.setChecked(false);
                            }
                            break;
                        case "normal":
                            if (directNormalSizeNameList.contains(childText)) {
                                childViewHolder.mCheckBox.setChecked(true);
                            } else {
                                childViewHolder.mCheckBox.setChecked(false);
                            }
                            break;
                    }
                } else if (groupName.equals("Cake Type")) {
                    if (cakeTypeList.contains(childText)) {
                        childViewHolder.mCheckBox.setChecked(true);
                    } else {
                        childViewHolder.mCheckBox.setChecked(false);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*------------------child of expandable list view save logic end----------------------*/

            childViewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    String selectedChild = laptopCollection.get(groupList.get(groupPosition)).get(childPosition);

                    if (isChecked) {
                        /*------------add data for saving in various collections in if block-----------*/
                        try {
                            if (groupName.equals("Size")) {
                                switch (sizeTag) {
                                    case "bothCategoryAndBrand":
                                        categoryBrandDependentSizeNameList.add(selectedChild);
                                        categoryBrandCount++;
                                        break;
                                    case "onlyNormalBrand":
                                        brandDependentSizeNameList.add(selectedChild);
                                        brandCount++;
                                        break;
                                    case "onlyCategory":
                                        categoryDependentSizeNameList.add(selectedChild);
                                        categoryCount++;
                                        break;
                                    case "normal":
                                        directNormalSizeNameList.add(selectedChild);
                                        directNormalCount++;
                                        break;
                                }
                            } else if (groupName.equals("Cake Type")) {
                                cakeTypeList.add(selectedChild);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        /*----------remove data for saving in various collections in else block---------*/
                        try {
                            if (groupName.equals("Size")) {
                                switch (sizeTag) {
                                    case "bothCategoryAndBrand":
                                        categoryBrandDependentSizeNameList.remove(selectedChild);
                                        categoryBrandCount--;
                                        break;
                                    case "onlyNormalBrand":
                                        brandDependentSizeNameList.remove(selectedChild);
                                        brandCount--;
                                        break;
                                    case "onlyCategory":
                                        categoryDependentSizeNameList.remove(selectedChild);
                                        categoryCount--;
                                        break;
                                    case "normal":
                                        directNormalSizeNameList.remove(selectedChild);
                                        directNormalCount--;
                                        break;
                                }
                            } else if (groupName.equals("Cake Type")) {
                                cakeTypeList.remove(selectedChild);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    /*----------------------OtherFilterCount logic start---------------------------*/
                    switch (sizeTag) {
                        case "bothCategoryAndBrand":
                            if (categoryBrandCount != 0) {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                                FilterLayout.tvOtherFilterCount.setText(String.valueOf(categoryBrandCount + cakeTypeList.size()));
                            } else {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case "onlyNormalBrand":
                            if (brandCount != 0) {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                                FilterLayout.tvOtherFilterCount.setText(String.valueOf(brandCount + cakeTypeList.size()));
                            } else {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case "onlyCategory":
                            if (categoryCount != 0) {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                                FilterLayout.tvOtherFilterCount.setText(String.valueOf(categoryCount + cakeTypeList.size()));
                            } else {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case "normal":
                            if (directNormalCount != 0) {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                                FilterLayout.tvOtherFilterCount.setText(String.valueOf(directNormalCount + cakeTypeList.size()));
                            } else {
                                FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                            }
                            break;
                    }

                    if (categoryBrandCount == 0 && brandCount == 0 && categoryCount == 0 && directNormalCount == 0) {
                        if (cakeTypeList.size() > 0) {
                            FilterLayout.tvOtherFilterCount.setVisibility(View.VISIBLE);
                            FilterLayout.tvOtherFilterCount.setText(String.valueOf(cakeTypeList.size()));
                        } else {
                            FilterLayout.tvOtherFilterCount.setVisibility(View.INVISIBLE);
                        }
                    }
                    /*----------------------OtherFilterCount logic end-----------------------------*/
                }
            });

            /*---------notify other filter adapter on click of clear button other filter-----------*/
            try {
                ((FilterLayout) getActivity()).setFragmentRefreshListener(new FilterLayout.FragmentRefreshListener() {
                    @Override
                    public void onRefresh() {
                        expListAdapter.notifyDataSetChanged();
                    }
                });
            } catch (Exception e) {
                Toast.makeText(getActivity(), "There are no data to clear", Toast.LENGTH_LONG).show();
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        public final class GroupViewHolder {
            TextView mGroupText;
        }

        public final class ChildViewHolder {
            TextView mChildText;
            CheckBox mCheckBox;
        }
    }

}
