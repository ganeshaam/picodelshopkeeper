package com.podmerchant.fragment.filter_fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


import com.podmerchant.R;
import com.podmerchant.activites.FilterLayout;
import com.podmerchant.activites.ProductListActivity;
import com.podmerchant.model.Movie;

/**
 * A simple {@link Fragment} subclass.
 */
public class SellerFilter extends Fragment {

    public static final ArrayList<Object> sellerSelectedList = new ArrayList<>();
    public static final ArrayList<String> sellerNameList = new ArrayList<>();
    private ListView listview;
    private SellerCustomListViewAdapter sellerAdapter;
    private CheckBox sellerCheckBox;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_seller_filter, container, false);
        listview = v.findViewById(R.id.sellerList);
        listview.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        setDataToSellerList();
        ((FilterLayout) getActivity()).setFragmentRefreshListener(new FilterLayout.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                sellerAdapter.notifyDataSetChanged();
            }
        });
        return v;
    }

    private void setDataToSellerList() {
        if (ProductListActivity.uniqueShopNames.size() > 0) {
            String[] sellerNames = ProductListActivity.uniqueShopNames.toArray(new String[ProductListActivity.uniqueShopNames.size()]);

            //for seller
            List<Movie> sellerMovies = new ArrayList<>();
            for (String sellerName : sellerNames) {
                Movie priceItem = new Movie(sellerName);
                sellerMovies.add(priceItem);
            }

            //for seller Listview
            sellerAdapter = new SellerCustomListViewAdapter(getActivity(), sellerMovies);
            listview.setAdapter(sellerAdapter);
        } else {
            Toast.makeText(getActivity(), "Connection seems to be slow, please check internet connection", Toast.LENGTH_LONG).show();
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterLayout.tvSellerFilterCount.setVisibility(View.VISIBLE);
                sellerCheckBox = view.findViewById(R.id.checkBox1);
                TextView sellerName = view.findViewById(R.id.checkBoxName);
                if (sellerCheckBox.isChecked()) {
                    sellerSelectedList.remove((Object) position);
                    sellerNameList.remove(sellerName.getText());
                    sellerCheckBox.setChecked(false);
                } else {
                    sellerSelectedList.add(position);
                    sellerNameList.add((String) sellerName.getText());
                    sellerCheckBox.setChecked(true);
                }
                if (sellerSelectedList.size() == 0) {
                    FilterLayout.tvSellerFilterCount.setVisibility(View.INVISIBLE);
                }
                FilterLayout.tvSellerFilterCount.setText("" + (SellerFilter.sellerSelectedList.size()));
            }
        });

    }

    private class SellerCustomListViewAdapter extends ArrayAdapter<Movie> {
        final Context context;
        private final List<Movie> items;
        private final int stepCount;


        public SellerCustomListViewAdapter(Context context, List<Movie> items) {
            super(context, R.layout.price_range_list_filter, items);
            this.context = context;
            this.items = items;
            this.stepCount = 0;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            Movie movie = getItem(position);
            if (stepCount == 0) {
                convertView = null;
                LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(R.layout.price_range_list_filter, null);
                CheckBox cbprice = convertView.findViewById(R.id.checkBox1);
                TextView priceRange = convertView.findViewById(R.id.checkBoxName);
                priceRange.setText(movie.getTitle());
                if (sellerSelectedList.contains(position))
                    cbprice.setChecked(true);
                else
                    cbprice.setChecked(false);
            }
            return convertView;
        }
    }

}
