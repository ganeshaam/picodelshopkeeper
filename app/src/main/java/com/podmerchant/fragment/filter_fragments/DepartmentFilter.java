package com.podmerchant.fragment.filter_fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.podmerchant.R;
import com.podmerchant.activites.FilterLayout;
import com.podmerchant.activites.ProductListActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 */
public class DepartmentFilter extends Fragment {

    public static final HashMap<Integer, boolean[]> mChildCheckStates = new HashMap<>();
    public static final ArrayList<String> departmentNameList = new ArrayList<>();

    private ExpandableListView expListView;
    private ExpListViewAdapterWithCheckbox expListAdapter;
    private List<String> groupList;
    private final Map<String, List<String>> laptopCollection = new LinkedHashMap<>();
    private int previousItem = -1;
    private boolean getChecked[];

    public DepartmentFilter() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__department_filter, container, false);
        expListView = v.findViewById(R.id.departmentExpList);

        createGroupList();
        createCollection();
        expListAdapter = new ExpListViewAdapterWithCheckbox(getActivity(), groupList, laptopCollection);
        expListView.setAdapter(expListAdapter);

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem) {
                    expListView.collapseGroup(previousItem);
                }
                previousItem = groupPosition;
            }
        });

        /*--------------------------DepartmentFilterCount in on create------------------------------*/
        if (departmentNameList.size() != 0) {
            FilterLayout.tvDepartmentFilterCount.setVisibility(View.VISIBLE);
        } else {
            FilterLayout.tvDepartmentFilterCount.setVisibility(View.INVISIBLE);
        }
        return v;
    }

    private void createCollection() {
        Set set = ProductListActivity.mainCat_CatHashMap.keySet();
        ArrayList<String> tpArrayList = new ArrayList<>();
        for (String temp : ProductListActivity.productMainCategory) {
            if (set.contains(temp)) {
                tpArrayList = (ArrayList<String>) ProductListActivity.mainCat_CatHashMap.get(temp);
                for (int a = 0; a < tpArrayList.size(); a++) {
                    String orgStr = tpArrayList.get(a);
                    if (orgStr.contains("#")) {
                        tpArrayList.remove(orgStr);
                        String str1 = orgStr.replace("#", ",");
                        tpArrayList.add(str1);
                    } else {

                    }
                }
            }
            Collections.sort(tpArrayList);
            laptopCollection.put(temp, tpArrayList);
        }
    }

    private void createGroupList() {
        try {
            groupList = new ArrayList<>();
            groupList.addAll(ProductListActivity.uniqueMainCategory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ExpListViewAdapterWithCheckbox extends BaseExpandableListAdapter {

        final Activity mContext;
        final Map<String, List<String>> mListDataChild;
        final List<String> mListDataGroup;
        ChildViewHolder childViewHolder;
        GroupViewHolder groupViewHolder;
        String groupText, childText;

        public ExpListViewAdapterWithCheckbox(Activity context, List<String> listDataGroup, Map<String, List<String>> listDataChild) {
            mContext = context;
            mListDataGroup = listDataGroup;
            mListDataChild = listDataChild;
        }

        @Override
        public int getGroupCount() {
            return mListDataGroup.size();
        }

        @Override
        public String getGroup(int groupPosition) {
            return mListDataGroup.get(groupPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }


        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            groupText = getGroup(groupPosition);

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.parent_group, null);
                groupViewHolder = new GroupViewHolder();
                groupViewHolder.mGroupText = convertView.findViewById(R.id.txtParent);
                convertView.setTag(groupViewHolder);
            } else {
                groupViewHolder = (GroupViewHolder) convertView.getTag();
            }
            groupViewHolder.mGroupText.setText(groupText);
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mListDataChild.get(mListDataGroup.get(groupPosition)).size();
        }

        @Override
        public String getChild(int groupPosition, int childPosition) {
            return mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            childText = getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.expandable_child, null);
                childViewHolder = new ChildViewHolder();
                childViewHolder.mChildText = convertView.findViewById(R.id.txtChild);
                childViewHolder.mCheckBox = convertView.findViewById(R.id.checkBoxChild);
                convertView.setTag(R.layout.expandable_child, childViewHolder);
            } else {
                childViewHolder = (ChildViewHolder) convertView.getTag(R.layout.expandable_child);
            }
            childViewHolder.mChildText.setText(childText);
            childViewHolder.mCheckBox.setOnCheckedChangeListener(null);

            /*------------------child of expandable list view save logic start--------------------*/
            try {
                if (mChildCheckStates.containsKey(groupPosition)) {
                    getChecked = mChildCheckStates.get(groupPosition);
                    childViewHolder.mCheckBox.setChecked(getChecked[childPosition]);
                } else {
                    getChecked = new boolean[getChildrenCount(groupPosition)];
                    mChildCheckStates.put(groupPosition, getChecked);
                    childViewHolder.mCheckBox.setChecked(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*------------------child of expandable list view save logic end----------------------*/


            childViewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    String selectedChild = laptopCollection.get(groupList.get(groupPosition)).get(childPosition);
                    String selectedGroup = groupList.get(groupPosition);
                    String finalName = selectedChild + "@@@" + selectedGroup;
                    if (isChecked) {
                        /*------------add data for saving in various collections in if block-----------*/
                        departmentNameList.add(finalName);
                        getChecked = mChildCheckStates.get(groupPosition);
                        getChecked[childPosition] = true;
                        mChildCheckStates.put(groupPosition, getChecked);
                    } else {
                        /*----------remove data for saving in various collections in else block---------*/
                        departmentNameList.remove(finalName);
                        getChecked = mChildCheckStates.get(groupPosition);
                        getChecked[childPosition] = false;
                        mChildCheckStates.put(groupPosition, getChecked);
                    }

                    /*----------------------DepartmentFilterCount logic start-----------------------*/
                    if (departmentNameList.size() != 0) {
                        FilterLayout.tvDepartmentFilterCount.setVisibility(View.VISIBLE);
                        FilterLayout.tvDepartmentFilterCount.setText("" + (departmentNameList.size()));
                    } else {
                        FilterLayout.tvDepartmentFilterCount.setVisibility(View.INVISIBLE);
                    }
                    /*----------------------DepartmentFilterCount logic end-------------------------*/
                }
            });

            /*---------notify department filter adapter on click of clear button department filter-----------*/
            try {
                ((FilterLayout) getActivity()).setFragmentRefreshListener(new FilterLayout.FragmentRefreshListener() {
                    public void onRefresh() {
                        expListAdapter.notifyDataSetChanged();
                    }
                });
            } catch (Exception e) {
                Toast.makeText(getActivity(), "There are no data to clear", Toast.LENGTH_LONG).show();
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        public final class GroupViewHolder {
            TextView mGroupText;
        }

        public final class ChildViewHolder {
            TextView mChildText;
            CheckBox mCheckBox;
        }
    }
}
