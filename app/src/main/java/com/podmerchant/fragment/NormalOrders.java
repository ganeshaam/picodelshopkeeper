package com.podmerchant.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.podmerchant.R;
import com.podmerchant.activites.MyOrder;
import com.podmerchant.activites.MyOrderDetails;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;


public class NormalOrders extends Fragment {

    private String cancel;
    private TextView tvRatingValue, tvRatingMessage;
    private EditText etOrderNo, etRemark;
    private AlertDialog alertDialog;
    private Spinner cancelSpinner;
    private RecyclerView recyclerView;
    private CustomAdapter customAdapter;
    private RelativeLayout relativeLayoutEmptyOrders;
    private final ArrayList<String> cancelSpinnerList = new ArrayList<>();
    private CoordinatorLayout MyOrderMainLayout;
    private final String class_name = this.getClass().getSimpleName();
    private LinkedHashSet<GateWay.ReviewPOJO> review = new LinkedHashSet<>();
    private BroadcastReceiver myReceiver;
    private MyOrder homePageActivity;

    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                relativeLayoutEmptyOrders.setVisibility(View.GONE);
                ((MyOrder) getActivity()).Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                MyOrder.txtNoConnection.setText("No connection");
                MyOrder.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(MyOrder.txtNoConnection);
            } else {
                ((MyOrder) getActivity()).Main_Layout_NoInternet.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                MyOrder.txtNoConnection.setText("Back online");
                MyOrder.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(MyOrder.txtNoConnection);

                getCancelSpinnerDetails();
                getManageOrderResults();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            Activity activity = getActivity();
            if (isAdded() && activity != null) {
                dialog(status);
            }
        }
    }

    private void initializeViews() {
        homePageActivity = (MyOrder) getActivity();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MyOrder) {
            homePageActivity = (MyOrder) context;
        }
    }

    public NormalOrders() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_orders, container, false);


        mContext = getActivity();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        MyOrderMainLayout = view.findViewById(R.id.MyOrderMainLayout);
        relativeLayoutEmptyOrders = view.findViewById(R.id.relativeLayoutEmptyOrders);
        recyclerView = view.findViewById(R.id.orders_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        initializeViews();

        myReceiver = new Network_Change_Receiver();



        return view;
    }

    private void getCancelSpinnerDetails() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, StaticUrl.urlCancelOrderSpinnerData, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray cancelSpinnerArray = response.getJSONArray("posts");
                        for (int i = 0; i < cancelSpinnerArray.length(); i++) {
                            JSONObject jSonCancelSpinnerData = cancelSpinnerArray.getJSONObject(i);
                            cancelSpinnerList.add(jSonCancelSpinnerData.getString("cancel"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private void getManageOrderResults() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            customAdapter = new CustomAdapter();

            //String strContact = gateWay.getContact();
            String strContact = sharedPreferencesUtils.getPhoneNumber();

            JSONObject params = new JSONObject();
            try {
                params.put("contact_no", strContact);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlMyOrderForUser, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            relativeLayoutEmptyOrders.setVisibility(View.VISIBLE);
                        } else {
                            relativeLayoutEmptyOrders.setVisibility(View.GONE);
                            Log.e("MyOrderResponse:",""+response);
                            JSONArray mainOrderArray = response.getJSONArray("posts");

                            for (int i = 0; i < mainOrderArray.length(); i++) {
                                JSONObject jSonMyOrderData = mainOrderArray.getJSONObject(i);

                                InnerMovie innerMovie = new InnerMovie(jSonMyOrderData.getString("order_id"), jSonMyOrderData.getString("payment_method"),
                                        jSonMyOrderData.getString("status"), jSonMyOrderData.getString("total_amount"),
                                        jSonMyOrderData.getString("discount"), jSonMyOrderData.getString("delivery_charges"),
                                        jSonMyOrderData.getString("total_items"), jSonMyOrderData.getString("order_date"),
                                        jSonMyOrderData.getString("email"), jSonMyOrderData.getString("contactNo"),
                                        jSonMyOrderData.getString("delivery_date"), jSonMyOrderData.getString("delivery_time"),
                                        jSonMyOrderData.getString("remark"), jSonMyOrderData.getString("rating"));
                                customAdapter.add(innerMovie);
                            }
                            recyclerView.setAdapter(customAdapter);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private class CustomAdapter extends RecyclerView.Adapter<ViewHolder> {

        final ArrayList<InnerMovie> mItems = new ArrayList<>();

        CustomAdapter() {
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_my_orders, parent, false);
            final ViewHolder viewHolder = new ViewHolder(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on view & order id is: " + viewHolder.tvOrderId.getText().toString(), getActivity());*/

                    Intent intent = new Intent(getActivity(), MyOrderDetails.class);
                    intent.putExtra("order_id", viewHolder.tvOrderId.getText().toString());
                    startActivity(intent);
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            InnerMovie movie = mItems.get(position);
            try {
                holder.tvOrderStatus.setText(movie.getStatus());
                holder.tvOrderId.setText(movie.getOrder_id());
                holder.tvPaymentType.setText(movie.getPayment_method());
                holder.tvTotalItems.setText(movie.getTotal_items());
                holder.tvGrandTotal.setText(movie.getTotal_amount());
                holder.tvDiscount.setText(movie.getDiscount());
                holder.tvContact.setText(movie.getContactNo());
                holder.tvEmail.setText(movie.getEmail());
                String status = (String) holder.tvOrderStatus.getText();
                if (status.equals("Pending")) {
                    if (movie.getRemark().equals("") && movie.getUser_rating().equals("")) {
                        holder.feedbackImg.setVisibility(View.VISIBLE);
                    } else {
                        holder.feedbackImg.setVisibility(View.GONE);
                    }
                    holder.tvOrderPlace.setText("Placed On:");
                    holder.tvOrderDate.setText(movie.getOrder_date());
                    holder.tvCancel.setVisibility(View.VISIBLE);
                    holder.tvOrderStatus.setTextColor(Color.parseColor("#ffab00"));
                } else {
                    holder.tvCancel.setVisibility(View.GONE);
                }
                if (status.equals("Reject by User")) {
                    if (movie.getRemark().equals("") && movie.getUser_rating().equals("")) {
                        holder.feedbackImg.setVisibility(View.VISIBLE);
                    } else {
                        holder.feedbackImg.setVisibility(View.GONE);
                    }
                    holder.tvOrderPlace.setText("Cancelled On:");
                    holder.tvOrderDate.setText(movie.getOrder_date());
                    holder.tvOrderStatus.setTextColor(Color.parseColor("#F44336"));
                }
                if (status.equals("Delivered")) {
                    if (movie.getRemark().equals("") && movie.getUser_rating().equals("")) {
                        holder.feedbackImg.setVisibility(View.VISIBLE);
                    } else {
                        holder.feedbackImg.setVisibility(View.GONE);
                    }
                    holder.tvOrderPlace.setText("Placed On:");
                    holder.tvOrderDate.setText(movie.getOrder_date());
                    holder.tvOrderStatus.setTextColor(Color.parseColor("#4caf50"));
                }
                holder.tvDeliveryDateTime.setText(movie.getDelivery_date() + " / " + movie.getDelivery_time());
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.feedbackImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on feedback button & order id is: " + holder.tvOrderId.getText().toString(), getActivity());*/

                    feedBackAlertDialog(holder.tvOrderId.getText().toString());
                }
            });

            holder.tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on cancel button & order id is: " + holder.tvOrderId.getText().toString(), getActivity());*/

                    cancelOrderAlertDialog(holder.tvOrderId.getText().toString(), holder.tvOrderStatus.getText().toString());
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(InnerMovie movie) {
            mItems.add(movie);
        }

    }

    private void cancelOrderAlertDialog(final String cancelOrderId, final String cancelOrderStatus) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View cancelView = layoutInflater.inflate(R.layout.dialog_cancel_order_of_my_order, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(cancelView);
        cancelSpinner = cancelView.findViewById(R.id.cancelSpinner);
        try {
            if (cancelSpinnerList.size() > 0) {
                ArrayAdapter<String> staticAdapter = new ArrayAdapter<>(getActivity(), R.layout.multi_line_spinner, cancelSpinnerList);
                staticAdapter.setDropDownViewResource(R.layout.multi_line_spinner);
                cancelSpinner.setAdapter(staticAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        builder.setTitle("Cancel Order");
        builder
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        submitCancelOrderReason(cancelOrderId, cancelOrderStatus);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();

        final Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setVisibility(View.GONE);

        cancelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cancel = (String) cancelSpinner.getSelectedItem();
                if (!cancel.equals("Reason for cancellation?")) {
                    positiveButton.setVisibility(View.VISIBLE);
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void submitCancelOrderReason(String cancelOrderId, String cancelOrderStatus) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            JSONObject params = new JSONObject();
            try {
                params.put("order_id", cancelOrderId);
                params.put("order_status", cancelOrderStatus);
                params.put("order_cancel_reason", cancel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCancelOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String result = response.getString("posts");
                        if (result.equals("true")) {
                            Toast.makeText(getActivity(), "Successfully cancel order", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getActivity(), MyOrder.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(MyOrderMainLayout);
        }
    }

    private void feedBackAlertDialog(String methodOrderId) {
        GateWay gateWay = new GateWay(getActivity());
        review = gateWay.fetchReviewData();

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View order_confirmView = layoutInflater.inflate(R.layout.dialog_feedback_form_of_my_order, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(order_confirmView);
        etOrderNo = order_confirmView.findViewById(R.id.txtOrder);
        etRemark = order_confirmView.findViewById(R.id.editOrderRemark);
        RatingBar ratingBar = order_confirmView.findViewById(R.id.ratingBar);
        tvRatingValue = order_confirmView.findViewById(R.id.txtRatingValue);
        tvRatingMessage = order_confirmView.findViewById(R.id.txtRatingMessage);

        etOrderNo.setText(methodOrderId);
        alertDialogBuilder
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                tvRatingValue.setText(String.valueOf(rating));
                for (GateWay.ReviewPOJO reviewPOJO : review) {
                    if (reviewPOJO.getRating().equals(String.valueOf(rating))) {
                        if (reviewPOJO.getReason().equals("")) {
                            tvRatingMessage.setVisibility(View.INVISIBLE);
                            tvRatingMessage.setText("");
                        } else {
                            tvRatingMessage.setVisibility(View.VISIBLE);
                            tvRatingMessage.setText(reviewPOJO.getReason());
                            tvRatingMessage.setTextColor(ContextCompat.getColor(getActivity(), reviewPOJO.getColor()));
                        }
                    }
                }
            }
        });
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(Color.parseColor("#3F51B5"));

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String remark = etRemark.getText().toString();
                if (remark.matches("")) {
                    Toast.makeText(getActivity(), "Please give feedback/suggestions to improve our services.", Toast.LENGTH_LONG).show();
                } else {
                    alertDialog.dismiss();
                      //      submitOrderRemark();

                    String order_id = etOrderNo.getText().toString();
                    String strRating = tvRatingValue.getText().toString();
                    String strRemark = etRemark.getText().toString();

                    Log.e("FeedbackParams:",""+order_id+":"+strRating+":"+remark);

                }
            }
        });
    }

    private void submitOrderRemark() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            String order_id = etOrderNo.getText().toString();
            String strRating = tvRatingValue.getText().toString();
            String strRemark = etRemark.getText().toString();

            JSONObject params = new JSONObject();
            try {
                params.put("order_id", order_id);
                params.put("strRating", strRating);
                params.put("strRemark", strRemark);
                Log.e("OrderRemark:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlOrderRemark, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String result = response.getString("posts");
                        if (result.equals("true")) {
                            Toast.makeText(getActivity(), "Successfully submit feedback.", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getActivity(), MyOrder.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "Please feedback again.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(MyOrderMainLayout);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvOrderDate;
        final TextView tvOrderId;
        final TextView tvOrderStatus;
        final TextView tvPaymentType;
        final TextView tvTotalItems;
        final TextView tvGrandTotal;
        final TextView tvDiscount;
        final TextView tvEmail;
        final TextView tvContact;
        final TextView tvCancel;
        final TextView tvOrderPlace;
        final TextView tvDeliveryDateTime;
        final ImageView feedbackImg;

        public ViewHolder(View itemView) {
            super(itemView);
            tvOrderDate = itemView.findViewById(R.id.txtOrderDate);
            tvOrderId = itemView.findViewById(R.id.txtOrderId);
            tvOrderStatus = itemView.findViewById(R.id.txtStatus);
            tvPaymentType = itemView.findViewById(R.id.txtPaymentType);
            tvTotalItems = itemView.findViewById(R.id.txtTotalItems);
            tvGrandTotal = itemView.findViewById(R.id.txtGrandTotal);
            tvDiscount = itemView.findViewById(R.id.txtSavings);
            tvEmail = itemView.findViewById(R.id.txtEmail);
            tvContact = itemView.findViewById(R.id.txtContactNo);
            tvCancel = itemView.findViewById(R.id.txtCancel);
            tvDeliveryDateTime = itemView.findViewById(R.id.txtDeliveryDateTime);
            tvOrderPlace = itemView.findViewById(R.id.txtOrderPlace);
            feedbackImg = itemView.findViewById(R.id.feedBackImg);
            feedbackImg.setTag(this);
            tvCancel.setTag(this);
            tvOrderPlace.setTag(this);
            tvOrderDate.setTag(this);
        }
    }

    private class InnerMovie {

        final String order_id;
        final String payment_method;
        final String status;
        final String total_amount;
        final String discount;
        final String delivery_charges;
        final String total_items;
        final String order_date;
        final String email;
        final String contactNo;
        final String delivery_date;
        final String delivery_time;
        final String remark;
        final String user_rating;

        InnerMovie(String order_id, String payment_method, String status, String total_amount, String discount,
                   String delivery_charges, String total_items, String order_date, String email, String contactNo,
                   String delivery_date, String delivery_time, String remark, String user_rating) {
            this.order_id = order_id;
            this.payment_method = payment_method;
            this.status = status;
            this.total_amount = total_amount;
            this.discount = discount;
            this.delivery_charges = delivery_charges;
            this.total_items = total_items;
            this.order_date = order_date;
            this.email = email;
            this.contactNo = contactNo;
            this.delivery_date = delivery_date;
            this.delivery_time = delivery_time;
            this.remark = remark;
            this.user_rating = user_rating;
        }

        public String getOrder_id() {
            return order_id;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public String getStatus() {
            return status;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public String getDiscount() {
            return discount;
        }

        public String getTotal_items() {
            return total_items;
        }

        public String getOrder_date() {
            return order_date;
        }

        public String getEmail() {
            return email;
        }

        public String getContactNo() {
            return contactNo;
        }

        public String getDelivery_date() {
            return delivery_date;
        }

        public String getDelivery_time() {
            return delivery_time;
        }

        public String getRemark() {
            return remark;
        }

        public String getUser_rating() {
            return user_rating;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

    }
}
