package com.podmerchant.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.podmerchant.R;
import com.podmerchant.activites.ChangeAddressActivity;
import com.podmerchant.activites.ContinueShopingActivity;
import com.podmerchant.activites.HomeActivity;
import com.podmerchant.activites.Order_Details_StepperActivity;
import com.podmerchant.local_storage.DBHelper;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

//ContinueShopingActivity;
//DeliveryAddress;
//import app.apneareamein.shopping.sync.UserTracking;

import dmax.dialog.SpotsDialog;

import static android.app.Activity.RESULT_OK;

//import app.apneareamein.shopping.utils.SharedPreferencesUtils;

public class Order_Details extends Fragment implements View.OnClickListener, Order_Details_StepperActivity.CheckBoxStateCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener{

    private final String class_name = this.getClass().getSimpleName();
    private DialogPlus dialog, dialogPlus;
    private RecyclerView coupon_recycler_view, viewcart_items_recycler_view;
    private Bundle bundle;
    private CardAdapter dialog_cardAdapter;
    private CouponsAdapter couponsAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private List<String> childArrayList;
    private final HashMap keyValueCategoryMap = new HashMap();
    private final Map<String, List<String>> childCollection = new LinkedHashMap<>();
    private ArrayList<String> mainScheduledDateValues = new ArrayList<>();
    private ArrayList<String> mainScheduledTimeValues = new ArrayList<>();
    private final List<Normal_Cart_Item> mItems = new ArrayList<>();
      ArrayList<UserInfo> arr_user_info;// = new ArrayList<>();
    private AlertDialog alertDialog;

    private String strEmail, strContact, cart_count, finalPrice,userid,fname,lname,mobileno;
    private String strAddress_Id,strAddress_addresId,address1,address2,address3,address4,address5,address,area;;
    private String final_price;
    private String strDynamicPriceMsg;
    private String strResponse;
    private String strInitialTotalAmount = null;
    private String strShippingAmount = null;
    private String strSavingAmount = null;
    private String strFinalAmount = null;
    private EditText etSpecialRemark;
    private double double_coupon_min_amt, double_final_amt;
    private String strCouponSavingAmt;
    private String coupon_code="";
    private String cashback_points;
    private String strNotifyMessage;
    private TextView tvPrice;
    private TextView tvDiscount;
    private TextView tvShippingCharge;
    private TextView txtpaymnentmsg;
    private TextView tvTotal;
    private TextView tvCouponDiscount;
    private TextView tvDiscountAmt;
    private TextView tvCouponMessage;
    private TextView tvAddress;
    //private TextView txtDefault;
    private TextView tvUserName;
    private TextView tvUserContact;
    private TextView tvItems;
    private TextView tvDialogTitle;
    private TextView tvPriceCond;
    private LinearLayout priceLinearLayout,coordinatorLayout;
    //private RelativeLayout coordinatorLayout;
    private CheckBox chkCashBackTerms;
    private String strName;
    private boolean chkStatus;
    private String SelectedDeliveryMethod;
    private String paymentType = "";
    private Spinner date_spinner;
    private Spinner coupondate_spinner;
    private Spinner time_spinner;
    private Spinner coupontime_spinner;
    private String scheduleSelectedDate;
    private String scheduleSelectedTime;
    private EditText editCouponCode;
    private EditText editTRN;
    private EditText edittrnAmt;
    private String couponCode;
    LinearLayout TRNLayout;
    LinearLayout datenTimeLayout;
    //String URL = "http://192.168.43.196/";
    private LinearLayout deliveryfeesLayout;
    private LinearLayout applyCouponLayout;
    LinearLayout delivery_optionsDisplayLayout;
    LinearLayout couponLayout;
    int position=0;
    String  latitude,longitude;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private RelativeLayout relativeLayoutEmptyCart;
    private LocationManager locationManager;
    Button btnShopNow;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        sharedPreferencesUtils = new SharedPreferencesUtils(getActivity());

        coordinatorLayout = view.findViewById(R.id.coordinatorLayout);
        Button tvChangeAddress = view.findViewById(R.id.txtChangeAddress);
        tvAddress = view.findViewById(R.id.txtAddress);
        //txtDefault = view.findViewById(R.id.txtDefault);
        tvUserName = view.findViewById(R.id.txtUserName);
        tvUserContact = view.findViewById(R.id.txtContact);
        tvItems = view.findViewById(R.id.txtItems);
        etSpecialRemark = view.findViewById(R.id.editSpecialRemark);
        tvPrice = view.findViewById(R.id.txtPrice);
        tvDiscount = view.findViewById(R.id.txtDiscount);
        tvShippingCharge = view.findViewById(R.id.txtShippingCharge);
        txtpaymnentmsg = view.findViewById(R.id.txtpaymnentmsg);
        tvTotal = view.findViewById(R.id.txtTotal);
        tvDiscountAmt = view.findViewById(R.id.txtDiscountAmt);
        tvCouponDiscount = view.findViewById(R.id.txtCouponDiscount);
        editCouponCode = view.findViewById(R.id.editCouponCode);
        editTRN = view.findViewById(R.id.editTRN);
        edittrnAmt = view.findViewById(R.id.edittrnAmt);
        Button btnAddCouponCode = view.findViewById(R.id.btnAddCouponCode);
        Button btnSkipCouponCode = view.findViewById(R.id.btnSkipCouponCode);
        Button btnAddTRN = view.findViewById(R.id.btnAddTRN);
        Button btnPlaceOrder = view.findViewById(R.id.btnPlaceOrder);
        expListView = view.findViewById(R.id.lvExp);

        date_spinner = view.findViewById(R.id.date_spinner);
        coupondate_spinner = view.findViewById(R.id.coupondate_spinner);
        time_spinner = view.findViewById(R.id.time_spinner);
        coupontime_spinner = view.findViewById(R.id.coupontime_spinner);
        relativeLayoutEmptyCart = view.findViewById(R.id.relativeLayoutEmptyCart);
        /* btnShopNow = view.findViewById(R.id.btnShopNow);*/

        LinearLayout addressDisplayLayout = view.findViewById(R.id.addressDisplayLayout);
        deliveryfeesLayout = view.findViewById(R.id.deliveryfeesLayout);
        applyCouponLayout = view.findViewById(R.id.applyCouponLayout);
        LinearLayout itemsDisplayLayout = view.findViewById(R.id.itemsDisplayLayout);
        delivery_optionsDisplayLayout = view.findViewById(R.id.delivery_optionsDisplayLayout);
        TRNLayout = view.findViewById(R.id.TRNLayout);
        datenTimeLayout = view.findViewById(R.id.datenTimeLayout);
        couponLayout = view.findViewById(R.id.couponLayout);
        priceLinearLayout = view.findViewById(R.id.priceLinearLayout);
        LinearLayout finalDisplayLayout = view.findViewById(R.id.FinalDisplayLayout);
        //coordinatorLayout_next = view.findViewById(R.id.coordinatorLayout_next);

        tvChangeAddress.setOnClickListener(this);
        tvItems.setOnClickListener(this);
        btnPlaceOrder.setOnClickListener(this);
        btnAddCouponCode.setOnClickListener(this);
        btnSkipCouponCode.setOnClickListener(this);
        btnAddTRN.setOnClickListener(this);
        txtpaymnentmsg.setOnClickListener(this);
        tvPriceCond = view.findViewById(R.id.txtPriceCond);
        final CheckBox chkAccept = view.findViewById(R.id.chkAcceptTerms);


        chkAccept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chkStatus = b;
            }
        });

        GateWay gateWay = new GateWay(getActivity());
        strEmail = sharedPreferencesUtils.getEmailId();//gateWay.getUserEmail();
        strContact = sharedPreferencesUtils.getPhoneNumber();//gateWay.getContact();
        strName = sharedPreferencesUtils.getUserName();//gateWay.getUserName();

        //here we get all values from bundle
        bundle = getArguments();
        finalPrice = bundle.getString("finalPrice");

        userid = bundle.getString("userid");
        fname = bundle.getString("fname");
        lname = bundle.getString("lname");
        mobileno = bundle.getString("mobileno");


        //strAddress_Id = sharedPreferencesUtils.getCategory();
        strAddress_Id = bundle.getString("address_id");
        String strTag = bundle.getString("tag");

        switch (strTag) {
            case "address_view":
                addressDisplayLayout.setVisibility(View.VISIBLE);
                break;
            case "item_view":
                itemsDisplayLayout.setVisibility(View.VISIBLE);
                break;
            case "final_order":
                finalDisplayLayout.setVisibility(View.VISIBLE);
                break;
            case "options_view":
                delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                getDeliveryOptions();
                /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Do you have Coupon Code?")
                        .setTitle("Confirm")
                        .setCancelable(false).setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                couponLayout.setVisibility(View.VISIBLE);
                                //delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                                getDeliveryOptions();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                couponLayout.setVisibility(View.GONE);
                                getDeliveryOptions();
                                delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();*/
                break;
        }

        ///Log.d("gotoNextStep",strAddress_Id);
       /* if (strAddress_Id != null && strAddress_Id.equals("address_id")) {
            //position=0;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    strAddress_Id = sharedPreferencesUtils.getCategory();
                    Log.d("getCategoryA",strAddress_Id);
                    //position=0;
                    fetchUserAddress(strAddress_Id);
                }
            },300);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    strAddress_Id = sharedPreferencesUtils.getCategory();
                    Log.d("getCategoryB",strAddress_Id);
                    //position=0;
                    fetchUserAddress(strAddress_Id);
                }
            },300);

        }*/



        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousItem = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem)
                    expListView.collapseGroup(previousItem);
                previousItem = groupPosition;
                if (date_spinner.getVisibility() == View.VISIBLE) {
                    date_spinner.setVisibility(View.GONE);
                }
                if (time_spinner.getVisibility() == View.VISIBLE) {
                    time_spinner.setVisibility(View.GONE);
                }

                if (priceLinearLayout.getVisibility() == View.VISIBLE) {
                    priceLinearLayout.setVisibility(View.GONE);
                }
                paymentType = listDataHeader.get(groupPosition);

                /*UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + " selected " + paymentType, getActivity());*/
            }
        });

        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int i) {
                if (date_spinner.getVisibility() == View.VISIBLE) {
                    date_spinner.setVisibility(View.GONE);
                }
                if (time_spinner.getVisibility() == View.VISIBLE) {
                    time_spinner.setVisibility(View.GONE);
                }
                if (priceLinearLayout.getVisibility() == View.VISIBLE) {
                    priceLinearLayout.setVisibility(View.GONE);
                }
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view,
                                        int groupPosition, int childPosition, long id) {
                if (parent.isGroupExpanded(groupPosition)) {
                    expListView.collapseGroup(groupPosition);
                }
                return false;
            }
        });

        chkCashBackTerms = view.findViewById(R.id.chkCashBackTerms);
        if (!chkCashBackTerms.isChecked()) {
            chkCashBackTerms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
/*
                    UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on cashback terms", getActivity());
*/
                    dialogPlus = DialogPlus.newDialog(getActivity())
                            .setContentHolder(new ViewHolder(R.layout.dialog_cashback_terms))
                            .setContentHeight(ViewGroup .LayoutParams.WRAP_CONTENT)
                            .setGravity(Gravity.CENTER)
                            .setHeader(R.layout.terms_header)
                            .setPadding(20, 20, 20, 20)
                            .setFooter(R.layout.terms_footer)
                            .setCancelable(true)
                            .create();
                    dialogPlus.show();

                    WebView terms = (WebView) dialogPlus.findViewById(R.id.webview);
                    terms.setVisibility(View.VISIBLE);
                    TextView tvAcceptTerms = (TextView) dialogPlus.findViewById(R.id.txtAcceptTerms);
                    terms.loadUrl(StaticUrl.cash_back);
                    tvAcceptTerms.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogPlus.dismiss();
                            chkCashBackTerms.setFocusable(true);
                            chkCashBackTerms.setClickable(false);
                        }
                    });
                }
            });
        }

        /*if (Connectivity.isConnected(getActivity())) {
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());
        }*/

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        checkLocation(); //check whether location service is enable or not in your  phone

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }

     /*   btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BaseActivity.class);
                intent.putExtra("tag", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });*/
        return view;
    }

    private void preparationForCollection() {

        Set tpSet = keyValueCategoryMap.keySet();
        ArrayList<String> tpArrayList = new ArrayList<>();
        for (String temp : listDataHeader) {
            if (tpSet.contains(temp)) {
                tpArrayList = (ArrayList<String>) keyValueCategoryMap.get(temp);
            }
            childCollection.put(temp, tpArrayList);
        }
        try {
            ExpandableListAdapter listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, childCollection);
            expListView.setAdapter(listAdapter);

            expListView.expandGroup(0);
            /*for (int i = 0; i < childCollection.size(); i++) {
                expListView.expandGroup(i);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //TODO Server method here String strcart_count,String strPincode
  /*  private void getDeliveryOptions() {
        if (Connectivity.isConnected(getActivity())) {
            String strPincode = arr_user_info.get(0).getStrPincode();
            cart_count = arr_user_info.get(0).getStrCount();

            listDataHeader = new ArrayList<>();

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("pin_code", strPincode);
                params.put("my_cart_count", cart_count);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, ApplicationUrlAndConstants.urlOrderDetails, params, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        Log.d("OrderDetails", "getDeliveryOptions response:" + response.toString());
                        JSONObject value = response.getJSONObject(0);
                        JSONObject value1 = response.getJSONObject(1);

                        JSONArray mainCat = value.getJSONArray("parent");
                        for (int i = 0; i < mainCat.length(); i++) {
                            JSONObject jsonMainCatObject = mainCat.getJSONObject(i);
                            listDataHeader.add(jsonMainCatObject.getString("key_text")); //add to arraylist
                        }

                        JSONArray mainProductCat = value1.getJSONArray("child");

                        for (int j = 0; j < mainProductCat.length(); j++) {
                            JSONObject jsonProductCatObject = mainProductCat.getJSONObject(j);
                            JSONArray mainProductCatArray = jsonProductCatObject.getJSONArray(listDataHeader.get(j));

                            childArrayList = new ArrayList<>();

                            for (int g = 0; g < mainProductCatArray.length(); g++) {

                                JSONObject jsonProductCatObjectArray = mainProductCatArray.getJSONObject(g);

                                childArrayList.add(jsonProductCatObjectArray.getString("child_text")); //add to arraylist
                            }
                            Collections.sort(childArrayList);

                            keyValueCategoryMap.put(listDataHeader.get(j), childArrayList);

                        }
                        gateWay.progressDialogStop();
                        preparationForCollection();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }*/

    private void getDeliveryOptions() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            String strPincode = "411028";//arr_user_info.get(0).getStrPincode();
            //cart_count = arr_user_info.get(0).getStrCount();

            listDataHeader = new ArrayList<>();

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("pin_code", "411028");
                params.put("my_cart_count", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }

                String targetUrl = StaticUrl.urlOrderDetails+"?pin_code="+strPincode+"&my_cart_count="+cart_count;

                 JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,targetUrl,null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.e("OrderDetails", "getDeliveryOptions response:" + response.toString());
                            JSONObject value = response.getJSONObject(0);
                            JSONObject value1 = response.getJSONObject(1);

                            JSONArray mainCat = value.getJSONArray("parent");
                            for (int i = 0; i < mainCat.length(); i++) {
                                JSONObject jsonMainCatObject = mainCat.getJSONObject(i);
                                listDataHeader.add(jsonMainCatObject.getString("key_text")); //add to arraylist
                            }

                            JSONArray mainProductCat = value1.getJSONArray("child");

                            for (int j = 0; j < mainProductCat.length(); j++) {
                                JSONObject jsonProductCatObject = mainProductCat.getJSONObject(j);
                                JSONArray mainProductCatArray = jsonProductCatObject.getJSONArray(listDataHeader.get(j));

                                childArrayList = new ArrayList<>();

                                for (int g = 0; g < mainProductCatArray.length(); g++) {

                                    JSONObject jsonProductCatObjectArray = mainProductCatArray.getJSONObject(g);

                                    childArrayList.add(jsonProductCatObjectArray.getString("child_text")); //add to arraylist
                                }
                                Collections.sort(childArrayList);

                                keyValueCategoryMap.put(listDataHeader.get(j), childArrayList);

                            }
                            gateWay.progressDialogStop();
                            preparationForCollection();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        gateWay.progressDialogStop();

                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    }
                });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);

        }
    }



    private void fetchUserAddress(String strAddress_Id) { //TODO  server method here
        if (Connectivity.isConnected(getActivity())) {
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            arr_user_info = new ArrayList<>();
            //arr_user_info.clear();
            JSONObject params = new JSONObject();
            try {
                params.put("id", strAddress_Id);//
                params.put("tag", "checkout");
                params.put("contact", strContact);
                params.put("email", strEmail);
                params.put("tag_for_Cart_Count", "normal_cart");
                //Log.d("fetchAddress_params",strAddress_Id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetuserdata, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        Log.d("fetchAddress",res);
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");

                        if (status.equals("success")) {
                            JSONArray addressArray = jsonObject.getJSONArray("user_data");

                            for (int i = 0; i < addressArray.length(); i++) {
                                JSONObject json_event_list = addressArray.getJSONObject(i);
                                UserInfo ui = new UserInfo(json_event_list.getString("id"),
                                        json_event_list.getString("name"),
                                        json_event_list.getString("address"), json_event_list.getString("contact"),
                                        json_event_list.getString("pincode"), json_event_list.getString("count"),
                                        json_event_list.getString("cashback_points"), json_event_list.getString("dynamic_price_msg"),
                                        json_event_list.getString("msg"), json_event_list.getString("address1"),
                                        json_event_list.getString("address2"), json_event_list.getString("address3"),
                                        json_event_list.getString("address4"), json_event_list.getString("address4"),
                                        json_event_list.getString("area"),json_event_list.getString("city"),
                                        json_event_list.getString("state"), json_event_list.getString("shop_latitude"),
                                        json_event_list.getString("shop_longitude"));
                                arr_user_info.add(ui);
                            }
                            //setDefaultAddress(arr_user_info.get(0).getAddress_id());//this is for old user when they have no default address set

                            tvUserName.setText(arr_user_info.get(position).getStrName());
                            /*if (arr_user_info.get(0).getAddress_type().equals("default")) {
                                txtDefault.setText("To be Delivered");
                            }*/
                            tvAddress.setText(arr_user_info.get(position).getAddress1()+" "+arr_user_info.get(position).getAddress2()+" "+arr_user_info.get(position).getAddress3()+" "+arr_user_info.get(position).getAddress4()+" "+arr_user_info.get(position).getArea());
                            //address1=arr_user_info.get(position).getAddress1();
                            //Log.d("testAddress1",address1);
                            tvUserContact.setText(arr_user_info.get(position).getStrContact());
                            cart_count = arr_user_info.get(position).getStrCount();
                            cashback_points = arr_user_info.get(position).getStrCB_Points();
                            strDynamicPriceMsg = arr_user_info.get(position).getStrDynamic_price_msg();
                            tvPriceCond.setText(strDynamicPriceMsg);

                            if (!cashback_points.equals(0) || !cashback_points.equals("") || !cashback_points.equals(null)) {
                                Order_Details_StepperActivity.LinCB_Points.setVisibility(View.VISIBLE);
                                String[] cb_res = cashback_points.split("@");
                                Order_Details_StepperActivity.tvCBPoints.setText(cb_res[0]);
                                Order_Details_StepperActivity.tvPoints.setText(cb_res[1]);
                            }

                            if (cart_count.equals(1)) {
                                tvItems.setText("View " + cart_count + " item in cart");
                            } else {
                                tvItems.setText("View " + cart_count + " items in cart");
                            }
                           //getDeliveryOptions();
                        } else {
                         /*   Intent intent = new Intent(getActivity(), EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("tag", "address");
                            bundle.putString("type", "add");
                            bundle.putString("finalPrice", finalPrice);
                            intent.putExtras(bundle);
                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            //getActivity().finish();*/
                           /* Intent intent = new Intent(getActivity(), DeliveryAddress.class);
                            String tag_address = "address";
                            intent.putExtra("tag", tag_address);
                            intent.putExtra("finalPrice", finalPrice);
                            //startActivity(intent);
                            startActivityForResult(intent, 1);*/
                           Toast.makeText(getActivity(),"No Address Found",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }


    //from this method we can set current address of user as a default address
  /*  private void setDefaultAddress(String address_id) { //TODO Server Method here
        GateWay gateWay = new GateWay(getActivity());

        JSONObject params = new JSONObject();
        try {
            params.put("contact", sharedPreferencesUtils.getEmailId());
            params.put("address_id", address_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlSetDefaultAddress, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("posts").equals("true")) {
                        Toast.makeText(getActivity(), "Set default address successfully", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }
*/
    @Override
    public Boolean getTheState() {
        return chkStatus;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
               // position = data.getIntExtra("addressPosition",0);
                String sstrAddress_Id = data.getStringExtra("address_id_test");

                Log.d("addressPosition",sstrAddress_Id+" "+""+position+" "+strAddress_Id);
               // arr_user_info.clear();
         /*       new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        strAddress_Id = sharedPreferencesUtils.getCategory();
                        Log.d("getCategoryC",strAddress_Id);
                        Log.d("onActivityResult",strAddress_Id);
                        //position=0;
                        fetchUserAddress(strAddress_Id);
                    }
                },500);*/
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

       new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // strAddress_Id = sharedPreferencesUtils.getCategory();
                Log.d("addressPositionR",+position+" "+strAddress_Id);
                //position=0;
                if (strAddress_Id != null && strAddress_Id.equals("address_id")) {
                    //position=0;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fetchUserAddress(strAddress_Id);
                        }
                    },300);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fetchUserAddress(strAddress_Id);
                        }
                    },300);
                }
            }
        },300);


    }

    //Model Class
    private class UserInfo {

          String address_id;
          String strName;
          String strAddress;
          String strContact;
          String strPincode;
          String strCount;
          String strCB_Points;
          String strDynamic_price_msg;
          String strSwipeCardMsg;
          String address1;
          String address2;
          String address3;
          String address4;
          String address5;
          String area;
          String city;
          String state;
          String user_lat;
          String user_long;

      /*  public UserInfo(String address_id, String strName, String strAddress, String strContact, String strPincode, String strCount, String strCB_Points, String strDynamic_price_msg, String strSwipeCardMsg) {
            this.address_id = address_id;
            this.strName = strName;
            this.strAddress = strAddress;
            this.strContact = strContact;
            this.strPincode = strPincode;
            this.strCount = strCount;
            this.strCB_Points = strCB_Points;
            this.strDynamic_price_msg = strDynamic_price_msg;
            this.strSwipeCardMsg = strSwipeCardMsg;
        }*/

        UserInfo(String address_id, String name, String address, String contact,
                 String strPincode, String strCount, String strCB_Points,
                 String strDynamic_price_msg, String strSwipeCardMsg,String address1,String address2,String address3,String address4,String address5,String area,String city,String state, String user_lat, String user_long) {
            this.address_id = address_id;
            this.strName = name;
            this.strAddress = address;
            this.strContact = contact;
            this.strPincode = strPincode;
            this.strCount = strCount;
            this.strCB_Points = strCB_Points;
            this.strDynamic_price_msg = strDynamic_price_msg;
            this.strSwipeCardMsg = strSwipeCardMsg;
            this.address1 = address1;
            this.address2 = address2;
            this.address3 = address3;
            this.address4 = address4;
            this.address5 = address5;
            this.area = area;
            this.city = city;
            this.state = state;
            this.user_lat = user_lat;
            this.user_long = user_long;
        }

        public String getAddress_id() {
            return address_id;
        }

        public String getStrName() {
            return strName;
        }

        public String getStrAddress() {
            return strAddress;
        }

        public String getStrContact() {
            return strContact;
        }

        public String getStrPincode() {
            return strPincode;
        }

        public String getStrCount() {
            return strCount;
        }

        public String getStrCB_Points() {
            return strCB_Points;
        }

        public String getStrDynamic_price_msg() {
            return strDynamic_price_msg;
        }

        public String getStrSwipeCardMsg() {
            return strSwipeCardMsg;
        }

        public String getAddress1() {
            return address1;
        }

        public String getAddress2() {
            return address2;
        }

        public String getAddress3() {
            return address3;
        }

        public String getAddress4() {
            return address4;
        }

        public String getAddress5() {
            return address5;
        }

        public String getArea() {
            return area;
        }

        public String getCity() {
            return city;
        }

        public String getState() {
            return state;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public void setStrName(String strName) {
            this.strName = strName;
        }

        public void setStrAddress(String strAddress) {
            this.strAddress = strAddress;
        }

        public void setStrContact(String strContact) {
            this.strContact = strContact;
        }

        public void setStrPincode(String strPincode) {
            this.strPincode = strPincode;
        }

        public void setStrCount(String strCount) {
            this.strCount = strCount;
        }

        public void setStrCB_Points(String strCB_Points) {
            this.strCB_Points = strCB_Points;
        }

        public void setStrDynamic_price_msg(String strDynamic_price_msg) {
            this.strDynamic_price_msg = strDynamic_price_msg;
        }

        public void setStrSwipeCardMsg(String strSwipeCardMsg) {
            this.strSwipeCardMsg = strSwipeCardMsg;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public void setAddress3(String address3) {
            this.address3 = address3;
        }

        public void setAddress4(String address4) {
            this.address4 = address4;
        }

        public void setAddress5(String address5) {
            this.address5 = address5;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public void setCity(String city) {
            this.city = city;
        }


        public String getUser_lat() {
            return user_lat;
        }


        public String getUser_long() {
            return user_long;
        }


        @Override
        public String toString() {
            return "UserInfo{" +
                    "address_id='" + address_id + '\'' +
                    ", strName='" + strName + '\'' +
                    ", strAddress='" + strAddress + '\'' +
                    ", strContact='" + strContact + '\'' +
                    ", strPincode='" + strPincode + '\'' +
                    ", strCount='" + strCount + '\'' +
                    ", strCB_Points='" + strCB_Points + '\'' +
                    ", strDynamic_price_msg='" + strDynamic_price_msg + '\'' +
                    ", strSwipeCardMsg='" + strSwipeCardMsg + '\'' +
                    ", address1='" + address1 + '\'' +
                    ", address2='" + address2 + '\'' +
                    ", address3='" + address3 + '\'' +
                    ", address4='" + address4 + '\'' +
                    ", address5='" + address5 + '\'' +
                    ", area='" + area + '\'' +
                    ", city='" + city + '\'' +
                    ", state='" + state + '\'' +
                    ", user_lat='" + user_lat + '\'' +
                    ", user_long='" + user_long + '\'' +
                    '}';
        }
    }

    private void fetch_cart_item() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            mItems.clear();

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetMyCartItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.isNull("posts")) {
                        } else {
                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");
                            final_price = response.getString("Final_Price");
                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jsonCart = mainClassificationJsonArray.getJSONObject(i);

                                Normal_Cart_Item c = new Normal_Cart_Item(jsonCart.getString("normal_cart_shop_id"),
                                        jsonCart.getString("product_id"), jsonCart.getString("product_name"),
                                        jsonCart.getString("normal_shop_name"), jsonCart.getDouble("product_price"),
                                        jsonCart.getString("product_qty"), jsonCart.getString("product_image"),
                                        jsonCart.getString("type"), jsonCart.getString("available_product_quantity"),
                                        jsonCart.getString("hindi_name"), jsonCart.getString("p_name"));
                                dialog_cardAdapter.add(c);
                            }
                            viewcart_items_recycler_view.setAdapter(dialog_cardAdapter);
                            double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                            tvDialogTitle.setText("â‚¹ " + finalValue);
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    public static class Normal_Cart_Item {
        final String str_normal_cart_shopId;
        final String str_product_Id;
        final String str_productName;
        final String str_shopName;
        final String str_productImg;
        final String strtype;
        final String str_AvailableProductQuantity;
        final String strHindi_Name;
        final double str_price;
        final String p_Name;
        String str_qty;

        Normal_Cart_Item(String normal_cart_shopId, String productId, String productName, String shopName,
                         double price, String qty, String productImg, String type,
                         String AvailableProductQuantity, String hindiname, String p_Name) {
            this.str_normal_cart_shopId = normal_cart_shopId;
            this.str_product_Id = productId;
            this.str_productName = productName;
            this.str_shopName = shopName;
            this.str_price = price;
            this.str_qty = qty;
            this.str_productImg = productImg;
            this.strtype = type;
            this.str_AvailableProductQuantity = AvailableProductQuantity;
            this.strHindi_Name = hindiname;
            this.p_Name = p_Name;
        }

        public String getP_Name() {
            return p_Name;
        }

        public String getStrHindi_Name() {
            return strHindi_Name;
        }

        public String getStr_normal_cart_shopId() {
            return str_normal_cart_shopId;
        }

        public String getStr_product_Id() {
            return str_product_Id;
        }

        public String getStr_productName() {
            return str_productName;
        }

        public String getStr_shopName() {
            return str_shopName;
        }

        public double getStr_price() {
            return str_price;
        }

        public String getStr_qty() {
            return str_qty;
        }

        public void setStr_qty(String str_qty) {
            this.str_qty = str_qty;
        }

        public String getStr_productImg() {
            return str_productImg;
        }

        public String getStrtype() {
            return strtype;
        }

        public String getStr_AvailableProductQuantity() {
            return str_AvailableProductQuantity;
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

        double totalAmt;
        DialogPlus dialog;

        public CardAdapter() {
            super();
        }

        @NonNull
        @Override
        public CardAdapter.ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_add_to_cart, viewGroup, false);
            return new CardAdapter.NormalProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull CardAdapter.ViewHolder viewHolder, int position) {
            final Normal_Cart_Item cart_item = mItems.get(position);

            if (cart_item != null) {
                final CardAdapter.NormalProductViewHolder holder1 = (CardAdapter.NormalProductViewHolder) viewHolder;
                String strType = cart_item.getStrtype();

                if (strType.equals("combo")) {
                    //TODO here show combo offer product images setup to gilde
                   /* Glide.with(Order_Details.this).load("http://s.apneareamein.com/seller/assets/" + cart_item.getStr_productImg())
                            .thumbnail(Glide.with(Order_Details.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/
                    Picasso.with(getActivity()).load("https://s.apneareamein.com/seller/assets/" + cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.removeItem.setVisibility(View.GONE);
                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                } else {
                    //TODO here show product images setup to gilde
                   /* Glide.with(Order_Details.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(Order_Details.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/

                    Picasso.with(getActivity()).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.removeItem.setVisibility(View.GONE);
                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                }

                holder1.tvProductName.setText(cart_item.getStr_productName());
                holder1.tvNormalSellerName.setText("By: " + cart_item.getStr_shopName());
                holder1.tvNormalPrice.setText("" + cart_item.getStr_price());
                holder1.etQuantity.setText(String.valueOf(cart_item.getStr_qty()));

                if (cart_item.getStrHindi_Name().equals("")) {
                    holder1.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    holder1.tvProductHindiName.setText(" ( " + cart_item.getStrHindi_Name() + " )");
                }


                try {
                    int qty = Integer.parseInt(cart_item.getStr_qty());
                    double productPrice = cart_item.getStr_price();
                    if (qty > 1) {
                        double total_price = qty * productPrice;
                        double finalValue = Math.round(total_price * 100.0) / 100.0;
                        holder1.tvProductTotal.setText("" + finalValue);
                    } else {
                        holder1.tvProductTotal.setText("" + cart_item.getStr_price());
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();

        }

        public void add(Normal_Cart_Item cart_item) {
            mItems.add(cart_item);
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(View itemView) {
                super(itemView);
            }
        }

        class NormalProductViewHolder extends CardAdapter.ViewHolder {

            final TextView tvProductName;
            final TextView tvNormalSellerName;
            final TextView tvNormalPrice;
            final TextView tvProductTotal;
            final TextView tvProductHindiName;
            final TextView tvMovetoWishlist;
            final ImageView removeItem;
            final ImageView productImg;
            final EditText etQuantity;

            NormalProductViewHolder(View v) {
                super(v);
                tvProductName = v.findViewById(R.id.txtProductName);
                tvProductHindiName = v.findViewById(R.id.txtProductHindiName);
                tvNormalSellerName = v.findViewById(R.id.txtNormalSellerName);
                tvNormalPrice = v.findViewById(R.id.txtNormalPrice);
                removeItem = v.findViewById(R.id.removeItem);
                etQuantity = v.findViewById(R.id.editQuantity);
                productImg = v.findViewById(R.id.productImage);
                tvProductTotal = v.findViewById(R.id.txtProductTotal);
                tvMovetoWishlist = v.findViewById(R.id.txtMovetoWishlist);
                removeItem.setTag(this);
                tvMovetoWishlist.setTag(this);
            }
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private final Context _context;
        private final List<String> _listDataHeader; // header titles
        private final LayoutInflater inflater;
        private RadioButton selected = null;
        final Map<String, List<String>> _listDataChild;

        ExpandableListAdapter(Context context, List<String> listDataHeader,
                              Map<String, List<String>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
            inflater = LayoutInflater.from(context);


        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final String childText = (String) getChild(groupPosition, childPosition);
            final ExpandableListAdapter.Holder holder;


            if (convertView == null) {
               holder = new ExpandableListAdapter.Holder();
                convertView = inflater.inflate(R.layout.delivery_options_list_item, null);
                holder.radioButton = convertView.findViewById(R.id.lblListItem);
                convertView.setTag(holder);
            } else {
                holder = (ExpandableListAdapter.Holder) convertView.getTag();
            }

            Log.e("D_option_RB",""+childText);
            if(childText.equalsIgnoreCase("Schedule Delivery")) {
                holder.radioButton.setText(childText);
            }else {


            }

            holder.radioButton.setChecked(false);

            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectedDeliveryMethod = childText;

/*
                    UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + " selected " + SelectedDeliveryMethod, getActivity());
*/

                    if (childText.contains("Schedule Delivery")) {
                        if (priceLinearLayout.getVisibility() == View.VISIBLE) {
                            priceLinearLayout.setVisibility(View.GONE);
                        }
                        if (Connectivity.isConnected(getActivity())) {
                            getNewScheduledDates("date_slot", "", "");
                        } else {
                            GateWay gateWay = new GateWay(getActivity());
                            gateWay.displaySnackBar(coordinatorLayout);
                        }
                    } else {
                        date_spinner.setVisibility(View.GONE);
                        time_spinner.setVisibility(View.GONE);
                        priceLinearLayout.setVisibility(View.GONE);

                        getFinalPriceFromServer(childText);
                    }

                    if (selected != null) {
                        selected.setChecked(false);
                    }
                    holder.radioButton.setChecked(true);
                    selected = holder.radioButton;
                }
            });

            return convertView;
        }

        class Holder {
            RadioButton radioButton;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            /*return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();*/
            return 1;


        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();

        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            final String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.delivery_options_list_group, null);
            }

            TextView lblListHeader = convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    private void getNewScheduledDates(final String tag, String date, final String selectedDeliveryMethod) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            mainScheduledDateValues = new ArrayList<>();
            mainScheduledDateValues.add(0, "Please Select Date");
            mainScheduledTimeValues = new ArrayList<>();
            mainScheduledTimeValues.add(0, "Please Select Time");

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("tag", tag);
                params.put("date", date);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlNewScheduleDates, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("OrderDetails", "onResponse: " + response.toString());

                    try {
                        try {
                            if (!response.isNull("posts1")) {
                                time_spinner.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = response.getJSONArray("posts1");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledTimeValues.add(jsonObject.getString("time"));
                                }
                                setValuesToTimeSpinner(selectedDeliveryMethod);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (!response.isNull("posts2")) {
                                date_spinner.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = response.getJSONArray("posts2");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledDateValues.add(jsonObject.getString("date"));
                                }
                                setValuesToDateSpinner();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void getNewScheduledDatesForCoupon(String tag) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            mainScheduledDateValues = new ArrayList<>();
            mainScheduledDateValues.add(0, "Please Select Date");
            mainScheduledTimeValues = new ArrayList<>();
            mainScheduledTimeValues.add(0, "Please Select Time");

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("couponCode", coupon_code);
                params.put("tag", tag);
                Log.d("OrderDetails", "params: " + params.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlgetNewScheduledDatesForCoupon, params, new Response.Listener<JSONObject>() {
                // JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL+"getNewScheduledDatesForCoupon.php", params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("OrderDetails", "onResponse: " + response.toString());

                    try {
                        try {
                            if (!response.isNull("posts1")) {
                                time_spinner.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = response.getJSONArray("posts1");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledTimeValues.add(jsonObject.getString("time"));
                                }
                                setValuesToTimeSpinnerForCoupon();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (!response.isNull("posts2")) {
                                date_spinner.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = response.getJSONArray("posts2");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledDateValues.add(jsonObject.getString("date"));
                                }
                                setValuesToDateSpinnerForCoupon();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void setValuesToDateSpinnerForCoupon() {
        try {
            if (mainScheduledDateValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledDateValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                coupondate_spinner.setAdapter(gameKindArray);

                coupondate_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        scheduleSelectedDate = (String) coupondate_spinner.getSelectedItem();

                        if (!scheduleSelectedDate.equals("Please Select Date")) {
                            getNewScheduledDatesForCoupon("time_slot");
                        } else {
                            time_spinner.setVisibility(View.GONE);
                            priceLinearLayout.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValuesToTimeSpinnerForCoupon() {
        try {
            if (mainScheduledTimeValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledTimeValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                coupontime_spinner.setAdapter(gameKindArray);

                coupontime_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        scheduleSelectedTime = (String) coupontime_spinner.getSelectedItem();
                        if (!scheduleSelectedTime.equals("Please Select Time")) {
                            priceLinearLayout.setVisibility(View.VISIBLE);
                            tvPrice.setText("\u20B9 " + strInitialTotalAmount);
                            tvShippingCharge.setText("\u20B9 " + strShippingAmount);
                            tvDiscount.setText("\u20B9 " + strSavingAmount);
                            tvTotal.setText("\u20B9 " + strFinalAmount);

                            applyCouponLayout.setVisibility(View.GONE);
                            deliveryfeesLayout.setVisibility(View.GONE);
                            // paymentType="Schedule Delivery";
                        } else {
                            priceLinearLayout.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValuesToDateSpinner() {
        try {
            if (mainScheduledDateValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledDateValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                date_spinner.setAdapter(gameKindArray);

                date_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        scheduleSelectedDate = (String) date_spinner.getSelectedItem();

                        if (!scheduleSelectedDate.equals("Please Select Date")) {
                            getNewScheduledDates("time_slot", scheduleSelectedDate, SelectedDeliveryMethod);
                        } else {
                            time_spinner.setVisibility(View.GONE);
                            priceLinearLayout.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValuesToTimeSpinner(final String selectedDeliveryMethod) {
        try {
            if (mainScheduledTimeValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledTimeValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                time_spinner.setAdapter(gameKindArray);

                time_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int positionF, long id) {
                        scheduleSelectedTime = (String) time_spinner.getSelectedItem();
                        if (!scheduleSelectedTime.equals("Please Select Time")) {

                            getFinalPriceFromServer(selectedDeliveryMethod);
                        } else {
                            priceLinearLayout.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Update url or change volley request
    private void getFinalPriceFromServer(final String deliveryMethod) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            String targetUrl = StaticUrl.urlGetPriceDetails+"?deliveryMethod="+ URLEncoder.encode(deliveryMethod)+"&coupon_code="+coupon_code+"&email="+strEmail+"&contact="+strContact;

            Log.d("getFinalPriceFromServer",targetUrl);
            /*JSONObject params = new JSONObject();
            try {
                params.put("deliveryMethod", deliveryMethod);
                params.put("coupon_code", coupon_code);
                params.put("email", strEmail);
                params.put("contact", strContact);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonArrayRequest request = new JsonArrayRequest(targetUrl, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    Log.e("ResponseFinalPrice:",""+response.toString());
                    try {
                      /*  if (tvCouponDiscount.getVisibility() == View.VISIBLE) {
                            Animation anim = new AlphaAnimation(0.0f, 1.0f);
                            anim.setDuration(100);
                            anim.setStartOffset(20);
                            anim.setRepeatMode(Animation.REVERSE);
                            anim.setRepeatCount(Animation.INFINITE);
                            tvCouponDiscount.startAnimation(anim);
                        }*/
                        JSONObject value = response.getJSONObject(0);

                        JSONArray seller = value.getJSONArray("posts");
                        for (int i = 0; i < seller.length(); i++) {
                            JSONObject jsonSellerObject = (JSONObject) seller.get(i);

                            strInitialTotalAmount = jsonSellerObject.getString("initialTotalAmount");
                            strShippingAmount = jsonSellerObject.getString("shippingAmount");
                            strSavingAmount = jsonSellerObject.getString("savingAmount");
                            strFinalAmount = jsonSellerObject.getString("finalAmount");
                            strResponse = jsonSellerObject.getString("response");
                            String remainCB_Points = jsonSellerObject.getString("user_cash_back_points");
                            Order_Details_StepperActivity.tvCBPoints.setText(remainCB_Points);
                            double_final_amt = Double.parseDouble(strFinalAmount);
                            if (jsonSellerObject.isNull("coupon_min_amt")) {
                            } else {
                                double_coupon_min_amt = Double.parseDouble(jsonSellerObject.getString("coupon_min_amt"));
                            }
                            if (jsonSellerObject.isNull("couponSavingAmt")) {
                            } else {
                                strCouponSavingAmt = jsonSellerObject.getString("couponSavingAmt");
                            }
                            if (jsonSellerObject.isNull("message")) {
                            } else {
                                strNotifyMessage = jsonSellerObject.getString("message");
                            }
                        }
                        if (strInitialTotalAmount.equals("0") || strInitialTotalAmount.equals("null")) {
                            priceLinearLayout.setVisibility(View.GONE);
                            if (!(getActivity()).isFinishing()) {
                                errorAlertDialog();
                            }

                        } else {
                            priceLinearLayout.setVisibility(View.VISIBLE);
                            tvPrice.setText("\u20B9 " + strInitialTotalAmount);
                            tvShippingCharge.setText("\u20B9 " + strShippingAmount);
                            tvDiscount.setText("\u20B9 " + strSavingAmount);
                            tvTotal.setText("\u20B9 " + strFinalAmount);

                            int couponSavingAmount = Integer.parseInt(strCouponSavingAmt);
                            if (couponSavingAmount != 0) {
                                if (strResponse.equals("true")) {
                                    tvDiscountAmt.setVisibility(View.VISIBLE);
                                    tvCouponDiscount.clearAnimation();
                                    tvCouponDiscount.setVisibility(View.GONE);
                                    tvDiscountAmt.setText("\u20B9 " + strCouponSavingAmt);
                                    //  Toast.makeText(getActivity(), strNotifyMessage, Toast.LENGTH_LONG).show();
                                }
                            } else {

                                if (strResponse.equals("null")) {
                                    tvDiscountAmt.setVisibility(View.GONE);
                                    tvCouponDiscount.setVisibility(View.VISIBLE);
                                } else {
                                    coupon_code = "";
                                    tvDiscountAmt.setVisibility(View.GONE);
                                    tvCouponDiscount.setVisibility(View.VISIBLE);
                                    // Toast.makeText(getActivity(), strNotifyMessage, Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void errorAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Oops! Seems your order got stuck due to some Technical Error.Please call/sms on 7219090909 for confirmation.Thanks - PICoDEL.com")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DBHelper dbHelper = new DBHelper(getActivity());
                        dbHelper.deleteOnlyCartTable();
                        deleteCartItemsAtProblematicCondition();
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteCartItemsAtProblematicCondition() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strName);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlProblematicDeleteCondition, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    gateWay.progressDialogStop();
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    gateWay.progressDialogStop();
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    //Place Order
    //send lat and long
    private void finalOrder(String deliveryMethod) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                String contact = tvUserContact.getText().toString();
                String userName = tvUserName.getText().toString();
                String specialRemark = etSpecialRemark.getText().toString();

                params.put("name", userName);
                params.put("email", strEmail);
                params.put("contactNo", strContact);
                params.put("contact", contact);
                params.put("address_id", arr_user_info.get(position).address_id);
                Log.d("address_id",""+arr_user_info.get(position).address_id+" "+arr_user_info.get(position).getStrAddress());
                params.put("address", arr_user_info.get(position).getStrAddress());
                params.put("address1", arr_user_info.get(position).getAddress1());//arr_user_info.get(position).getAddress1());
                params.put("address2", arr_user_info.get(position).getAddress2());
                params.put("address3", arr_user_info.get(position).getAddress3());
                params.put("address4", arr_user_info.get(position).getAddress4());
                params.put("address5", arr_user_info.get(position).getAddress5());
                params.put("area", arr_user_info.get(position).getArea());
                params.put("city", arr_user_info.get(position).getCity());
                params.put("state", arr_user_info.get(position).getState());
                params.put("totalAmt", strFinalAmount);
                params.put("paymentType", paymentType);
                params.put("deliveryType", deliveryMethod);
                params.put("time", scheduleSelectedTime);
                params.put("pDate", scheduleSelectedDate);
                params.put("coupon_code", coupon_code);
                params.put("coupon_save_amt", strCouponSavingAmt);
                params.put("special_remark", specialRemark);
                params.put("latitude", arr_user_info.get(position).getUser_lat());
                params.put("longitude", arr_user_info.get(position).getUser_long());
                params.put("order_latitude", latitude);
                params.put("order_longitude", longitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("OrderDetails", ""+params);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCashOnDelivery, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("finalOrder"," "+response);
                    sendNotification();
                    //orderPlacedAlertDialog();
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtChangeAddress:
                if (Connectivity.isConnected(getActivity())) {
                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + " clicked on change address button", getActivity());
*/
  /*                  //Intent intent = new Intent(getActivity(), BaseActivity.class);
                    Intent intent = new Intent(getActivity(), DeliveryAddress.class);
                    String tag_address = "address";
                    intent.putExtra("tag", tag_address);
                    intent.putExtra("finalPrice", finalPrice);
                    //startActivity(intent);
                    startActivityForResult(intent, 1);

  */              // Toast.makeText(getActivity(),"No Address Selection",Toast.LENGTH_SHORT).show();
                    Intent address = new Intent(getActivity(), ChangeAddressActivity.class);
                    startActivity(address);
                } else {
                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.displaySnackBar(coordinatorLayout);
                }
                break;

            case R.id.txtItems:
                if (Connectivity.isConnected(getActivity())) {
                    /*UserTracking UT3 = new UserTracking(UserTracking.context);
                    UT3.user_tracking(class_name + ": clicked on view cart item button", getActivity());
*/
                    dialog = DialogPlus.newDialog(getActivity())
                            .setContentHolder(new ViewHolder(R.layout.dialog_viewcart_items))
                            .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                            .setGravity(Gravity.BOTTOM)
                            .create();

                    TextView tvViewCartItemsTitle = (TextView) dialog.findViewById(R.id.txtTitle);
                    tvDialogTitle = (TextView) dialog.findViewById(R.id.txtDialogTotal);

                    if (arr_user_info.get(0).getStrCount().equals(1)) {
                        tvViewCartItemsTitle.setText(cart_count = arr_user_info.get(0).getStrCount() + " " + "Item");
                    } else {
                        tvViewCartItemsTitle.setText(cart_count = arr_user_info.get(0).getStrCount() + " " + "Items");
                    }

                    ImageView closedDialog1 = (ImageView) dialog.findViewById(R.id.imgClosedDialog);
                    closedDialog1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    viewcart_items_recycler_view = (RecyclerView) dialog.findViewById(R.id.viewcart_items_recycler_view);
                    viewcart_items_recycler_view.setHasFixedSize(true);
                    LinearLayoutManager mLayoutManagerForviewcart_items = new LinearLayoutManager(getActivity());
                    viewcart_items_recycler_view.setLayoutManager(mLayoutManagerForviewcart_items);
                    dialog_cardAdapter = new CardAdapter();
                    dialog.show();
                    fetch_cart_item();
                } else {
                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.displaySnackBar(coordinatorLayout);
                }
                break;

         /*   case R.id.txtCouponDiscount:
                if (Connectivity.isConnected(getActivity())) {
                    UserTracking UT2 = new UserTracking(UserTracking.context);
                    UT2.user_tracking(class_name + ": clicked on apply coupon button", getActivity());

                    dialog = DialogPlus.newDialog(getActivity())
                            .setContentHolder(new ViewHolder(R.layout.dialog_coupons))
                            .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                            .setGravity(Gravity.BOTTOM)
                            .create();

                    tvCouponMessage = (TextView) dialog.findViewById(R.id.txtCouponMessage);
                    ImageView closedDialog = (ImageView) dialog.findViewById(R.id.imgClosedDialog);
                    closedDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    coupon_recycler_view = (RecyclerView) dialog.findViewById(R.id.coupon_recycler_view);
                    coupon_recycler_view.setHasFixedSize(true);
                    LinearLayoutManager mLayoutManagerForCoupons = new LinearLayoutManager(getActivity());
                    coupon_recycler_view.setLayoutManager(mLayoutManagerForCoupons);
                    couponsAdapter = new CouponsAdapter();
                    coupon_recycler_view.setAdapter(couponsAdapter);
                    getCoupons();
                    dialog.show();
                } else {
                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.displaySnackBar(coordinatorLayout);
                }
                break;*/

            case R.id.btnPlaceOrder:
               /* if (Connectivity.isConnected(getActivity())) {
                    UserTracking UT1 = new UserTracking(UserTracking.context);
                    UT1.user_tracking(class_name + " clicked on place order button", getActivity());
                }*/
                StopOrder();
                break;
            case R.id.btnAddCouponCode:

                coupon_code = editCouponCode.getText().toString();
                Log.d("OrderDetails", ":coupon_code " + coupon_code);

                if (!coupon_code.equalsIgnoreCase("")) {
                    sendCouponCode();
                }
                break;
            case R.id. btnSkipCouponCode:
                couponLayout.setVisibility(View.GONE);
                delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.btnAddTRN:
                String paid_amt= edittrnAmt.getText().toString();
                String TRN_No= editTRN.getText().toString();
                if(!TRN_No.equalsIgnoreCase("")&& !paid_amt.equalsIgnoreCase("")){

                    sendTRNAmt(paid_amt,TRN_No);
                }else{
                    Toast.makeText(getActivity(), "Please enter TRN No and Paid Amount", Toast.LENGTH_SHORT).show();
                }
                getNewScheduledDatesForCoupon("date_slot");
                break;
            case R.id.txtpaymnentmsg:
                datenTimeLayout.setVisibility(View.VISIBLE);
                TRNLayout.setVisibility(View.VISIBLE);
                Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("https://www.ftcash.com/9552544539"));
                startActivity(browserIntent);
                break;

        }
    }

    /*private void getCouponCodePayment() {
        if (Connectivity.isConnected(getActivity())) {

            listDataHeader = new ArrayList<>();

            final GateWay gateWay = new GateWay(getActivity());
             gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("couponCode", coupon_code);
                // params.put("my_cart_count", cart_count);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("OrderDetails", ":params " + params.toString());

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, URL + "couponKeyText.php", params, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d("OrderDetails", ":response " + response.toString());

                    try {
                        JSONObject value = response.getJSONObject(0);
                        JSONObject value1 = response.getJSONObject(1);

                        JSONArray mainCat = value.getJSONArray("parent");
                        for (int i = 0; i < mainCat.length(); i++) {
                            JSONObject jsonMainCatObject = mainCat.getJSONObject(i);
                            listDataHeader.add(jsonMainCatObject.getString("Key")); //add to arraylist
                        }

                        JSONArray mainProductCat = value1.getJSONArray("child");

                        for (int j = 0; j < mainProductCat.length(); j++) {
                            JSONObject jsonProductCatObject = mainProductCat.getJSONObject(j);
                            JSONArray mainProductCatArray = jsonProductCatObject.getJSONArray(listDataHeader.get(j));

                            childArrayList = new ArrayList<>();

                            for (int g = 0; g < mainProductCatArray.length(); g++) {

                                JSONObject jsonProductCatObjectArray = mainProductCatArray.getJSONObject(g);

                                childArrayList.add(jsonProductCatObjectArray.getString("child_text")); //add to arraylist
                            }
                            Collections.sort(childArrayList);

                            keyValueCategoryMap.put(listDataHeader.get(j), childArrayList);

                        }
                        gateWay.progressDialogStop();
                        preparationForCollection();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Log.d("getCouponCodePayment", ":error " + error.toString());

                      gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }*/

    private void sendTRNAmt(String paid_amt, String TRN_No) {

        final GateWay gateWay = new GateWay(getActivity());
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            //  gateWay.progressDialogStart();
            JSONObject params = new JSONObject();
            try {
                params.put("name", strName);
                params.put("contact", strContact);
                params.put("email", strEmail);
                params.put("transId", TRN_No);
                params.put("amt", paid_amt);
                Log.d("OrderDetails", ":params " + params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlverifyTRN, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(getActivity(), "Order will be confirmed after verification of Advance Payment", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //  gateWay.progressDialogStop();
                    Log.d("OrderDetails", "sendCouponCode:error " + error.toString());
                    error.printStackTrace();


                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void sendCouponCode() {

        final GateWay gateWay = new GateWay(getActivity());
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            gateWay.progressDialogStart();
            JSONObject params = new JSONObject();
            try {
                params.put("couponCode", coupon_code);
                params.put("totalPrice", finalPrice);
                Log.d("OrderDetails", ":params " + params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlsendcouponCode, params, new Response.Listener<JSONObject>() {
                //  JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL+"sendcouponCode.php", params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    gateWay.progressDialogStop();
                    Log.d("OrderDetails", "response " + response.toString());


                    try {
                        if (response.isNull("posts")) {

                        } else {
                            JSONArray array = response.getJSONArray("posts");
                            JSONObject jsonObject = array.getJSONObject(0);
                            String message = jsonObject.getString("message");
                            if(message.equalsIgnoreCase("Success")) {
                                strFinalAmount = jsonObject.getString("total");
                                strSavingAmount = jsonObject.getString("discount");
                                strInitialTotalAmount = jsonObject.getString("Price");
                                couponCode = jsonObject.getString("couponCode");

                                if (couponCode.equalsIgnoreCase("AAMBEN1500")) {
                                    txtpaymnentmsg.setVisibility(View.VISIBLE);
                                } else {
                                    datenTimeLayout.setVisibility(View.VISIBLE);
                                    getNewScheduledDatesForCoupon("date_slot");
                                }
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } catch (Exception e) {
                        gateWay.progressDialogStop();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();
                    Log.d("OrderDetails", "sendCouponCode:error " + error.toString());
                    error.printStackTrace();


                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void StopOrder() {
        final GateWay gateWay = new GateWay(getActivity());
        if (Connectivity.isConnected(getActivity())) {
            try {
                if(coupon_code.equalsIgnoreCase("")){
                    finalOrder(SelectedDeliveryMethod);
                }else{
                    finalOrder("Schedule Delivery");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }
      /*  if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlStopOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            if (Connectivity.isConnected(getActivity())) {
                                try {
                                    if(coupon_code.equalsIgnoreCase("")){
                                        finalOrder(SelectedDeliveryMethod);
                                    }else{
                                        finalOrder("Schedule Delivery");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                gateWay.displaySnackBar(coordinatorLayout);
                            }
                        } else {

                            JSONArray array = response.getJSONArray("posts");
                            JSONObject jsonObject = array.getJSONObject(0);

                            String reason = jsonObject.getString("reason");
                            String image = jsonObject.getString("image");

                            dialog = DialogPlus.newDialog(getActivity())
                                    .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_stop_order))
                                    .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                    .setGravity(Gravity.CENTER)
                                    .setCancelable(false)
                                    .setPadding(20, 20, 20, 20)
                                    .create();
                            dialog.show();
                            ImageView imgStopOrder = (ImageView) dialog.findViewById(R.id.imgCashBack);
                            RelativeLayout stop_orderLayout = (RelativeLayout) dialog.findViewById(R.id.stop_orderLayout);
                            TextView tvMsg = (TextView) dialog.findViewById(R.id.txtMessage);
                            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                            if (reason.equals("")) {
                                tvMsg.setVisibility(View.GONE);
                            } else {
                                tvMsg.setVisibility(View.VISIBLE);
                                tvMsg.setText(reason);
                            }
                            if (image.equals("")) {
                                stop_orderLayout.setVisibility(View.GONE);
                            } else {
                                stop_orderLayout.setVisibility(View.VISIBLE);

                                //TODO here out of stock GIF image setup to glide
                                Glide.with(getActivity())
                                        .load(image)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(imgStopOrder);
                            }

                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }*/
    }

    private class CouponsAdapter extends RecyclerView.Adapter<MainViewHolder> {

        final ArrayList<InnerMovie> innerMovies = new ArrayList<>();

        CouponsAdapter() {
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_coupons, parent, false);
            return new MainViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder holder, int position) {
            InnerMovie innerMovie = innerMovies.get(position);

            holder.tvSellerName.setText("By:" + " " + innerMovie.getShop_name());
            holder.tvCouponCode.setText(innerMovie.getCoupon_code());
            holder.tvCouponInfo.setText(innerMovie.getOffer_amt() + innerMovie.getAmt_in() + " off on minimum amount of on " + "\u20B9 " + innerMovie.getMin_amt());
            holder.tvEndDate.setText(innerMovie.getCoupon_enddate());

            holder.tvCouponCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        //coupon_code = holder.tvCouponCode.getText().toString();
                        if (couponCode.equalsIgnoreCase(""))
                            getFinalPriceFromServer(SelectedDeliveryMethod);
                        dialog.dismiss();
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(coordinatorLayout);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return innerMovies.size();
        }

        public void add(InnerMovie innerMovie) {
            innerMovies.add(innerMovie);
        }

    }

    private class MainViewHolder extends RecyclerView.ViewHolder {

        final TextView tvSellerName;
        final TextView tvCouponCode;
        final TextView tvCouponInfo;
        final TextView tvEndDate;

        public MainViewHolder(View contentView) {
            super(contentView);

            tvSellerName = contentView.findViewById(R.id.txtSellerName);
            tvCouponCode = contentView.findViewById(R.id.txtCouponCode);
            tvCouponInfo = contentView.findViewById(R.id.txtCouponInfo);
            tvEndDate = contentView.findViewById(R.id.txtEndDate);
        }
    }

    private class InnerMovie {

        final String shop_name;
        final String coupon_code;
        final String min_amt;
        final String offer_amt;
        final String amt_in;
        final String coupon_enddate;

        InnerMovie(String shop_name, String coupon_code, String min_amt, String offer_amt, String amt_in, String coupon_enddate) {
            this.shop_name = shop_name;
            this.coupon_code = coupon_code;
            this.min_amt = min_amt;
            this.offer_amt = offer_amt;
            this.amt_in = amt_in;
            this.coupon_enddate = coupon_enddate;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public String getMin_amt() {
            return min_amt;
        }

        public String getOffer_amt() {
            return offer_amt;
        }

        public String getAmt_in() {
            return amt_in;
        }

        public String getCoupon_enddate() {
            return coupon_enddate;
        }
    }

    private void getCoupons() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", sharedPreferencesUtils.getPhoneNumber());
                params.put("email", sharedPreferencesUtils.getEmailId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCouponsDetails, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            coupon_recycler_view.setVisibility(View.GONE);
                            tvCouponMessage.setVisibility(View.VISIBLE);
                        } else {
                            JSONArray couponJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < couponJsonArray.length(); i++) {
                                JSONObject jsonObjectCoupons = couponJsonArray.getJSONObject(i);
                                InnerMovie innerMovie = new InnerMovie(jsonObjectCoupons.getString("shop_name"), jsonObjectCoupons.getString("coupon_code"),
                                        jsonObjectCoupons.getString("min_amt"), jsonObjectCoupons.getString("offer_amt"),
                                        jsonObjectCoupons.getString("amt_in"), jsonObjectCoupons.getString("coupon_enddate"));
                                couponsAdapter.add(innerMovie);
                            }
                            coupon_recycler_view.setAdapter(couponsAdapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            dialog.dismiss();
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

   /* private void orderPlacedAlertDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View review = layoutInflater.inflate(R.layout.order_placed_successfully, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(review); // set alert_dialog_verification_code.xml to shopReviewDialog builder
        alertDialogBuilder // set dialog message
                .setCancelable(false)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        alertDialog = alertDialogBuilder.create(); // create alert dialog
        alertDialog.show(); // show it

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(Color.parseColor("#03A9F4"));
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              *//*  UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on order place dialog button", getActivity());
*//*
                if (alertDialog != null && alertDialog.isShowing()) { //this condition is for window leak error at runtime
                    alertDialog.dismiss();

                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                    //coordinatorLayout_next.setVisibility(View.GONE);
                    // relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                }
            }
        });
    }
*/
    private void sendNotification() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlOrderConfirmationNotification, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("sendNotification",""+response);

                        DBHelper db = new DBHelper(getActivity());
                        db.NormalCartDeleteAll();
                        //orderPlacedAlertDialog();
                        Intent intent = new Intent(getActivity(), ContinueShopingActivity.class);
                        intent.putExtra("tag", "");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }
    //Location
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            Log.d("Location",mLocation.getLatitude()+" "+mLocation.getLongitude());
            latitude = String.valueOf(mLocation.getLatitude());
            longitude = String.valueOf(mLocation.getLongitude());
            //mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            //  Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //  Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }
/*
    @Override
    public void onLocationChanged(Location location) {

    }*/

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
     /*   mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);*/
        mLocationRequest = LocationRequest.create();//.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(getActivity(), "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.d("UpdatedLocation",msg);
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        //mLongitudeTextView.setText(String.valueOf(location.getLongitude() ));
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        //LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
}
