package com.podmerchant.customFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class ButtonFont extends androidx.appcompat.widget.AppCompatButton {

    public ButtonFont(Context context) {
        super(context);
        style(context);
    }

    public ButtonFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public ButtonFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Medium.ttf");
        setTypeface(tf);

    }

}