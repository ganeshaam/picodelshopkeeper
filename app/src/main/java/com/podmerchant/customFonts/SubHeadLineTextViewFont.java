package com.podmerchant.customFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class SubHeadLineTextViewFont extends androidx.appcompat.widget.AppCompatTextView {

    public SubHeadLineTextViewFont(Context context) {
        super(context);
        style(context);
    }

    public SubHeadLineTextViewFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public SubHeadLineTextViewFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "Roboto-Medium.ttf");
        setTypeface(tf);
    }

}