package com.podmerchant.customFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextFont extends EditText {

    public EditTextFont(Context context) {
        super(context);
        style(context);
    }

    public EditTextFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public EditTextFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Roboto-Medium.ttf");
            setTypeface(tf);
        }
    }

}