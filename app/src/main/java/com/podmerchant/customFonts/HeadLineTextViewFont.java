package com.podmerchant.customFonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class HeadLineTextViewFont extends TextView {

    public HeadLineTextViewFont(Context context) {
        super(context);
        style(context);
    }

    public HeadLineTextViewFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public HeadLineTextViewFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "Roboto-Medium.ttf");
        setTypeface(tf);
    }

}