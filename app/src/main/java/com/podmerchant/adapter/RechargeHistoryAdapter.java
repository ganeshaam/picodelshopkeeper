package com.podmerchant.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.RechargePlanModel;

import java.util.ArrayList;

public class RechargeHistoryAdapter extends RecyclerView.Adapter<RechargeHistoryAdapter.PlanListHolder> {

    Context mContext;
    ArrayList<RechargePlanModel> rechargePlanModel;

    public RechargeHistoryAdapter(Context mContext, ArrayList<RechargePlanModel> rechargePlanModel) {
        this.mContext = mContext;
        this.rechargePlanModel = rechargePlanModel;
    }

    @NonNull
    @Override
    public RechargeHistoryAdapter.PlanListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_recharge_histroy,viewGroup,false);
        RechargeHistoryAdapter.PlanListHolder planListHolder = new RechargeHistoryAdapter.PlanListHolder(view);
        return planListHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RechargeHistoryAdapter.PlanListHolder planListHolder, int position) {
        TextView tv_planName = planListHolder.tv_planName;
        TextView tv_recharge = planListHolder.tv_recharge;
        TextView tv_points = planListHolder.tv_points;
        TextView tv_paymentMode  = planListHolder.tv_paymentMode;
        TextView tv_paidto  = planListHolder.tv_paidto;
        TextView tv_rdate  = planListHolder.tv_rdate;

        tv_planName.setText(rechargePlanModel.get(position).getPlanName());
        tv_recharge.setText("INR "+rechargePlanModel.get(position).getRecharge());
        tv_points.setText("Points: "+rechargePlanModel.get(position).getPoints());
        tv_paymentMode.setText(rechargePlanModel.get(position).getPayment_mode());
        tv_paidto.setText("Paid To: "+rechargePlanModel.get(position).getStaffname());
        tv_rdate.setText("Date :"+rechargePlanModel.get(position).getRdate());
    }

    @Override
    public int getItemCount() {
        return rechargePlanModel.size();
    }

    public class PlanListHolder extends RecyclerView.ViewHolder{

        TextView tv_planName,tv_recharge,tv_points,tv_paymentMode,tv_paidto,tv_rdate;

        public PlanListHolder(@NonNull View itemView) {
            super(itemView);
            tv_planName = itemView.findViewById(R.id.tv_planName);
            tv_recharge = itemView.findViewById(R.id.tv_recharge);
            tv_points = itemView.findViewById(R.id.tv_points);
            tv_paymentMode = itemView.findViewById(R.id.tv_paymentMode);
            tv_paidto = itemView.findViewById(R.id.tv_paidto);
            tv_rdate = itemView.findViewById(R.id.tv_rdate);

        }
    }
}

