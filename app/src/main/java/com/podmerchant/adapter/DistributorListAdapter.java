package com.podmerchant.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import com.podmerchant.R;
import com.podmerchant.activites.DistributorDeliverActivity;

import com.podmerchant.activites.PickupDeliveyActivity;
import com.podmerchant.model.DistributorItemModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DistributorListAdapter extends RecyclerView.Adapter<DistributorListAdapter.DistributorHolder> {

    ArrayList<DistributorItemModel> arrayList;
    ArrayList<String> Slot_schedules;
    Context mContext;
    String orderId;

    public DistributorListAdapter(ArrayList<DistributorItemModel> arrayList, Context mContext, String orderId,ArrayList<String> Slot_schedules) {
        this.arrayList = arrayList;
        this.mContext = mContext;
        this.orderId = orderId;
        this.Slot_schedules = Slot_schedules;
    }

    @NonNull
    @Override
    public DistributorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_dist_item_layout,parent,false);
        DistributorHolder distributorHolder = new DistributorHolder(view);
        return distributorHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DistributorHolder holder, final int position) {

        TextView tv_BrandCode = holder.tv_BrandCode;
        TextView tv_Brandcount = holder.tv_Brandcount;
        Button accept = holder.btn_dist_accept;
        ImageView iv_pickup_location = holder.iv_pickup_location;
        final Button deliver = holder.btn_dist_deliver;

        tv_BrandCode.setText(arrayList.get(position).getProduct_brand());
        final String product_brand = arrayList.get(position).getProduct_brand();
        final String cart_uniqueId = arrayList.get(position).getCart_unique_id();

        tv_Brandcount.setText(arrayList.get(position).getProduct_Count());

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"Accept Button Clicked",Toast.LENGTH_LONG).show();

                DestributorTimeSlot(product_brand,cart_uniqueId,arrayList.get(position).getId());
                }

        });

        deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"Delivered Button Clicked",Toast.LENGTH_LONG).show();
                chkOrderStatus(product_brand,cart_uniqueId,arrayList.get(position).getId());
            }
        });

        iv_pickup_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PickupDeliveyActivity.class);
                intent.putExtra("order_id",orderId);
                intent.putExtra("shopkpwise_id",arrayList.get(position).getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class DistributorHolder extends RecyclerView.ViewHolder{
        TextView tv_BrandCode,tv_Brandcount;
        Button btn_dist_accept,btn_dist_deliver;

        ImageView iv_pickup_location;


        public DistributorHolder(@NonNull View itemView) {
            super(itemView);
            tv_BrandCode = itemView.findViewById(R.id.tv_BrandCode);
            tv_Brandcount = itemView.findViewById(R.id.tv_Brandcount);
            btn_dist_accept = itemView.findViewById(R.id.btn_dist_accept);
            btn_dist_deliver = itemView.findViewById(R.id.btn_dist_deliver);
            iv_pickup_location = itemView.findViewById(R.id.iv_pickup_location);

        }
    }


    public void chkOrderStatus(final String product_brand, final String cart_unique_id, final String shopkpwise_id){
        String urlAcceptOrder = "";
        if(Connectivity.isConnected(mContext)){

            SharedPreferencesUtils sharedPreferencesUtils =new SharedPreferencesUtils(mContext);

            /* params.put("cart_unique_id", cartUniqueId);
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                params.put("order_id", orderId);
                params.put("product_brand", product_brand);
                params.put("tblName", sharedPreferencesUtils.getAdminType());*/


             try {


                 urlAcceptOrder = StaticUrl.accept_distributor + "?order_id=" + orderId
                         + "&cart_unique_id=" + cart_unique_id
                         + "&product_brand=" + URLEncoder.encode(product_brand, "utf-8")
                         + "&tblName=" + sharedPreferencesUtils.getAdminType()
                         + "&shop_id=" + sharedPreferencesUtils.getShopID();
             }catch (UnsupportedEncodingException e){
                 e.printStackTrace();
             }

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("checkDistributor_status",response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONArray jsonObjectData =  jsonObject.getJSONArray("data");

                                    String approvedStatus = jsonObjectData.getJSONObject(0).getString("distributor_approve");
                                    String contact = jsonObjectData.getJSONObject(0).getString("contact");


                                    if(approvedStatus.equals("Approved")){

                                        Toast.makeText(mContext,approvedStatus,Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(mContext, DistributorDeliverActivity.class);
                                        intent.putExtra("order_id",orderId);
                                        intent.putExtra("product_brand",product_brand);
                                        intent.putExtra("contact",contact);
                                        intent.putExtra("shopkpwise_id",shopkpwise_id);
                                        intent.putExtra("cart_unique_id",cart_unique_id);
                                        mContext.startActivity(intent);
                                    }
                                    else{
                                        Toast.makeText(mContext,"Please Accept Order First",Toast.LENGTH_SHORT).show();

                                    }
                                }else if(!status){
                                    Toast.makeText(mContext,"No Order Found Refresh Order List.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    // Dialog for the Accept Time slot for the Distributor

    public void DestributorTimeSlot(final String product_brand,final String cartUniqueId,final String shopkpwise_id) {
        final Dialog dialog = new Dialog(mContext,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        TimeSlotAdaptor DistributorTimeAdaptor;
        //View view = getLayoutInflater().inflate(R.layout.activity_time_slot, null);
        LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.activity_time_slot, null);

        final RecyclerView rv_timeslot_rb = (RecyclerView) view.findViewById(R.id.rv_timeslot_rb);
        final  Button btn_close = (Button) view.findViewById(R.id.btn_close);
        btn_close.setVisibility(View.VISIBLE);
        rv_timeslot_rb.hasFixedSize();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        rv_timeslot_rb.setLayoutManager(layoutManager);
        rv_timeslot_rb.setItemAnimator(new DefaultItemAnimator());

        Log.e("CartList Size:",""+arrayList.size());
        DistributorTimeAdaptor = new TimeSlotAdaptor(mContext,Slot_schedules);
        rv_timeslot_rb.setAdapter(DistributorTimeAdaptor);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        rv_timeslot_rb.addOnItemTouchListener(new  RecyclerTouchListener(mContext, rv_timeslot_rb,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {

                        String slot = Slot_schedules.get(position);
                        accept_Distributor(product_brand,cartUniqueId,slot,shopkpwise_id);

                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));
        dialog.setContentView(view);
        dialog.show();
    }

    // If the Distributor Accept the Order
    private String accept_Distributor(final String product_brand, final String cartUniqueId, String delivery_time, final String shopkpwise_id) { //TODO Server method here

        final String[] result_string = {""};

        if (Connectivity.isConnected(mContext)) {
            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
            JSONObject params = new JSONObject();
            try {
                params.put("cart_unique_id", cartUniqueId);
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                params.put("order_id", orderId);
                params.put("product_brand", product_brand);
                params.put("tblName", sharedPreferencesUtils.getAdminType());
                params.put("delivery_time", delivery_time);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAccept_by_distributor, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("AcceptDistributor_Res:",""+response);

                    if (response.isNull("posts")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < categoryJsonArray.length(); i++) {
                                JSONObject jsonCatData = categoryJsonArray.getJSONObject(i);
                                String value = jsonCatData.getString("accept");
                                result_string[0] = value;
                                if(value.equalsIgnoreCase("successfull")){
                                    DialogAcceptDistributor("Order Accept Successfully");
                                    // Mail Generation API Method Called
                                    generateInvoiceMail(product_brand,cartUniqueId,shopkpwise_id);
                                }else {
                                    DialogAcceptDistributor("Order Not Accept, Something Went Wrong ");
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }

        return result_string[0];
    }

    private void DialogAcceptDistributor(String message){
        new AlertDialog.Builder(mContext)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Call Distributor Accept API
                        dialog.dismiss();
                        // accept_Distributor(product_brand);

                    }
                })
                .show();
    }

    // Invoice Generate while Accepting the Order By the Distributor
    private void generateInvoiceMail(String product_brand, String cartUniqueId ,String shopkpwise_id) { //TODO Server method here



        if (Connectivity.isConnected(mContext)) {
            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
            JSONObject params = new JSONObject();
            try {
                params.put("cart_unique_id", cartUniqueId);
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                params.put("order_id", orderId);
                params.put("product_brand", product_brand);
                params.put("admin_type", sharedPreferencesUtils.getAdminType());
                params.put("shopkpwise_id", shopkpwise_id);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.invoice_manufacturer, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("InvoiceEmail_Res:",""+response);

                    if (response.isNull("posts")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts");
                            /*for (int i = 0; i < categoryJsonArray.length(); i++) {
                                JSONObject jsonCatData = categoryJsonArray.getJSONObject(i);
                                String value = jsonCatData.getString("accept");
                                result_string[0] = value;
                                if(value.equalsIgnoreCase("successfull")){
                                    //DialogApproved("Order Accept Successfully");
                                    Toast.makeText(mContext,"Email Invoice Generated Successfully",Toast.LENGTH_LONG).show();
                                }else {
                                    //DialogApproved("Order Not Accept, Something Went Wrong ");
                                }
                            }*/


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }


    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

}
