package com.podmerchant.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.StaffModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class StaffListAdaptor extends RecyclerView.Adapter<StaffListAdaptor.StaffListHolder>{

    Context mContext;
    ArrayList<StaffModel> staffModelArrayList;
    private int lastSelectionPosition = -1;

    public StaffListAdaptor(Context mContext, ArrayList<StaffModel> staffModelArrayList) {
        this.mContext = mContext;
        this.staffModelArrayList = staffModelArrayList;
    }

    @NonNull
    @Override
    public StaffListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View  view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_stafflist,viewGroup,false);
        StaffListAdaptor.StaffListHolder staffListHolder = new  StaffListAdaptor.StaffListHolder(view);
        return staffListHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StaffListHolder staffListHolder, final int position) {
        TextView tv_cm_name = staffListHolder.tv_cm_name;
        //TextView tv_cm_age = staffListHolder.tv_cm_age;
        CircleImageView staff_imgage = staffListHolder.iv_cm_boy;
        RadioButton rb_select_staff = staffListHolder.rb_select_staff;

        tv_cm_name.setText(staffModelArrayList.get(position).getStaffName());
       // tv_cm_age.setText("Age: "+staffModelArrayList.get(position).getAge());
        //String imageUrl = staffModelArrayList.get(position).getImageUrl();

        if(staffModelArrayList.get(position).getImageUrl().isEmpty()) {
            staff_imgage.setImageResource(R.drawable.ic_account_circle_white_24dp);
        }else {
             Picasso.with(mContext)
                    .load(staffModelArrayList.get(position).getImageUrl())
                    .error(R.drawable.ic_account_circle_white_24dp)
                    .into(staff_imgage);
        }

        //since only one radio button is allowed to be selected,
        //this condition un-checks previous selections
        staffListHolder.rb_select_staff.setChecked(lastSelectionPosition==position);

        rb_select_staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectionPosition = position;
                notifyDataSetChanged();

                Intent intent = new Intent();
                intent.putExtra("staffName", staffModelArrayList.get(position).getStaffName());
                //intent.putExtra("staffAge", staffModelArrayList.get(position).getAge());
                intent.putExtra("staffMobile", staffModelArrayList.get(position).getMobile());
                intent.putExtra("staffImage", staffModelArrayList.get(position).getImageUrl());
                intent.putExtra("staffId",staffModelArrayList.get(position).getId());
                ((Activity) mContext).setResult(RESULT_OK, intent);
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return staffModelArrayList.size();
    }

    public class StaffListHolder extends RecyclerView.ViewHolder{

        TextView tv_cm_name;//,tv_cm_age
        CircleImageView iv_cm_boy;
        RadioButton rb_select_staff;

        public StaffListHolder(@NonNull View itemView) {
            super(itemView);

            tv_cm_name=itemView.findViewById(R.id.tv_cm_name);
            //tv_cm_age=itemView.findViewById(R.id.tv_cm_age);
            iv_cm_boy=itemView.findViewById(R.id.iv_cm_boy);
            rb_select_staff=itemView.findViewById(R.id.rb_select_staff);

        }
    }
}
