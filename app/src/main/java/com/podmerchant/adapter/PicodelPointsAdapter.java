package com.podmerchant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.podmerchant.R;
import com.podmerchant.model.PicodelPointsModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PicodelPointsAdapter extends RecyclerView.Adapter<PicodelPointsAdapter.PicodelPointsHolder>{

    ArrayList<PicodelPointsModel> picodelPointsModelArrayList;
    Context mContext;

    public PicodelPointsAdapter(ArrayList<PicodelPointsModel> picodelPointsModelArrayList, Context mContext) {
        this.picodelPointsModelArrayList = picodelPointsModelArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public PicodelPointsAdapter.PicodelPointsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_picodel_points,parent,false);
        PicodelPointsAdapter.PicodelPointsHolder picodelPointsHolder =new PicodelPointsAdapter.PicodelPointsHolder(view);
        return picodelPointsHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PicodelPointsAdapter.PicodelPointsHolder holder, int position) {
        TextView tv_points = holder.txt_referPoints;
        TextView tv_referToShopName = holder.tv_referToShopName;
        RelativeLayout rl_picodelPoints = holder.rl_picodelPoints;

        tv_points.setText(picodelPointsModelArrayList.get(position).getPoints());
        tv_referToShopName.setText("Refer To: \n"+picodelPointsModelArrayList.get(position).getReferto());

        String status = picodelPointsModelArrayList.get(position).getStatus();

        if(status.equals("0")){
            rl_picodelPoints.setBackgroundResource(R.color.colorPrimary);
        }else if(status.equals("1")){
            rl_picodelPoints.setBackgroundResource(R.color.colorPrimaryDark);
        }
    }

    @Override
    public int getItemCount() {
        return picodelPointsModelArrayList.size();
    }


    public class PicodelPointsHolder extends RecyclerView.ViewHolder{

        RelativeLayout rl_picodelPoints;
        TextView txt_referPoints,tv_referToShopName;

        public PicodelPointsHolder(@NonNull View itemView) {
            super(itemView);

            txt_referPoints = itemView.findViewById(R.id.txt_referPoints);
            tv_referToShopName = itemView.findViewById(R.id.tv_referToShopName);
            rl_picodelPoints = itemView.findViewById(R.id.rl_picodelPoints);
        }
    }
}
