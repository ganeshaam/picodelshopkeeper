package com.podmerchant.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.model.CartitemModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.CarlistHolder>{

    ArrayList<CartitemModel> cartArrayList;
    Context mContext;
    String type,shopid,orderId;

    public CartListAdapter(ArrayList<CartitemModel> cartArrayList, Context mContext, String type, String shopid, String orderId) {
        this.cartArrayList = cartArrayList;
        this.mContext = mContext;
        this.type = type;
        this.shopid = shopid;
        this.orderId = orderId;
    }

    @NonNull
    @Override
    public CarlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cart_item_cancel_layout,parent,false);
        CartListAdapter.CarlistHolder carlistHolder = new  CartListAdapter.CarlistHolder(view);
        return carlistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarlistHolder carlistHolder, final int position) {


        final LinearLayout layout = carlistHolder.ll_not_avail;
        TextView tv_productName = carlistHolder.tv_productName;
        TextView tv_productPrice = carlistHolder.tv_productPrice;
        TextView tv_productQty = carlistHolder.tv_productQty;
        TextView tv_productBrand = carlistHolder.tv_productBrand;
        ImageView iv_productImage =  carlistHolder.iv_productImage;
        CheckBox cb_item_not_aval =carlistHolder.cb_cart;
        final ImageView item_cancel =carlistHolder.iv_item_cancel;
        final ImageView item_checked =carlistHolder.iv_item_checked;

        /*if(type.equalsIgnoreCase("Cancel")){
            carlistHolder.cb_cart.setVisibility(View.VISIBLE);
            //carlistHolder.ll_not_avail.setVisibility(View.VISIBLE);
        }else {
            carlistHolder.cb_cart.setVisibility(View.GONE);
            //carlistHolder.ll_not_avail.setVisibility(View.VISIBLE);
        }*/

        if(type.equalsIgnoreCase("Accept")){
            item_checked.setVisibility(View.GONE);

            if(cartArrayList.get(position).getUpdated_status().equalsIgnoreCase("old")){
                item_cancel.setVisibility(View.VISIBLE);
            }else if(cartArrayList.get(position).getUpdated_status().equalsIgnoreCase("new")){
                item_checked.setVisibility(View.VISIBLE);
            }
        }

        if(carlistHolder.cb_cart.isChecked()){
            cartArrayList.get(position).getProduct_name();
            Log.e("ProductNotAvail:",""+cartArrayList.get(position).getProduct_name());
        }




        carlistHolder.rl_cart_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equalsIgnoreCase("Cancel")){

                    //layout.setVisibility(View.VISIBLE);
                    //item_cancel.setVisibility(View.VISIBLE);
                    if(item_checked.getVisibility()==View.VISIBLE){
                        item_cancel.setVisibility(View.VISIBLE);
                        item_checked.setVisibility(View.GONE);
                    }else {
                        item_checked.setVisibility(View.VISIBLE);
                        item_cancel.setVisibility(View.GONE);
                        RmoveCartItem(cartArrayList.get(position).getProduct_id());
                    }
                   // layout.setBackgroundColor(Color.LTGRAY);
                }
            }
        });
        /*carlistHolder.cb_cart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    layout.setVisibility(View.VISIBLE);
                }else {
                    layout.setVisibility(View.GONE);
                }
            }
        });*/

        String imgProductUrl = "https://www.simg.picodel.com/"+cartArrayList.get(position).getProduct_image();
        Picasso.with(mContext).load(imgProductUrl)
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(iv_productImage);
        tv_productBrand.setText(cartArrayList.get(position).getBrand_name());
        tv_productName.setText(cartArrayList.get(position).getProduct_name());
        tv_productPrice.setText("MRP: "+cartArrayList.get(position).getProduct_mrp() +"\nSelling Price: "+ cartArrayList.get(position).getItem_price()+" Quantity: "+cartArrayList.get(position).getIten_qty());
        Double item_price = Double.valueOf(cartArrayList.get(position).getItem_price());
        Double item_qty = Double.valueOf(cartArrayList.get(position).getIten_qty());
        try {
            Double total = item_qty * item_price;
            String.format("%.2f", total);
            tv_productQty.setText("  Total: " + total);
        }catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }

    public static class CarlistHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_not_avail;
        RelativeLayout rl_cart_items;
        ImageView iv_productImage,iv_item_cancel,iv_item_checked;
        CheckBox cb_cart;
        TextView tv_productName,tv_productPrice,tv_productQty,tv_productBrand;
        public CarlistHolder(@NonNull View itemView) {
            super(itemView);
            tv_productName = itemView.findViewById(R.id.tv_productName);
            tv_productPrice = itemView.findViewById(R.id.tv_productPrice);
            tv_productQty = itemView.findViewById(R.id.tv_productQty);
            iv_productImage= itemView.findViewById(R.id.iv_productimage);
            tv_productBrand= itemView.findViewById(R.id.tv_productBrand);
            cb_cart= itemView.findViewById(R.id.cb_cart);
            ll_not_avail = itemView.findViewById(R.id.ll_not_avail);
            rl_cart_items =itemView.findViewById(R.id.rl_cart_items);
            iv_item_checked =itemView.findViewById(R.id.iv_item_checked);
            iv_item_cancel =itemView.findViewById(R.id.iv_item_cancel);
        }
    }

    // RemoveCartItems
    public void RmoveCartItem(String product_id) {
        if (Connectivity.isConnected(mContext)) {


            String cancelcartitem = null;

                cancelcartitem = StaticUrl.removecartitem
                        + "?shop_id=" + shopid
                        + "&order_id=" +  orderId
                        + "&product_id=" + product_id;

            Log.d("urlCancelOrderItem",cancelcartitem);
            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    cancelcartitem,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                Log.e("cancelCartRes:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status = true) {
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext, "Cart item Remove", Toast.LENGTH_SHORT).show();

                                } else if (status = false) {
                                    Toast.makeText(mContext, "Sorry. This Can't Remove.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

}
