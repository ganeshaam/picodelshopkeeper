package com.podmerchant.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.DeliveryBoyModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DeliveryBoyListAdaptor extends RecyclerView.Adapter<DeliveryBoyListAdaptor.boyListHolder> {

    ArrayList<DeliveryBoyModel> arrayList;
    private Context mContext;
    private int lastSelectionPosition = -1;

    public DeliveryBoyListAdaptor(ArrayList<DeliveryBoyModel> arrayList, Context mContext) {
        this.arrayList = arrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public boyListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_deliveryboy_list_items,viewGroup,false);
        DeliveryBoyListAdaptor.boyListHolder boyListHolder = new DeliveryBoyListAdaptor.boyListHolder(view);
        return boyListHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull boyListHolder boyListHolder, int position) {
        TextView tv_name = boyListHolder.tv_boyname;
        TextView tv_contact= boyListHolder.tv_boycontact;
        CircleImageView imageView = boyListHolder.boy_profile;

        tv_name.setText(arrayList.get(position).getName());
        if(arrayList.get(position).getMobile_number().equalsIgnoreCase("0")){

        }else {
            tv_contact.setText(arrayList.get(position).getMobile_number());
        }

        if(arrayList.get(position).getImgeurl().isEmpty()) {
            imageView.setImageResource(R.drawable.ic_account_circle_white_24dp);
        }else {
            Picasso.with(mContext)
                    .load(arrayList.get(position).getImgeurl())
                    .error(R.drawable.ic_account_circle_white_24dp)
                    .into(imageView);
        }

        //since only one radio button is allowed to be selected,
        //this condition un-checks previous selections
        boyListHolder.rb_selectDelBoy.setChecked(lastSelectionPosition==position);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class boyListHolder extends RecyclerView.ViewHolder{

        TextView tv_boyname,tv_boycontact,tv_email;
        CircleImageView boy_profile;
        RadioButton rb_selectDelBoy;
        public boyListHolder(@NonNull View itemView) {
            super(itemView);
            tv_boyname = itemView.findViewById(R.id.tv_boyname);
            tv_boycontact = itemView.findViewById(R.id.tv_boycontact);
            tv_email = itemView.findViewById(R.id.tv_email);
            boy_profile = itemView.findViewById(R.id.boy_profile);
            rb_selectDelBoy = itemView.findViewById(R.id.rb_selectDelBoy);

            rb_selectDelBoy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectionPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }
}
