package com.podmerchant.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.RechargePlanModel;

import java.util.ArrayList;

public class RechargePlanAdapter extends RecyclerView.Adapter<RechargePlanAdapter.PlanListHolder> {

    Context mContext;
    ArrayList<RechargePlanModel> rechargePlanModel;
    private int lastSelectionPosition = -1;

    public RechargePlanAdapter(Context mContext, ArrayList<RechargePlanModel> rechargePlanModel) {
        this.mContext = mContext;
        this.rechargePlanModel = rechargePlanModel;
    }

    @NonNull
    @Override
    public PlanListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_recharge_plan,viewGroup,false);
        RechargePlanAdapter.PlanListHolder planListHolder = new RechargePlanAdapter.PlanListHolder(view);
        return planListHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PlanListHolder planListHolder, int position) {
        TextView tv_planName = planListHolder.tv_planName;
        TextView tv_recharge = planListHolder.tv_recharge;
        TextView tv_points = planListHolder.tv_points;
        TextView tv_rechargeAfterGst = planListHolder.tv_rechargeAfterGst;
        RelativeLayout rl_recharge_list = planListHolder.rl_recharge_list;

        tv_planName.setText(rechargePlanModel.get(position).getPlanName());
        tv_recharge.setText("INR "+rechargePlanModel.get(position).getRecharge());
        tv_points.setText("Points: "+rechargePlanModel.get(position).getPoints());
        tv_rechargeAfterGst.setText("After GST deduction INR: "+rechargePlanModel.get(position).getAfterGst());
        String bgColor = rechargePlanModel.get(position).getBgcolor();
        rl_recharge_list.setBackgroundColor(Color.parseColor(bgColor));
        //since only one radio button is allowed to be selected,
        //this condition un-checks previous selections
        planListHolder.rb_selectPlan.setChecked(lastSelectionPosition==position);
    }

    @Override
    public int getItemCount() {
        return rechargePlanModel.size();
    }

    public class PlanListHolder extends RecyclerView.ViewHolder{

        TextView tv_planName,tv_recharge,tv_points,tv_rechargeAfterGst;

        RadioButton rb_selectPlan;
        RelativeLayout rl_recharge_list;

        public PlanListHolder(@NonNull View itemView) {

            super(itemView);
            tv_planName = itemView.findViewById(R.id.tv_planName);
            tv_recharge = itemView.findViewById(R.id.tv_recharge);
            tv_points = itemView.findViewById(R.id.tv_points);
            tv_rechargeAfterGst = itemView.findViewById(R.id.tv_rechargeAfterGst);
            rl_recharge_list = itemView.findViewById(R.id.rl_recharge_list);
            rb_selectPlan = itemView.findViewById(R.id.rb_selectPlan);

            rb_selectPlan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectionPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }
}
