package com.podmerchant.adapter;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.podmerchant.R;
import com.podmerchant.model.PartnerKitModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PartnerkitAdapter extends RecyclerView.Adapter<PartnerkitAdapter.partnerKitHolder>{

    ArrayList<PartnerKitModel> partnerKitModels;
    Context mContext;

    public PartnerkitAdapter(ArrayList<PartnerKitModel> partnerKitModels, Context mContext,SparseBooleanArray itemStateArray) {
        this.partnerKitModels = partnerKitModels;
        this.mContext = mContext;
    }
    public void setEmployees(ArrayList<PartnerKitModel> employees) {
        this.partnerKitModels = new ArrayList<>();
        this.partnerKitModels = employees;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public PartnerkitAdapter.partnerKitHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_partnerkit,viewGroup,false);
        PartnerkitAdapter.partnerKitHolder partnerKitHolder = new PartnerkitAdapter.partnerKitHolder(view);
        return partnerKitHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PartnerkitAdapter.partnerKitHolder partnerKitHolder, final int position) {

        TextView tv_partnerkit = partnerKitHolder.tv_partnerkit;
        TextView tv_amount = partnerKitHolder.tv_amount;
        final CheckBox cb_partnerkit = partnerKitHolder.cb_partnerkit;

        tv_partnerkit.setText(partnerKitModels.get(position).getKitName());
        tv_amount.setText("INR "+partnerKitModels.get(position).getKitAmount());

        partnerKitHolder.bind(partnerKitModels.get(position));

    }

        @Override
    public int getItemCount() {
        return partnerKitModels.size();
    }


    public class partnerKitHolder extends RecyclerView.ViewHolder{

        TextView tv_partnerkit,tv_amount;
        CheckBox cb_partnerkit;

        public partnerKitHolder(@NonNull View itemView) {
            super(itemView);

            tv_amount = itemView.findViewById(R.id.tv_partnrekitAmount);
            tv_partnerkit = itemView.findViewById(R.id.tv_partnrekit);
            cb_partnerkit = itemView.findViewById(R.id.cb_partnerkit);
        }

        void bind(final PartnerKitModel employee) {
           // imageView.setVisibility(employee.isChecked() ? View.VISIBLE : View.GONE);
            //textView.setText(employee.getName());
           // cb_partnerkit.setChecked(employee.isChecked());
            cb_partnerkit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    employee.setChecked(!employee.isChecked());
                   // cb_partnerkit.setChecked(employee.isChecked());
                    //imageView.setVisibility(employee.isChecked() ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    public ArrayList<PartnerKitModel> getSelected() {
        ArrayList<PartnerKitModel> selected = new ArrayList<>();
        for (int i = 0; i < partnerKitModels.size(); i++) {
            if (partnerKitModels.get(i).isChecked()) {
                selected.add(partnerKitModels.get(i));
            }
        }
        return selected;
    }
}
