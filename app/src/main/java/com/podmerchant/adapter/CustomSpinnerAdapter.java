package com.podmerchant.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.podmerchant.R;

import java.util.ArrayList;

public class CustomSpinnerAdapter extends BaseAdapter {

    private final LayoutInflater inflter;
    private final ArrayList<String> PriceArray;
    private final ArrayList<String> DiscountPriceArray;
    private final ArrayList<String> SizeArray;
    private final String tag;

    public CustomSpinnerAdapter(Context applicationContext, ArrayList<String> PriceArray, ArrayList<String> DiscountPriceArray, ArrayList<String> SizeArray, String tag) {
        this.PriceArray = PriceArray;
        this.DiscountPriceArray = DiscountPriceArray;
        this.SizeArray = SizeArray;
        this.tag = tag;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return PriceArray.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        switch (tag) {
            case "homepage": {
                view = inflter.inflate(R.layout.custom_spinner_homepage_for_price, viewGroup, false);
                TextView tvPrice = view.findViewById(R.id.txtPrice);
                TextView tvDiscount = view.findViewById(R.id.txtDiscountPrice);
                TextView tvSize = view.findViewById(R.id.txtSize);
                tvSize.setText(SizeArray.get(i));
                tvSize.setMaxLines(1);
                if (DiscountPriceArray.size() > 0) {
                    if (PriceArray.get(i).equals(DiscountPriceArray.get(i))) {
                        tvPrice.setVisibility(View.VISIBLE);
                        tvDiscount.setVisibility(View.GONE);
                    } else {
                        tvPrice.setVisibility(View.VISIBLE);
                        tvDiscount.setVisibility(View.VISIBLE);
                        tvDiscount.setText("₹ " + DiscountPriceArray.get(i));
                        tvPrice.setTextColor(Color.parseColor("#BDBDBD"));
                        tvPrice.setBackgroundResource(R.drawable.dash);
                    }
                } else {
                    tvDiscount.setVisibility(View.GONE);
                }
                tvPrice.setText("₹ " + PriceArray.get(i));
                break;
            }
            case "location": {
                view = inflter.inflate(R.layout.custom_spinner_homepage_for_price, viewGroup, false);
                TextView tvPrice = view.findViewById(R.id.txtPrice);
                tvPrice.setVisibility(View.GONE);
                TextView tvDiscount = view.findViewById(R.id.txtDiscountPrice);
                tvDiscount.setVisibility(View.GONE);
                TextView tvSize = view.findViewById(R.id.txtSize);
                tvSize.setTextSize(14);
                tvSize.setGravity(Gravity.LEFT);
                tvSize.setText(PriceArray.get(i));
                break;
            }
            default: {
                view = inflter.inflate(R.layout.custom_spinner_for_price, viewGroup, false);
                TextView tvPrice = view.findViewById(R.id.txtPrice);
                TextView tvDiscount = view.findViewById(R.id.txtDiscountPrice);
                TextView tvSize = view.findViewById(R.id.txtSize);
                tvSize.setText(SizeArray.get(i));
                if (DiscountPriceArray.size() > 0) {
                    if (PriceArray.get(i).equals(DiscountPriceArray.get(i))) {
                        tvPrice.setVisibility(View.VISIBLE);
                        tvDiscount.setVisibility(View.GONE);
                    } else {
                        tvPrice.setVisibility(View.VISIBLE);
                        tvDiscount.setVisibility(View.VISIBLE);
                        tvDiscount.setText("₹ " + DiscountPriceArray.get(i));
                        tvPrice.setTextColor(Color.parseColor("#BDBDBD"));
                        tvPrice.setBackgroundResource(R.drawable.dash);
                    }
                } else {
                    tvDiscount.setVisibility(View.GONE);
                }
                tvPrice.setText("₹ " + PriceArray.get(i));
                break;
            }
        }

        return view;
    }

}
