package com.podmerchant.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.podmerchant.R;
import com.podmerchant.model.SliderModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SliderPagerAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<SliderModel> sliderModelArrayList;
    LayoutInflater layoutInflater;

    public SliderPagerAdapter(Context mContext, ArrayList<SliderModel> sliderModelArrayList) {
        this.mContext = mContext;
        this.sliderModelArrayList = sliderModelArrayList;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return sliderModelArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View view = layoutInflater.inflate(R.layout.custom_slider,container,false);
        ImageView iv_slider = view.findViewById(R.id.iv_slider);
        Picasso.with(mContext)
                .load(sliderModelArrayList.get(position).getImgUrl())
                .error(R.drawable.ic_account_circle_white_24dp)
                .into(iv_slider);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        super.destroyItem(container, position, object);
//        container.removeView((LinearLayout)object);
    }
}
