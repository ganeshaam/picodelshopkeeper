package com.podmerchant.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.podmerchant.R;
import com.podmerchant.model.CartitemModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.CartDetailHolder>{

    Context mContext;
    ArrayList<CartitemModel> arrayList;

    public ReportListAdapter(Context mContext, ArrayList<CartitemModel> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public CartDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cart_item_layout,parent,false);
        ReportListAdapter.CartDetailHolder cartDetailHolder = new  ReportListAdapter.CartDetailHolder(view);
        return cartDetailHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull CartDetailHolder holder, int position) {


        TextView tv_productName = holder.tv_productName;
        TextView tv_productPrice = holder.tv_productPrice;
        TextView tv_productQty = holder.tv_productQty;
        TextView tv_productBrand = holder.tv_productBrand;
        ImageView iv_productImage =  holder.iv_productImage;

        String imgProductUrl = "https://www.simg.picodel.com/"+arrayList.get(position).getProduct_image();
        Picasso.with(mContext).load(imgProductUrl)
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(iv_productImage);
        tv_productBrand.setText(arrayList.get(position).getBrand_name());
        tv_productName.setText(arrayList.get(position).getProduct_name());
        tv_productPrice.setText(arrayList.get(position).getProduct_size());
        Log.e("product_name",""+arrayList.get(position).getProduct_name());
        //Double item_price = Double.valueOf(arrayList.get(position).getItem_price());
        tv_productQty.setText("Qty: "+arrayList.get(position).getIten_qty());


    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public static class CartDetailHolder extends RecyclerView.ViewHolder{

        ImageView iv_productImage;
        TextView tv_productName,tv_productPrice,tv_productQty,tv_productBrand;

       public CartDetailHolder(@NonNull View itemView) {
           super(itemView);
           tv_productName = itemView.findViewById(R.id.tv_productName);
           tv_productPrice = itemView.findViewById(R.id.tv_productPrice);
           tv_productQty = itemView.findViewById(R.id.tv_productQty);
           iv_productImage= itemView.findViewById(R.id.iv_productimage);
           tv_productBrand= itemView.findViewById(R.id.tv_productBrand);
       }
   }

}
