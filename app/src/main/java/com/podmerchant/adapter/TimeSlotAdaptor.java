package com.podmerchant.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.podmerchant.R;

import java.util.ArrayList;

public class TimeSlotAdaptor extends RecyclerView.Adapter<TimeSlotAdaptor.paymentModeHolder> {

    ArrayList<String> arrayList;
    Context context;
    private int lastSelectionPosition = -1;


    public TimeSlotAdaptor(Context context, ArrayList<String> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public paymentModeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_timeslots_items,viewGroup,false);
        TimeSlotAdaptor.paymentModeHolder paymentModeHolder = new TimeSlotAdaptor.paymentModeHolder(view);
        return paymentModeHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final paymentModeHolder paymentModeHolder, final int position) {

        paymentModeHolder.tv_payment_type.setText(arrayList.get(position));
        Log.e("Array_text:",""+arrayList.get(position));
        paymentModeHolder.rb_selectPaymentMode.setChecked(lastSelectionPosition==position);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class paymentModeHolder extends RecyclerView.ViewHolder{

        TextView tv_payment_type;
        CardView card_layout;
        RadioButton rb_selectPaymentMode;

        public paymentModeHolder(@NonNull View itemView) {
            super(itemView);
            tv_payment_type = itemView.findViewById(R.id.tv_payment_type);
            card_layout = itemView.findViewById(R.id.card_layout);
            rb_selectPaymentMode = itemView.findViewById(R.id.rb_selectPaymentMode);;

            rb_selectPaymentMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectionPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }
}
