package com.podmerchant.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.activites.OrderDetailActivity;
import com.podmerchant.model.CartitemModel;
import com.podmerchant.model.ProductCodeWiseProduct;
import com.podmerchant.model.productCodeWithProductList;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class SimilarProductCardAdapter extends RecyclerView.Adapter<SimilarProductCardAdapter.SimilarProductsViewHolder>{

    List<productCodeWithProductList> mItems;
    Context mContext;
    String cartUniqueId,userId,email_id,order_id,click_product_id,contactNo;



    /*public SimilarProductCardAdapter(ArrayList<productCodeWithProductList> finalList, OrderDetailActivity orderDetailActivity, String cartUniqueId, String userId, String orderId) {
        this.mItems = finalList;
    }*/

    public SimilarProductCardAdapter(List<productCodeWithProductList> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }

    public SimilarProductCardAdapter(List<productCodeWithProductList> mItems, Context mContext, String cartUniqueId, String userId, String order_id, String click_product_id , String contactNo ) {
        this.mItems = mItems;
        this.mContext = mContext;
        this.cartUniqueId = cartUniqueId;
        this.userId = userId;
        this.order_id = order_id;
        this.click_product_id = click_product_id;
        this.contactNo =contactNo;
    }

    @NonNull
    @Override
    public SimilarProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_for_ultimate_products, parent, false);
        //return null;
        return new SimilarProductsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final SimilarProductsViewHolder holder, int position) {

        ArrayList<String> sizesArrayList = new ArrayList<>();
        ArrayList<String> pricesArrayList = new ArrayList<>();
        ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

        //All the Spinner Size of the Product Array list or spinner module

        try {
            productCodeWithProductList newProductList = mItems.get(position);
            for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                ProductCodeWiseProduct tp1 = newProductList.similarProductCodeWiseProducts.get(l);
                sizesArrayList.add(tp1.getProduct_size());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            productCodeWithProductList newProductList = mItems.get(position);
            for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                ProductCodeWiseProduct tp3 = newProductList.similarProductCodeWiseProducts.get(l);
                DiscountpricesArrayList.add(tp3.getProduct_price());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            productCodeWithProductList newProductList = mItems.get(position);
            for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                ProductCodeWiseProduct tp2 = newProductList.similarProductCodeWiseProducts.get(l);
                pricesArrayList.add(tp2.getProduct_mrp());

                if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                    holder.LayoutSpinner.setVisibility(View.VISIBLE);
                    CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(mContext, pricesArrayList, DiscountpricesArrayList, sizesArrayList, "homepage");
                    holder.spinner.setAdapter(customSpinnerAdapter);
                } else {
                    holder.LayoutSpinner.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*--------------------------------for default value------------------------------*/
        try {
            productCodeWithProductList movie = mItems.get(position);
            final ProductCodeWiseProduct tp = movie.similarProductCodeWiseProducts.get(0);

            if (tp.getStrHindiName().equals("")) {
                holder.tvProductHindiName.setVisibility(View.GONE);
            } else {
                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                holder.tvProductHindiName.setText(tp.getStrHindiName());
            }

                /*HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                String btnCheckStatus = (String) AddToCartInfo.get("new_pid");*/
            String server_cart_status = tp.getCart_pstatus();

            if (server_cart_status.equalsIgnoreCase("0")) {
                holder.btnAddToCart.setText(mContext.getResources().getString(R.string.add_to_cart));
                holder.btnAddToCart.setBackground(mContext.getResources().getDrawable(R.drawable.button_border));
            } else {
                holder.btnAddToCart.setText(mContext.getResources().getString(R.string.go_to_cart));
                holder.btnAddToCart.setTextColor(Color.WHITE);
                holder.btnAddToCart.setBackground(mContext.getResources().getDrawable(R.drawable.btn_green));
            }

            //TODO here product image setup to glide

            Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgProduct);

            String size = tp.getProduct_size();
            holder.tvProductName.setText(tp.getProduct_name() + " " + size);
            String dis = tp.getProduct_discount();
            String discount = tp.setProduct_discount(dis);
            holder.tvDiscount.setText(discount + "% OFF");
            String productMrp = tp.getProduct_mrp();
            String discountPrice = tp.getProduct_price();

            if (productMrp.equals(discountPrice)) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                //holder.tvMrp.setVisibility(View.GONE);
            } else {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.VISIBLE);
                holder.tvMrp.setVisibility(View.VISIBLE);
                holder.tvMrp.setBackgroundResource(R.drawable.dash);
            }
            Log.e("productMrp_3",""+productMrp+""+discountPrice);
            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
            /*--------------------------------for default value------------------------------*/

            String QTY = tp.getStrAvailable_Qty();
            final String pId = tp.getProduct_id();
            final String maincategory = tp.getProduct_maincat();
            Log.e("AvailQty",""+QTY);
            if (QTY.equals("0")) {
                holder.btnAddToCart.setVisibility(View.GONE);
                holder.imgOutOfStock.setVisibility(View.VISIBLE);
                //holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));


                //TODO here out of stock GIF image setup to glide
                /*Picasso.with(mContext)
                        .load(R.drawable.outofstock)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.icerror_outofstock)
                        .into(holder.imgOutOfStock);*/
            } else {
                holder.btnAddToCart.setVisibility(View.VISIBLE);
                holder.imgOutOfStock.setVisibility(View.GONE);
            }

            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                            ProductCodeWiseProduct tp = movie.similarProductCodeWiseProducts.get(pos);

                            movie.setPosition(pos); //set here position when user any wants to buy multiple size products

                            if (tp.getStrHindiName().equals("")) {
                                holder.tvProductHindiName.setVisibility(View.GONE);
                            } else {
                                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                                holder.tvProductHindiName.setText(tp.getStrHindiName());
                            }

                            //TODO here product image setup to glide when user select different product variant from spinner
                                /*Glide.with(SingleProductInformation.this).load("http://simg.picodel.com/" + tp.getProduct_image())
                                        .thumbnail(Glide.with(SingleProductInformation.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(holder.imgProduct);*/
                            Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                    .placeholder(R.drawable.loading)
                                    .error(R.drawable.ic_app_transparent)
                                    .into(holder.imgProduct);

                            String size = tp.getProduct_size();
                            holder.tvProductName.setText(tp.getProduct_name() + " " + size);
                            String dis = tp.getProduct_discount();
                            String discount = tp.setProduct_discount(dis);
                            holder.tvDiscount.setText(discount + "% OFF");
                            String productMrp = tp.getProduct_mrp();
                            String discountPrice = tp.getProduct_price();
                            String QTY = tp.getProduct_qty();

                            if (productMrp.equals(discountPrice)) {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.tvDiscount.setVisibility(View.INVISIBLE);
                                // holder.tvMrp.setVisibility(View.GONE);
                            } else {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.tvDiscount.setVisibility(View.VISIBLE);
                                holder.tvMrp.setVisibility(View.VISIBLE);
                                holder.tvMrp.setBackgroundResource(R.drawable.dash);
                            }
                            Log.e("productMrp_4",""+productMrp+""+discountPrice);
                            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                                /*HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                                String btnCheckStatus = (String) AddToCartInfo.get("new_pid");*/
                            String server_cart_status = tp.getCart_pstatus();

                            if (server_cart_status.equalsIgnoreCase("0")) {
                                tp.setStatus(false);
                                holder.btnAddToCart.setText(mContext.getResources().getString(R.string.add_to_cart));
                                holder.btnAddToCart.setBackground(mContext.getResources().getDrawable(R.drawable.button_border));
                            } else {
                                tp.setStatus(true);
                                if (QTY.equalsIgnoreCase("0")) {
                                    holder.btnAddToCart.setText(mContext.getResources().getString(R.string.add_to_cart));
                                } else {
                                    holder.btnAddToCart.setText(mContext.getResources().getString(R.string.go_to_cart));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("SimilerAdaptor","error in Adaptor");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

            holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addCartItemsToServer(pId,maincategory);
                }
                private void addCartItemsToServer(String pId, String maincategory) { //TODO Server method here
                    JSONObject params = new JSONObject();

                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", "78");
                        params.put("selectedType", "");
                        params.put("versionCode", "2.0.37");
                        params.put("Qty", 1);
                        params.put("cart_unique_id", cartUniqueId);
                        params.put("order_id", order_id);
                        params.put("click_product_id", click_product_id);
                        params.put("contactNo", contactNo);
                        params.put("email", "Ganeshb.aam@gmail.com");
                        params.put("sessionMainCat", maincategory);
                        Log.e("AddtoCart_param",""+params);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlnewAddUpdateByShop, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                Log.e("Add to Cart Res:",""+response);
                                String posts = response.getString("posts");
                                //String promotional = response.getString("promotional");
                                //String promotional_message = response.getString("promo_message");

                                if (posts.equals("true")){

                                    holder.btnAddToCart.setText(mContext.getResources().getString(R.string.go_to_cart));
                                    holder.btnAddToCart.setBackground(mContext.getResources().getDrawable(R.drawable.btn_green));
                                    holder.btnAddToCart.setTextColor(Color.WHITE);

                                    Log.d("addCartItemsToServer1",""+posts);
                                    Toast toast = Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    //openSessionDialog(posts,"session");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            GateWay gateWay = new GateWay(mContext);
                            gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                        }
                    });
                    VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }



    class SimilarProductsViewHolder extends RecyclerView.ViewHolder {

        final ImageView imgProduct;
        final TextView tvProductName;
        final TextView tvProductHindiName;
        final TextView tvMrp;
        final TextView tvDiscount;
        final TextView tvPrice;
        final TextView btnAddToCart;
        final Spinner spinner;
        final RelativeLayout LayoutSpinner;
        final ImageView imgOutOfStock;

        SimilarProductsViewHolder(View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvProductName = itemView.findViewById(R.id.txtProductName);
            tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
            tvMrp = itemView.findViewById(R.id.txtTotal);
            tvDiscount = itemView.findViewById(R.id.txtDiscount);
            tvPrice = itemView.findViewById(R.id.txtPrice);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            spinner = itemView.findViewById(R.id.spinner);
            LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
            imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
            btnAddToCart.setTag(this);
        }
    }



}
