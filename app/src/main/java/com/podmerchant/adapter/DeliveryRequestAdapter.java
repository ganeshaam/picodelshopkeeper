package com.podmerchant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.DeliveryModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DeliveryRequestAdapter extends RecyclerView.Adapter<DeliveryRequestAdapter.deliveryHolder> {

    ArrayList<DeliveryModel> orderArrayList;
    Context mContext;
    String flag;

    public DeliveryRequestAdapter(ArrayList<DeliveryModel> orderArrayList, Context mContext, String flag) {
        this.orderArrayList = orderArrayList;
        this.mContext = mContext;
        this.flag = flag;
    }

    @NonNull
    @Override
    public deliveryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_delivery_request_adapter,parent,false);
        DeliveryRequestAdapter.deliveryHolder orderlistHolder = new DeliveryRequestAdapter.deliveryHolder(view);
        return orderlistHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull deliveryHolder holder, int position) {

        TextView tv_request_id = holder.tv_request_id;
        TextView tv_from_place = holder.tv_from_place;
        TextView tv_destination_id = holder.tv_destination_id;
        TextView tv_Request_Amount = holder.tv_Request_Amount;
        TextView tv_Request_Weight = holder.tv_Request_Weight;
        TextView tv_Request_items = holder.tv_Request_items;
        TextView tv_request_status = holder.tv_request_status;

        tv_from_place.setText("From:"+orderArrayList.get(position).getD_from());
        tv_destination_id.setText("To:"+orderArrayList.get(position).getD_destination());
        tv_Request_Weight.setText("Weight :"+orderArrayList.get(position).getD_weight());
        tv_Request_items.setText("No of Items: "+orderArrayList.get(position).getD_no_of_items());
        tv_Request_Amount.setText("Amount: "+orderArrayList.get(position).getD_amount());
        tv_request_status.setText("Status: "+orderArrayList.get(position).getD_status());
        tv_request_id.setText(orderArrayList.get(position).getShop_id());

    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }


    public static class  deliveryHolder extends RecyclerView.ViewHolder{

        TextView tv_request_id,tv_from_place,tv_destination_id,tv_Request_Amount,tv_Request_Weight,tv_Request_items,tv_request_status;

        public deliveryHolder(@NonNull View itemView) {
            super(itemView);
            tv_request_id = itemView.findViewById(R.id.tv_request_id);
            tv_from_place = itemView.findViewById(R.id.tv_from_place);
            tv_destination_id = itemView.findViewById(R.id.tv_destination_id);
            tv_Request_Amount = itemView.findViewById(R.id.tv_Request_Amount);
            tv_Request_Weight = itemView.findViewById(R.id.tv_Request_Weight);
            tv_Request_items = itemView.findViewById(R.id.tv_Request_items);
            tv_request_status = itemView.findViewById(R.id.tv_request_status);
        }
    }


}
