package com.podmerchant.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.BrandModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BrandListAdapter extends RecyclerView.Adapter<BrandListAdapter.boyListHolder> {

    ArrayList<BrandModel> arrayList;
    private Context mContext;

    public BrandListAdapter(ArrayList<BrandModel> arrayList, Context mContext) {
        this.arrayList = arrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public BrandListAdapter.boyListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_brand,viewGroup,false);
        BrandListAdapter.boyListHolder boyListHolder = new BrandListAdapter.boyListHolder(view);
        return boyListHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BrandListAdapter.boyListHolder boyListHolder, int position) {
         TextView tv_name = boyListHolder.tv_brandName;
         ImageView imageView = boyListHolder.iv_brandImg;

        tv_name.setText(arrayList.get(position).getBrandName());
        if(arrayList.get(position).getBrandImgUrl().isEmpty()) {
            imageView.setImageResource(R.drawable.ic_account_circle_white_24dp);
        }else {//"https://www.picodel.com/seller/assets/"+
            String imgUrl = arrayList.get(position).getBrandImgUrl();
            Picasso.with(mContext)
                    .load(imgUrl)
                    .error(R.drawable.ic_account_circle_white_24dp)
                    .into(imageView);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class boyListHolder extends RecyclerView.ViewHolder{

          TextView tv_brandName;
          ImageView iv_brandImg;

          public boyListHolder(@NonNull View itemView) {
            super(itemView);
              tv_brandName = itemView.findViewById(R.id.tv_brandName);
              iv_brandImg = itemView.findViewById(R.id.iv_brandImg);

        }
    }
}
