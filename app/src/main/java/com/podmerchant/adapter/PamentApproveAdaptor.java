package com.podmerchant.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.podmerchant.model.PaymentCollectedModel;

import java.util.ArrayList;

public class PamentApproveAdaptor extends RecyclerView.Adapter<PamentApproveAdaptor.paymentViewHolder>{

    ArrayList<PaymentCollectedModel> arrayList;
    Context mcontext;


    public PamentApproveAdaptor(ArrayList<PaymentCollectedModel> arrayList, Context mcontext) {
        this.arrayList = arrayList;
        this.mcontext = mcontext;
    }


    @NonNull
    @Override
    public paymentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }


    @Override
    public void onBindViewHolder(@NonNull paymentViewHolder holder, int position) {

    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public  class paymentViewHolder extends RecyclerView.ViewHolder{

        public paymentViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
