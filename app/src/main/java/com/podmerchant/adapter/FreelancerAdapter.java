package com.podmerchant.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.FreelancerModel;

import java.util.ArrayList;

public class FreelancerAdapter extends RecyclerView.Adapter<FreelancerAdapter.FreelancerViewHolder>{

    ArrayList<FreelancerModel> freelancerModelArrayList;
    Context mContext;

    public FreelancerAdapter(ArrayList<FreelancerModel> freelancerModelArrayList, Context mContext) {
        this.freelancerModelArrayList = freelancerModelArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public FreelancerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_freelancer,parent,false);
        FreelancerAdapter.FreelancerViewHolder freelancerViewHolder = new FreelancerAdapter.FreelancerViewHolder(view);
        return freelancerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FreelancerViewHolder freelancerViewHolder, int position) {
        TextView tv_areaName = freelancerViewHolder.tv_areaName;
        //TextView tv_freelancerReason = freelancerViewHolder.tv_freelancerReason;

        tv_areaName.setText(freelancerModelArrayList.get(position).getAreaName());
        //tv_freelancerReason.setText(freelancerModelArrayList.get(position).getNoOfRejection());
    }

    @Override
    public int getItemCount() {
        return freelancerModelArrayList.size();
    }

    public static class FreelancerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_areaName,tv_freelancerReason;
        public FreelancerViewHolder(@NonNull View itemView) {
              super(itemView);
            tv_areaName = itemView.findViewById(R.id.tv_areaName);
            //tv_freelancerReason = itemView.findViewById(R.id.tv_freelancer_reason);
        }
    }
}
