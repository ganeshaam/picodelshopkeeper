package com.podmerchant.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.podmerchant.R;

class ImagesListAdaptor extends RecyclerView.Adapter<ImagesListAdaptor.MyViewHolder> {


    @NonNull
    @Override
    public ImagesListAdaptor.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_shop_images, viewGroup, false);

        return new ImagesListAdaptor.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesListAdaptor.MyViewHolder myViewHolder, int i) {


    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView cardImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cardImage = (ImageView) itemView.findViewById(R.id.img_cardimage);
        }
    }

}
