package com.podmerchant.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.model.OrderlistModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class OrderlistAdapter extends RecyclerView.Adapter<OrderlistAdapter.OrderlistHolder> {

    ArrayList<OrderlistModel> orderArrayList;
    Context mContext;

    public OrderlistAdapter(ArrayList<OrderlistModel> orderArrayList, Context mContext) {
        this.orderArrayList = orderArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public OrderlistAdapter.OrderlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_order_list_items,parent,false);
        OrderlistAdapter.OrderlistHolder orderlistHolder = new OrderlistAdapter.OrderlistHolder(view);
        return orderlistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderlistAdapter.OrderlistHolder orderlistHolder, int position) {
            TextView tv_orderid = orderlistHolder.tv_orderid;
            TextView tv_status= orderlistHolder.tv_status;
            TextView tv_orderaddress= orderlistHolder.tv_orderaddress;
            TextView tv_orderdatetime= orderlistHolder.tv_orderdatetime;
            ImageView iv_location = orderlistHolder.iv_location;
            View  viewOrderStatus = orderlistHolder.viewOrderStatus;
            CardView cv_order = orderlistHolder.cv_order_list;
            RelativeLayout rl_layout_item = orderlistHolder.rl_order_item;
            //,tv_orderdistance= orderlistHolder.tv_orderid;

        if(orderArrayList.get(position).getColor_flag().equalsIgnoreCase("yes")) {
            orderlistHolder.rl_order_item.setBackgroundColor(Color.parseColor("#E0E0E0"));
            //orderlistHolder.rl_order_item.setBackgroundColor(Color.parseColor("#FF9800"));
        }

            orderlistHolder.cv_order_list.setVisibility(View.VISIBLE);
            tv_orderid.setText("OrderId: " + orderArrayList.get(position).getOrder_id());
            tv_status.setText(orderArrayList.get(position).getStatus());
            tv_orderdatetime.setText(orderArrayList.get(position).getOrder_date());
            if (orderArrayList.get(position).getStatus().equals("Delivered")) {
                tv_orderaddress.setVisibility(View.GONE);
                iv_location.setVisibility(View.GONE);
                viewOrderStatus.setBackgroundColor(Color.parseColor("#7d8472"));
            } else if (orderArrayList.get(position).getAdmin_approve().equals("Not Approved")) {
                viewOrderStatus.setBackgroundColor(Color.parseColor("#e62f2d"));
                orderlistHolder.rl_order_item.setBackgroundColor(Color.parseColor("#ffffff"));
            } else if (orderArrayList.get(position).getAdmin_approve().equals("Approved")) {

                if (orderArrayList.get(position).getAssign_delboy_id().equals("0")) {
                    viewOrderStatus.setBackgroundColor(Color.parseColor("#fbf100"));
                }
            }
            tv_orderaddress.setText(orderArrayList.get(position).getAddress2() + " " + orderArrayList.get(position).getArea());




    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }

    public static class OrderlistHolder extends RecyclerView.ViewHolder {
        TextView tv_orderid,tv_status,tv_orderaddress,tv_orderdatetime;//,tv_orderdistance
        ImageView iv_location;
        View viewOrderStatus;
        CardView cv_order_list;
        RelativeLayout rl_order_item;
        public OrderlistHolder(@NonNull View itemView) {
            super(itemView);
            tv_orderid = itemView.findViewById(R.id.tv_orderid);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_orderaddress = itemView.findViewById(R.id.tv_orderaddress);
            tv_orderdatetime = itemView.findViewById(R.id.tv_orderdatetime);
            iv_location = itemView.findViewById(R.id.iv_location);
            viewOrderStatus = itemView.findViewById(R.id.view);
            cv_order_list = itemView.findViewById(R.id.cv_order_list);
            rl_order_item = itemView.findViewById(R.id.rl_order_item);
            //,tv_orderdistance = itemView.findViewById(R.id.tv_orderid);
        }
    }


}
