package com.podmerchant.activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.PaymentModeAdaptor;
import com.podmerchant.model.PaymentModeModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class OrderDeliverdActivity extends AppCompatActivity {

    AlertDialog progressDialog;
    Context mContext;
    ArrayList <PaymentModeModel> paymentModeList;
    RecyclerView rv_paymentMode;
    Button btn_deliverd_status;
    String orderId="",shopID="",delboyid="",Selected_PaymentMode="",payID="0";
    PaymentModeAdaptor paymentModeAdaptor;
    LinearLayoutManager layoutManager;
    TextView tv_selectedvalue;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_deliverd);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_select_paymentmode);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = OrderDeliverdActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        rv_paymentMode = findViewById(R.id.rv_paymentMode);
        btn_deliverd_status = findViewById(R.id.btn_deliverd_status);
        tv_selectedvalue = findViewById(R.id.tv_selectedvalue);

        rv_paymentMode.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        paymentModeList = new ArrayList<>();

        rv_paymentMode.setLayoutManager(layoutManager);
        rv_paymentMode.setItemAnimator(new DefaultItemAnimator());
        paymentModeAdaptor = new PaymentModeAdaptor(mContext,paymentModeList);
        rv_paymentMode.setAdapter(paymentModeAdaptor);

        paymentModeList();

        Intent intent = getIntent();
        orderId= intent.getStringExtra("orderid");
        delboyid= intent.getStringExtra("delboyid");
        Log.e("commite test","ok");

        rv_paymentMode.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_paymentMode,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                       //  delBoyId =  paymentModeList.get(position).getDelBoyId();
                        Selected_PaymentMode= paymentModeList.get(position).getPaymentType();
                        payID= paymentModeList.get(position).getID();
                        Toast.makeText(mContext,paymentModeList.get(position).getPaymentType(),Toast.LENGTH_SHORT).show();
                        tv_selectedvalue.setText("Payment Mode:"+Selected_PaymentMode);
                        //paymentModeAdaptor.notifyDataSetChanged();
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        btn_deliverd_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(Selected_PaymentMode.equals("")){
                    Toast.makeText(mContext,"Please select Payment Mode",Toast.LENGTH_LONG).show();
               }else {
                   DeliverybyandPaymentMode();
               }
            }
        });

    }


    //Get paymentmode Api method
    public void paymentModeList(){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            //String urlAcceptOrder = StaticUrl.getPaymentMode;
            String urlAcceptOrder = StaticUrl.getPaymentMode+"?user="+"shop";

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONArray jsonArray =  jsonObject.getJSONArray("data");

                                    for (int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        PaymentModeModel paymentMode_model = new PaymentModeModel();
                                        if(sharedPreferencesUtils.getAdminType().equals("Delivery boy")){
                                            paymentMode_model.setPaymentType(jsonObject1.getString("payment_type"));
                                            paymentMode_model.setPayment_details(" ");
                                        }else {
                                            paymentMode_model.setPaymentType(jsonObject1.getString("payment_type"));
                                            paymentMode_model.setPayment_details(" ");
                                        }

                                        paymentModeList.add(paymentMode_model);
                                    }
                                    paymentModeAdaptor.notifyDataSetChanged();

                                  //  Toast.makeText(mContext,"Payment List Successfully fetched",Toast.LENGTH_SHORT).show();
                                }else if(status=false){
                                    Toast.makeText(mContext,"Sorry. No PaymentMode Found.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //Set Deliver status Api method
    public void DeliverybyandPaymentMode(){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.setdeliverstatus
                    +"?order_id="+orderId
                    +"&payment_type="+ URLEncoder.encode(Selected_PaymentMode)
                    +"&tblName="+sharedPreferencesUtils.getAdminType()
                    +"&shop_id"+sharedPreferencesUtils.getShopID()
                    +"&assign_delboy_id="+delboyid;
            Log.e("OrderDeliver",""+urlAcceptOrder);

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e("OrderDeliverRes",""+response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext,"Order Delivered Successfully",Toast.LENGTH_SHORT).show();
                                    if(sharedPreferencesUtils.getAdminType().equals("Delivery boy")) {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        intent.putExtra("flag", "assigned_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }else {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        intent.putExtra("flag", "new_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }else if(status==false){
                                    Toast.makeText(mContext,"Order doesn't Delivered.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  OrderDeliverdActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
