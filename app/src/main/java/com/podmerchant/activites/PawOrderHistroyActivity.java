package com.podmerchant.activites;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.OrderlistAdapter;
import com.podmerchant.model.OrderlistModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;

public class PawOrderHistroyActivity extends AppCompatActivity  {

    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    private String cancel;
    private TextView tvRatingValue, tvRatingMessage;
    private EditText etOrderNo, etRemark;
    private AlertDialog alertDialog;
    private Spinner cancelSpinner;
    private RecyclerView recyclerView;
    private CustomAdapter customAdapter;
    private RelativeLayout relativeLayoutEmptyOrders;
    private final ArrayList<String> cancelSpinnerList = new ArrayList<>();
    private final String class_name = this.getClass().getSimpleName();
    private LinkedHashSet<GateWay.ReviewPOJO> review = new LinkedHashSet<>();
    private MyOrder homePageActivity;
    private ProgressBar simpleProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paw_order_histroy);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getText(R.string.title_order_histroy));

        mContext = PawOrderHistroyActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        relativeLayoutEmptyOrders =  findViewById(R.id.relativeLayoutEmptyOrders);
        recyclerView = findViewById(R.id.orders_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PawOrderHistroyActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

        Log.e("testConnectivity",""+sharedPreferencesUtils.getPhoneNumber());
        if(Connectivity.isConnected(mContext)) {
            getManageOrderResults();
        }else{
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
     }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                relativeLayoutEmptyOrders.setVisibility(View.GONE);
                //Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                MyOrder.txtNoConnection.setText("No connection");
                MyOrder.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(MyOrder.txtNoConnection);
            } else {
                // Main_Layout_NoInternet.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                MyOrder.txtNoConnection.setText("Back online");
                MyOrder.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(MyOrder.txtNoConnection);

                //getCancelSpinnerDetails();
                //getManageOrderResults();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void getManageOrderResults() { //TODO Server method here

        if (Connectivity.isConnected(PawOrderHistroyActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(PawOrderHistroyActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            customAdapter = new CustomAdapter();

            String strContact = sharedPreferencesUtils.getPhoneNumber();//gateWay.getContact();
            String strPawMode = sharedPreferencesUtils.getPawMode();

            JSONObject params = new JSONObject();
            try {
                params.put("contact_no", strContact);
                params.put("pawmode", strPawMode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST,
                    StaticUrl.urlPawOrderHistroy ,
                    params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            relativeLayoutEmptyOrders.setVisibility(View.VISIBLE);
                        } else {
                            relativeLayoutEmptyOrders.setVisibility(View.GONE);
                            JSONArray mainOrderArray = response.getJSONArray("posts");

                            for (int i = 0; i < mainOrderArray.length(); i++) {
                                JSONObject jSonMyOrderData = mainOrderArray.getJSONObject(i);

                                InnerMovie innerMovie = new InnerMovie(jSonMyOrderData.getString("order_id"), jSonMyOrderData.getString("payment_method"),
                                        jSonMyOrderData.getString("status"), jSonMyOrderData.getString("total_amount"),
                                        jSonMyOrderData.getString("discount"), jSonMyOrderData.getString("delivery_charges"),
                                        jSonMyOrderData.getString("total_items"), jSonMyOrderData.getString("order_date"),
                                        jSonMyOrderData.getString("email"), jSonMyOrderData.getString("contactNo"),
                                        jSonMyOrderData.getString("delivery_date"), jSonMyOrderData.getString("delivery_time"),
                                        jSonMyOrderData.getString("remark"), jSonMyOrderData.getString("rating"),
                                        "pawname","pawmobile");
                                customAdapter.add(innerMovie);
                            }
                            recyclerView.setAdapter(customAdapter);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(PawOrderHistroyActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(PawOrderHistroyActivity.this).addToRequestQueue(request);
        }
    }

     /*feed back alert
     private void feedBackAlertDialog(String methodOrderId) {
         GateWay gateWay = new GateWay(getActivity());
         review = gateWay.fetchReviewData();

         LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
         View order_confirmView = layoutInflater.inflate(R.layout.dialog_feedback_form_of_my_order, null);
         final androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
         alertDialogBuilder.setView(order_confirmView);
         etOrderNo = order_confirmView.findViewById(R.id.txtOrder);
         etRemark = order_confirmView.findViewById(R.id.editOrderRemark);
         RatingBar ratingBar = order_confirmView.findViewById(R.id.ratingBar);
         tvRatingValue = order_confirmView.findViewById(R.id.txtRatingValue);
         tvRatingMessage = order_confirmView.findViewById(R.id.txtRatingMessage);

         etOrderNo.setText(methodOrderId);
         alertDialogBuilder
                 .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int id) {
                     }
                 }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int id) {
                 dialog.dismiss();
             }
         });
         ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
             @Override
             public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                 tvRatingValue.setText(String.valueOf(rating));
                 for (GateWay.ReviewPOJO reviewPOJO : review) {
                     if (reviewPOJO.getRating().equals(String.valueOf(rating))) {
                         if (reviewPOJO.getReason().equals("")) {
                             tvRatingMessage.setVisibility(View.INVISIBLE);
                             tvRatingMessage.setText("");
                         } else {
                             tvRatingMessage.setVisibility(View.VISIBLE);
                             tvRatingMessage.setText(reviewPOJO.getReason());
                             tvRatingMessage.setTextColor(ContextCompat.getColor(getActivity(), reviewPOJO.getColor()));
                         }
                     }
                 }
             }
         });
         // create alert dialog
         alertDialog = alertDialogBuilder.create();
         // show it
         alertDialog.show();

         Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
         positiveButton.setTextColor(Color.parseColor("#3F51B5"));

         positiveButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 String remark = etRemark.getText().toString();
                 if (remark.matches("")) {
                     Toast.makeText(getActivity(), "Please give feedback/suggestions to improve our services.", Toast.LENGTH_LONG).show();
                 } else {
                     alertDialog.dismiss();
                     submitOrderRemark();
                 }
             }
         });
     }
      */
     /*cancel order alert
     private void cancelOrderAlertDialog(final String cancelOrderId, final String cancelOrderStatus) {
         LayoutInflater layoutInflater = LayoutInflater.from(PawOrderHistroyActivity.this);
         View cancelView = layoutInflater.inflate(R.layout.dialog_cancel_order_of_my_order, null);
         androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(PawOrderHistroyActivity.this);
         builder.setView(cancelView);
         cancelSpinner = cancelView.findViewById(R.id.cancelSpinner);
         try {
             if (cancelSpinnerList.size() > 0) {
                 ArrayAdapter<String> staticAdapter = new ArrayAdapter<>(getActivity(), R.layout.multi_line_spinner, cancelSpinnerList);
                 staticAdapter.setDropDownViewResource(R.layout.multi_line_spinner);
                 cancelSpinner.setAdapter(staticAdapter);
             }
         } catch (Exception e) {
             e.printStackTrace();
         }
         builder.setTitle("Cancel Order");
         builder
                 .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int id) {
                         submitCancelOrderReason(cancelOrderId, cancelOrderStatus);
                     }
                 }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int id) {
                 dialog.dismiss();
             }
         });

         alertDialog = builder.create();
         alertDialog.show();

         final Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
         positiveButton.setVisibility(View.GONE);

         cancelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 cancel = (String) cancelSpinner.getSelectedItem();
                 if (!cancel.equals("Reason for cancellation?")) {
                     positiveButton.setVisibility(View.VISIBLE);
                 } else {
                 }
             }

             @Override
             public void onNothingSelected(AdapterView<?> parent) {
             }
         });
     }
     */
     //Adapter
    private class CustomAdapter extends RecyclerView.Adapter<ViewHolder> {

        final ArrayList<InnerMovie> mItems = new ArrayList<>();

        CustomAdapter() {
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_paw_orders, parent, false);
            final ViewHolder viewHolder = new ViewHolder(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(PawOrderHistroyActivity.this, MyOrderDetails.class);
//                    intent.putExtra("order_id", viewHolder.tvOrderId.getText().toString());
//                    startActivity(intent);
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            InnerMovie movie = mItems.get(position);
            try {
                holder.tvOrderStatus.setText(movie.getStatus());
                holder.tvOrderId.setText(movie.getOrder_id());
                holder.tvPaymentType.setText(movie.getPayment_method());
                holder.tvTotalItems.setText(movie.getTotal_items());
                holder.tvGrandTotal.setText(movie.getTotal_amount());
                holder.tvDiscount.setText(movie.getDiscount());
                holder.tvContact.setText(movie.getContactNo());
                holder.tvEmail.setText(movie.getEmail());
                String status = (String) holder.tvOrderStatus.getText();
                if (status.equals("Pending")||status.equals(" ")) {
                    if (movie.getRemark().equals("") && movie.getUser_rating().equals("")) {
                        holder.feedbackImg.setVisibility(View.VISIBLE);
                    } else {
                        holder.feedbackImg.setVisibility(View.GONE);
                    }
                    holder.tvOrderPlace.setText("Placed On:");
                    holder.tvOrderDate.setText(movie.getOrder_date());
                    holder.tvCancel.setVisibility(View.VISIBLE);
                    holder.tvOrderStatus.setTextColor(Color.parseColor("#ffab00"));
                } else if(status.equals("Delivered")){
                    holder.tvCancel.setVisibility(View.GONE);
                    //holder.tvCancel.setVisibility(View.VISIBLE);
                }else {
                    holder.tvCancel.setVisibility(View.VISIBLE);
                }
                if (status.equals("Reject by User")) {
                    if (movie.getRemark().equals("") && movie.getUser_rating().equals("")) {
                        holder.feedbackImg.setVisibility(View.VISIBLE);
                    } else {
                        holder.feedbackImg.setVisibility(View.GONE);
                    }
                    holder.tvOrderPlace.setText("Cancelled On:");
                    holder.tvOrderDate.setText(movie.getOrder_date());
                    holder.tvOrderStatus.setTextColor(Color.parseColor("#F44336"));
                }
                if (status.equals("Delivered")) {
                    if (movie.getRemark().equals("") && movie.getUser_rating().equals("")) {
                        holder.feedbackImg.setVisibility(View.VISIBLE);
                    } else {
                        holder.feedbackImg.setVisibility(View.GONE);
                    }
                    holder.tvOrderPlace.setText("Placed On:");
                    holder.tvOrderDate.setText(movie.getOrder_date());
                    holder.tvOrderStatus.setTextColor(Color.parseColor("#4caf50"));
                }
                holder.tvDeliveryDateTime.setText(movie.getDelivery_date() + " / " + movie.getDelivery_time());
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.feedbackImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // feedBackAlertDialog(holder.tvOrderId.getText().toString());
                }
            });

            holder.tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 //   cancelOrderAlertDialog(holder.tvOrderId.getText().toString(), holder.tvOrderStatus.getText().toString());
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(InnerMovie movie) {
            mItems.add(movie);
        }

    }
    //View Holder
    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvOrderDate;
        final TextView tvOrderId;
        final TextView tvOrderStatus;
        final TextView tvPaymentType;
        final TextView tvTotalItems;
        final TextView tvGrandTotal;
        final TextView tvDiscount;
        final TextView tvEmail;
        final TextView tvContact;
        final TextView tvCancel;
        final TextView tvOrderPlace;
        final TextView tvDeliveryDateTime;
        final ImageView feedbackImg;

        public ViewHolder(View itemView) {
            super(itemView);
            tvOrderDate = itemView.findViewById(R.id.txtOrderDate);
            tvOrderId = itemView.findViewById(R.id.txtOrderId);
            tvOrderStatus = itemView.findViewById(R.id.txtStatus);
            tvPaymentType = itemView.findViewById(R.id.txtPaymentType);
            tvTotalItems = itemView.findViewById(R.id.txtTotalItems);
            tvGrandTotal = itemView.findViewById(R.id.txtGrandTotal);
            tvDiscount = itemView.findViewById(R.id.txtSavings);
            tvEmail = itemView.findViewById(R.id.txtEmail);
            tvContact = itemView.findViewById(R.id.txtContactNo);
            tvCancel = itemView.findViewById(R.id.txtCancel);
            tvDeliveryDateTime = itemView.findViewById(R.id.txtDeliveryDateTime);
            tvOrderPlace = itemView.findViewById(R.id.txtOrderPlace);
            feedbackImg = itemView.findViewById(R.id.feedBackImg);
            feedbackImg.setTag(this);
            tvCancel.setTag(this);
            tvOrderPlace.setTag(this);
            tvOrderDate.setTag(this);
        }
    }
    //Model Class
    private class InnerMovie {

        final String order_id;
        final String payment_method;
        final String status;
        final String total_amount;
        final String discount;
        final String delivery_charges;
        final String total_items;
        final String order_date;
        final String email;
        final String contactNo;
        final String delivery_date;
        final String delivery_time;
        final String remark;
        final String user_rating;
        final String paw_user_name;
        final String paw_user_mobileno;

        InnerMovie(String order_id, String payment_method, String status, String total_amount, String discount,
                   String delivery_charges, String total_items, String order_date, String email, String contactNo,
                   String delivery_date, String delivery_time, String remark, String user_rating, String paw_user_name,
                   String paw_user_mobileno) {
            this.order_id = order_id;
            this.payment_method = payment_method;
            this.status = status;
            this.total_amount = total_amount;
            this.discount = discount;
            this.delivery_charges = delivery_charges;
            this.total_items = total_items;
            this.order_date = order_date;
            this.email = email;
            this.contactNo = contactNo;
            this.delivery_date = delivery_date;
            this.delivery_time = delivery_time;
            this.remark = remark;
            this.user_rating = user_rating;
            this.paw_user_name = paw_user_name;
            this.paw_user_mobileno = paw_user_mobileno;
        }

        public String getOrder_id() {
            return order_id;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public String getStatus() {
            return status;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public String getDiscount() {
            return discount;
        }

        public String getTotal_items() {
            return total_items;
        }

        public String getOrder_date() {
            return order_date;
        }

        public String getEmail() {
            return email;
        }

        public String getContactNo() {
            return contactNo;
        }

        public String getDelivery_date() {
            return delivery_date;
        }

        public String getDelivery_time() {
            return delivery_time;
        }

        public String getRemark() {
            return remark;
        }

        public String getUser_rating() {
            return user_rating;
        }

        public String getDelivery_charges() {
            return delivery_charges;
        }

        public String getPaw_user_name() {
            return paw_user_name;
        }

        public String getPaw_user_mobileno() {
            return paw_user_mobileno;
        }
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private PawOrderHistroyActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final PawOrderHistroyActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
