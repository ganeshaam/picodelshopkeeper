package com.podmerchant.activites;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class RechargeOTP_VerifyActivity extends AppCompatActivity {

    Context mContext;
    String firebaseTokan,emailid,moibleno,vcode,vdate,shop_id,shop_latitude,shop_longitude,shop_distanceKm,seller_name,admin_type,shop_status,
            deliveryboyID="0",managerID="0",shopName,shopAdminType;
    ProgressDialog progressDialog;
    Button btn_verifyotp;
    TextView tv_resend;
    EditText et_verifyotp;
    SharedPreferencesUtils sharedPreferencesUtils;
    Locale local;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        mContext = RechargeOTP_VerifyActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        btn_verifyotp = findViewById(R.id.btn_verifyotp);
        tv_resend = findViewById(R.id.tv_resend);
        et_verifyotp = findViewById(R.id.et_verifyotp);
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new ProgressDialog(mContext);

        Resources resources = getResources();
        local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        Intent intent = getIntent();
        if(intent!=null){
            emailid = intent.getStringExtra("email");
            moibleno = intent.getStringExtra("contact");
            vcode = intent.getStringExtra("vcode");
            vdate = intent.getStringExtra("vdate");
        }

        btn_verifyotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyOtp();
            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reSendOtp();
            }
        });


    }

    public void verifyOtp(){

       /* progressDialog.setMessage("Please wait...");
         progressDialog.setCancelable(false);
         progressDialog.show();*/

        if(et_verifyotp.getText().toString().equals("")) {
            //  progressDialog.dismiss();
            Toast.makeText(mContext,"Please Enter OTP", Toast.LENGTH_SHORT).show();
        }else {
            if(vcode.equals(et_verifyotp.getText().toString().trim())){
                // progressDialog.dismiss();

                Intent otp_intend = new Intent(RechargeOTP_VerifyActivity.this,PaymentActivity.class);
                startActivity(otp_intend);

                //finish();
            }else {
                Toast.makeText(mContext,"Invalid OTP", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //send otp
    public void reSendOtp(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String urlOtpsend = StaticUrl.sendotp
                    +"?mobileno="+moibleno
                    +"&email="+emailid;

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");

                                    Intent intent = new Intent(mContext,OtpVerifyActivity.class);
                                    intent.putExtra("vcode",vcode);
                                    intent.putExtra("email",email);
                                    intent.putExtra("contact",contact);
                                    intent.putExtra("vdate",vdate);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    //finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

}
