package com.podmerchant.activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.RechargePlanAdapter;
import com.podmerchant.model.RechargePlanModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class RechargeActivity extends AppCompatActivity {

    String rechargeFrom,rechargePlanId="",rechargePoints,rechargeAmount,rechargePlanName,rechargeAfter_gst;
    Button btn_next_recharge;
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;
    ArrayList<RechargePlanModel> planModelArrayList;
    private RecyclerView.LayoutManager layoutManager;
    private RechargePlanAdapter rechargePlanAdapter;
    RecyclerView rv_recharge_planList;
    AlertDialog progressDialog;
    TextView tv_network_con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.recharge_plan_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = RechargeActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        btn_next_recharge = findViewById(R.id.btn_next_recharge);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        rv_recharge_planList = findViewById(R.id.rv_recharge_planlist);
        tv_network_con  = findViewById(R.id.tv_network_con);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        final Intent intent = getIntent();
        rechargeFrom = intent.getStringExtra("rechargeFrom");

        rv_recharge_planList.setHasFixedSize(true);
        planModelArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        rv_recharge_planList.setLayoutManager(layoutManager);
        rv_recharge_planList.setItemAnimator(new DefaultItemAnimator());
        rechargePlanAdapter = new RechargePlanAdapter(mContext,planModelArrayList);
        rv_recharge_planList.setAdapter(rechargePlanAdapter);

        btn_next_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rechargePlanId.equals("")){
                    Toast.makeText(mContext,"Select Plan",Toast.LENGTH_SHORT).show();
                }else {
                    Intent paymentIntent = new Intent(mContext, PaymentActivity.class);
                    paymentIntent.putExtra("rechargePlanId", rechargePlanId);
                    paymentIntent.putExtra("rechargePoints", rechargePoints);
                    paymentIntent.putExtra("rechargeAmount", rechargeAmount);
                    paymentIntent.putExtra("rechargePlanName", rechargePlanName);
                    paymentIntent.putExtra("flagPayment", "recharge");
                    startActivity(paymentIntent);
                }
            }
        });

        if(Connectivity.isConnected(mContext)){
            getPlanList();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        //order details
        rv_recharge_planList.addOnItemTouchListener(new RechargeActivity.RecyclerTouchListener(mContext, rv_recharge_planList,
                new RechargeActivity.RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        rechargePlanId =  planModelArrayList.get(position).getId();
                        rechargePoints =  planModelArrayList.get(position).getPoints();
                        rechargeAmount =  planModelArrayList.get(position).getRecharge();
                        rechargePlanName =  planModelArrayList.get(position).getPlanName();
                        rechargeAfter_gst =  planModelArrayList.get(position).getAfterGst();
                        Toast.makeText(mContext,planModelArrayList.get(position).getPlanName(),Toast.LENGTH_SHORT).show();
                        /*Intent intent = new Intent();
                        Log.d("delboyid",boyModelArrayList.get(position).getDelBoyId());
                        intent.putExtra("delBoyId", position);
                        setResult(RESULT_OK, intent);
                        finish();*/

                        Intent paymentIntent = new Intent(mContext, PaymentActivity.class);
                        paymentIntent.putExtra("rechargePlanId", rechargePlanId);
                        paymentIntent.putExtra("rechargePoints", rechargePoints);
                        paymentIntent.putExtra("rechargeAmount", rechargeAmount);
                        paymentIntent.putExtra("rechargePlanName", rechargePlanName);
                        paymentIntent.putExtra("flagPayment", "recharge");
                        startActivity(paymentIntent);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    //recharge list
    public void getPlanList() {

        progressDialog.show();
        planModelArrayList.clear();

        String targetUrlOrderlist = StaticUrl.getrechargeplan+"?adminType="+sharedPreferencesUtils.getAdminType();
        Log.d("planlist",targetUrlOrderlist);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus=true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        RechargePlanModel model = new RechargePlanModel();
                                        model.setId(olderlist.getString("id"));
                                        model.setPlanName(olderlist.getString("plan"));
                                        model.setPoints(olderlist.getString("points"));
                                        model.setRecharge(olderlist.getString("amount"));
                                        model.setAfterGst(olderlist.getString("after_gst"));
                                        model.setBgcolor(olderlist.getString("bgcolor"));
                                        planModelArrayList.add(model);
                                    }
                                    rechargePlanAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }
    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RechargeActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RechargeActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
  /*  @Override
    public void onBackPressed() {
            super.onBackPressed();
        }*/
}
