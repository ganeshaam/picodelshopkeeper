package com.podmerchant.activites;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.DeliveryBoyListAdaptor;
import com.podmerchant.model.DeliveryBoyModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleyMultipartRequest;
import com.podmerchant.util.VolleySingleton;
import android.app.AlertDialog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class AddDeliveryBoyActivity extends AppCompatActivity {

    String areaName,aam_delivery_charge,updatedat,showcart, pincode, flag, action_time_intent,action_time, orderId, cartUniqueId, userId, cartUserId, shipping_address, latitude, longitude, total_amount, order_date, address_id, assign_delboy_id, reason_of_rejection,delivery_time="", address1, address2, address3, address4, address5, area,delivery_date,delivery_time_get,delivery_method,payment_option,user_fname;
    Button btn_add_deliveryboy,btn_assignDeliveryBoy;
    EditText et_delivery_boyname,et_delivery_contact;
    Context mContext;
    String mRequestBody="",assignOrder,delBoyId,orderid,delBoyName;
    public static final String TAG = "AddDeliveryBoyActivity";
    String MobilePattern = "[0-9]{10}";
    FloatingActionButton fb_boys_list;
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    ArrayList<DeliveryBoyModel> boyModelArrayList;
    RecyclerView recycler_DeliveryBoys;
    private RecyclerView.LayoutManager layoutManager;
    private DeliveryBoyListAdaptor boyListAdaptor;
    public static final int PICK_IMAGE = 1,CAPTURE_IMG=2;
    Bitmap delivery_boybitmap=null;
    ImageView Deliveryboy_image;
    CardView cv_bottomlayout;

    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_delivery_boy);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_delivery_boy);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = AddDeliveryBoyActivity.this;

        initView();
        //Code to check permission in the above marshmallow versions
        if (!hasPermissions(AddDeliveryBoyActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(AddDeliveryBoyActivity.this, PERMISSIONS, PERMISSION_ALL);
        }

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        btn_assignDeliveryBoy = findViewById(R.id.btn_assignDeliveryBoy);
        //recycler_DeliveryBoys.setHasFixedSize(true);
        boyModelArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        recycler_DeliveryBoys.setLayoutManager(layoutManager);
        boyListAdaptor = new DeliveryBoyListAdaptor(boyModelArrayList,mContext);
        recycler_DeliveryBoys.setAdapter(boyListAdaptor);

        Intent intent = getIntent();
        assignOrder  = intent.getStringExtra("assignOrder");


        if(assignOrder.equals("assignOrder")){
            orderid = intent.getStringExtra("orderid");
            action_time = intent.getStringExtra("action_time");

            cartUniqueId = intent.getStringExtra("cartUniqueId");
            userId = intent.getStringExtra("userId");
            cartUserId = intent.getStringExtra("cartUserId");
            //shipping_address = intent.getStringExtra("shipping_address");
            latitude = intent.getStringExtra("latitude");
            longitude = intent.getStringExtra("longitude");
            total_amount = intent.getStringExtra("total_amount");
            order_date = intent.getStringExtra("order_date");
            address_id = intent.getStringExtra("address_id");
            assign_delboy_id = intent.getStringExtra("assign_delboy_id");
            action_time_intent = intent.getStringExtra("action_time");
            address1 = intent.getStringExtra("address1");
            address2 = intent.getStringExtra("address2");
            address3 = intent.getStringExtra("address3");
            address4 = intent.getStringExtra("address4");
            address5 = intent.getStringExtra("address5");
            area = intent.getStringExtra("area");
            pincode = intent.getStringExtra("pincode");
            flag = intent.getStringExtra("flag");
            showcart = intent.getStringExtra("showcart");
            updatedat = intent.getStringExtra("updatedat");
            aam_delivery_charge = intent.getStringExtra("aam_delivery_charge");
            areaName = intent.getStringExtra("areaName");
            delivery_date = intent.getStringExtra("delivery_date");
            delivery_time_get = intent.getStringExtra("delivery_time");
            delivery_method = intent.getStringExtra("delivery_method");
            payment_option = intent.getStringExtra("payment_option");


            btn_assignDeliveryBoy.setVisibility(View.VISIBLE);
            cv_bottomlayout.setVisibility(View.VISIBLE);
        }

        fb_boys_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog();
            }
        });

        getDeliveryBoyDetails();

        //order details
        recycler_DeliveryBoys.addOnItemTouchListener(new RecyclerTouchListener(mContext, recycler_DeliveryBoys,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        delBoyId =  boyModelArrayList.get(position).getDelBoyId();
                        delBoyName  =  boyModelArrayList.get(position).getName();
                        Toast.makeText(mContext,boyModelArrayList.get(position).getName(),Toast.LENGTH_SHORT).show();
                      /*Intent intent = new Intent();
                        Log.d("delboyid",boyModelArrayList.get(position).getDelBoyId());
                        intent.putExtra("delBoyId", position);
                        setResult(RESULT_OK, intent);
                        finish();*/
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                       // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        btn_assignDeliveryBoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assignOrder(delBoyId,orderid,action_time); //shop_id,email,action_time
            }
        });
    }


    public void initView(){
        btn_add_deliveryboy = findViewById(R.id.btn_add_deliveryboy);
        et_delivery_boyname = findViewById(R.id.et_delivery_boyname);
        et_delivery_contact = findViewById(R.id.et_delivery_contact);
        recycler_DeliveryBoys = findViewById(R.id.recycler_DeliveryBoys);
        fb_boys_list = findViewById(R.id.fb_boys_list);
        cv_bottomlayout = findViewById(R.id.cv_bottomlayout);
    }

    // using volley multipart
    private void getError (String Error){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddDeliveryBoyActivity.this);
        alertDialogBuilder.setMessage(Error);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void getDeliveryBoyDetails(){
        //get Mobile verify
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.getDeliveryboys+"?shop_id="+sharedPreferencesUtils.getShopID();
            // +"?shop_id="+"0";
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                boyModelArrayList.clear();
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    Log.e("ProfileResponse:",""+jsonArray.toString());

                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        DeliveryBoyModel boyModel = new DeliveryBoyModel();
                                        boyModel.setName(jsonObject1.getString("name"));
                                        boyModel.setMobile_number(jsonObject1.getString("mobile_number"));
                                        boyModel.setShop_id(jsonObject1.getString("shop_id"));
                                        boyModel.setImgeurl(jsonObject1.getString("imageurl"));
                                        boyModel.setDelBoyId(jsonObject1.getString("ID"));
                                        boyModelArrayList.add(boyModel);
                                    }
                                    Log.e("arraylist size", String.valueOf(boyModelArrayList.size()));
                                    boyListAdaptor.notifyDataSetChanged();
                                }else if(status==false) {
                                    Toast.makeText(mContext,"No Records Found",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void select_images(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
    }

    private void capture_image(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAPTURE_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE) {
            //TODO: action
            // Get the url from data
            Uri selectedImageUri = data.getData();

            //for bit map data
            try {
                   /* Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                    shop_photo1.setImageBitmap(thumbnail);
                    shopbitmap=thumbnail;*/
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                Deliveryboy_image.setImageBitmap(bitmap);
                //delivery_boybitmap = bitmap;
                delivery_boybitmap = getResizedBitmap( bitmap,500);
                Log.e("shopbitmap", "" + delivery_boybitmap);
            } catch (Exception e) {
                e.getMessage();
                Log.e("BitmapExp", "" + e.getMessage());
            }
        }

        if(requestCode == CAPTURE_IMG){
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            //thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            thumbnail.compress(Bitmap.CompressFormat.PNG, 75, bytes);

            try {
                Deliveryboy_image.setImageBitmap(thumbnail);
                //delivery_boybitmap = thumbnail;
                delivery_boybitmap = getResizedBitmap(thumbnail,500);
                Log.e("ImageBitCapture:",""+delivery_boybitmap.toString());
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    private  void  customDialog(){
        final Dialog dialog = new Dialog(AddDeliveryBoyActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dailog_custom_adddeliveryboy);

        final EditText name = dialog.findViewById(R.id.et_delivery_boyname);
        final EditText  contact= dialog.findViewById(R.id.et_delivery_contact);
        Deliveryboy_image= dialog.findViewById(R.id.img_deliveryboy);
        final Button  upload = dialog.findViewById(R.id.btn_upload_boy_photo);
        // text.setText(msg);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        Deliveryboy_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*f (delivery_boybitmap != null) {
                    delivery_boybitmap.recycle();
                    delivery_boybitmap = null;
                }*/
                //select_images();
                capture_image();

                if(delivery_boybitmap!=null){
                    Deliveryboy_image.setImageBitmap(delivery_boybitmap);
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_images();
            }
        });

        Button dialogButton2 =  dialog.findViewById(R.id.btn_add_deliveryboy);
        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("ValueName:",""+name.getText().toString());
                Log.e("ValueContact:",""+contact.getText().toString());

                if(name.getText().toString().equalsIgnoreCase("")){
                    name.requestFocus();
                    name.setError("Please Enter Name");
                }/*else if(contact.getText().toString().equalsIgnoreCase("")){
                    contact.requestFocus();
                    contact.setError("Please Enter Contact");
                }else if(!contact.getText().toString().matches(MobilePattern)) {
                    Toast.makeText(mContext,"Please Enter valid Contact",Toast.LENGTH_LONG).show();
                }else */{
                    DeleveryBoyRegistration(name.getText().toString(), contact.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    public void DeleveryBoyRegistration(final String NAME,final  String CONTACT) {

        if(Connectivity.isConnected(mContext)) {

            progressDialog.show();

            String url = StaticUrl.adddeliveryboy;

            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    Log.d("delreg",resultResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e(TAG, "respDeleveryboy" + resultResponse.toString());
                        Log.e(TAG, "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status = true)
                        {
                            JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                            /*shop_id = jsonObjectData.getString("shop_id");
                              seller_name =  jsonObjectData.getString("name");
                              admin_type =  jsonObjectData.getString("admin_type");*/
                             getDeliveryBoyDetails();

                             Toast.makeText(mContext,"Delivery Boy Registered",Toast.LENGTH_LONG).show();

                        }else if(status = false){
                            Log.d(TAG,"status_false"+resultResponse);
                            Toast.makeText(mContext,"Registration Not Done..!!",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleYIMAGE_EXP:", "" + e.getMessage());
                        progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;

                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";

                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<>();
                    params.put("shop_id", sharedPreferencesUtils.getShopID());
                    params.put("name", NAME);
                    params.put("mobile_number", CONTACT);
                    params.put("user_type", "Delivery Boy");
                    params.put("notification_id", "update_del_boy_login");
                    params.put("imageurl", "test_url");

                    Log.e("Params", "" + params);
                    return params;
                }

           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = "#concord$" + ":" + "!@#con co@$rd!@#";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }*/

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("imageurl", new DataPart(getBytesFromBitmap(delivery_boybitmap)));
                    Log.e("ParamsImg", "" + params);
                    return params;
                }
            };

            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        }
        else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  AddDeliveryBoyActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final AddDeliveryBoyActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    public void assignOrder(final String delBoyId,final String orderId, String action_times){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAssignOrder = null;
            try {
                urlAssignOrder = StaticUrl.assignorder
                        +"?order_id="+orderId
                        +"&assign_delboy_id="+delBoyId
                        +"&approveBy="+sharedPreferencesUtils.getEmailId()
                        +"&assign_shop_id="+sharedPreferencesUtils.getShopID()
                        //+"?manager_id=" + sharedPreferencesUtils.getManagerId()
                        +"&action_time="+ URLEncoder.encode(action_times,"utf-8")
                        +"&tblName="+sharedPreferencesUtils.getAdminType()
                        +"&delboyName="+URLEncoder.encode(delBoyName,"utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Log.d("urlAssignOrder",urlAssignOrder);

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAssignOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext,"Order Assigned. Please check in your Your Order list",Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(mContext,OrderDetailActivity.class);
                                    intent.putExtra("flag","assigned_order");
                                    intent.putExtra("action_time", action_time_intent);
                                    intent.putExtra("orderId",orderId);
                                    intent.putExtra("cartUniqueId",cartUniqueId);
                                    intent.putExtra("userId",userId);
                                    intent.putExtra("cartUserId",cartUserId);
                                    intent.putExtra("shipping_address",shipping_address);
                                    intent.putExtra("latitude",latitude);
                                    intent.putExtra("longitude",longitude);
                                    intent.putExtra("total_amount",total_amount);
                                    intent.putExtra("order_date",order_date);
                                    intent.putExtra("address_id",address_id);
                                    intent.putExtra("assign_delboy_id",delBoyId);
                                    intent.putExtra("action_time",action_time);
                                    intent.putExtra("address1",address1);
                                    intent.putExtra("address2",address2);
                                    intent.putExtra("address3",address3);
                                    intent.putExtra("address4",address4);
                                    intent.putExtra("address5",address5);
                                    intent.putExtra("area",area);
                                    intent.putExtra("pincode",pincode);
                                    intent.putExtra("showcart",showcart);
                                    //intent.putExtra("flag",flag);
                                    intent.putExtra("updatedat",updatedat);
                                    intent.putExtra("aam_delivery_charge",aam_delivery_charge);
                                    intent.putExtra("areaName",areaName);
                                    intent.putExtra("delivery_date",delivery_date);
                                    intent.putExtra("delivery_time",delivery_time_get);
                                    intent.putExtra("delivery_method",delivery_method);
                                    intent.putExtra("payment_option",payment_option);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }else if(!status){
                                    Toast.makeText(mContext,"Sorry. This order you can not assign.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

   /*@Override
     public void onBackPressed() {
        super.onBackPressed();
        finish();
    }*/

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();
      switch (id) {
          case android.R.id.home:
              finish();
              return true;
          default:
              return super.onOptionsItemSelected(item);
      }
  }

  public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
