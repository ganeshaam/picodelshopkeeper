package com.podmerchant.activites;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TimePicker;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.podmerchant.R;
import com.podmerchant.util.SharedPreferencesUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {

    Button btn_selectTime;//btn_notiOnOff,
    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    Switch switchNotificaiton;
    TimePicker timePickerStart,timePickerEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getText(R.string.nav_partnetkit_setting));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = SettingsActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        switchNotificaiton = findViewById(R.id.switchNotification);
        //btn_notiOnOff = findViewById(R.id.btn_notiOnOff);


        /*btn_notiOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               sharedPreferencesUtils.setNotification("1");
            }
        });*/
        if(sharedPreferencesUtils.getNotification().equals(0)){
            switchNotificaiton.setChecked(true);
        }else  if(sharedPreferencesUtils.getNotification().equals(1)){
            switchNotificaiton.setChecked(true);
        }

        switchNotificaiton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    sharedPreferencesUtils.setNotification("0");
                    switchNotificaiton.setChecked(true);
                } else {
                    // The toggle is disabled
                    setNotificationTime();
                }
            }
        });

       /* getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this).registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                Log.d("testPreference",s);
                if(s.equals("notifications")){

                }
            }
        });
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }*/
    }
    SimpleDateFormat sdf = new SimpleDateFormat("HH mm", Locale.getDefault());

    public void setNotificationTime(){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notificaiton_time);
        btn_selectTime = dialog.findViewById(R.id.btn_selectTime);
        timePickerStart = dialog.findViewById(R.id.timePickerStart);
        timePickerEnd = dialog.findViewById(R.id.timePickerEnd);
        btn_selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hour_start,hour_end, minute_start,minute_end;
                String am_pm_start,am_pm_end;
                if (Build.VERSION.SDK_INT >= 23 ){
                    hour_start = timePickerStart.getHour();
                    minute_start = timePickerStart.getMinute();

                    hour_end = timePickerEnd.getHour();
                    minute_end = timePickerEnd.getMinute();
                }else{
                    hour_start = timePickerStart.getCurrentHour();
                    minute_start = timePickerStart.getCurrentMinute();

                    hour_end = timePickerEnd.getCurrentHour();
                    minute_end = timePickerEnd.getCurrentMinute();
                }
                // String currentDateandTime = sdf.format(new Date());
                Calendar cal = Calendar.getInstance();
                int millisecond = cal.get(Calendar.MILLISECOND);
                int second = cal.get(Calendar.SECOND);
                int minute = cal.get(Calendar.MINUTE);
                //12 hour format
                int hour = cal.get(Calendar.HOUR);
                //24 hour format
                int hourofday = cal.get(Calendar.HOUR_OF_DAY);
                 Log.d("Start ",hour_start+" "+minute_start+ " End: "+hour_end+" "+minute_end+ " currentDateandTime:"+hourofday+" "+minute);

                /*if(hour_start > 12) {
                    am_pm = "PM";
                    hour = hour - 12;
                }
                else
                {
                    am_pm="AM";
                }*/

                switchNotificaiton.setChecked(false);
                sharedPreferencesUtils.setNotification("1");
                sharedPreferencesUtils.setNotificationHour(hour_end);
                sharedPreferencesUtils.setNotificationMin(minute_end);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}