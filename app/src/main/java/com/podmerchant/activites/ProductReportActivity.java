package com.podmerchant.activites;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.ReportListAdapter;
import com.podmerchant.model.CartitemModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import dmax.dialog.SpotsDialog;

public class ProductReportActivity extends AppCompatActivity {

    RecyclerView recyclerViewCartItems;
    ReportListAdapter reportListAdapter;
    ArrayList<CartitemModel> arrayList;
    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    LinearLayoutManager layoutManager;
    AlertDialog progressDialog;
    TextView tv_selecteddate,tv_selectedenddate,tv_network_con;

    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day,time;
    private String gender,mobile_no;
    private DatePickerDialog datePickerDialog;
    private DatePickerDialog datePickerDialogEndDate;
    ImageView btn_calender,btn_enddate;
    String startDate,endDate;
    Button btn_submit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_report);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Product Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerViewCartItems = findViewById(R.id.recyclerViewCartItems);

        /*for the date layout initialization*/
        btn_calender = findViewById(R.id.btn_calender);
        btn_enddate = findViewById(R.id.btn_enddate);
        tv_selecteddate = findViewById(R.id.tv_selecteddate);
        btn_submit = findViewById(R.id.btn_submit);
        tv_selectedenddate = findViewById(R.id.tv_selectedenddate);
        tv_network_con = findViewById(R.id.tv_network_con);

        mContext = ProductReportActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        arrayList = new ArrayList<>();

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();


        recyclerViewCartItems.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        recyclerViewCartItems.setLayoutManager(layoutManager);
        recyclerViewCartItems.setItemAnimator(new DefaultItemAnimator());
        /*reportListAdapter = new ReportListAdapter(mContext,arrayList);
        recyclerViewCartItems.setAdapter(reportListAdapter);
        reportListAdapter.notifyDataSetChanged()*/;

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        btn_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate();
            }
        });
        btn_enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateEnd();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Connectivity.isConnected(mContext)){
                    DeliveredCartItemsFromServer();
                    //getOrderList(startDate,endDate);//2019-08-13
                }else {
                    tv_network_con.setVisibility(View.VISIBLE);
                }
            }
        });


        DeliveredCartItemsFromServer();


    }

    public void setDate()
    {
        showDialog(999);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }
    public void  setDateEnd()
    {
        showDialog(100);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        // TODO Auto-generated method stub
        if (id == 999)
        {
            datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return datePickerDialog;
        }else  if (id == 100)
        {
            datePickerDialogEndDate = new DatePickerDialog(this, myDateListenerEndDate, year, month, day);
            datePickerDialogEndDate.getDatePicker().setMaxDate(System.currentTimeMillis());
            return datePickerDialogEndDate;
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener()
    {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
        {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            //arg0.setBackgroundColor(red);
            arg0.setMaxDate(System.currentTimeMillis());
            showDate(arg1, arg2+1, arg3);
        }
    };
    private DatePickerDialog.OnDateSetListener myDateListenerEndDate = new DatePickerDialog.OnDateSetListener()
    {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
        {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            //arg0.setBackgroundColor(red);
            arg0.setMaxDate(System.currentTimeMillis());
            showEndDate(arg1, arg2+1, arg3);
        }
    };
    String  months;
    private void showDate(int year, int month, int day)
    {

        tv_selecteddate.setText(day+"-"+month+"-"+year);
        //Date d = new Date(year, month, day);
        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        //String strDate = dateFormatter.format(year,month,day);
        //getOrderList(year+"-"+month+"-"+day);
        startDate=year+"-"+month+"-"+day;
        //getOrderList(year+"-"+month+"-"+day);
    }
    private void showEndDate(int year, int month, int day)
    {
        tv_selectedenddate.setText(new StringBuilder().append(day).append("-").append(month).append("-").append(year));
        //Date d = new Date(year, month, day);
        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        //String strDate = dateFormatter.format(year,month,day);
        //getOrderList(year+"-"+month+"-"+day);
        endDate=year+"-"+month+"-"+(day+1);
        //getOrderList(year+"-"+month+"-"+day);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void DeliveredCartItemsFromServer() { //TODO Server method here
        JSONObject params = new JSONObject();
        progressDialog.show();

         String shop_id = sharedPreferencesUtils.getShopID();

        try {

            params.put("shop_id", shop_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlgetShopWiseProductCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    Log.e("Delivered_CartRes:",""+response);
                    String posts = response.getString("posts");
                    progressDialog.dismiss();



                        try {

                                JSONArray jsonArray = response.getJSONArray("posts");
                                if (jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject cartListObject = jsonArray.getJSONObject(i);
                                        CartitemModel model = new CartitemModel();
                                        model.setBrand_name(cartListObject.getString("product_brand"));
                                        model.setProduct_name(cartListObject.getString("product_name"));
                                        model.setIten_qty(cartListObject.getString("qty"));
                                        model.setProduct_image(cartListObject.getString("product_image"));
                                        model.setProduct_id(cartListObject.getString("product_id"));
                                        model.setProduct_size(cartListObject.getString("product_size"));
                                        arrayList.add(model);

                                    }
                                    reportListAdapter = new ReportListAdapter(mContext,arrayList);
                                    recyclerViewCartItems.setAdapter(reportListAdapter);
                                    reportListAdapter.notifyDataSetChanged();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                GateWay gateWay = new GateWay(mContext);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }
}
