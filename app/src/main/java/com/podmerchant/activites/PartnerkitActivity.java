package com.podmerchant.activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.PartnerkitAdapter;
import com.podmerchant.model.PartnerKitModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class PartnerkitActivity extends AppCompatActivity {

    CheckBox ch_board,ch_scale,ch_bags;
    Button btn_confirm;
    String str_board="0",str_scale="0",str_bags="0";
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;
    AlertDialog progressDialog;
    ArrayList<PartnerKitModel> partnerKitModels;
    RecyclerView rv_partnerkit;
    private PartnerkitAdapter partnerkitAdapter;
    private RecyclerView.LayoutManager layoutManager;
    SparseBooleanArray itemStateArray;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partnerkit);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.nav_partnetkit_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PartnerkitActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
        itemStateArray =new SparseBooleanArray();
         btn_confirm = findViewById(R.id.btn_confirmkit);
        rv_partnerkit = findViewById(R.id.rv_partnerkit);
        rv_partnerkit.setHasFixedSize(true);
        partnerKitModels = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        rv_partnerkit.setLayoutManager(layoutManager);
        rv_partnerkit.setItemAnimator(new DefaultItemAnimator());
        partnerkitAdapter = new PartnerkitAdapter(partnerKitModels,mContext,itemStateArray);
        rv_partnerkit.setAdapter(partnerkitAdapter);

        getPartnerKit();

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (partnerkitAdapter.().size() > 0) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < partnerkitAdapter.getSelected().size(); i++) {
                        stringBuilder.append(partnerkitAdapter.getSelected().get(i).getName());
                        stringBuilder.append("\n");
                    }
                    //showToast(stringBuilder.toString());
                } else {
                   // showToast("No Selection");
                }*/Log.d("kitlistN",""+partnerkitAdapter.getSelected().size());
                if (partnerkitAdapter.getSelected().size() > 0) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < partnerkitAdapter.getSelected().size(); i++) {
                        stringBuilder.append(partnerkitAdapter.getSelected().get(i).getKitName()+" ");
                        stringBuilder.append("INR "+partnerkitAdapter.getSelected().get(i).getKitAmount()+" ");
                        Log.e("amount_kit:",""+partnerkitAdapter.getSelected().get(i).getKitAmount());
                        stringBuilder.append("<br>");
                    }
                    Log.d("kitlist",""+stringBuilder.toString());
                    insertPartnerKit(stringBuilder.toString());
                } else {
                    Toast.makeText(mContext,"Select Partner Kit",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void insertPartnerKit(String partnerkitlist){

        if(Connectivity.isConnected(mContext)){

            String partnerKitUrl = null;
            try {
                partnerKitUrl = StaticUrl.setpartnerkit
                        +"?shopid="+sharedPreferencesUtils.getShopID()
                        +"&partnerkitlist="+ URLEncoder.encode(partnerkitlist,"utf-8")
                        +"&flag=pkit";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            progressDialog.show();
            StringRequest stringRequestKit = new StringRequest(Request.Method.GET,
                    partnerKitUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.e("responseInsertPKit",""+response);

                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    String Kitamount = jsonObject.getString("Kitamount");

                                    Toast.makeText(mContext,"Thanks, Your Partner kit is Confirmed.",Toast.LENGTH_SHORT).show();

                                    Intent paymentIntent = new Intent(mContext, PaymentActivity.class);
                                    paymentIntent.putExtra("rechargePlanId", "podkit");
                                    paymentIntent.putExtra("rechargePoints", "000");
                                    paymentIntent.putExtra("rechargeAmount", Kitamount);
                                    paymentIntent.putExtra("rechargePlanName", "Partnerkit");
                                    paymentIntent.putExtra("flagPayment", "Partnerkit");
                                    startActivity(paymentIntent);

                                }else if(!status){
                                    Toast.makeText(mContext,"Please Contact to PICODEL Admin",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestKit);

        }else {
            Toast.makeText(mContext,"Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getPartnerKit(){

        if(Connectivity.isConnected(mContext)){
            progressDialog.show();

            String targetUrl = StaticUrl.partnerkitlist+"?shopid="+sharedPreferencesUtils.getShopID()+"&flag="+"kit";
            String shop_category= sharedPreferencesUtils.getRShopCatType();

            StringRequest requestPartnerKit = new StringRequest(Request.Method.GET,
                    targetUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                partnerKitModels.clear();
                                JSONObject jsonObject = new JSONObject(response);
                                boolean strStatus = jsonObject.getBoolean("status");
                                if(strStatus){
                                    JSONArray  jsonArray = jsonObject.getJSONArray("data");
                                    for(int i =0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        PartnerKitModel partnerKit = new PartnerKitModel();
                                        partnerKit.setKitName(object.getString("kitname"));
                                        partnerKit.setKitAmount(object.getString("amount"));
                                        partnerKit.setShopcategory(object.getString("shopcategory"));
                                        partnerKitModels.add(partnerKit);
                                    }
                                    partnerkitAdapter.notifyDataSetChanged();
                                }else if(!strStatus){
                                        Toast.makeText(mContext,"No Records Found",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
                MySingleton.getInstance(mContext).addToRequestQueue(requestPartnerKit);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
