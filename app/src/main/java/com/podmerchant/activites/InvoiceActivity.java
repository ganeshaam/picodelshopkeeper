package com.podmerchant.activites;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.print.PdfPrint;
import android.print.PrintAttributes;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import dmax.dialog.SpotsDialog;

import static androidx.core.content.FileProvider.getUriForFile;

public class InvoiceActivity extends AppCompatActivity {

    WebView wv_invoice;
    FloatingActionButton btn_print;
    String flagPayment,logo, invoiceId, staffName, staffImage, planName, points, amounts, paymentMode, address1, address2, address3, address4, address5, area, contact, city, picodelAddress, terms, totalPoints,rdate;
    static String pdfPath;
    Context mContext;
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    public static int REQUEST_PERMISSIONS = 1;
    boolean boolean_permission;
    boolean boolean_save;
    final private int REQUEST_CODE_ASK_PERMISSION = 111;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    int PERMISSION_ALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.viewinvoice);//Advance against PICODEL Commission
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = InvoiceActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        wv_invoice = findViewById(R.id.wv_invoice);
        btn_print = findViewById(R.id.btn_print);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        Intent intent = getIntent();
        invoiceId = intent.getStringExtra("invoiceId");
        staffName = intent.getStringExtra("staffName");
        staffImage = intent.getStringExtra("staffImage");
        planName = intent.getStringExtra("planName");
        points = intent.getStringExtra("points");
        amounts = intent.getStringExtra("amounts");
        paymentMode = intent.getStringExtra("paymentMode");
        totalPoints = intent.getStringExtra("totalPoints");
        flagPayment= intent.getStringExtra("flagPayment");
        //rdate= intent.getStringExtra("rdate");

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createWebPrintJob(wv_invoice);
            }
        });

        if (Connectivity.isConnected(mContext)) {
            getInvoiceDetails();
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }

        if (!hasPermissions(InvoiceActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(InvoiceActivity.this, PERMISSIONS, PERMISSION_ALL);
        }

        if(sharedPreferencesUtils.getAdminType().equals("Manufacturer")) {
            getSupportActionBar().setTitle("Advance against PICODEL Commission");
        }
    }


    public void getInvoiceDetails() {
        progressDialog.show();

        String invoiceUrl = null;
        try {
            invoiceUrl = StaticUrl.invoicedetails
                    + "?shopid=" + sharedPreferencesUtils.getShopID()
                    + "&staffName=" + URLEncoder.encode(staffName, "utf-8")
                    + "&staffImage=" + URLEncoder.encode(staffImage, "utf-8")
                    + "&planName=" + URLEncoder.encode(planName, "utf-8")
                    + "&points=" + URLEncoder.encode(points, "utf-8")
                    + "&amounts=" + URLEncoder.encode(amounts, "utf-8")
                    + "&paymentMode=" + URLEncoder.encode(paymentMode, "utf-8")
                    + "&totalPoints=" + URLEncoder.encode(totalPoints, "utf-8")
                    + "&invoiceId=" + URLEncoder.encode(invoiceId, "utf-8")
                    + "&userName=" + URLEncoder.encode(sharedPreferencesUtils.getUserName(), "utf-8")
                    + "&shopName=" + URLEncoder.encode(sharedPreferencesUtils.getShopName(), "utf-8")
                    + "&flagPayment=" + flagPayment;
            Log.d("invoiceUrl",invoiceUrl);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                invoiceUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("invoice", response);
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                // JSONArray jsonArray = jsonObject.getJSONArray("data");
                                // JSONObject data = jsonArray.getJSONObject(0);
                               /* address1 = addressObject.getString("address1");
                                address2 = addressObject.getString("address2");
                                address3 = addressObject.getString("address3");
                                address4 = addressObject.getString("address4");
                                address5 = addressObject.getString("address5");
                                area = addressObject.getString("area");
                                contact = addressObject.getString("contact");
                                city = addressObject.getString("city");

                                picodelAddress = jsonObject.getString("pocodel");
                                terms= jsonObject.getString("terms");
                                logo= jsonObject.getString("logo");
                                printView();
*/
                                String data = jsonObject.getString("data");
                                wv_invoice.loadData(data, "text/html", "UTF-8");

                            } else {
                                Toast.makeText(mContext, "Contact to PICODEL Admin", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(stringRequest);

    }

    public static void getPath(File path, String fileName) {
        //pdfPath = path+fileName;
        pdfPath = fileName;
        Log.d("pathUrl", pdfPath);
    }

    public void openPdf(String pdfPathUrl) {
    progressDialog.show();
       File pdfFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +" DCIM/picodel/"+ pdfPathUrl);
      // Uri pdfFile = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", pdfFile);
        //Uri path = Uri.parse("file:///storage/sdcard0/DCIM/picodel/"+pdfPathUrl); //"file:///storage/sdcard0/DCIM/picodel/PICODEL_1563629715308.pdf"
        String pdfLocalPath = "file:///storage/sdcard0/DCIM/picodel/" + pdfPathUrl;
        //Toast.makeText(mContext,""+pdfLocalPath,Toast.LENGTH_LONG).show();
        Log.e("pdfLocalPath", "" + pdfLocalPath);
        //  try {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Uri path =  getUriForFile(this, "com.podmerchant.fileprovider", pdfFile);
            Log.e("createPdfUriPath", "" + path);
           // Toast.makeText(mContext,""+path,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
            sendNotification(pdfPathUrl,path);
        } else {
            Uri path = Uri.parse("file:///storage/sdcard0/DCIM/picodel/" + pdfPathUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
            sendNotification(pdfPathUrl,path);
        }


        progressDialog.dismiss();
    }

    private void sendNotification(String messageBody, Uri path) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(path, "application/pdf");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.picodel_logo)
                        .setContentTitle("Download complete")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "PICODEL Merchant",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void createWebPrintJob (WebView webView){
            //progressDialog.show();
            String jobName = getString(R.string.app_name) + "Documents";
            PrintAttributes attributes = new PrintAttributes.Builder()
                    .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
                    .setResolution(new PrintAttributes.Resolution("pdf", "pdf", 600, 600))
                    .setMinMargins(PrintAttributes.Margins.NO_MARGINS).build();
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/picodel/");// + "/Documents/"
            Log.d("path", "" + path);
            PdfPrint pdfPrint = new PdfPrint(attributes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                pdfPrint.print(webView.createPrintDocumentAdapter(jobName), path, "PICODEL_" + System.currentTimeMillis() + ".pdf");
            } else {
                pdfPrint.print(webView.createPrintDocumentAdapter(), path, "PICODEL_" + System.currentTimeMillis() + ".pdf");
            }

           //openPdf(pdfPath);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d("TestpdfUrl", pdfPath);
                    openPdf(pdfPath);
                }
            }, 2000);

            //"Total Points: "+totalPoints+"</br>"+
            //progressDialog.show();
          // dialog();
        }

    public void dialog(){
            new AlertDialog.Builder(this)
                    .setMessage("Your Invoice is downloaded. Please check PICODEL folder from file manager")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            //.setNegativeButton("No", null)
        }

    public static boolean hasPermissions (Context context, String...permissions){
            if (context != null && permissions != null) {
                for (String permission : permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false;
                    }
                }
            }
            return true;
        }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
            int id = item.getItemId();
            switch (id) {
                case android.R.id.home:
                    finish();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }

 }
