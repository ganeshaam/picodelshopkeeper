package com.podmerchant.activites;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.PaymentModeAdaptor;
import com.podmerchant.model.PaymentModeModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class PaymentActivity extends AppCompatActivity {

    Context mContext;
    AlertDialog progressDialog;
    ArrayList<PaymentModeModel> paymentModeList;
    RecyclerView rv_paymentMode;
    Button btn_payment_status;
    String flagPayment,vcode,orderId="",shopID="",delboyid="",Selected_PaymentMode="100",payID="0",rechargePlanId="",rechargePoints,rechargeAmount,staffId="0",staffName=" ",staffImage="",rechargePlanName,staffMobile="0";
    PaymentModeAdaptor paymentModeAdaptor;
    LinearLayoutManager layoutManager;
    TextView tv_selectedvalue,tv_cm_name,tv_recharge_point,tv_recharge_amount,yourpoints;//tv_cm_age,
    CircleImageView iv_cm_boy;
    SharedPreferencesUtils sharedPreferencesUtils;
    LinearLayout ll_cm_select;
    RelativeLayout rl_mc_boy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.paidto);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PaymentActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        rv_paymentMode = findViewById(R.id.rv_paymentMode);
        btn_payment_status = findViewById(R.id.btn_payment_status);
        tv_selectedvalue = findViewById(R.id.tv_selectedvalue);
        tv_cm_name = findViewById(R.id.tv_cm_name);
        //tv_cm_age = findViewById(R.id.tv_cm_age);
        iv_cm_boy = findViewById(R.id.iv_cm_boy);
        ll_cm_select = findViewById(R.id.ll_cm_select);
        rl_mc_boy = findViewById(R.id.rl_mc_boy);
        tv_recharge_point = findViewById(R.id.tv_recharge_point);
        tv_recharge_amount = findViewById(R.id.tv_recharge_amount);
        yourpoints = findViewById(R.id.yourpoints);
        rv_paymentMode.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        paymentModeList = new ArrayList<>();
        rv_paymentMode.setLayoutManager(layoutManager);
        rv_paymentMode.setItemAnimator(new DefaultItemAnimator());
        paymentModeAdaptor = new PaymentModeAdaptor(mContext,paymentModeList);
        rv_paymentMode.setAdapter(paymentModeAdaptor);

        Intent intent = getIntent();
        rechargePlanId = intent.getStringExtra("rechargePlanId");
        rechargePoints = intent.getStringExtra("rechargePoints");
        rechargeAmount = intent.getStringExtra("rechargeAmount");
        rechargePlanName = intent.getStringExtra("rechargePlanName");
        flagPayment= intent.getStringExtra("flagPayment");

        if(flagPayment.equals("recharge")){
            tv_recharge_point.setText(rechargePoints);
            tv_recharge_amount.setText(getResources().getString(R.string.rechargamt) + " " + rechargeAmount);
        }else if(flagPayment.equals("Partnerkit")) {
            yourpoints.setVisibility(View.GONE);
            tv_recharge_point.setText( "INR " + rechargeAmount);
            //tv_recharge_amount.setText(getResources().getString(R.string.rechargamt) + " " + rechargeAmount);
            btn_payment_status.setText(getResources().getString(R.string.submit));
        }else if(flagPayment.equals("Registration")) {
            yourpoints.setVisibility(View.GONE);
            tv_recharge_point.setText( "INR " + rechargeAmount);
            //tv_recharge_amount.setText(getResources().getString(R.string.rechargamt) + " " + rechargeAmount);
            btn_payment_status.setText(getResources().getString(R.string.submit));
        }
        rv_paymentMode.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_paymentMode,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        //delBoyId =  boyModelArrayList.get(position).getDelBoyId();
                        Selected_PaymentMode= paymentModeList.get(position).getPaymentType();
                        payID= paymentModeList.get(position).getID();
                        Log.e("selected PAYMENT MODE",""+Selected_PaymentMode);
                        Toast.makeText(mContext,paymentModeList.get(position).getPaymentType(),Toast.LENGTH_SHORT).show();
                        //tv_selectedvalue.setText("Payment Mode:"+Selected_PaymentMode);
                        //paymentModeAdaptor.notifyDataSetChanged();
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        btn_payment_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(Selected_PaymentMode.equals("0")||Selected_PaymentMode.equalsIgnoreCase("")||Selected_PaymentMode.isEmpty()){
                  if(Selected_PaymentMode.equalsIgnoreCase("100")){
                    Toast.makeText(mContext,"Please select Payment Mode",Toast.LENGTH_LONG).show();
                  }else if(staffMobile.equalsIgnoreCase("0")){
                      Toast.makeText(mContext,"Please select Staff",Toast.LENGTH_LONG).show();
                  }
                else
                if(Selected_PaymentMode.equals("Cash")){
                    Toast.makeText(mContext,"in cash mode",Toast.LENGTH_LONG).show();
                    Log.e("selected PAYMENT MODE",""+Selected_PaymentMode);
                    sendOtp();

                }else {
                    sendOtp();
                    //recharge();
                }

            }
        });

        ll_cm_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentStaffList = new Intent(mContext,StafflistActivity.class);
                startActivityForResult(intentStaffList, 1);
            }
        });
        rl_mc_boy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentStaffList = new Intent(mContext,StafflistActivity.class);
                startActivityForResult(intentStaffList, 1);
            }
        });

        paymentModeList();
    }

    //Get paymentmode Api method
    public void paymentModeList(){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            //String urlAcceptOrder = StaticUrl.getPaymentMode;
            String urlAcceptOrder = StaticUrl.getPaymentMode+"?user="+"shop";// +"?order_id="+orderId

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONArray jsonArray =  jsonObject.getJSONArray("data");
                                    Log.e("paymentModData:",""+jsonArray);

                                    for (int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        PaymentModeModel paymentMode_model = new PaymentModeModel();
                                        paymentMode_model.setPaymentType(jsonObject1.getString("payment_type"));
                                        paymentMode_model.setPayment_details(jsonObject1.getString("payment_details"));
                                        //payment_details
                                        paymentModeList.add(paymentMode_model);
                                    }
                                    paymentModeAdaptor.notifyDataSetChanged();
                               //     Toast.makeText(mContext,"Payment List Successfully fetched",Toast.LENGTH_SHORT).show();
                                }else if(status=false){
                                    Toast.makeText(mContext,"Sorry. No Payment Mode Found.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //get staff details
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                 staffName = data.getStringExtra("staffName");
                String staffAge = data.getStringExtra("staffAge");
                staffImage = data.getStringExtra("staffImage");
                staffId = data.getStringExtra("staffId");
                staffMobile = data.getStringExtra("staffMobile");
                Log.d("teststaffName", staffName);

                tv_cm_name.setVisibility(View.VISIBLE);
                //tv_cm_age.setVisibility(View.VISIBLE);
                iv_cm_boy.setVisibility(View.VISIBLE);

                tv_cm_name.setText("Name: "+staffName);
                //tv_cm_age.setText("Age: "+staffAge);

                if (staffImage.isEmpty()) {
                    iv_cm_boy.setImageResource(R.drawable.ic_account_circle_white_24dp);
                } else {
                    Picasso.with(mContext)
                            .load(staffImage)
                            .error(R.drawable.ic_account_circle_white_24dp)
                            .into(iv_cm_boy);
                }
                ll_cm_select.setVisibility(View.GONE);
            }
        }
    }
    String rechargeUrl;
    //place recharge
    public void recharge(){
        if (Connectivity.isConnected(mContext)){
            progressDialog.show();

            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            int currentYear = calendar.get(Calendar.YEAR);
            int currentMonth = calendar.get(Calendar.MONTH) + 1;
            int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
            //Random random = new Random(System.nanoTime() % 100000);
            //int randomInt = random.nextInt(1000000000);
            final String invoiceId = "merchant"+currentMonth+currentYear+(new Date()).getTime();

            try {
                rechargeUrl = StaticUrl.placerecharge
                        +"?shop_id="+sharedPreferencesUtils.getShopID()
                        +"&invoiceid="+invoiceId
                        +"&recharge_plan="+ URLEncoder.encode(rechargePlanName,"utf-8")
                        +"&recharge_plan_id="+rechargePlanId
                        +"&recharge_points="+rechargePoints
                        +"&recharge_amt="+rechargeAmount
                        +"&payment_mode="+ URLEncoder.encode(Selected_PaymentMode,"utf-8")
                        +"&staffid="+staffId
                        +"&staffname="+URLEncoder.encode(staffName,"utf-8")
                        +"&staffImage="+staffImage
                        +"&userName=" + URLEncoder.encode(sharedPreferencesUtils.getUserName(), "utf-8")
                        +"&shopName=" + URLEncoder.encode(sharedPreferencesUtils.getShopName(), "utf-8")
                        +"&email=" + sharedPreferencesUtils.getEmailId()
                        +"&flagPayment=" + flagPayment;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Log.e("rechargeUrl_params",rechargeUrl);

            StringRequest rechargeRequest = new StringRequest(Request.Method.GET,
                    rechargeUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.e("Registration_rchrg_resp",""+response);
                            try {
                                JSONObject jsonObject =  new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){

                                    JSONObject dataObject = jsonObject.getJSONObject("data");
                                    String id = dataObject.getString("id");
                                    //String rechargeAmount = dataObject.getString("recharge_amt");
                                    //String newPoints = dataObject.getString("recharge_points");
                                    JSONObject shopObject = jsonObject.getJSONObject("shoppoint");
                                    String totalPoints = shopObject.getString("recharge_points");

                                    Intent intentRechargeDone = new Intent(mContext,RechargeDoneActivity.class);
                                    intentRechargeDone.putExtra("totalPoints",totalPoints);
                                    intentRechargeDone.putExtra("invoiceId",invoiceId);
                                    intentRechargeDone.putExtra("staffName",staffName);
                                    intentRechargeDone.putExtra("staffImage",staffImage);
                                    intentRechargeDone.putExtra("planName",rechargePlanName);
                                    intentRechargeDone.putExtra("points",rechargePoints);
                                    intentRechargeDone.putExtra("rechargeAmount",rechargeAmount);
                                    intentRechargeDone.putExtra("paymentMode",Selected_PaymentMode);
                                    intentRechargeDone.putExtra("flagPayment", flagPayment);
                                    //intentRechargeDone.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    //intentRechargeDone.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intentRechargeDone);

                                }else {
                                    Toast.makeText(mContext,"Recharge Not Proceed",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(rechargeRequest);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  PaymentActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //send otp
    public void sendOtp(){
        if(Connectivity.isConnected(mContext)){


            String urlOtpsend = StaticUrl.sendotp
                    +"?mobileno="+staffMobile.trim()//"7276201058"//
                    +"&email="+sharedPreferencesUtils.getEmailId();

            Log.d("urlOtpsend",urlOtpsend);

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("sendOtp",response);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss)
                                {
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    vcode = jsonObjectData.getString("vcode");
                                    String contact = jsonObjectData.getString("contact");
                                    //String vdate = jsonObjectData.getString("vdate");
                                    Log.e("VCODE:",""+vcode);
                                    VerifyOtpDailog();

                                }else {
                                    Toast.makeText(mContext,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void VerifyOtpDailog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_custom_otp_verify, null);

        final EditText OTP_VALUE = (EditText) view.findViewById(R.id.et_recharge_otp);
        final Button btn_submit = (Button) view.findViewById(R.id.btn_submit_otp);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if(vcode.equals(OTP_VALUE.getText().toString().trim())){
                Toast.makeText(PaymentActivity.this,"Recharge Successfully",Toast.LENGTH_LONG).show();
                dialog.dismiss();
                recharge();
            }else {
                Toast.makeText(PaymentActivity.this,"Please enter Valid OTP",Toast.LENGTH_LONG).show();
            }

            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

}
