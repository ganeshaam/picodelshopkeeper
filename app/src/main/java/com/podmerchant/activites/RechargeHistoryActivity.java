package com.podmerchant.activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.RechargeHistoryAdapter;
import com.podmerchant.model.RechargePlanModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class RechargeHistoryActivity extends AppCompatActivity {

    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;
    ArrayList<RechargePlanModel> planModelArrayList;
    private RecyclerView.LayoutManager layoutManager;
    private RechargeHistoryAdapter rechargePlanAdapter;
    RecyclerView rv_recharge_planList;
    AlertDialog progressDialog;
    TextView tv_network_con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_histroy);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.recharge_histroy);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = RechargeHistoryActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        rv_recharge_planList = findViewById(R.id.rv_recharge_planlist);
        tv_network_con  = findViewById(R.id.tv_network_con);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());


        rv_recharge_planList.setHasFixedSize(true);
        planModelArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        rv_recharge_planList.setLayoutManager(layoutManager);
        rv_recharge_planList.setItemAnimator(new DefaultItemAnimator());
        rechargePlanAdapter = new RechargeHistoryAdapter(mContext,planModelArrayList);
        rv_recharge_planList.setAdapter(rechargePlanAdapter);

        if(Connectivity.isConnected(mContext)){
            getRechargeHistory();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        rv_recharge_planList.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_recharge_planList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent  intentInvoice = new Intent(mContext,InvoiceActivity.class);
                intentInvoice.putExtra("invoiceId",planModelArrayList.get(position).getInvoiceId());
                intentInvoice.putExtra("staffName",planModelArrayList.get(position).getStaffname());
                intentInvoice.putExtra("staffImage",planModelArrayList.get(position).getStaffImgUrl());
                intentInvoice.putExtra("planName",planModelArrayList.get(position).getPlanName());
                intentInvoice.putExtra("points",planModelArrayList.get(position).getPoints());
                intentInvoice.putExtra("amounts",planModelArrayList.get(position).getRecharge());
                intentInvoice.putExtra("paymentMode",planModelArrayList.get(position).getPayment_mode());
                //intentInvoice.putExtra("rdate",planModelArrayList.get(position).getRdate());
                if(planModelArrayList.get(position).getPlanName().equalsIgnoreCase("PartnerKit")){
                    intentInvoice.putExtra("flagPayment","Partnerkit");
                }else if(planModelArrayList.get(position).getPlanName().equalsIgnoreCase("recharge")){
                    intentInvoice.putExtra("flagPayment","recharge");
                }else if(planModelArrayList.get(position).getPlanName().equalsIgnoreCase("Registration")){
                    intentInvoice.putExtra("flagPayment","Registration");
                }

                intentInvoice.putExtra("totalPoints","000");
                startActivity(intentInvoice);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    //recharge Histroy
    public void getRechargeHistory() {

        progressDialog.show();
        planModelArrayList.clear();

        String targetUrlRechargeHistroy = StaticUrl.getrechargehistroy+"?shop_id="+sharedPreferencesUtils.getShopID();

        StringRequest histroyRequest = new StringRequest(Request.Method.GET,
                targetUrlRechargeHistroy,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus=true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        RechargePlanModel model = new RechargePlanModel();
                                        model.setId(olderlist.getString("id"));
                                        model.setPlanName(olderlist.getString("recharge_plan"));
                                        model.setPoints(olderlist.getString("recharge_points"));
                                        model.setRecharge(olderlist.getString("recharge_amt"));
                                        model.setPayment_mode(olderlist.getString("payment_mode"));
                                        model.setStaffid(olderlist.getString("payment_mode"));
                                        model.setStaffname(olderlist.getString("staffname"));
                                        model.setStaffImgUrl(olderlist.getString("staffImage"));
                                        model.setInvoiceId(olderlist.getString("invoiceid"));
                                        model.setRdate(olderlist.getString("rdate"));
                                        //model.set(olderlist.getString("flagPayment"));//flagPayment
                                        planModelArrayList.add(model);
                                    }
                                    rechargePlanAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(histroyRequest);
    }
    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RechargeHistoryActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RechargeHistoryActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
