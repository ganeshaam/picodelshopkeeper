package com.podmerchant.activites;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2500;
    SharedPreferencesUtils sharedPreferencesUtils;
    //ProgressBar productBar;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedPreferencesUtils = new SharedPreferencesUtils(this);
        mContext = MainActivity.this;
       // productBar = findViewById(R.id.productBar);

        if(sharedPreferencesUtils.getLoginFlag()) {
            checkStatus();
                    /*Intent loginInent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(loginInent);
                    finish();*/
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent loginInent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(loginInent);
                    finish();
                }
            },2000);

        }

      /*  SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this *//* Activity context *//*);
        String name = sharedPreferences.getString("notifications", "");
        Log.d("name",name);*/
    }
    //check deactivate flag
    public void checkStatus(){
        if(Connectivity.isConnected(mContext)){

            String urlCheckStatus = StaticUrl.checkstatus +"?shopid="+sharedPreferencesUtils.getShopID();
            Log.d("urlCheckStatus",urlCheckStatus);

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlCheckStatus,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                           // progressDialog.dismiss();
                            Log.d("urlCheckStatusR",sharedPreferencesUtils.getAdminType());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("statuss");
                                if(status.equals("true")){
                                     //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    //String vcode = jsonObjectData.getString("vcode");

                                    if(sharedPreferencesUtils.getAdminType().equals("PAW")){
                                        Intent loginInent = new Intent(MainActivity.this, PawHomeActivity.class);
                                        startActivity(loginInent);
                                        finish();
                                    }else {
                                        Intent loginInent = new Intent(MainActivity.this, HomeActivity.class);
                                        startActivity(loginInent);
                                        finish();
                                    }
                                }else if(status.equals("false")){
                                     JSONArray jsonArray = jsonObject.getJSONArray("data");
                                     JSONObject jsonObjectStatus = jsonArray.getJSONObject(0);
                                     String statusObj = jsonObjectStatus.getString("status");
                                      String message = jsonObject.getString("mesg");
                                      String whatsAppNo = jsonObject.getString("whatsappno");
                                      if(statusObj.equalsIgnoreCase("1")) {
                                          deactivateDialog(message, whatsAppNo);
                                      }else if(statusObj.equalsIgnoreCase("2")) {

                                          sharedPreferencesUtils.logout();
                                          Intent intent = new Intent(mContext, LoginActivity.class);
                                          startActivity(intent);
                                          finish();
                                      }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                          //  progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void deactivateDialog(String message, final String whatsAppNo) {

            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            View view = getLayoutInflater().inflate(R.layout.dialog_deactivateshop, null);
            TextView tv_deactivateshop = view.findViewById(R.id.tv_deactivateshop);
            Button btn_whatsapp = view.findViewById(R.id.btn_whatsapp);
            Button btn_close = view.findViewById(R.id.btn_close);
            tv_deactivateshop.setText(message);
            btn_whatsapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    openWhatsApp(whatsAppNo);
                    //send to whatsApp
                }
            });
           btn_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setContentView(view);
            dialog.show();
    }

    private void openWhatsApp(String mobileNo) {
        /*String smsNumber = mobileno;//"7****"; // E164 format without '+' sign
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "PICODEL Admin.");
        sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
        sendIntent.setPackage("com.whatsapp");
        if (sendIntent.resolveActivity(getPackageManager()) == null) {
            //Toast.makeText(this, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            return;
        }*/

//        Intent sendIntent = new Intent("android.intent.action.MAIN");
//        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
//        sendIntent.putExtra("Hi", PhoneNumberUtils.stripSeparators(mobileNo)+"@s.whatsapp.net");
//        startActivity(sendIntent);
        try {
            String text = "Hi PICODEL Admin";// Replace with your message.

            String toNumber = mobileNo; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+toNumber +"&text="+text));
            startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


}
