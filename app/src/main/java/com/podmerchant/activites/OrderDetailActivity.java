package com.podmerchant.activites;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnBackPressListener;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.podmerchant.R;
import com.podmerchant.adapter.CartListAdapter;
import com.podmerchant.adapter.DistributorListAdapter;
import com.podmerchant.adapter.SimilarProductCardAdapter;
import com.podmerchant.adapter.TimeSlotAdaptor;
import com.podmerchant.model.CartitemModel;
import com.podmerchant.model.DistributorItemModel;
import com.podmerchant.model.ProductCodeWiseProduct;
import com.podmerchant.model.productCodeWithProductList;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class OrderDetailActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    String admin_aprove,color_flag,areaName,aam_delivery_charge,updatedat,showcart, pincode, flag, action_time, orderId="0", cartUniqueId, userId, cartUserId, shipping_address, latitude, longitude, total_amount, order_date, address_id, assign_delboy_id, reason_of_rejection,delivery_time="", address1, address2, address3, address4, address5, area,delivery_date,delivery_time_get,delivery_method,payment_option,user_fname,user_contactno;
    TextView tv_od_delorderdate,tv_od_deliverycharge,tv_od_orderdelidate,tv_view_cartitem, tv_delboyname, tv_delboymo, tv_od_orderid, tv_od_totalamount, tv_od_orderdate, tv_network_con, tv_address_pincode, tv_user_name,tv_address1, tv_address2, tv_address3, tv_address4, tv_address5, tv_area,tv_payment_options,tv_payment_type,
            tv_pickup_address,tv_whatsapp_cartitem,tv_cartitem,tv_sendEmail,tv_user_terms,tv_user_remarks,tv_addItems;
    RecyclerView recyclerViewCartItems,recyclerBrandItems;
    Button btn_acceptorder, btn_cancelorder, btn_assignorder, btn_deliverorder, btn_intrested_but,btn_Approve;
    CardView cv_manufacturer_approve;
    ImageView iv_payment;
    Context mContext;
    ArrayList<CartitemModel> cartitemArrayList;
    ArrayList<CartitemModel> cartitemArrayList_filter;
    ArrayList<DistributorItemModel> distributorItemList;

    //related to Simmiller Items Initializations
    private final LinkedHashMap similarProductListHashMap = new LinkedHashMap();// will contain all
    private final ArrayList<ProductCodeWiseProduct> similarProductWiseList = new ArrayList<>();
    private final ArrayList<productCodeWithProductList> SimilarFinalList = new ArrayList<>();
    private SimilarProductCardAdapter similarProductCardAdapter;

    ArrayList<String> products_ids;
    private RecyclerView.LayoutManager layoutManager;
    private CartListAdapter cartListAdapter;
    private DistributorListAdapter distributorListAdapter;
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    RelativeLayout rl_delboydetail;
    private RecyclerView similarProductRecyclerView;
    LinearLayout map_location,layout_pickup;
    private LinearLayout similarProductLinearLayout;
    ImageView iv_delboy;
    String slot="two";
    private DialogPlus dialog;
    SwipeRefreshLayout srl_refreshorderlist;
    TimeSlotAdaptor DistributorTimeAdaptor;

    ArrayList<String> Next3Hours;
    ArrayList<String> Slot_ONE;
    ArrayList<String> Slot_schedules;
    ArrayList<String> Slot_TWO;
    ArrayList<String> Slot_Three;
    ArrayList<String> Slot_Four;
    ArrayList<String> Slot_Five;
    ArrayList<String> Slot_Six;
    ArrayList<String> Slot_Seven;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_order_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = OrderDetailActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        Next3Hours = new ArrayList<>();
        products_ids = new ArrayList<>();
        //srl_refreshorderlist = findViewById(R.id.srl_refreshorderlist);
        tv_od_orderid = findViewById(R.id.tv_od_orderid);
        tv_od_totalamount = findViewById(R.id.tv_od_totalamount);
        tv_od_orderdate = findViewById(R.id.tv_od_orderdate);
        tv_user_name = findViewById(R.id.tv_user_name);
        tv_address1 = findViewById(R.id.tv_address1);
        tv_address2 = findViewById(R.id.tv_address2);
        tv_address3 = findViewById(R.id.tv_address3);
        tv_address4 = findViewById(R.id.tv_address4);
        tv_address5 = findViewById(R.id.tv_address5);
        tv_area = findViewById(R.id.tv_area);
        tv_address_pincode = findViewById(R.id.tv_address_pincode);
        //srl_refreshorderlist.setOnRefreshListener(this);
        tv_user_terms = findViewById(R.id.tv_user_terms);
        tv_user_remarks = findViewById(R.id.tv_user_remarks);

        tv_network_con = findViewById(R.id.tv_network_con);
        recyclerViewCartItems = findViewById(R.id.recyclerViewCartItems);
        btn_acceptorder = findViewById(R.id.btn_acceptorder);
        btn_cancelorder = findViewById(R.id.btn_cancelorder);
        btn_intrested_but = findViewById(R.id.btn_intrested_but);
        btn_assignorder = findViewById(R.id.btn_assignorder);
        btn_deliverorder = findViewById(R.id.btn_deliverorder);

        rl_delboydetail = findViewById(R.id.rl_delboydetail);
        iv_delboy = findViewById(R.id.iv_delboy);
        tv_delboyname = findViewById(R.id.tv_delboyname);
        tv_delboymo = findViewById(R.id.tv_delboymo);
        tv_view_cartitem = findViewById(R.id.tv_view_cartitem);
        tv_od_orderdelidate= findViewById(R.id.tv_od_orderdelidate);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        tv_network_con = findViewById(R.id.tv_network_con);
        tv_od_deliverycharge = findViewById(R.id.tv_od_deliverycharge);
        tv_od_delorderdate = findViewById(R.id.tv_od_delorderdate);
        tv_payment_options = findViewById(R.id.tv_payment_options);
        map_location = findViewById(R.id.map_location);
        recyclerBrandItems = findViewById(R.id.recyclerBrandItems);
        tv_whatsapp_cartitem = findViewById(R.id.tv_whatsapp_cartitem);
        tv_cartitem = findViewById(R.id.tv_cartitem);
        tv_sendEmail = findViewById(R.id.tv_sendEmail);
        tv_addItems = findViewById(R.id.tv_addItems);

        recyclerViewCartItems.setHasFixedSize(true);
        cartitemArrayList = new ArrayList<>();
        cartitemArrayList_filter = new ArrayList<>();

        layoutManager = new LinearLayoutManager(mContext);
        recyclerViewCartItems.setLayoutManager(layoutManager);
        recyclerViewCartItems.setItemAnimator(new DefaultItemAnimator());
        cartListAdapter = new CartListAdapter(cartitemArrayList, mContext,"Accept",sharedPreferencesUtils.getShopID(),orderId);
        recyclerViewCartItems.setAdapter(cartListAdapter);

        recyclerBrandItems.setHasFixedSize(true);
        distributorItemList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        recyclerBrandItems.setLayoutManager(layoutManager);
        recyclerBrandItems.setItemAnimator(new DefaultItemAnimator());
       /* distributorListAdapter = new DistributorListAdapter(distributorItemList, mContext);
        recyclerBrandItems.setAdapter(distributorListAdapter);*/

       /*Manufacture Payment Approval*/

        cv_manufacturer_approve = findViewById(R.id.cv_manufacturer_approve);
        tv_payment_type = findViewById(R.id.tv_payment_type);
        iv_payment =findViewById(R.id.iv_payment);
        btn_Approve = findViewById(R.id.btn_Approve);

        final Intent intent = getIntent();
        orderId = intent.getStringExtra("orderId");
        cartUniqueId = intent.getStringExtra("cartUniqueId");
        userId = intent.getStringExtra("userId");
        Log.e("order_user_id",""+userId);
        cartUserId = intent.getStringExtra("cartUserId");
        //shipping_address = intent.getStringExtra("shipping_address");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        total_amount = intent.getStringExtra("total_amount");
        order_date = intent.getStringExtra("order_date");
        address_id = intent.getStringExtra("address_id");
        assign_delboy_id = intent.getStringExtra("assign_delboy_id");
        action_time = intent.getStringExtra("action_time");
        address1 = intent.getStringExtra("address1");
        address2 = intent.getStringExtra("address2");
        address3 = intent.getStringExtra("address3");
        address4 = intent.getStringExtra("address4");
        address5 = intent.getStringExtra("address5");
        area = intent.getStringExtra("area");
        pincode = intent.getStringExtra("pincode");
        flag = intent.getStringExtra("flag");
        showcart = intent.getStringExtra("showcart");
        updatedat = intent.getStringExtra("updatedat");
        aam_delivery_charge = intent.getStringExtra("aam_delivery_charge");
        areaName = intent.getStringExtra("areaName");
        delivery_date = intent.getStringExtra("delivery_date");
        delivery_time_get = intent.getStringExtra("delivery_time");
        delivery_method = intent.getStringExtra("delivery_method");
        payment_option = intent.getStringExtra("payment_option");
        color_flag = intent.getStringExtra("color_flag");
        admin_aprove = intent.getStringExtra("admin_aprove");

        calculateDeliveryTime2();


        Log.e("flag_value:",""+flag);

        if (flag.equals("new_order")) {

                if (admin_aprove.equalsIgnoreCase("Approved")) {
                    btn_acceptorder.setVisibility(View.GONE);
                    btn_intrested_but.setVisibility(View.GONE);
                } else {
                    btn_acceptorder.setVisibility(View.VISIBLE);
                    if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Distributor")){
                        btn_intrested_but.setVisibility(View.GONE);
                        recyclerBrandItems.setVisibility(View.GONE);
                    }else {
                        btn_intrested_but.setVisibility(View.VISIBLE);
                        btn_acceptorder.setVisibility(View.VISIBLE);
                        rejectionStatus(sharedPreferencesUtils.getShopID(), orderId);
                    }
                }
        } else if (flag.equals("approved_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.VISIBLE);
            btn_assignorder.setVisibility(View.VISIBLE);
            btn_intrested_but.setVisibility(View.GONE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            tv_whatsapp_cartitem.setVisibility(View.VISIBLE);
            tv_addItems.setVisibility(View.VISIBLE);
        } else if (flag.equals("assigned_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.VISIBLE);
            btn_assignorder.setVisibility(View.VISIBLE);
            btn_deliverorder.setVisibility(View.VISIBLE);
            rl_delboydetail.setVisibility(View.VISIBLE);
           // btn_assignorder.setText("Re-assign");
            btn_assignorder.setText(getResources().getString(R.string.btn_reassign_order));
            btn_intrested_but.setVisibility(View.GONE);
            tv_whatsapp_cartitem.setVisibility(View.VISIBLE);
            tv_addItems.setVisibility(View.VISIBLE);
        } else if (flag.equals("assigned_delboy_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.GONE);
            btn_assignorder.setVisibility(View.GONE);
            btn_deliverorder.setVisibility(View.VISIBLE);
            btn_intrested_but.setVisibility(View.GONE);
        } else if (flag.equals("histroy_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.GONE);
            btn_assignorder.setVisibility(View.GONE);
            btn_deliverorder.setVisibility(View.GONE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            btn_intrested_but.setVisibility(View.GONE);
        }else if(flag.equals("testorder")){
            /*if (admin_aprove.equalsIgnoreCase("Approved")) {
                btn_acceptorder.setVisibility(View.GONE);
                btn_intrested_but.setVisibility(View.GONE);
            } else {*/
                btn_acceptorder.setVisibility(View.VISIBLE);
                btn_intrested_but.setVisibility(View.VISIBLE);
                //rejectionStatus(sharedPreferencesUtils.getShopID(), orderId);
            //}
        }else {
            btn_acceptorder.setVisibility(View.VISIBLE);
        }


       // Log.e("commitLog_monday:", "" + "ok_1_07_2019");
        //Log.e("ActionTime:", "" + action_time);

        //Check cancel order time calculations
        DiffCalculate(action_time);
        setOrderDetails();
        //Check Rejected status
        //rejectionStatus(sharedPreferencesUtils.getShopID(), orderId);


        if (Connectivity.isConnected(mContext)) {
            if (flag.equals("histroy_order")) {
                getDeliveryboydetails(assign_delboy_id);
            } else {//||flag.equals("assigned_order")
                getDeliveryboydetails(assign_delboy_id);
                getOrderCartList();
            }
            //getUserOrderAddress(address_id,userId);
        } else {
            tv_network_con.setVisibility(View.VISIBLE);
        }
        //accept order
        btn_acceptorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sharedPreferencesUtils.getUserName().isEmpty()){
                    Intent profile = new Intent(mContext, ShopProfileActivity.class);
                    startActivity(profile);
                    finish();
                }else {
                    chkOrderStatus();
                }
            }
        });
        //cancel accepted order
        btn_cancelorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Shop keeper cancel order with in 30 min required to check action_time is more than 30 min
                Log.d("action_time", action_time);
                //change status   assign_shop_id=sharedPreferencesUtils.getShopID()
                String name[] = {"Price Mismatch\n(प्रोडक्ट के मूल्य में अंतर)", "Delivery Boy issue\n(डिलीवरी बॉय नही है)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)","Other issue\n(अन्य समस्या)","Below items are not available\n(निम्न आइटम उपलब्ध नहीं हैं।)"};
                //String name[] = {"Price Mismatch\n(प्रोडक्ट के मूल्य में अंतर)", "Delivery Boy issue\n(डिलीवरी बॉय नही है)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)","1 item not available\n(1 आइटम उपलब्ध नही है)", "2 items not available\n(2 आइटम उपलब्ध नही है)","3 items not available\n(3 आइटम उपलब्ध नही है)", "4 items not available\n(4 आइटम उपलब्ध नही है)","5 items not available\n(5 आइटम उपलब्ध नही है)","6 items not available\n(6 आइटम उपलब्ध नही है)","7 items not available\n(7 आइटम उपलब्ध नही है)","8 items not available\n(8 आइटम उपलब्ध नही है)", "Other issue\n(अन्य समस्या)"};
                selectReason(name);
            }
        });
        //cancel before accept
        btn_intrested_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Manufacturer")) {
                    String name[] = {"Price changed\n(प्रोडक्ट के मूल्य में अंतर)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)","Other issue\n(अन्य समस्या)","Below items stock are not available\n(निम्न आइटम उपलब्ध नहीं हैं।)"};
                    //String name[] = {"Price Mismatch\n(प्रोडक्ट के मूल्य में अंतर)", "Delivery Boy issue\n(डिलीवरी बॉय नही है)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)","1 item not available\n(1 आइटम उपलब्ध नही है)", "2 items not available\n(2 आइटम उपलब्ध नही है)","3 items not available\n(3 आइटम उपलब्ध नही है)", "4 items not available\n(4 आइटम उपलब्ध नही है)","5 items not available\n(5 आइटम उपलब्ध नही है)","6 items not available\n(6 आइटम उपलब्ध नही है)","7 items not available\n(7 आइटम उपलब्ध नही है)","8 items not available\n(8 आइटम उपलब्ध नही है)", "Other issue\n(अन्य समस्या)"};
                    assign_delboy_id = "0";
                    selectReason(name);
                }else {
                    String name[] = {"Price Mismatch\n(प्रोडक्ट के मूल्य में अंतर)", "Delivery Boy issue\n(डिलीवरी बॉय नही है)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)","Other issue\n(अन्य समस्या)","Below items are not available\n(निम्न आइटम उपलब्ध नहीं हैं।)"};
                    //String name[] = {"Price Mismatch\n(प्रोडक्ट के मूल्य में अंतर)", "Delivery Boy issue\n(डिलीवरी बॉय नही है)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)","1 item not available\n(1 आइटम उपलब्ध नही है)", "2 items not available\n(2 आइटम उपलब्ध नही है)","3 items not available\n(3 आइटम उपलब्ध नही है)", "4 items not available\n(4 आइटम उपलब्ध नही है)","5 items not available\n(5 आइटम उपलब्ध नही है)","6 items not available\n(6 आइटम उपलब्ध नही है)","7 items not available\n(7 आइटम उपलब्ध नही है)","8 items not available\n(8 आइटम उपलब्ध नही है)", "Other issue\n(अन्य समस्या)"};
                    assign_delboy_id = "0";
                    selectReason(name);
                }


            }
        });
        //assign order
        btn_assignorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open delivery boy list

                        Intent intent = new Intent(mContext, AddDeliveryBoyActivity.class);
                        intent.putExtra("assignOrder", "assignOrder");
                        intent.putExtra("orderid", orderId);
                        intent.putExtra("action_time", action_time);
                        intent.putExtra("orderId", orderId);
                        intent.putExtra("cartUniqueId", cartUniqueId);
                        intent.putExtra("userId", userId);
                        intent.putExtra("cartUserId", cartUserId);
                        intent.putExtra("shipping_address", shipping_address);
                        intent.putExtra("latitude", latitude);
                        intent.putExtra("longitude", longitude);
                        intent.putExtra("total_amount", total_amount);
                        intent.putExtra("order_date", order_date);
                        intent.putExtra("address_id", address_id);
                        intent.putExtra("assign_delboy_id", assign_delboy_id);
                        intent.putExtra("action_time", action_time);
                        intent.putExtra("address1", address1);
                        intent.putExtra("address2", address2);
                        intent.putExtra("address3", address3);
                        intent.putExtra("address4", address4);
                        intent.putExtra("address5", address5);
                        intent.putExtra("area", area);
                        intent.putExtra("pincode", pincode);
                        intent.putExtra("showcart", showcart);
                        //intent.putExtra("flag",flag);
                        intent.putExtra("updatedat", updatedat);
                        intent.putExtra("aam_delivery_charge", aam_delivery_charge);
                        intent.putExtra("areaName", areaName);
                        intent.putExtra("delivery_date", delivery_date);
                        intent.putExtra("delivery_time", delivery_time_get);
                        intent.putExtra("delivery_method", delivery_method);
                        intent.putExtra("payment_option", payment_option);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);


            }
        });

        //delivered order
        btn_deliverorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //orderid, shopid, delboyid
                Intent deliverOrdered_intent = new Intent(mContext, OrderDeliverdActivity.class);
                deliverOrdered_intent.putExtra("orderid", orderId);
                deliverOrdered_intent.putExtra("delboyid", assign_delboy_id);
                deliverOrdered_intent.putExtra("orderId",orderId);
                deliverOrdered_intent.putExtra("cartUniqueId",cartUniqueId);
                deliverOrdered_intent.putExtra("userId",userId);
                deliverOrdered_intent.putExtra("cartUserId",cartUserId);
                deliverOrdered_intent.putExtra("shipping_address",shipping_address);
                deliverOrdered_intent.putExtra("latitude",latitude);
                deliverOrdered_intent.putExtra("longitude",longitude);
                deliverOrdered_intent.putExtra("total_amount",total_amount);
                deliverOrdered_intent.putExtra("order_date",order_date);
                deliverOrdered_intent.putExtra("address_id",address_id);
                deliverOrdered_intent.putExtra("assign_delboy_id",assign_delboy_id);
                deliverOrdered_intent.putExtra("action_time",action_time);
                deliverOrdered_intent.putExtra("address1",address1);
                deliverOrdered_intent.putExtra("address2",address2);
                deliverOrdered_intent.putExtra("address3",address3);
                deliverOrdered_intent.putExtra("address4",address4);
                deliverOrdered_intent.putExtra("address5",address5);
                deliverOrdered_intent.putExtra("area",area);
                deliverOrdered_intent.putExtra("pincode",pincode);
                deliverOrdered_intent.putExtra("showcart",showcart);
                deliverOrdered_intent.putExtra("flag",flag);
                deliverOrdered_intent.putExtra("updatedat",updatedat);
                deliverOrdered_intent.putExtra("aam_delivery_charge",aam_delivery_charge);
                deliverOrdered_intent.putExtra("areaName",areaName);
                deliverOrdered_intent.putExtra("delivery_date",delivery_date);
                deliverOrdered_intent.putExtra("delivery_time",delivery_time_get);
                deliverOrdered_intent.putExtra("delivery_method",delivery_method);
                deliverOrdered_intent.putExtra("payment_option",payment_option);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(deliverOrdered_intent);
            }
        });

        //view cart item from order history if show cart status is 1
        tv_view_cartitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showcart.equals("1")) {
                    getOrderCartList();
                } else {
                    tv_network_con.setVisibility(View.VISIBLE);
                    tv_network_con.setText("Please Contact to PICODEL Admin  To view this cart item list");
                }
            }
        });

        map_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map_intent = new Intent(OrderDetailActivity.this,MapsActivity.class);
                map_intent.putExtra("latitude",latitude);
                map_intent.putExtra("longitude",longitude);
                startActivity(map_intent);
            }
        });

        if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Distributor")){
            recyclerBrandItems.setVisibility(View.VISIBLE);
            getBrandList();
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.GONE);
            btn_assignorder.setVisibility(View.GONE);
            btn_deliverorder.setVisibility(View.GONE);
            rl_delboydetail.setVisibility(View.GONE);
            btn_intrested_but.setVisibility(View.GONE);
        }

        if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Manufacturer")){
            if (flag.equals("new_order")) {
                btn_acceptorder.setVisibility(View.VISIBLE);
            }else {
                btn_acceptorder.setVisibility(View.GONE);
            }
            btn_cancelorder.setVisibility(View.GONE);
            btn_assignorder.setVisibility(View.GONE);
            btn_deliverorder.setVisibility(View.GONE);
            rl_delboydetail.setVisibility(View.GONE);
            btn_intrested_but.setVisibility(View.GONE);
            approve_distributorPayment();
        }

        tv_sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendConfirmEmailbyShop();
            }
        });


        recyclerBrandItems.addOnItemTouchListener(new RecyclerTouchListener(mContext, recyclerBrandItems, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                // String product= cartitemArrayList.get(position).getProduct_name();
                cartitemArrayList_filter.clear();
                String brand = distributorItemList.get(position).getProduct_brand();
                for(int i=0; i<cartitemArrayList.size();i++) {
                    if (cartitemArrayList.get(i).getBrand_name().contains(brand)) {
                        Log.e("brand_Name",""+cartitemArrayList.get(i).getBrand_name());
                        cartitemArrayList_filter.add(cartitemArrayList.get(i));
                    }
                }
                cartListAdapter = new CartListAdapter(cartitemArrayList_filter, mContext,"Accept",sharedPreferencesUtils.getShopID(),orderId);
                recyclerViewCartItems.setAdapter(cartListAdapter);
                cartListAdapter.notifyDataSetChanged();
                //AcceptOrderDialog(brand);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        //************ Cart List Modified by the Shopkeeper ********************//
        recyclerViewCartItems.addOnItemTouchListener(new RecyclerTouchListener(mContext, recyclerViewCartItems, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                //if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Seller")) {
                    if (flag.equals("approved_order")) {
                        if (cartitemArrayList.get(position).getUpdated_status().equalsIgnoreCase("old")) {
                            Toast.makeText(OrderDetailActivity.this, "Already Modified" + "", Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("Product_MainCat:",""+cartitemArrayList.get(position).getProduct_maincat());
                            if(cartitemArrayList.get(position).getProduct_maincat().equalsIgnoreCase("Grocery")) {
                                //Loose
                                if(cartitemArrayList.get(position).getBrand_name().equalsIgnoreCase("Loose")){
                                    Toast.makeText(OrderDetailActivity.this, "Loose Brand Can Not Modified" + "", Toast.LENGTH_LONG).show();
                                }else {
                                    getSimilarProducts(cartitemArrayList.get(position).getProduct_id());
                                    Toast.makeText(OrderDetailActivity.this, "Clicked" + "" + cartitemArrayList.get(position).getProduct_name(), Toast.LENGTH_LONG).show();
                                }
                            }else {
                                Toast.makeText(OrderDetailActivity.this, "Fruits and Vegetables Not Modified" + "", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
               // }//admin type Seller


            }

            @Override
            public void onLongClick(View view, int position) {

                String productName = cartitemArrayList.get(position).getProduct_name();
                String product_valueid = cartitemArrayList.get(position).getProduct_id();
                String session_maincat = cartitemArrayList.get(position).getProduct_maincat();
                if (flag.equals("approved_order")) {
                    RemoveCartITEMS_Dailog(productName, product_valueid,session_maincat);
                }

            }
        }));


        btn_Approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Approveby_manufacturer("",cartUniqueId);
            }
        });

        getUserTerms();

        // WhatsApp Cart Item List Module
        final ArrayList<String> list = new ArrayList<>();
        tv_whatsapp_cartitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list.add("OrderId: "+orderId);
                list.add("User Name: "+user_fname);
                list.add("Contact:"+user_contactno);
                for(int i=0; i < cartitemArrayList.size(); i++) {

                    list.add(cartitemArrayList.get(i).getProduct_name()+" INR "+cartitemArrayList.get(i).getItem_price()+" Qty "+cartitemArrayList.get(i).getIten_qty());
                }

                list.add("Total: "+total_amount);
                list.add("Delivery Charge: "+aam_delivery_charge);
                Log.e("result: ",""+ list);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+"919284238996"+"&text="+ list));
                startActivity(intent);

            }
        });

        tv_addItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddProduct_Dialog();
            }
        });


    }

    // Remove Cart ITEMS
      public void RemoveCartITEMS_Dailog(String ProductName, final String ProductID, final  String session_maincat){
          new AlertDialog.Builder(this)
                  .setMessage("Are you sure you want to Remove Cart Item "+ProductName+" ?")
                  .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                  {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                          RemoveCartItemsFromServer(ProductID,session_maincat);
                          getOrderCartList();
                      }
                  })
                  .setNegativeButton("No", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {

                          dialog.dismiss();
                      }
                  })
                  .show();
      }

    //order details
    public void setOrderDetails() {
        tv_od_orderid.setText("Order Id: " + orderId);
        tv_od_totalamount.setText("Total Amount: " + total_amount + " " + getString(R.string.Rs));
        //tv_od_orderdate.setText("Order Placed Date: " + order_date);
        tv_od_orderdate.setText("Order Placed Date: " + twentyFourTo_12HoursFormat(order_date));
        tv_od_deliverycharge.setText("Delivery Charge: " + aam_delivery_charge + " " + getString(R.string.Rs));
        tv_od_delorderdate.setText("Delivery DateTime: "+delivery_date+" "+delivery_time_get);

        tv_address2.setText("Building: "+address2);
        tv_area.setText(area);
        tv_payment_options.setText("Payment Options: " + payment_option);
        String price_restrict = sharedPreferencesUtils.getPriceRestrict();

        if(price_restrict.equalsIgnoreCase("true")){
            tv_address1.setVisibility(View.GONE);
            tv_address2.setVisibility(View.GONE);
            tv_od_totalamount.setVisibility(View.GONE);
            btn_deliverorder.setVisibility(View.GONE);
        }

        if ((flag.equals("assigned_order")) || (flag.equals("assigned_delboy_order")) || (flag.equals("approved_order"))) {  //approved_order
            getUserName();
            tv_address1.setVisibility(View.VISIBLE);
            tv_address3.setVisibility(View.VISIBLE);
            tv_address4.setVisibility(View.VISIBLE);
            tv_address5.setVisibility(View.VISIBLE);
            tv_address_pincode.setVisibility(View.VISIBLE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            tv_user_name.setVisibility(View.VISIBLE);

            tv_address1.setText("Flat:"+address1);
            tv_address3.setText(address3);
            tv_address4.setText(address4);
            tv_address5.setText(address5);
            tv_address_pincode.setText(pincode);
        } else if (flag.equals("histroy_order")) {
            tv_address1.setVisibility(View.GONE);
            tv_address2.setVisibility(View.GONE);
            tv_address3.setVisibility(View.GONE);
            tv_address4.setVisibility(View.GONE);
            tv_address5.setVisibility(View.GONE);
            tv_address_pincode.setVisibility(View.GONE);
            tv_area.setVisibility(View.GONE);
            tv_user_name.setVisibility(View.GONE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            tv_view_cartitem.setVisibility(View.VISIBLE);
            tv_od_orderdelidate.setVisibility(View.VISIBLE);
            tv_od_orderdelidate.setText("Order Delivered Date: " + updatedat);
        }
    }

    //cart list
    public void getBrandList() {

        progressDialog.show();
        cartitemArrayList.clear();

        String urlCartlist = StaticUrl.Brandcartitem
                + "?cart_unique_id=" + cartUniqueId
                + "&user_id=" + userId
                +"&shop_id=" +sharedPreferencesUtils.getShopID()
                + "&tblName="+URLEncoder.encode(sharedPreferencesUtils.getAdminType());

        Log.d("urlBrandist", urlCartlist);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlCartlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("Brandist", response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus = true)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject cartListObject = jsonArray.getJSONObject(i);
                                        DistributorItemModel model = new DistributorItemModel();
                                        model.setProduct_brand(cartListObject.getString("product_brand"));
                                        //model.setProduct_name(cartListObject.getString("product_name")+"  "+cartListObject.getString("product_size")+" "+cartListObject.getString("product_sizein"));
                                        model.setProduct_Count(cartListObject.getString("brand_count"));
                                        //model.setProduct_mrp(cartListObject.getString("product_mrp"));
                                        //model.setItem_price(cartListObject.getString("item_price"));
                                        //model.setProduct_image(cartListObject.getString("product_image"));
                                       // model.setUser_id(cartListObject.getString("user_id"));
                                        //model.setUser_id(cartListObject.getString("cart_user_id"));
                                        model.setId(cartListObject.getString("shopkpwise_id"));
                                        model.setCart_unique_id(cartListObject.getString("cart_unique_id"));

                                        distributorItemList.add(model);

                                    }
                                    distributorListAdapter = new DistributorListAdapter(distributorItemList, mContext,orderId,Slot_schedules);
                                    recyclerBrandItems.setAdapter(distributorListAdapter);
                                    distributorListAdapter.notifyDataSetChanged();
                                } else {
                                    tv_network_con.setText("No Records Found");
                                }
                            } else if (strStatus = false) {
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(requestCartList);

    }

    //cart list
    public void getOrderCartList() {
        progressDialog.show();
        cartitemArrayList.clear();

        String urlCartlist = StaticUrl.cartlist
                + "?cart_unique_id=" + cartUniqueId
                + "&user_id=" + userId
                + "&city=" + sharedPreferencesUtils.getRCity()
                +"&shop_id=" +sharedPreferencesUtils.getShopID()
                + "&tblName="+URLEncoder.encode(sharedPreferencesUtils.getAdminType());

        Log.d("urlCartlist", urlCartlist);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlCartlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("cartListRes", response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus = true)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject cartListObject = jsonArray.getJSONObject(i);
                                        CartitemModel model = new CartitemModel();
                                        model.setBrand_name(cartListObject.getString("product_brand"));
                                        model.setProduct_name(cartListObject.getString("product_name")+"  "+cartListObject.getString("product_size")+" "+cartListObject.getString("product_sizein"));
                                        model.setIten_qty(cartListObject.getString("iten_qty"));
                                        model.setProduct_mrp(cartListObject.getString("product_mrp"));
                                        model.setItem_price(cartListObject.getString("item_price"));
                                        model.setProduct_image(cartListObject.getString("product_image"));
                                        model.setUser_id(cartListObject.getString("user_id"));
                                        model.setCart_user_id(cartListObject.getString("cart_user_id"));
                                        model.setCart_unique_id(cartListObject.getString("cart_unique_id"));
                                        model.setProduct_id(cartListObject.getString("product_id"));
                                        model.setProduct_cat(cartListObject.getString("product_cat"));
                                        model.setProduct_size(cartListObject.getString("product_size"));
                                        model.setUpdated_status(cartListObject.getString("updated_status"));
                                        model.setProduct_maincat(cartListObject.getString("sessionMainCat"));
                                        cartitemArrayList.add(model);
                                        if(cartListObject.getString("updated_status").contains("old")){
                                            tv_sendEmail.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    cartListAdapter = new CartListAdapter(cartitemArrayList, mContext,"Accept",sharedPreferencesUtils.getShopID(),orderId);
                                    recyclerViewCartItems.setAdapter(cartListAdapter);
                                    cartListAdapter.notifyDataSetChanged();
                                } else {
                                    tv_network_con.setText("No Records Found");

                                }
                            } else if (strStatus = false) {
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(requestCartList);

    }

    //get delivery boy details
    public void getDeliveryboydetails(String assign_delboy_id) {
        progressDialog.show();

        String targetUrl = StaticUrl.getDeliveryboy + "?assign_delboy_id=" + assign_delboy_id;

        StringRequest requestUserAdddress = new StringRequest(Request.Method.GET,
                targetUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getDeliveryboydetails", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");

                            if (status == true) {
                                JSONObject jsonDelboy = jsonObject.getJSONObject("data");

                                String imgUrl = jsonDelboy.getString("imageurl");
                                Picasso.with(mContext).load(imgUrl)
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_account_box_black_24dp)
                                        .into(iv_delboy);
                                tv_delboyname.setText(jsonDelboy.getString("name"));
                                tv_delboymo.setText(jsonDelboy.getString("mobile_number"));
                            } else if (status == false) {
                                String data = jsonObject.getString("data");
                               // Toast.makeText(mContext, data, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }
        );
        MySingleton.getInstance(mContext).addToRequestQueue(requestUserAdddress);
    }

    //check order is accepted
    public void chkOrderStatus(){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.orderstatus+"?order_id="+orderId
                    +"&tblName="+sharedPreferencesUtils.getAdminType();

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("checkstauts",response);
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String approvedStatus = jsonObjectData.getString("admin_approve");
                                    String message = jsonObject.getString("message");
                                    if(approvedStatus.equals("Not Approved")){
                                        //change status as accept
                                        //acceptOrder(sharedPreferencesUtils.getShopID(), orderId);
                                        // selectTimeSlot();


                                            /*String[] timeslot_array = getResources().getStringArray(R.array.Time_slots);
                                            selectTimeSlot2(timeslot_array,"ONE");*/
                                            Intent intent = new Intent(mContext,TimeSlotActivity.class);
                                            intent.putExtra("total_amount",total_amount);
                                            intent.putExtra("orderid", orderId);
                                            intent.putExtra("action_time", action_time);
                                            intent.putExtra("orderId",orderId);
                                            intent.putExtra("cartUniqueId",cartUniqueId);
                                            intent.putExtra("userId",userId);
                                            intent.putExtra("cartUserId",cartUserId);
                                            intent.putExtra("shipping_address",shipping_address);
                                            intent.putExtra("latitude",latitude);
                                            intent.putExtra("longitude",longitude);
                                            intent.putExtra("total_amount",total_amount);
                                            intent.putExtra("order_date",order_date);
                                            intent.putExtra("address_id",address_id);
                                            intent.putExtra("assign_delboy_id",assign_delboy_id);
                                            intent.putExtra("action_time",action_time);
                                            intent.putExtra("address1",address1);
                                            intent.putExtra("address2",address2);
                                            intent.putExtra("address3",address3);
                                            intent.putExtra("address4",address4);
                                            intent.putExtra("address5",address5);
                                            intent.putExtra("area",area);
                                            intent.putExtra("pincode",pincode);
                                            intent.putExtra("showcart",showcart);
                                            intent.putExtra("flag",flag);
                                            intent.putExtra("updatedat",updatedat);
                                            intent.putExtra("aam_delivery_charge",aam_delivery_charge);
                                            intent.putExtra("areaName",areaName);
                                            intent.putExtra("delivery_date",delivery_date);
                                            intent.putExtra("delivery_time",delivery_time_get);
                                            intent.putExtra("delivery_method",delivery_method);
                                            intent.putExtra("payment_option",payment_option);
                                            intent.putExtra("admin_aprove",admin_aprove);
                                            if(delivery_method.equals("Within next 3 Hrs")|| delivery_method.equals("Within next 2 Hrs")){
                                                intent.putStringArrayListExtra("slot_array",Next3Hours);
                                            }else{
                                                intent.putStringArrayListExtra("slot_array", Slot_schedules);
                                            }
                                            startActivity(intent);

                                    }
                                    else{
                                        //Toast.makeText(mContext,message,Toast.LENGTH_SHORT).show();
                                        chkApprovedDialog(message);
                                    }
                                 }else if(!status){
                                    Toast.makeText(mContext,"No Order Found Refresh Order List.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    //accept order
    public void acceptOrder(String shopID, String orderId, String delivery_time) {
        if (Connectivity.isConnected(mContext)) {
            progressDialog.show();

            String urlAcceptOrder = null;
            try {
                urlAcceptOrder = StaticUrl.acceptorder
                        + "?order_id=" + orderId
                        + "&approveBy=" +  sharedPreferencesUtils.getEmailId()
                        //+ "&manager_id=" + sharedPreferencesUtils.getManagerId()
                        + "&delivery_time=" + URLEncoder.encode(delivery_time,"utf-8")
                        + "&assign_shop_id=" + shopID
                        + "&tblName="+sharedPreferencesUtils.getAdminType()
                        + "&totalAmount="+total_amount
                        + "&shopName="+sharedPreferencesUtils.getShopName();
                //+ "&test_order="+"test_order";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Log.d("urlAcceptOrder",urlAcceptOrder);
            // + "&delivery_time="+delivery_time;
            Log.d("urlAcceptOrder",urlAcceptOrder);
            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                Log.e("acceptorderRes:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status == true) {
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext, "Order Accepted. Please check in your Pending Order list", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(mContext,OrderListActivity.class);
                                    intent.putExtra("flag","approved_order");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(intent);

                                } else if (status == false) {
                                   // Toast.makeText(mContext, "Sorry. This order you can not accept.", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
    //show dialog order already accepted
    private void chkApprovedDialog(String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_chkapprove);

        TextView text = dialog.findViewById(R.id.tv_dialog_msg);
        text.setText(message);
        Button dg_btn_refreshlist =  dialog.findViewById(R.id.btn_refreshlist);

        dg_btn_refreshlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
           //     getOrderList(flag,"areaName");
            }
        });

        dialog.show();
    }

    //select reason when shop keeper cancel accepted order
    int lastSelectionPosition = -1;
    public void selectReason(final String name[]) {
        //final Dialog dialog = new Dialog(this);
        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.reason_list_dialog, null);

        final boolean[] flagcart_item = {false};
        final ListView lv = view.findViewById(R.id.lv_reasonlist);
        Button btn_submitreson = view.findViewById(R.id.btn_reason_list);
        final RecyclerView cart_list = (RecyclerView) view.findViewById(R.id.rv_cart_list);
        cart_list.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        cart_list.setLayoutManager(layoutManager);
        /* cart_list.setItemAnimator(new DefaultItemAnimator());*/
        cart_list.setVisibility(View.VISIBLE);
       // getOrderCartList();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, name);
        lv.setAdapter(adapter);
        lv.getSelectedItemPosition();


        Log.e("CartList Size:",""+cartitemArrayList.size());
        final CartListAdapter cartListAdapter2 = new CartListAdapter(cartitemArrayList, OrderDetailActivity.this,"Cancel",sharedPreferencesUtils.getShopID(),orderId);
        cart_list.setAdapter(cartListAdapter2);

        cart_list.addOnItemTouchListener(new RecyclerTouchListener(mContext, cart_list,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        //delBoyId =  boyModelArrayList.get(position).getDelBoyId();
                        String product= cartitemArrayList.get(position).getProduct_name();
                        String product_id= cartitemArrayList.get(position).getProduct_id();
                        String user_id= cartitemArrayList.get(position).getUser_id();
                        String cart_user_id= cartitemArrayList.get(position).getCart_user_id();
                       String cart_unique_id = cartitemArrayList.get(position).getCart_unique_id();
                       String product_size = cartitemArrayList.get(position).getProduct_size();
                       String product_mrp = cartitemArrayList.get(position).getProduct_mrp();
                       String iten_qty = cartitemArrayList.get(position).getIten_qty();
                       String product_cat = cartitemArrayList.get(position).getProduct_cat();
                       String item_price = cartitemArrayList.get(position).getItem_price();
                        products_ids.add(product_id);
                        Log.e("selected Product",""+product);
                        Toast.makeText(mContext,product,Toast.LENGTH_SHORT).show();

                        // Insert into cancel cart itmes service
                       CancelCartItem(user_id,cart_user_id,cart_unique_id,product,product_id,product_size,product_mrp,iten_qty,product_cat,item_price);

                        flagcart_item[0] = true;
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason_of_rejection = name[position];
                //lv.getChildAt(position).setBackgroundColor(Color.parseColor("#00743D"));
                //lastSelectionPosition = adapter.getPosition(reason_of_rejection);
                /*lastSelectionPosition=position;
                adapter.notifyDataSetChanged();
                view.setSelected(true);*/
               /* if(position == 3){
                if(position == 4){
                    cart_list.setVisibility(View.VISIBLE);
                }*/
                toggleBackgroundItem(view);
            }
        });

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reason_of_rejection==null||reason_of_rejection.isEmpty()||reason_of_rejection.equals("")) {
                    Toast.makeText(mContext, "Select Reason", Toast.LENGTH_SHORT).show();
                }else if (flagcart_item[0]==false) {
                    Toast.makeText(mContext, "Please Select Items", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    cancelOrder(sharedPreferencesUtils.getShopID(), orderId, assign_delboy_id, reason_of_rejection); //products_ids
                    //if(flagcart_item[0]){
                      //  sendcancelmail();
                    //}
                }
            }
        });
        dialog.setContentView(view);

        dialog.show();
    }
    //On your adapter create a variable:
    private View lastSelectedItem;
    //Define the folowing method:
    private void toggleBackgroundItem(View view) {
        if (lastSelectedItem != null) {
            lastSelectedItem.setBackgroundColor(Color.TRANSPARENT);
        }
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        lastSelectedItem = view;

    }

    //cancel order String assign_shop_id=shopId
    public void cancelOrder(String shopId, String orderId, String assign_delboy_id, String reason_of_rejection) {
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();

            // String urlCancelOrder =  String.format(StaticUrl.cancelorder,orderId,shopId,assign_delboy_id,reason_of_rejection);

            String urlCancelOrder = null;
            try {
                urlCancelOrder = StaticUrl.cancelorder
                        + "?order_id=" + orderId
                        + "&assign_shop_id=" + shopId
                        //+ "?manager_id=" + sharedPreferencesUtils.getManagerId()
                        + "&assign_delboy_id=" + assign_delboy_id
                        + "&reason_of_rejection=" + URLEncoder.encode(reason_of_rejection,"utf-8")//URLEncoder.encode(product_name,"utf-8")
                        + "&area="+URLEncoder.encode(area,"utf-8")
                        + "&tblName="+sharedPreferencesUtils.getAdminType();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Log.e("Parma_cancel_order:",""+urlCancelOrder);

            StringRequest stringRequestCancelOrder = new StringRequest(Request.Method.GET,
                    urlCancelOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.e("CancelResp:", "" + response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    //JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                                    //Log.e("cancel_Order_Res:", "" + jsonObjectData.toString());
                                    sendcancelmail();

                                    if (flag.equals("new_order")) {
                                        Intent intent = new Intent(mContext, OrderListActivity.class);
                                        intent.putExtra("flag", "new_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                    }else if (flag.equals("test_order")) {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        intent.putExtra("flag", "new_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }else {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        intent.putExtra("flag", "new_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                } else if (!status) {
                                    Toast.makeText(mContext, "Sorry. This order you can not Cancel Now.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestCancelOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    //assign order to delivery boy, this method added on AddDeliverBoyActivity
    public void assignOrder(String delBoyId) {
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.acceptorder
                    + "?order_id=" + orderId
                    //+ "?manager_id=" + sharedPreferencesUtils.getManagerId()
                    + "&assign_delboy_id=" + delBoyId;

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status = true) {
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext, "Order Assigned. Please check in your Your Order list", Toast.LENGTH_SHORT).show();
                                } else if (status = false) {
                                    Toast.makeText(mContext, "Sorry. This order you can not assign.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
            super.onBackPressed();
            finish();
        }else {
            super.onBackPressed();
           // finish();
            /*Intent intent = new Intent(mContext, OrderListActivity.class);
             intent.putExtra("flag", flag);
            intent.putExtra("areaName", areaName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);*/

            Intent intent = new Intent(mContext, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
             startActivity(intent);
             finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                 finish();
                }else {
                  /*  Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("flag", flag);
                    intent.putExtra("areaName", areaName);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                  */
                    Intent intent = new Intent(mContext, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String CurrentTimeData() {
        //"action_time":"2019-06-19 1:11pm"
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String formattedDate = dateFormat.format(new Date()).toString();
        System.out.println(formattedDate);
        Log.e("Current_DateTime", "" + formattedDate);
        return formattedDate;

    }

    private String ActionTimeData(String order_date) {
        //"action_time":"2019-06-19 1:11pm"
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        //String formattedDate = dateFormat.format(new Date()).toString();
        String formattedDate = dateFormat.format("2019-06-19 1:11pm");
        System.out.println(formattedDate);
        Log.e("Current_DateTime", "" + formattedDate);
        return formattedDate;
    }

    private void DiffCalculate(String action_time) {

        try {
            Date userDob = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(action_time);
            //Date userDob = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse("2019-06-22 10:11");
            Date today = new Date();
            long diff = today.getTime() - userDob.getTime();
            int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
            int hours = (int) (diff / (1000 * 60 * 60));
            int minutes = (int) (diff / (1000 * 60));
            int seconds = (int) (diff / (1000));

            Log.e("Diffrence is:", "" + numOfDays + "/" + hours + "/" + minutes + "/" + seconds);

            if (numOfDays > 1) {
                //btn_cancelorder.setVisibility(View.GONE);
                //Toast.makeText(mContext,"Exceed the Time in :" +numOfDays+"Days",Toast.LENGTH_LONG).show();

            } else {
                if (hours > 1) {
                  //  btn_cancelorder.setVisibility(View.GONE);
                    //  Toast.makeText(mContext,"Exceed the Time :"+hours+"Hours",Toast.LENGTH_LONG).show();
                } else {
                    if (minutes > 15) {
                    //    btn_cancelorder.setVisibility(View.GONE);
                        //    Toast.makeText(mContext,"Exceed the Time:"+minutes+"Minutes",Toast.LENGTH_LONG).show();
                    } else if (minutes < 15) {
                      //  btn_cancelorder.setVisibility(View.VISIBLE);
                       // Toast.makeText(mContext, "Yes you can Cancel the Order", Toast.LENGTH_LONG).show();
                    }
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }



    }

    //check Rejection status
    //cancel order String assign_shop_id=shopId
    public void rejectionStatus(String shopId, String orderId) {
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();

            // String urlCancelOrder =  String.format(StaticUrl.cancelorder,orderId,shopId,assign_delboy_id,reason_of_rejection);

            String rejectorder = StaticUrl.checkintrestedbut
                    + "?order_id=" + orderId
                    //+ "?manager_id=" + sharedPreferencesUtils.getManagerId()
                    + "&assign_shop_id=" + shopId;

            Log.e("rejectURL",""+rejectorder);


            StringRequest stringRequestCancelOrder = new StringRequest(Request.Method.GET,
                    rejectorder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.e("RejectResp:", "" + response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    btn_intrested_but.setVisibility(View.GONE);
                                } else if (!status) {
                                    btn_intrested_but.setVisibility(View.VISIBLE);
                                    //Toast.makeText(mContext, "Sorry. This order you can not Cancel Now.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestCancelOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    // Dialog for Time slot
    public void selectTimeSlot() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_custom_time_slots, null);

        final Spinner timeslot = (Spinner) view.findViewById(R.id.sp_time_slots);
        final Button btn_submitreson = (Button) view.findViewById(R.id.btn_time_slots);
        final String[] selected_timeslot = new String[1];
       final ArrayAdapter<CharSequence> timeslotadaptor = ArrayAdapter.createFromResource(OrderDetailActivity.this, R.array.Time_slots, android.R.layout.simple_spinner_item);
        timeslotadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeslot.setAdapter(timeslotadaptor);

        /*timeslot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selected_timeslot[0] =timeslot.getSelectedItem().toString();
                Log.e("selected_timeslot:",""+selected_timeslot);
            }
        });*/

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!timeslot.getSelectedItem().toString().equalsIgnoreCase("Please Select Delivery Time")) {
                    selected_timeslot[0] = timeslot.getSelectedItem().toString();
                    Log.e("selected_timeslot:", "" + selected_timeslot[0].toString());
                     acceptOrder(sharedPreferencesUtils.getShopID(), orderId,delivery_time);
                    dialog.dismiss();
                }else {
                    Toast.makeText(mContext,"Please Select Delivery Time",Toast.LENGTH_LONG).show();
                }

            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

    public void selectTimeSlot2(final String name[], final String type) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.reason_list_dialog, null);

        final ListView lv = view.findViewById(R.id.lv_reasonlist);
        Button btn_submitreson = view.findViewById(R.id.btn_reason_list);
        TextView tv_reasonlist = view.findViewById(R.id.tv_reasonlist);
        tv_reasonlist.setText("Select Time Slot");

        if(type.equalsIgnoreCase("Next3Hours")){
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Next3Hours);
            lv.setAdapter(adapter);
            lv.getSelectedItemPosition();
        }else {
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, name);
            lv.setAdapter(adapter);
            lv.getSelectedItemPosition();
        }




        //lv.setAdapter(new ArrayAdapter<String>(mContext,android.R.layout.simple_list_item_1, name));
        //lv.getChildAt(lastSelectionPosition).setBackgroundColor(Color.parseColor("#00743D"));


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                delivery_time = name[position];
                //lv.getChildAt(position).setBackgroundColor(Color.parseColor("#00743D"));
                //lastSelectionPosition = adapter.getPosition(delivery_time);
                /*lastSelectionPosition=position;
                adapter.notifyDataSetChanged();
                view.setSelected(true);*/
                toggleBackgroundItem(view);
            }
        });

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (delivery_time.equals("")) {
                    Toast.makeText(mContext, "Select Time slot", Toast.LENGTH_SHORT).show();
                } else {
                    acceptOrder(sharedPreferencesUtils.getShopID(), orderId,delivery_time);//delivery_time
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(view);

        dialog.show();
    }



    public String calculateDeliveryTime2(){

        String ordedate = order_date;
        Log.e("ordedate",""+delivery_time_get+" "+delivery_method);

        if(delivery_time_get!=null||delivery_method!=null) {
            //int  cmpvallue = Integer.parseInt(finalString);  /*	$time1='9 AM - 12 PM';
            //	$time2='12 PM - 3 PM';
            //	$time3='3 PM - 6 PM';
            //	$time4='6 PM - 9 PM';*/
            //String   cmpvallue = "3 PM - 6 PM";
            Log.e("delivery_time", delivery_time_get);
            //delivery_time":"11:21 AM - 02:21 PM"
            String cmpvallue = delivery_time_get;

            if (delivery_method.equals("Within next 3 Hrs")) {
                slot = "nine";
                calculateNext3Hours(delivery_time_get);
            }else if (delivery_method.equals("Within next 2 Hrs")) {
                slot = "nine";
                calculateNext3Hours(delivery_time_get);
            }else if (delivery_method.equals("Within next 12 Hrs")) {
                slot = "nine";
                //calculateNext3Hours(delivery_time_get);
                Next3Hours.add(delivery_time_get);
            } else {

                getTimeSlots(delivery_time_get);
            }
        }

        return slot;

    }

    private void calculateNext3Hours(String timeValue){

        Next3Hours.clear();
        String fslot = timeValue.substring(0,8);
        String to_time = timeValue.substring(timeValue.length()- 8);
        Log.e("ActualTime_slot:",""+fslot+"-"+to_time);
        String fromTime = fslot;
        String toTime = to_time;
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        try {
            Date dStart = df.parse(fromTime);
            Date dEnd = df.parse(toTime);
            System.out.println(dStart);
            System.out.println(dEnd);
            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTime(dStart);
            while(calendarStart.getTime().before(dEnd)){
                 String v1 =df.format(calendarStart.getTime());
                calendarStart.add(Calendar.MINUTE, 60);
                String v2 = df.format(calendarStart.getTime());
                Log.e("Slot_AMPM:",""+v1+" - "+v2);
                Next3Hours.add(v1+" - "+v2);
            }
        }catch (Exception E){

        }

    }

    /**
     * Called when a swipe gesture triggers a refresh.
     */
    @Override
    public void onRefresh() {
        getOrderCartList();
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    // Get User Name of Order id
    public void getUserName(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.getUserName
                    +"?userid="+userId;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){

                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                                    user_fname = jsonObjectData.getString("fname");
                                    user_contactno = jsonObjectData.getString("contactno");
                                    tv_user_name.setText("Name: "+user_fname+" "+"Contact:"+user_contactno);
                                    Log.e("Orde_User_Data",""+jsonObjectData);

                                }else if(!statuss) {

                                    Toast.makeText(mContext,"No User Found..!!",Toast.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    // CancelCartItems
    public void CancelCartItem(String user_id, String cart_user_id, String cart_unique_id, String product_name, String product_id, String product_size, String product_mrp ,String iten_qty, String product_cat, String item_price) {
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();
//cancelcartitem?shop_id=313&user_id=4063&order_id=4722&cart_user_id=abcd&cart_unique_id=xyz&product_id=2089&product_name=amul&product_size=1&product_mrp=99&iten_qty=1&product_cat=Grocery
            String cancelcartitem = null;
            try {
                cancelcartitem = StaticUrl.cancelcartitem
                        + "?shop_id=" + sharedPreferencesUtils.getShopID()
                        + "&user_id=" +  user_id
                        + "&order_id=" +  orderId
                        + "&cart_user_id=" + cart_user_id
                        + "&cart_unique_id=" + cart_unique_id
                        + "&product_id=" + product_id
                        + "&product_name=" + URLEncoder.encode(product_name,"utf-8")
                        + "&product_size=" + product_size
                        + "&product_mrp="+ product_mrp
                        + "&iten_qty="+ iten_qty
                        + "&product_cat="+ URLEncoder.encode(product_cat,"utf-8")
                        + "&item_price="+ item_price;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            // + "&delivery_time="+delivery_time;
            Log.d("urlCancelOrderItem",cancelcartitem);
            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    cancelcartitem,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                Log.e("cancelCartRes:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status = true) {
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    //Toast.makeText(mContext, "Cart item inserted", Toast.LENGTH_SHORT).show();



                                } else if (status = false) {
                                    Toast.makeText(mContext, "Sorry. This order you can not accept.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
     // Send email to admin when order is cancel by the shop keeper
    public void sendcancelmail(){

        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.sendcancelmail
                    +"?shop_id="+sharedPreferencesUtils.getShopID()
                    + "&order_id=" +  orderId;

            //progressDialog.setMessage("Please wait...");
            //progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.e("Email_Rs:",""+response);

                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){

                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                                   /*String cart_unique_id = jsonObjectData.getString("cart_unique_id");
                                    Log.e("cart_unique_id",""+cart_unique_id);
                                    Log.e("CancelOrderEmail",""+jsonObjectData);*/

                                    if (flag.equals("new_order")) {
                                        Intent intent = new Intent(mContext, OrderListActivity.class);
                                        intent.putExtra("flag", "new_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                    }else if (flag.equals("test_order")) {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        intent.putExtra("flag", "new_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }else {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        intent.putExtra("flag", "new_order");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }

                                }else if(!statuss) {

                                    Toast.makeText(mContext,"No Items Found",Toast.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void getTimeSlots(final String slot) { //TODO Server method here
        if (Connectivity.isConnected(mContext)) {

            Slot_schedules = new ArrayList<>();

            JSONObject params = new JSONObject();
            try {
                params.put("slot", slot);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.newScheduleforShopkeeper, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("TimeslotRes:",""+response);

                    if (response.isNull("posts1")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts1");
                            for (int i = 0; i < categoryJsonArray.length(); i++) {
                                JSONObject jsonCatData = categoryJsonArray.getJSONObject(i);
                                Slot_schedules.add(jsonCatData.getString("time"));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private String twentyFourTo_12HoursFormat(String order_date2){
        String final_value = null;
        String date_string = null;
        String time_sting = null;
        try {
            //String _24HourTime = "22:15";
            //String _24HourTime = order_date2;
            Log.e("MyOrderDate:",""+order_date);//2019-10-07 16:20:00

            time_sting = order_date.substring(order_date.length() - 8);
            date_string = order_date.substring(0,10);

            String _24HourTime = time_sting;

            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            Log.e("_24HourDt",""+_24HourDt.toString());
            Log.e("_12HourSDF"," "+_12HourSDF.format(_24HourDt));
            final_value = date_string+""+_12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return final_value;
    }

    //Get Distributor order payments status Details  with Images
    public void approve_distributorPayment( ) {
        if (Connectivity.isConnected(mContext)) {
            progressDialog.show();
            String urlAcceptOrder = null;
            try {
                urlAcceptOrder = StaticUrl.Getdeliveredproducts
                        + "?order_id=" + orderId
                        + "&shop_id=" + sharedPreferencesUtils.getShopID()
                        + "&tblName="+sharedPreferencesUtils.getAdminType()
                        + "&cart_unique_id="+cartUniqueId;
                        //+ "&product_brand="+product_brand;
                //+ "&test_order="+"test_order";
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Getdeliveredproducts",urlAcceptOrder);


            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                Log.e("Getdeliveredproducts:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status == true) {

                                    //Toast.makeText(mContext, "Order Accepted. Successfully", Toast.LENGTH_SHORT).show();

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject jsonObjectData =  jsonArray.getJSONObject(i);
                                          String value = jsonObjectData.getString("distributor_approve");
                                          //String imag_url = jsonObjectData.getString("image5");
                                          String imag_url = jsonObjectData.getString("payment_screenshot");
                                          if(value.equalsIgnoreCase("Approved")){
                                              cv_manufacturer_approve.setVisibility(View.VISIBLE);

                                              try {
                                                  Picasso.with(mContext).load(imag_url)
                                                          .placeholder(R.drawable.loading)
                                                          .error(R.drawable.loading)
                                                          .into(iv_payment);
                                                  tv_payment_type.setText(jsonObjectData.getString("payment_type"));
                                              }catch (Exception e){
                                                  e.getMessage();
                                              }

                                          }
                                    }



                                } else if (status == false) {
                                    // Toast.makeText(mContext, "Sorry. This order you can not accept.", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(mContext, jsonObject.getString("Not Approve Yet"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    // Approve by Manufacturer which is already delivered by Distributor

    private String Approveby_manufacturer(String product_brand, String cartUniqueId) { //TODO Server method here

        final String[] result_string = {""};

        if (Connectivity.isConnected(mContext)) {
            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
            JSONObject params = new JSONObject();
            try {
                params.put("cart_unique_id", cartUniqueId);
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                params.put("order_id", orderId);
                params.put("product_brand", product_brand);
                params.put("tblName", sharedPreferencesUtils.getAdminType());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAccept_by_distributor, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("AcceptDistributor_Res:",""+response);

                    if (response.isNull("posts")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < categoryJsonArray.length(); i++) {
                                JSONObject jsonCatData = categoryJsonArray.getJSONObject(i);
                                String value = jsonCatData.getString("accept");
                                result_string[0] = value;
                                    if(value.equalsIgnoreCase("Approved Successfully"))
                                    {
                                        //Approved Successfully
                                        DialogApproved();
                                    }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }

        return result_string[0];
    }

    private void DialogApproved(){
        new AlertDialog.Builder(this)
                .setMessage("Payment Approved Successfully ")
                .setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Call Distributor Accept API
                        dialog.dismiss();
                        // accept_Distributor(product_brand);
                    }
                })
                .show();
    }

    private void getSimilarProducts(String pid) {
        //String pid = null;
        String strPName = null;
        String strPSize = null;
        /*for (int i = 0; i < allProductItemsList.size(); i++) {
            productCodeWithProductList movie = allProductItemsList.get(i);
            int selectedPosition = movie.getPosition();
            ProductCodeWiseProduct forSimilarProduct = movie.productCodeWiseProducts.get(selectedPosition);

            pid = forSimilarProduct.getProduct_id();
            strPName = forSimilarProduct.getProduct_name();
            strPSize = forSimilarProduct.getProduct_size();
        }*/


        dialog = DialogPlus.newDialog(OrderDetailActivity.this)
                .setContentHolder(new ViewHolder(R.layout.activity_similar_products))
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.BOTTOM)
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        getOrderCartList();
                        dialog.dismiss();
                    }
                }).setOnBackPressListener(new OnBackPressListener() {
                    @Override
                    public void onBackPressed(DialogPlus dialog) {
                        getOrderCartList();
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();


        similarProductLinearLayout = (LinearLayout) dialog.findViewById(R.id.similarProductLinearLayout);
        similarProductRecyclerView = (RecyclerView) dialog.findViewById(R.id.similarProductRecyclerView);
        similarProductRecyclerView.setHasFixedSize(true);
        LinearLayoutManager similarLayoutManager = new LinearLayoutManager(OrderDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        similarProductRecyclerView.setLayoutManager(similarLayoutManager);
        similarProductsGetFromServer(pid);


    }

    private void similarProductsGetFromServer(final String click_product_id) { //TODO Server method here
        if (Connectivity.isConnected(OrderDetailActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(OrderDetailActivity.this);
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("city", "Any State from India");
                params.put("area", "Any City from India");
                params.put("product_id", click_product_id);
                params.put("contactno", user_contactno);
                Log.e("Similar_p_parm",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlSimilarResult11, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            Toast.makeText(OrderDetailActivity.this, "There are no similar products", Toast.LENGTH_LONG).show();
                            similarProductLinearLayout.setVisibility(View.GONE);
                            //dialog.dismiss();
                        } else {
                            similarProductListHashMap.clear();
                            similarProductWiseList.clear();
                            SimilarFinalList.clear();

                            similarProductLinearLayout.setVisibility(View.VISIBLE);
                            JSONArray mainShopJsonArray = response.getJSONArray("posts");

                            Log.e("SingleProduct_pstatus:",""+response.toString());

                            for (int i = 0; i < mainShopJsonArray.length(); i++) {
                                JSONObject jSonShopData = mainShopJsonArray.getJSONObject(i);


                                similarProductListHashMap.put(jSonShopData.getString("code"), "");

                                similarProductWiseList.add(new ProductCodeWiseProduct(jSonShopData.getString("code"), jSonShopData.getString("shop_id"),
                                        jSonShopData.getString("shop_name"), jSonShopData.getString("shop_category"),
                                        jSonShopData.getString("product_name"), jSonShopData.getString("product_brand"),
                                        jSonShopData.getString("product_id"), jSonShopData.getString("product_image"),
                                        jSonShopData.getString("product_image1"), jSonShopData.getString("product_image2"),
                                        jSonShopData.getString("product_image3"), jSonShopData.getString("product_image4"),
                                        jSonShopData.getString("product_size"), jSonShopData.getString("product_mrp"),
                                        jSonShopData.getString("product_price"), jSonShopData.getString("product_discount"),
                                        jSonShopData.getString("product_description"), jSonShopData.getString("similar_product_status"),
                                        jSonShopData.getString("type"), jSonShopData.getString("product_qty"),jSonShopData.getString("product_maincat"), jSonShopData.getString("hindi_name"),jSonShopData.getString("cart_pstatus")));
                            }
                            dialog.show();
                            for (Object o : similarProductListHashMap.keySet()) {
                                String key = (String) o;
                                productCodeWithProductList withProductCode = new productCodeWithProductList();
                                withProductCode.code = key;
                                withProductCode.similarProductCodeWiseProducts = new ArrayList<>();
                                for (ProductCodeWiseProduct pp : similarProductWiseList) {
                                    if (pp.code.equals(key)) {
                                        withProductCode.similarProductCodeWiseProducts.add(pp);
                                    }
                                }
                                SimilarFinalList.add(withProductCode);
                            }
                            //similarProductCardAdapter = new SimilarProductCardAdapter(SimilarFinalList,OrderDetailActivity.this);
                            similarProductCardAdapter = new SimilarProductCardAdapter(SimilarFinalList,OrderDetailActivity.this,cartUniqueId,userId,orderId,click_product_id,user_contactno);
                            similarProductRecyclerView.setAdapter(similarProductCardAdapter);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(OrderDetailActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(OrderDetailActivity.this).addToRequestQueue(request);
        } else {
            Log.e("Similler_items",""+"error");
        }
    }

    private void sendConfirmEmailbyShop() { //TODO Server method here
        if (Connectivity.isConnected(OrderDetailActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(OrderDetailActivity.this);
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);
            progressDialog.show();
            JSONObject params = new JSONObject();
            try {
                params.put("user_id", userId);
                params.put("order_id", orderId);
                params.put("cart_unique_id",cartUniqueId);
                params.put("contactno", user_contactno);
                params.put("shop_id", sharedPreferencesUtils.getShopID());

                Log.e("Similar_p_parm",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlsend_UpdatedCart_Email, params, new Response.Listener<JSONObject>() {
           // JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlsend_AddedCart_Email, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            Toast.makeText(OrderDetailActivity.this, "Email Not Send", Toast.LENGTH_LONG).show();
                            similarProductLinearLayout.setVisibility(View.GONE);
                            dialog.dismiss();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                           // JSONArray mainShopJsonArray = response.getJSONArray("posts");
                            //String fname = response.getString("fName");

                            Log.e("UpdateCart_Email:",""+response.toString());
                            //Log.e("fname:",""+fname);


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDialog.dismiss();
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(OrderDetailActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(OrderDetailActivity.this).addToRequestQueue(request);
        } else {
            Log.e("Similler_items",""+"error");
            progressDialog.dismiss();
        }
    }

    // Product Added by the Shopkeeper

    private void sendAddedEmailbyShop() { //TODO Server method here
        if (Connectivity.isConnected(OrderDetailActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(OrderDetailActivity.this);
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);
            progressDialog.show();
            JSONObject params = new JSONObject();
            try {
                params.put("user_id", userId);
                params.put("order_id", orderId);
                params.put("cart_unique_id",cartUniqueId);
                params.put("contactno", user_contactno);
                params.put("shop_id", sharedPreferencesUtils.getShopID());

                Log.e("Similar_p_parm",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlsend_AddedCart_Email, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            Toast.makeText(OrderDetailActivity.this, "Email Not Send", Toast.LENGTH_LONG).show();
                            similarProductLinearLayout.setVisibility(View.GONE);
                            dialog.dismiss();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            // JSONArray mainShopJsonArray = response.getJSONArray("posts");
                            //String fname = response.getString("fName");

                            Log.e("UpdateCart_Email:",""+response.toString());
                            //Log.e("fname:",""+fname);


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDialog.dismiss();
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(OrderDetailActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(OrderDetailActivity.this).addToRequestQueue(request);
        } else {
            Log.e("Similler_items",""+"error");
            progressDialog.dismiss();
        }
    }

    private void RemoveCartItemsFromServer(String pId, String maincategory) { //TODO Server method here
        JSONObject params = new JSONObject();
            progressDialog.show();

        try {
            params.put("product_id", pId);
            params.put("shop_id", "78");
            params.put("selectedType", "");
            params.put("versionCode", "2.0.37");
            params.put("Qty", 1);
            params.put("cart_unique_id", cartUniqueId);
            params.put("order_id", orderId);
            params.put("contactNo", user_contactno);
            params.put("sessionMainCat", maincategory);
            Log.e("AddtoCart_param",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlremoveCartItemByShop, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    Log.e("RemoveCartRes:",""+response);
                    String posts = response.getString("posts");
                    //String promotional = response.getString("promotional");
                    //String promotional_message = response.getString("promo_message");
                    progressDialog.dismiss();

                    if (posts.equals("true")){
                        Log.d("RemoveCartItemsServer",""+posts);
                        Toast toast = Toast.makeText(mContext, "Removed  product From cart.", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }else {
                        //openSessionDialog(posts,"session");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                GateWay gateWay = new GateWay(mContext);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    //User Terms Condition Options
    public void getUserTerms() {

        progressDialog.show();
        cartitemArrayList.clear();

        String urlCartlist = StaticUrl.userTermsCondition
                + "?order_id=" + orderId;


        Log.d("userTermsCondition", urlCartlist);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlCartlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("SingleItemRes", response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            String user_terms = jsonObject.getString("user_terms");
                            String user_remark = jsonObject.getString("user_remark");
                            tv_user_terms.setVisibility(View.VISIBLE);
                            tv_user_terms.setText("Alternate Brand and All Items:"+user_terms);
                            tv_user_remarks.setText("User Remark: "+user_remark);
                            if ((strStatus == true)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() > 0) {

                                    //cartListAdapter.notifyDataSetChanged();
                                } else {
                                    //tv_network_con.setText("No Records Found");
                                }
                            } else if (strStatus == false) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(requestCartList);

    }

    //Method to Add the new Product into the cart
    public void AddProduct_Dialog() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.activity_orderbyid, null);

        Button btn_submitreson = view.findViewById(R.id.btn_search);
        final EditText et_product_name = view.findViewById(R.id.et_product_name);
        final EditText et_product_qty = view.findViewById(R.id.et_product_qty);
        final EditText et_product_size = view.findViewById(R.id.et_product_size);
        final EditText et_product_price = view.findViewById(R.id.et_product_price);
        // getOrderCartList();

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String product_name = et_product_name.getText().toString();
                String qty = et_product_qty.getText().toString();
                String product_size = et_product_size.getText().toString();
                String product_price = et_product_price.getText().toString();

                InsertCartbyShopkeeper(product_name,qty,product_size,product_price);
                dialog.dismiss();

                //}
            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

    // API Call for the Insert new Product into the cart_items
    private void InsertCartbyShopkeeper(String product_name, String iten_qty, String product_size, String item_price) { //TODO Server method here
        if (Connectivity.isConnected(OrderDetailActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(OrderDetailActivity.this);
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);
            progressDialog.show();
            JSONObject params = new JSONObject();
            try {
                params.put("user_id", userId);
                params.put("cart_unique_id",cartUniqueId);
                params.put("order_id",orderId);
                params.put("product_name", product_name);
                params.put("item_price", item_price);
                params.put("iten_qty", iten_qty);
                params.put("product_size", product_size);
                params.put("shop_id", sharedPreferencesUtils.getShopID());

                Log.e("InsertToCart_parm",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urladdToCartbyShopkeeper, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            Toast.makeText(OrderDetailActivity.this, "Product Not Inserted", Toast.LENGTH_LONG).show();

                            dialog.dismiss();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            // JSONArray mainShopJsonArray = response.getJSONArray("posts");
                            //String fname = response.getString("fName");
                            Toast.makeText(OrderDetailActivity.this, "Product Inserted Successfully", Toast.LENGTH_LONG).show();
                            Log.e("UpdateCart_Product:",""+response.toString());
                            getOrderCartList();
                            sendAddedEmailbyShop();

                            //Log.e("fname:",""+fname);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDialog.dismiss();
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(OrderDetailActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(OrderDetailActivity.this).addToRequestQueue(request);
        } else {
            Log.e("Similler_items",""+"error");
            progressDialog.dismiss();
        }
    }

}
