package com.podmerchant.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.StaffListAdaptor;
import com.podmerchant.adapter.StaffListReferAdapter;
import com.podmerchant.model.StaffModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class StaffListReferActivity extends AppCompatActivity {

    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;
    public static final String TAG = "StaffListReferActivity";
    AlertDialog progressDialog;
    ArrayList<StaffModel> staffModelArrayList,staffModelArrayList2;
    RecyclerView recycler_view_staff;
    private RecyclerView.LayoutManager layoutManager;
    private StaffListReferAdapter staffListAdaptor;
    RelativeLayout rl_selfsignup;
    TextView tv_selfName;
    EditText et_search_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_list_refer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.staff_list_refer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = StaffListReferActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        recycler_view_staff = findViewById(R.id.recycler_view_staff);
        rl_selfsignup = findViewById(R.id.rl_selfsignup);
        tv_selfName= findViewById(R.id.tv_selfName);
        et_search_contact = findViewById(R.id.et_search_contact);

        tv_selfName.setText(sharedPreferencesUtils.getUserName());
        staffModelArrayList = new ArrayList<>();
        staffModelArrayList2 = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        recycler_view_staff.setLayoutManager(layoutManager);
        staffListAdaptor = new StaffListReferAdapter(mContext,staffModelArrayList);
        recycler_view_staff.setAdapter(staffListAdaptor);

        getStaffList();

        et_search_contact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //if(s.length()>=2) {
                staffModelArrayList2.clear();
                for (int i = 0; i < staffModelArrayList.size(); i++) {
                    if(staffModelArrayList.get(i).getMobile().contains(s.toString())){
                        //if(staffModelArrayList.get(i).getStaffName().contains(s.toString())) {
                        staffModelArrayList2.add(staffModelArrayList.get(i));
                    }
                }

                staffListAdaptor = new StaffListReferAdapter(mContext, staffModelArrayList2);
                recycler_view_staff.setAdapter(staffListAdaptor);
                staffListAdaptor.notifyDataSetChanged();
                //}
            }

            @Override
            public void afterTextChanged(Editable s) {



            }
        });
        //staff list
        recycler_view_staff.addOnItemTouchListener(new StaffListReferActivity.RecyclerTouchListener(mContext, recycler_view_staff,
                new StaffListReferActivity.RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        Toast.makeText(mContext,staffModelArrayList.get(position).getStaffName(), Toast.LENGTH_SHORT).show();

                        signupSugestedBy(staffModelArrayList.get(position).getId());
                        /*intent.putExtra("staffName", staffModelArrayList.get(position).getStaffName());
                        intent.putExtra("staffAge", staffModelArrayList.get(position).getAge());
                        intent.putExtra("staffImage", staffModelArrayList.get(position).getImageUrl());
                        setResult(RESULT_OK, intent);
                        finish();*/
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        rl_selfsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signupSugestedBy("self");
            }
        });

    }

    private void getStaffList(){
        //get Mobile verify
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.stafflist
            +"?city="+sharedPreferencesUtils.getRCity();
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                staffModelArrayList.clear();
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    Log.e("ProfileResponse:",""+jsonArray.toString());

                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        StaffModel staffModel = new StaffModel();
                                        staffModel.setId(jsonObject1.getString("id"));
                                        staffModel.setStaffName(jsonObject1.getString("staffname"));
                                        staffModel.setMobile(jsonObject1.getString("contact"));
                                        //staffModel.setAge(jsonObject1.getString("age"));
                                        staffModel.setImageUrl(jsonObject1.getString("image"));
                                        staffModelArrayList.add(staffModel);
                                    }
                                    Log.e("arraylist size", String.valueOf(staffModelArrayList.size()));
                                    staffListAdaptor.notifyDataSetChanged();
                                }else if(status==false) {
                                    Toast.makeText(mContext,"No Records Found",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    //update sign up
    private void signupSugestedBy(String signUpBy){
            if(Connectivity.isConnected(mContext)){
                String targetUrl = StaticUrl.signupSugestedBy+"?shop_id="+sharedPreferencesUtils.getShopID()+"&signUpBy="+signUpBy;

                progressDialog.show();

                StringRequest  requestSignUpBy = new StringRequest(Request.Method.GET,
                        targetUrl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                try {
                                JSONObject jsonObject =  new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true) {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        startActivity(intent);
                                        finishAffinity();
                                    }else if(status==false){

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                            }
                        });
                MySingleton.getInstance(mContext).addToRequestQueue(requestSignUpBy);
            }else {
                Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
            }
    }
    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  StaffListReferActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final StaffListReferActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

}
