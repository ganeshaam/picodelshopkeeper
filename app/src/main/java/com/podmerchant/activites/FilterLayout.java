package com.podmerchant.activites;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.podmerchant.R;
import com.podmerchant.fragment.filter_fragments.BrandFilter;
import com.podmerchant.fragment.filter_fragments.DepartmentFilter;
import com.podmerchant.fragment.filter_fragments.OtherFilter;
import com.podmerchant.fragment.filter_fragments.PriceFilter;
import com.podmerchant.fragment.filter_fragments.SellerFilter;
import com.podmerchant.fragment.filter_fragments.SortFilter;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;

public class FilterLayout extends AppCompatActivity implements View.OnClickListener {

    private final String tag = "department";
    private String tag_in_Filter_Layout;
    private String strSelectedName;
    private String strCity;
    private String strArea;
    private final String DEPARTMENT_TAG = "department", SORT_TAG = "sort", BRAND_TAG = "brand", PRICE_TAG = "price",
            SELLER_TAG = "seller", OTHER_TAG = "other";
    private int openView;
    private TextView tvDepartment;
    private TextView tvSort;
    private TextView tvBrand;
    private TextView tvPrice;
    private TextView tvSeller;
    private TextView tvOther;
    public static TextView tvPriceFilterCount, tvSortFilterCount, tvSellerFilterCount, tvDepartmentFilterCount, tvBrandFilterCount,
            tvOtherFilterCount;
    private ImageView imageViewDepartment, imageViewSort, imageViewBrand, imageViewPriceNOffer, imageViewSeller, imageViewOther;
    private FragmentManager fm;
    private AlertDialog alertDialog;
    private LinearLayout departmentLayout, sortLayout, brandLayout, priceLayout, sellerLayout, otherLayout;
    private ArrayList<String> localOtherFilterCheckList = new ArrayList<>();
    public static Set<String> categoryDependentUniqueBrandNames = new TreeSet<>();
    public static Set<String> categoryDependentUniqueSizeList = new TreeSet<>();
    public static Set<String> categoryAndBrandDependentUniqueSizeList = new TreeSet<>();
    private ArrayList<String> localBrandFilterCheckList = new ArrayList<>();
    private final String class_name = this.getClass().getSimpleName();
    private RelativeLayout filterMainLayout;
    private TextView txtNoConnection;
    private BroadcastReceiver myReceiver;
    private LinearLayout Main_Layout_NoInternet;

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                filterMainLayout.setVisibility(View.GONE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                filterMainLayout.setVisibility(View.VISIBLE);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    private FragmentRefreshListener fragmentRefreshListener;

    public FilterLayout() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_layout);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_filter_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strSelectedName = bundle.getString("selectedName");
            openView = bundle.getInt("open");
        }

        myReceiver = new Network_Change_Receiver();
        GateWay gateWay = new GateWay(this);
        strCity = "Pune";//gateWay.getCity();
        strArea = "Hadpser";//gateWay.getArea();

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        txtNoConnection = findViewById(R.id.txtNoConnection);
        tvDepartmentFilterCount = findViewById(R.id.txtDepartmentFilterCount);
        filterMainLayout = findViewById(R.id.filterMainLayout);
        tvSortFilterCount = findViewById(R.id.txtSortFilterCount);
        tvBrandFilterCount = findViewById(R.id.txtBrandFilterCount);
        tvPriceFilterCount = findViewById(R.id.txtPriceFilterCount);
        tvSellerFilterCount = findViewById(R.id.txtSellerFilterCount);
        tvOtherFilterCount = findViewById(R.id.txtOtherFilterCount);

        departmentLayout = findViewById(R.id.departmentLayout);
        sortLayout = findViewById(R.id.sortLayout);
        brandLayout = findViewById(R.id.brandLayout);
        priceLayout = findViewById(R.id.priceLayout);
        sellerLayout = findViewById(R.id.sellerLayout);
        otherLayout = findViewById(R.id.otherLayout);

        tvDepartment = findViewById(R.id.txtDepartment);
        tvSort = findViewById(R.id.txtSort);
        tvBrand = findViewById(R.id.txtBrand);
        tvPrice = findViewById(R.id.txtPrice);
        tvSeller = findViewById(R.id.txtSeller);
        tvOther = findViewById(R.id.txtOther);
        TextView tvApply = findViewById(R.id.txtApply);
        TextView tvClear = findViewById(R.id.txtClear);

        imageViewDepartment = findViewById(R.id.imgDepartment);
        imageViewSort = findViewById(R.id.imgSort);
        imageViewBrand = findViewById(R.id.imgBrand);
        imageViewPriceNOffer = findViewById(R.id.imgPrice);
        imageViewSeller = findViewById(R.id.imgSeller);
        imageViewOther = findViewById(R.id.imgOther);

        fm = getSupportFragmentManager();

        tag_in_Filter_Layout = tag;

        //to display the just 1st view
        changeView(tag);

        displayView(openView);

        departmentLayout.setOnClickListener(this);
        sortLayout.setOnClickListener(this);
        brandLayout.setOnClickListener(this);
        priceLayout.setOnClickListener(this);
        sellerLayout.setOnClickListener(this);
        otherLayout.setOnClickListener(this);
        tvApply.setOnClickListener(this);
        tvClear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.departmentLayout:
               /* UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on department layout", FilterLayout.this);
*/
                changeView(tag);
                openView = 0;
                displayView(openView);
                tag_in_Filter_Layout = tag;
                break;

            case R.id.sortLayout:

  /*              UserTracking UT1 = new UserTracking(UserTracking.context);
                UT1.user_tracking(class_name + ": clicked on sort layout", FilterLayout.this);
*/
                String tag1 = "sort";
                changeView(tag1);
                openView = 1;
                displayView(openView);
                tag_in_Filter_Layout = tag1;
                break;

            case R.id.brandLayout:

  /*              UserTracking UT2 = new UserTracking(UserTracking.context);
                UT2.user_tracking(class_name + ": clicked on brand layout", FilterLayout.this);
*/
                String tag2 = "brand";
                changeView(tag2);
                openView = 2;
                tag_in_Filter_Layout = tag2;
                JSONArray jsonCatArray = new JSONArray();
                jsonCatArray = new JSONArray(DepartmentFilter.departmentNameList);
                if (jsonCatArray.length() != 0) {
                    getDependentDataOnCategory(jsonCatArray);
                } else {
                    displayView(openView);
                }
                break;

            case R.id.priceLayout:

  /*              UserTracking UT3 = new UserTracking(UserTracking.context);
                UT3.user_tracking(class_name + ": clicked on price layout", FilterLayout.this);
*/
                String tag3 = "price";
                changeView(tag3);
                openView = 3;
                displayView(openView);
                tag_in_Filter_Layout = tag3;
                break;

            case R.id.sellerLayout:

  /*              UserTracking UT4 = new UserTracking(UserTracking.context);
                UT4.user_tracking(class_name + ": clicked on seller layout", FilterLayout.this);
*/
                String tag4 = "seller";
                changeView(tag4);
                openView = 4;
                displayView(openView);
                tag_in_Filter_Layout = tag4;
                break;

            case R.id.otherLayout:
  /*              UserTracking UT5 = new UserTracking(UserTracking.context);
                UT5.user_tracking(class_name + ": clicked on other layout", FilterLayout.this);
*/
                String tag5 = "other";
                changeView(tag5);
                openView = 5;
                tag_in_Filter_Layout = tag5;
                jsonCatArray = new JSONArray();
                JSONArray jsonBrandArray = new JSONArray();

                jsonCatArray = new JSONArray(DepartmentFilter.departmentNameList);
                if (BrandFilter.brandTag.equals("normalTag")) {
                    jsonBrandArray = new JSONArray(BrandFilter.normalBrandNameList);
                } else {
                    jsonBrandArray = new JSONArray(BrandFilter.dependentBrandNameList);
                }

                /*----------------both category and brands condition------------------*/
                if ((jsonCatArray.length() > 0) && (jsonBrandArray.length() > 0)) {
                    if ((jsonCatArray.length() != 0) && (jsonBrandArray.length() != 0)) {
                        getDependentDataOnBothOrCategoryOrBrand(jsonCatArray, jsonBrandArray);
                    }
                } else if (jsonBrandArray.length() > 0) { /*--only brands condition---*/
                    if (jsonBrandArray.length() != 0) {
                        getDependentDataOnBothOrCategoryOrBrand(jsonCatArray, jsonBrandArray);
                    }
                } else if (jsonCatArray.length() > 0) { /*--only category condition---*/
                    if (jsonCatArray.length() != 0) {
                        getDependentDataOnCategory(jsonCatArray);
                    }
                } else {  /*--------------------none condition------------------------*/
                    displayView(openView);
                }
                break;

            case R.id.txtApply:
  /*              UserTracking UT6 = new UserTracking(UserTracking.context);
                UT6.user_tracking(class_name + ": clicked on apply button", FilterLayout.this);
*/
                if (DepartmentFilter.departmentNameList.size() > 0 || SortFilter.sortNameList.size() > 0 ||
                        localBrandFilterCheckList.size() > 0 || PriceFilter.priceNameList.size() > 0 ||
                        PriceFilter.discountNameList.size() > 0 || SellerFilter.sellerNameList.size() > 0 ||
                        localOtherFilterCheckList.size() > 0 || OtherFilter.cakeTypeList.size() > 0) {
                    filterAlertDialog();
                } else {
                    applyButtonMethod();

                }
                break;

            case R.id.txtClear:
  /*              UserTracking UT7 = new UserTracking(UserTracking.context);
                UT7.user_tracking(class_name + ": clicked on clear button", FilterLayout.this);
*/
                switch (tag_in_Filter_Layout) {

                    case DEPARTMENT_TAG:
                        if (DepartmentFilter.departmentNameList.size() > 0 && BrandFilter.dependentBrandNameList.size() > 0 && OtherFilter.categoryBrandDependentSizeNameList.size() > 0) {
                            callClearInterface();
                        } else if (DepartmentFilter.departmentNameList.size() > 0 && BrandFilter.dependentBrandNameList.size() > 0) {
                            callClearInterface();
                        } else if (DepartmentFilter.departmentNameList.size() > 0 && categoryDependentUniqueBrandNames.size() == 0) {
                            callClearInterface();
                        } else if (DepartmentFilter.departmentNameList.size() > 0 && OtherFilter.categoryDependentSizeNameList.size() > 0) {
                            callClearInterface();
                        } else if (DepartmentFilter.departmentNameList.size() > 0) {
                            callClearInterface();
                        }
                        /*--------------department--------------------*/
                        DepartmentFilter.departmentNameList.clear();
                        DepartmentFilter.mChildCheckStates.clear();
                        tvDepartmentFilterCount.setText("");
                        tvDepartmentFilterCount.setVisibility(View.INVISIBLE);
                        /*--------------brand--------------------*/
                        BrandFilter.dependentBrandNameList.clear();
                        BrandFilter.dependentBrandNameListLastResult.clear();
                        tvBrandFilterCount.setText("");
                        tvBrandFilterCount.setVisibility(View.INVISIBLE);
                        /*-----------other ArrayList-------------*/
                        OtherFilter.categoryBrandDependentSizeNameList.clear();
                        OtherFilter.categoryDependentSizeNameList.clear();
                        tvOtherFilterCount.setText("");
                        tvOtherFilterCount.setVisibility(View.INVISIBLE);
                        break;

                    case SORT_TAG:
                        SortFilter.sortRangeSelectedList.clear();
                        SortFilter.sortNameList.clear();
                        tvSortFilterCount.setText("");
                        tvSortFilterCount.setVisibility(View.INVISIBLE);
                        callClearInterface();
                        break;

                    case BRAND_TAG:
                        if (BrandFilter.brandTag.equals("normalTag")) {
                            BrandFilter.normalBrandNameList.clear();
                            OtherFilter.brandDependentSizeNameList.clear();
                        } else {
                            BrandFilter.dependentBrandNameList.clear();
                            BrandFilter.dependentBrandNameListLastResult.clear();
                            OtherFilter.categoryBrandDependentSizeNameList.clear();
                        }
                        tvBrandFilterCount.setText("");
                        tvBrandFilterCount.setVisibility(View.INVISIBLE);
                        tvOtherFilterCount.setText("");
                        tvOtherFilterCount.setVisibility(View.INVISIBLE);
                        callClearInterface();
                        break;

                    case PRICE_TAG:
                        PriceFilter.priceRangeSelectedList.clear();
                        PriceFilter.priceNameList.clear();
                        tvPriceFilterCount.setText("");
                        tvPriceFilterCount.setVisibility(View.INVISIBLE);
                        PriceFilter.discountRangeSelectedList.clear();
                        PriceFilter.discountNameList.clear();
                        tvPriceFilterCount.setText("");
                        tvPriceFilterCount.setVisibility(View.INVISIBLE);
                        callClearInterface();
                        break;

                    case SELLER_TAG:
                        SellerFilter.sellerSelectedList.clear();
                        SellerFilter.sellerNameList.clear();
                        tvSellerFilterCount.setText("");
                        tvSellerFilterCount.setVisibility(View.INVISIBLE);
                        callClearInterface();
                        break;

                    case OTHER_TAG:
                        OtherFilter.cakeTypeList.clear();
                        switch (OtherFilter.sizeTag) {
                            case "bothCategoryAndBrand":
                                OtherFilter.categoryBrandDependentSizeNameList.clear();
                                break;
                            case "onlyNormalBrand":
                                OtherFilter.brandDependentSizeNameList.clear();
                                break;
                            case "onlyCategory":
                                OtherFilter.categoryDependentSizeNameList.clear();
                                break;
                            case "normal":
                                OtherFilter.directNormalSizeNameList.clear();
                                break;
                        }
                        tvOtherFilterCount.setText("");
                        tvOtherFilterCount.setVisibility(View.INVISIBLE);
                        callClearInterface();
                        break;
                }
                break;
        }
    }

    private void applyButtonMethod() {
        Intent intent = new Intent(FilterLayout.this, ProductListActivity.class);
        intent.putExtra("tag", "filter_tag");
        intent.putExtra("selectedName", strSelectedName);
        startActivity(intent);
        finish();
    }

    private void getDependentDataOnBothOrCategoryOrBrand(JSONArray jsonCatArray, JSONArray jsonBrandArray) { //TODO Server method here

        categoryAndBrandDependentUniqueSizeList = new TreeSet<>();

        final GateWay gateWay = new GateWay(FilterLayout.this);
        gateWay.progressDialogStart();

        JSONObject params = new JSONObject();
        try {
            params.put("city", strCity);
            params.put("area", strArea);
            params.put("category", jsonCatArray);
            params.put("brand", jsonBrandArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetDependentSize, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (response.isNull("posts")) {
                    displayView(openView);
                } else {
                    ArrayList<String> bothDependentSizeList = new ArrayList<>();
                    ArrayList<String> bothDependentSizeParameterList = new ArrayList<>();
                    ArrayList<String> bothDependentAllSizeList = new ArrayList<>();
                    try {
                        JSONArray subCategoryJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                            JSONObject jsonSubCatData = subCategoryJsonArray.getJSONObject(i);
                            bothDependentSizeList.add(jsonSubCatData.getString("size").trim());
                            bothDependentSizeParameterList.add(jsonSubCatData.getString("sizeIn").trim());
                        }

                        if (bothDependentSizeList.size() == bothDependentSizeParameterList.size()) {
                            for (int q = 0; q < bothDependentSizeList.size(); q++) {
                                bothDependentAllSizeList.add(bothDependentSizeList.get(q) + bothDependentSizeParameterList.get(q));
                            }
                        }
                        bothDependentAllSizeList.removeAll(Collections.singleton(""));
                        categoryAndBrandDependentUniqueSizeList = new TreeSet<>(bothDependentAllSizeList);

                        if (tag_in_Filter_Layout.equals("brand")) {
                            displayView(openView);
                        } else if (tag_in_Filter_Layout.equals("other")) {
                            displayView(openView);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                gateWay.progressDialogStop();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                gateWay.progressDialogStop();

                GateWay gateWay = new GateWay(FilterLayout.this);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        VolleySingleton.getInstance(FilterLayout.this).addToRequestQueue(request);
    }

    private void getDependentDataOnCategory(JSONArray jArray) { //TODO Server method here
        categoryDependentUniqueBrandNames = new TreeSet<>();
        categoryDependentUniqueSizeList = new TreeSet<>();

        final GateWay gateWay = new GateWay(FilterLayout.this);
        gateWay.progressDialogStart();

        JSONObject params = new JSONObject();
        try {
            params.put("city", strCity);
            params.put("area", strArea);
            params.put("name", jArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetDependentBrand, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (response.isNull("posts")) {
                    displayView(openView);
                } else {
                    ArrayList<String> dependentBrandList = new ArrayList<>();
                    ArrayList<String> dependentSizeList = new ArrayList<>();
                    ArrayList<String> dependentSizeParameterList = new ArrayList<>();
                    ArrayList<String> dependentAllSizeList = new ArrayList<>();
                    try {
                        JSONArray subCategoryJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                            JSONObject jsonSubCatData = subCategoryJsonArray.getJSONObject(i);

                            String notNullBrand = jsonSubCatData.getString("product_brand").trim();
                            if (!notNullBrand.equals("")) {
                                dependentBrandList.add(notNullBrand); //add to arraylist
                            }
                            dependentSizeList.add(jsonSubCatData.getString("size").trim());
                            dependentSizeParameterList.add(jsonSubCatData.getString("sizeIn").trim());
                        }
                        categoryDependentUniqueBrandNames = new TreeSet<>(dependentBrandList);

                        if (dependentSizeList.size() == dependentSizeParameterList.size()) {
                            for (int q = 0; q < dependentSizeList.size(); q++) {
                                dependentAllSizeList.add(dependentSizeList.get(q) + dependentSizeParameterList.get(q));
                            }
                        }
                        dependentAllSizeList.removeAll(Collections.singleton(""));
                        categoryDependentUniqueSizeList = new TreeSet<>(dependentAllSizeList);

                        if (tag_in_Filter_Layout.equals("brand")) {
                            displayView(openView);
                        } else if (tag_in_Filter_Layout.equals("other")) {
                            displayView(openView);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                gateWay.progressDialogStop();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                gateWay.progressDialogStop();

                GateWay gateWay = new GateWay(FilterLayout.this);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        VolleySingleton.getInstance(FilterLayout.this).addToRequestQueue(request);
    }

    private void callClearInterface() {
        if (getFragmentRefreshListener() != null) {
            getFragmentRefreshListener().onRefresh();
        }
    }

    private void changeView(String finalTag) {
        switch (finalTag) {
            case DEPARTMENT_TAG:

                //setting color to linearLayouts
                departmentLayout.setBackgroundResource(R.color.colorPrimary);
                sortLayout.setBackgroundResource(R.color.filter_background_color);
                brandLayout.setBackgroundResource(R.color.filter_background_color);
                priceLayout.setBackgroundResource(R.color.filter_background_color);
                sellerLayout.setBackgroundResource(R.color.filter_background_color);
                otherLayout.setBackgroundResource(R.color.filter_background_color);

                //setting color to textView
                tvDepartment.setTextColor(getResources().getColor(R.color.whiteBtnTextColor));
                tvSort.setTextColor(getResources().getColor(R.color.textColor));
                tvBrand.setTextColor(getResources().getColor(R.color.textColor));
                tvPrice.setTextColor(getResources().getColor(R.color.textColor));
                tvSeller.setTextColor(getResources().getColor(R.color.textColor));
                tvOther.setTextColor(getResources().getColor(R.color.textColor));

                //change images
                imageViewDepartment.setImageDrawable(getResources().getDrawable(R.drawable.ic_department_replace));
                imageViewSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_sort));
                imageViewBrand.setImageDrawable(getResources().getDrawable(R.drawable.ic_brand));
                imageViewPriceNOffer.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
                imageViewSeller.setImageDrawable(getResources().getDrawable(R.drawable.ic_seller));
                imageViewOther.setImageDrawable(getResources().getDrawable(R.drawable.ic_other));
                break;

            case SORT_TAG:

                //setting color to linearLayouts
                sortLayout.setBackgroundResource(R.color.colorPrimary);
                departmentLayout.setBackgroundResource(R.color.filter_background_color);
                brandLayout.setBackgroundResource(R.color.filter_background_color);
                priceLayout.setBackgroundResource(R.color.filter_background_color);
                sellerLayout.setBackgroundResource(R.color.filter_background_color);
                otherLayout.setBackgroundResource(R.color.filter_background_color);

                //setting color to textView
                tvDepartment.setTextColor(getResources().getColor(R.color.textColor));
                tvSort.setTextColor(getResources().getColor(R.color.whiteBtnTextColor));
                tvBrand.setTextColor(getResources().getColor(R.color.textColor));
                tvPrice.setTextColor(getResources().getColor(R.color.textColor));
                tvSeller.setTextColor(getResources().getColor(R.color.textColor));
                tvOther.setTextColor(getResources().getColor(R.color.textColor));

                //change images
                imageViewDepartment.setImageDrawable(getResources().getDrawable(R.drawable.ic_department));
                imageViewSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_sort_replace));
                imageViewBrand.setImageDrawable(getResources().getDrawable(R.drawable.ic_brand));
                imageViewPriceNOffer.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
                imageViewSeller.setImageDrawable(getResources().getDrawable(R.drawable.ic_seller));
                imageViewOther.setImageDrawable(getResources().getDrawable(R.drawable.ic_other));
                break;

            case BRAND_TAG:

                //setting color to linearLayouts
                sortLayout.setBackgroundResource(R.color.filter_background_color);
                departmentLayout.setBackgroundResource(R.color.filter_background_color);
                brandLayout.setBackgroundResource(R.color.colorPrimary);
                priceLayout.setBackgroundResource(R.color.filter_background_color);
                sellerLayout.setBackgroundResource(R.color.filter_background_color);
                otherLayout.setBackgroundResource(R.color.filter_background_color);

                //setting color to textView
                tvDepartment.setTextColor(getResources().getColor(R.color.textColor));
                tvSort.setTextColor(getResources().getColor(R.color.textColor));
                tvBrand.setTextColor(getResources().getColor(R.color.whiteBtnTextColor));
                tvPrice.setTextColor(getResources().getColor(R.color.textColor));
                tvSeller.setTextColor(getResources().getColor(R.color.textColor));
                tvOther.setTextColor(getResources().getColor(R.color.textColor));

                //change images
                imageViewDepartment.setImageDrawable(getResources().getDrawable(R.drawable.ic_department));
                imageViewSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_sort));
                imageViewBrand.setImageDrawable(getResources().getDrawable(R.drawable.ic_brand_replace));
                imageViewPriceNOffer.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
                imageViewSeller.setImageDrawable(getResources().getDrawable(R.drawable.ic_seller));
                imageViewOther.setImageDrawable(getResources().getDrawable(R.drawable.ic_other));
                break;

            case PRICE_TAG:

                //setting color to linearLayouts
                sortLayout.setBackgroundResource(R.color.filter_background_color);
                departmentLayout.setBackgroundResource(R.color.filter_background_color);
                brandLayout.setBackgroundResource(R.color.filter_background_color);
                priceLayout.setBackgroundResource(R.color.colorPrimary);
                sellerLayout.setBackgroundResource(R.color.filter_background_color);
                otherLayout.setBackgroundResource(R.color.filter_background_color);

                //setting color to textView
                tvDepartment.setTextColor(getResources().getColor(R.color.textColor));
                tvSort.setTextColor(getResources().getColor(R.color.textColor));
                tvBrand.setTextColor(getResources().getColor(R.color.textColor));
                tvPrice.setTextColor(getResources().getColor(R.color.whiteBtnTextColor));
                tvSeller.setTextColor(getResources().getColor(R.color.textColor));
                tvOther.setTextColor(getResources().getColor(R.color.textColor));

                //change images
                imageViewDepartment.setImageDrawable(getResources().getDrawable(R.drawable.ic_department));
                imageViewSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_sort));
                imageViewBrand.setImageDrawable(getResources().getDrawable(R.drawable.ic_brand));
                imageViewPriceNOffer.setImageDrawable(getResources().getDrawable(R.drawable.ic_price_replace));
                imageViewSeller.setImageDrawable(getResources().getDrawable(R.drawable.ic_seller));
                imageViewOther.setImageDrawable(getResources().getDrawable(R.drawable.ic_other));
                break;

            case SELLER_TAG:

                //setting color to linearLayouts
                sortLayout.setBackgroundResource(R.color.filter_background_color);
                departmentLayout.setBackgroundResource(R.color.filter_background_color);
                brandLayout.setBackgroundResource(R.color.filter_background_color);
                priceLayout.setBackgroundResource(R.color.filter_background_color);
                sellerLayout.setBackgroundResource(R.color.colorPrimary);
                otherLayout.setBackgroundResource(R.color.filter_background_color);

                //setting color to textView
                tvDepartment.setTextColor(getResources().getColor(R.color.textColor));
                tvSort.setTextColor(getResources().getColor(R.color.textColor));
                tvBrand.setTextColor(getResources().getColor(R.color.textColor));
                tvPrice.setTextColor(getResources().getColor(R.color.textColor));
                tvSeller.setTextColor(getResources().getColor(R.color.whiteBtnTextColor));
                tvOther.setTextColor(getResources().getColor(R.color.textColor));

                //change images
                imageViewDepartment.setImageDrawable(getResources().getDrawable(R.drawable.ic_department));
                imageViewSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_sort));
                imageViewBrand.setImageDrawable(getResources().getDrawable(R.drawable.ic_brand));
                imageViewPriceNOffer.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
                imageViewSeller.setImageDrawable(getResources().getDrawable(R.drawable.ic_seller_replace));
                imageViewOther.setImageDrawable(getResources().getDrawable(R.drawable.ic_other));
                break;

            case OTHER_TAG:

                //setting color to linearLayouts
                sortLayout.setBackgroundResource(R.color.filter_background_color);
                departmentLayout.setBackgroundResource(R.color.filter_background_color);
                brandLayout.setBackgroundResource(R.color.filter_background_color);
                priceLayout.setBackgroundResource(R.color.filter_background_color);
                sellerLayout.setBackgroundResource(R.color.filter_background_color);
                otherLayout.setBackgroundResource(R.color.colorPrimary);

                //setting color to textView
                tvDepartment.setTextColor(getResources().getColor(R.color.textColor));
                tvSort.setTextColor(getResources().getColor(R.color.textColor));
                tvBrand.setTextColor(getResources().getColor(R.color.textColor));
                tvPrice.setTextColor(getResources().getColor(R.color.textColor));
                tvSeller.setTextColor(getResources().getColor(R.color.textColor));
                tvOther.setTextColor(getResources().getColor(R.color.whiteBtnTextColor));

                //change images
                imageViewDepartment.setImageDrawable(getResources().getDrawable(R.drawable.ic_department));
                imageViewSort.setImageDrawable(getResources().getDrawable(R.drawable.ic_sort));
                imageViewBrand.setImageDrawable(getResources().getDrawable(R.drawable.ic_brand));
                imageViewPriceNOffer.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
                imageViewSeller.setImageDrawable(getResources().getDrawable(R.drawable.ic_seller));
                imageViewOther.setImageDrawable(getResources().getDrawable(R.drawable.ic_other_replace));
                break;
        }
    }

    private void displayView(int position) {
        switch (position) {
            case 0:
                FragmentTransaction ft = fm.beginTransaction();
                Fragment departmentFilter = new DepartmentFilter();
                ft.replace(R.id.frame_container, departmentFilter);
                ft.commit();
                break;
            case 1:
                ft = fm.beginTransaction();
                Fragment sortFilter = new SortFilter();
                ft.replace(R.id.frame_container, sortFilter);
                ft.commit();
                break;
            case 2:
                ft = fm.beginTransaction();
                Fragment brandFilter = new BrandFilter();
                ft.replace(R.id.frame_container, brandFilter);
                ft.commit();
                break;
            case 3:
                ft = fm.beginTransaction();
                Fragment priceFilter = new PriceFilter();
                ft.replace(R.id.frame_container, priceFilter);
                ft.commit();
                break;
            case 4:
                ft = fm.beginTransaction();
                Fragment sellerFilter = new SellerFilter();
                ft.replace(R.id.frame_container, sellerFilter);
                ft.commit();
                break;
            case 5:
                ft = fm.beginTransaction();
                Fragment otherFilter = new OtherFilter();
                ft.replace(R.id.frame_container, otherFilter);
                ft.commit();
                break;

            default:
                break;
        }
    }

    public interface FragmentRefreshListener {
        void onRefresh();
    }

    @Override
    public void onBackPressed() {
        switch (OtherFilter.sizeTag) {
            case "bothCategoryAndBrand":
                localOtherFilterCheckList = new ArrayList<>();
                localOtherFilterCheckList = OtherFilter.categoryBrandDependentSizeNameList;
                break;
            case "onlyNormalBrand":
                localOtherFilterCheckList = new ArrayList<>();
                localOtherFilterCheckList = OtherFilter.brandDependentSizeNameList;
                break;
            case "onlyCategory":
                localOtherFilterCheckList = new ArrayList<>();
                localOtherFilterCheckList = OtherFilter.categoryDependentSizeNameList;
                break;
            case "normal":
                localOtherFilterCheckList = new ArrayList<>();
                localOtherFilterCheckList = OtherFilter.directNormalSizeNameList;
                break;
            case "":
                localOtherFilterCheckList = new ArrayList<>();
                break;
        }

        if (BrandFilter.brandTag.equals("normalTag")) {
            localBrandFilterCheckList = new ArrayList<>();
            localBrandFilterCheckList = BrandFilter.normalBrandNameList;
        } else {
            localBrandFilterCheckList = new ArrayList<>();
            localBrandFilterCheckList = BrandFilter.dependentBrandNameList;
        }

        if (DepartmentFilter.departmentNameList.size() > 0 || SortFilter.sortNameList.size() > 0 ||
                localBrandFilterCheckList.size() > 0 || PriceFilter.priceNameList.size() > 0 ||
                PriceFilter.discountNameList.size() > 0 || SellerFilter.sellerNameList.size() > 0 ||
                localOtherFilterCheckList.size() > 0 || OtherFilter.cakeTypeList.size() > 0) {
            filterAlertDialog();
        } else {
            super.onBackPressed();
        }
    }

    private void filterAlertDialog() { //to be add
        LayoutInflater layoutInflater = LayoutInflater.from(FilterLayout.this);
        final View filterView = layoutInflater.inflate(R.layout.filter_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(FilterLayout.this);
        builder.setView(filterView);

        TextView tvExit = filterView.findViewById(R.id.extAnyBtn);
        TextView tvApply = filterView.findViewById(R.id.applyFilterBtn);

        tvExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAllFilterArrayListOnExit();
                onBackPressed();
                alertDialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null && alertDialog.isShowing()) { //this condition is for window leak error at runtime
                    alertDialog.dismiss();
                    applyButtonMethod();
                }
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void clearAllFilterArrayListOnExit() { //to be add
        /*--------------department--------------------*/
        DepartmentFilter.departmentNameList.clear();
        DepartmentFilter.mChildCheckStates.clear();
        /*--------------Sort--------------------*/
        SortFilter.sortRangeSelectedList.clear();
        SortFilter.sortNameList.clear();
        /*--------------Brand--------------------*/
        BrandFilter.normalBrandNameList.clear();
        BrandFilter.dependentBrandNameList.clear();
        BrandFilter.dependentBrandNameListLastResult.clear();
        /*--------------Price--------------------*/
        PriceFilter.priceRangeSelectedList.clear();
        PriceFilter.priceNameList.clear();
        PriceFilter.discountRangeSelectedList.clear();
        PriceFilter.discountNameList.clear();
        /*--------------Seller--------------------*/
        SellerFilter.sellerSelectedList.clear();
        SellerFilter.sellerNameList.clear();
        /*--------------Other Cake Type--------------------*/
        OtherFilter.cakeTypeList.clear();
        /*--------------Others Size--------------------*/
        OtherFilter.categoryBrandDependentSizeNameList.clear();
        OtherFilter.brandDependentSizeNameList.clear();
        OtherFilter.categoryDependentSizeNameList.clear();
        OtherFilter.directNormalSizeNameList.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
/*
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(FilterLayout.this)) {
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, FilterLayout.this);
        }
*/
    }
}
