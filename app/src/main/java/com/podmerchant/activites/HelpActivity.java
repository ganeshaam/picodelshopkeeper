package com.podmerchant.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class HelpActivity extends AppCompatActivity {

    WebView wv_help;
    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    AlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getText(R.string.nav_helps));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = HelpActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        wv_help = findViewById(R.id.wv_help);
        shopHelpDocument();

    }

    public void shopHelpDocument(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlHelpMerchant = StaticUrl.helpmerchant
                    + "?tblName="+sharedPreferencesUtils.getAdminType();

            Log.d("urlHelpMerchant",urlHelpMerchant);

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlHelpMerchant,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){

                                    String TERMS = jsonObject.getString("terms");
                                    wv_help.loadData(TERMS, "text/html", "UTF-8");
                                }else if(!status){
                                    wv_help.loadData("PICODEL Help ", "text/html", "UTF-8");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
