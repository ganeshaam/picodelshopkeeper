package com.podmerchant.activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.podmerchant.R;
import com.podmerchant.fragment.Order_Details;
import com.podmerchant.stepper.SteppersItem;
import com.podmerchant.stepper.SteppersView;
import com.podmerchant.stepper.interfaces.OnCancelAction;
import com.podmerchant.stepper.interfaces.OnClickContinue;
import com.podmerchant.util.Connectivity;

import java.util.ArrayList;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;

public class Order_Details_StepperActivity extends AppCompatActivity {

    public SteppersView steppersView;
    private final ArrayList<String> arrayList_Tags = new ArrayList<>();
    public static TextView tvCBPoints, tvPoints;
    public static RelativeLayout LinCB_Points;
    private final String class_name = this.getClass().getSimpleName();
    private Network_Change_Receiver myReceiver;
    private LinearLayout Main_Layout_NoInternet;
    private TextView txtNoConnection;

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private void gotoNextStep() {
        Bundle bundle1 = getIntent().getExtras();
        String finalPrice = bundle1.getString("finalPrice");
        final String strAddress_Id = bundle1.getString("address_id");

        steppersView = findViewById(R.id.steppersView);
        SteppersView.Config steppersViewConfig = new SteppersView.Config();

        steppersViewConfig.setOnCancelAction(new OnCancelAction() {
            @Override
            public void onCancel() {
                Intent intent = new Intent(Order_Details_StepperActivity.this, HomeActivity.class);
                intent.putExtra("tag", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        steppersViewConfig.setFragmentManager(getSupportFragmentManager());
        ArrayList<SteppersItem> steps = new ArrayList<>();

        /*int i = 1;
        while (i <= 4) {
            final SteppersItem item = new SteppersItem();
            item.setLabel("Step " + i);

            if (i == 1) {
                Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Your Delivery Address");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("address_id", strAddress_Id);
                bundle.putString("tag", "address_view");
                arrayList_Tags.add("address_view");
                blankFragment.setArguments(bundle);

                item.setOnClickContinue(new OnClickContinue() {
                    @Override
                    public void onClick() {
                       *//* new AlertDialog.Builder(Order_Details_StepperActivity.this)
                                .setTitle("Do you want to continue?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {
                                            UserTracking UT = new UserTracking(UserTracking.context);
                                            UT.user_tracking(class_name + " clicked on step 1 continue button ", Order_Details_StepperActivity.this);

                                            steppersView.nextStep();
                                        } else {
                                            Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();*//*
                        if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {
                           *//* UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + " clicked on step 1 continue button ", Order_Details_StepperActivity.this);
*//*
                            steppersView.nextStep();
                        } else {
                            Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                        }
                        // steppersView.nextStep();
                    }
                });
                item.setFragment(blankFragment);
            }
            if (i == 2) {
                Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Review Your Order");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("tag", "item_view");
                arrayList_Tags.add("item_view");
                blankFragment.setArguments(bundle);

                item.setOnClickContinue(new OnClickContinue() {
                    @Override
                    public void onClick() {
                      *//*  new AlertDialog.Builder(Order_Details_StepperActivity.this)
                                .setTitle("Do you want to continue?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {
                                            UserTracking UT = new UserTracking(UserTracking.context);
                                            UT.user_tracking(class_name + " clicked on step 2 continue button", Order_Details_StepperActivity.this);

                                            steppersView.nextStep();
                                        } else {
                                            Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
*//*
                        if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {
  *//*                          UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + " clicked on step 2 continue button", Order_Details_StepperActivity.this);
  *//*                          steppersView.nextStep();
                        } else {
                            Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                        }
                        //steppersView.nextStep();
                    }
                });
                item.setFragment(blankFragment);
            }

            if (i == 3) {
                final Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Terms & Conditions");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("tag", "final_order");
                blankFragment.setArguments(bundle);
                item.setFragment(blankFragment);

                item.setOnClickContinue(new OnClickContinue() {
                    @Override
                    public void onClick() {
                       *//*  new AlertDialog.Builder(Order_Details_StepperActivity.this)
                                .setTitle("Do you want to continue?")
                                 .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialogInterface, int i) {
                                         Boolean b = blankFragment.getTheState();
                                         if (b) {
                                             if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {
                                                 UserTracking UT = new UserTracking(UserTracking.context);
                                                 UT.user_tracking(class_name + " clicked on step 3 continue button", Order_Details_StepperActivity.this);

                                                 steppersView.nextStep();
                                             } else {
                                                 Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                                             }
                                         } else {
                                             Toast.makeText(Order_Details_StepperActivity.this, "First You have to Accept Conditions", Toast.LENGTH_SHORT).show();
                                         }

                                     }
                                 })
                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();*//*
                        Boolean b = blankFragment.getTheState();
                        if (b) {
                            if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {
    *//*                            UserTracking UT = new UserTracking(UserTracking.context);
                                UT.user_tracking(class_name + " clicked on step 3 continue button", Order_Details_StepperActivity.this);
*//*
                                steppersView.nextStep();
                            } else {
                                Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Order_Details_StepperActivity.this, "First You have to Accept Conditions", Toast.LENGTH_SHORT).show();
                        }
                        //steppersView.nextStep();
                    }
                });
            }

            if (i == 4) {
                Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Choose Delivery Option & Place Your Order");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("tag", "options_view");
                arrayList_Tags.add("options_view");
                blankFragment.setArguments(bundle);
                item.setFragment(blankFragment);
            }
            steps.add(item);
            i++;
        }
        steppersView.setConfig(steppersViewConfig);
        steppersView.setItems(steps);
        steppersView.build();*/

        int i = 1;
        while (i <= 3) {
            final SteppersItem item = new SteppersItem();
            item.setLabel("Step " + i);

            if (i == 1) {
                Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Your Delivery Address");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("address_id", strAddress_Id);
                bundle.putString("tag", "address_view");
                arrayList_Tags.add("address_view");
                blankFragment.setArguments(bundle);

                item.setOnClickContinue(new OnClickContinue() {
                    @Override
                    public void onClick() {

                        if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {

                            steppersView.nextStep();
                        } else {
                            Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                item.setFragment(blankFragment);
            }
            /*if (i == 2) {
                Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Review Your Order");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("tag", "item_view");
                arrayList_Tags.add("item_view");
                blankFragment.setArguments(bundle);

                item.setOnClickContinue(new OnClickContinue() {
                    @Override
                    public void onClick() {

                        if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {
                           steppersView.nextStep();
                        } else {
                            Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                        }
                        //steppersView.nextStep();
                    }
                });
                item.setFragment(blankFragment);
            }*/

            if (i == 2) {
                final Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Terms & Conditions");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("tag", "final_order");
                blankFragment.setArguments(bundle);
                item.setFragment(blankFragment);

                item.setOnClickContinue(new OnClickContinue() {
                    @Override
                    public void onClick() {

                        Boolean b = blankFragment.getTheState();
                        if (b) {
                            if (Connectivity.isConnected(Order_Details_StepperActivity.this)) {

                                steppersView.nextStep();
                            } else {
                                Toast.makeText(Order_Details_StepperActivity.this, "Internet connection not available", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Order_Details_StepperActivity.this, "First You have to Accept Conditions", Toast.LENGTH_SHORT).show();
                        }
                        //steppersView.nextStep();
                    }
                });
            }

            if (i == 3) {
                Order_Details blankFragment = new Order_Details();
                item.setSubLabel("Choose Delivery Option & Place Your Order");
                Bundle bundle = new Bundle();
                bundle.putString("finalPrice", finalPrice);
                bundle.putString("tag", "options_view");
                arrayList_Tags.add("options_view");
                blankFragment.setArguments(bundle);
                item.setFragment(blankFragment);
            }
            steps.add(item);
            i++;
        }
        steppersView.setConfig(steppersViewConfig);
        steppersView.setItems(steps);
        steppersView.build();

    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        //gotoNextStep();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__details__stepper);

        myReceiver = new Network_Change_Receiver();

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        txtNoConnection = findViewById(R.id.txtNoConnection);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvCBPoints = findViewById(R.id.txtCBPoints);
        tvPoints = findViewById(R.id.txtPoints);
        LinCB_Points = findViewById(R.id.LinCB_Points);
        tvTitle.setText(R.string.title_activity_order_details);
        setSupportActionBar(toolbar);

        gotoNextStep();
    }

    public interface CheckBoxStateCallback {
        Boolean getTheState();
    }
}



