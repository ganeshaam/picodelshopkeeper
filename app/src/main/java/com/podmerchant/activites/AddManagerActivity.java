package com.podmerchant.activites;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.DeliveryBoyListAdaptor;
import com.podmerchant.model.DeliveryBoyModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleyMultipartRequest;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class AddManagerActivity extends AppCompatActivity {

    Context mContext;
    String mRequestBody="";
    public static final String TAG = "AddManagerActivity";
    String MobilePattern = "[0-9]{10}";

    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    FloatingActionButton fb_manager_list;
    RecyclerView recycler_Managers;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<DeliveryBoyModel> managerModellArrayList;
    private DeliveryBoyListAdaptor managerListAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_manager);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_manager_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = AddManagerActivity.this;
        initView();

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        recycler_Managers.setHasFixedSize(true);
        managerModellArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        recycler_Managers.setLayoutManager(layoutManager);

        managerListAdaptor = new DeliveryBoyListAdaptor(managerModellArrayList,mContext);
        recycler_Managers.setAdapter(managerListAdaptor);

        fb_manager_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMangerCount();
                //customDialog();
            }
        });

        getManagersDetails();
    }

    void initView(){
        fb_manager_list = findViewById(R.id.fb_manager_list);
        recycler_Managers = findViewById(R.id.recycler_Managers);
    }

    private  void  customDialog(){
        final Dialog dialog = new Dialog(AddManagerActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dailog_custom_addmanager);

        final EditText manager_name = dialog.findViewById(R.id.et_manager_name);
        final EditText  manager_contact= dialog.findViewById(R.id.et_manager_contact);
        final EditText  email= dialog.findViewById(R.id.et_manager_email);
       // Deliveryboy_image= dialog.findViewById(R.id.img_deliveryboy);
       // final Button upload = dialog.findViewById(R.id.btn_upload_boy_photo);
        // text.setText(msg);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        /*upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                *//*if(delivery_boybitmap!=null){
                    //Deliveryboy_image.setImageBitmap(delivery_boybitmap);
                }*//*
            }
        });*/

        Button dialogButton1 =  dialog.findViewById(R.id.btn_add_manager);
        dialogButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* if(validation()) {

                }*/
                Log.e("ValueName:",""+manager_name.getText().toString());
                Log.e("ValueContact:",""+manager_contact.getText().toString());

                if(manager_name.getText().toString().equalsIgnoreCase("")){
                    manager_name.requestFocus();
                    manager_name.setError("Please Enter Name");
                }else if(manager_contact.getText().toString().equalsIgnoreCase("")){
                    manager_contact.requestFocus();
                    manager_contact.setError("Please Enter Contact");
                }else if(!manager_contact.getText().toString().matches(MobilePattern)) {
                    Toast.makeText(mContext,"Please Enter valid Contact",Toast.LENGTH_LONG).show();
                }else {

                    ManagerRegistration(manager_name.getText().toString(), manager_contact.getText().toString(),email.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    public void ManagerRegistration(final String NAME,final  String CONTACT, final String EMAIL) {
        if(Connectivity.isConnected(mContext)) {

            progressDialog.show();

            String url = StaticUrl.addShopmanager;

            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    try {

                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e(TAG, "respmanager_res" + resultResponse.toString());
                        Log.e(TAG, "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status = true)
                        {
                            JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                            /*shop_id = jsonObjectData.getString("shop_id");
                            seller_name =  jsonObjectData.getString("name");
                            admin_type =  jsonObjectData.getString("admin_type");*/
                            //getDeliveryBoyDetails();
                            getManagersDetails();
                            Toast.makeText(mContext,"Manager Registered Successfully",Toast.LENGTH_LONG).show();

                        }else if(status = false){
                            Log.d(TAG,"status_false"+resultResponse);
                            Toast.makeText(mContext,"Registration Not Done..!!",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleYIMAGE_EXP:", "" + e.getMessage());
                        progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;

                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";

                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {



                    Map<String, String> params = new HashMap<>();
                    params.put("shop_id", sharedPreferencesUtils.getShopID());
                    params.put("managername", NAME);
                    params.put("contact", CONTACT);
                    params.put("email", EMAIL);
                    //params.put("notification_id", "ttVjuT");
                    //params.put("imageurl", "test_url");


                    Log.e("Params", "" + params);
                    return params;
                }

           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = "#concord$" + ":" + "!@#con co@$rd!@#";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }*/

                /*@Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("imageurl", new DataPart(getBytesFromBitmap(delivery_boybitmap)));

                    Log.e("ParamsImg", "" + params);
                    return params;
                }*/
            };

            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        }
        else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void getManagersDetails(){
        //get Mobile verify
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.getmanagers
                    +"?shop_id="+sharedPreferencesUtils.getShopID();
            // +"?shop_id="+"0";

            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                managerModellArrayList.clear();
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    Log.e("ProfileResponse:",""+jsonArray.toString());

                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        DeliveryBoyModel boyModel = new DeliveryBoyModel();
                                        boyModel.setName(jsonObject1.getString("sellername"));
                                        boyModel.setMobile_number(jsonObject1.getString("contact"));
                                        boyModel.setShop_id(jsonObject1.getString("shop_id"));
                                        boyModel.setImgeurl(jsonObject1.getString("email"));
                                        //boyModel.setImgeurl(jsonObject1.getString("imageurl"));
                                        boyModel.setDelBoyId(jsonObject1.getString("shopadmin_id"));
                                        managerModellArrayList.add(boyModel);
                                    }
                                    Log.e("arraylist size", String.valueOf(managerModellArrayList.size()));
                                    managerListAdaptor.notifyDataSetChanged();
                                }else if(status==false) {
                                    Toast.makeText(mContext,"No Records Found",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void checkMangerCount(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();
            String urlManagerCount = StaticUrl.checkmanager
                    +"?shopid="+sharedPreferencesUtils.getShopID()
                    +"&adminType="+sharedPreferencesUtils.getAdminType();
            StringRequest requestCheckManager = new StringRequest(Request.Method.GET,
                    urlManagerCount,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {

                            JSONObject jsonObject = jsonObject = new JSONObject(response);

                            boolean status = jsonObject.getBoolean("status");

                            if(status==true){

                                String message = jsonObject.getString("message");

                                Toast.makeText(mContext,""+message,Toast.LENGTH_SHORT).show();

                            }else if(status==false){
                                customDialog();
                            }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
                MySingleton.getInstance(mContext).addToRequestQueue(requestCheckManager);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
