package com.podmerchant.activites;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.print.PdfPrint;
import android.print.PrintAttributes;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

import static androidx.core.content.FileProvider.getUriForFile;

public class ShopCancelActivity extends AppCompatActivity {

    WebView wv_invoice;
    FloatingActionButton btn_print;
    String flagPayment,logo, invoiceId;
    static String pdfPath;
    Context mContext;
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    public static int REQUEST_PERMISSIONS = 1;
    boolean boolean_permission;
    boolean boolean_save;
    final private int REQUEST_CODE_ASK_PERMISSION = 111;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    int PERMISSION_ALL = 1;
    Button btn_submit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancelation);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Cancellation Policy");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mContext = ShopCancelActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        wv_invoice = findViewById(R.id.wv_cancellation);
        btn_print = findViewById(R.id.btn_print);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        btn_submit = findViewById(R.id.btn_submit);

        if (Connectivity.isConnected(mContext)) {
            getCancelationPolicy();
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }

        if (!hasPermissions(ShopCancelActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(ShopCancelActivity.this, PERMISSIONS, PERMISSION_ALL);
        }

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createWebPrintJob(wv_invoice);
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getShopCancel();
                cancel_dailog();
            }
        });
    }


    public  void  cancel_dailog(){
        //super.onBackPressed();
        new AlertDialog.Builder(this)
                .setMessage("Are you sure want to Cancel PICODEL Partner?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finishAffinity();
                        getShopCancel();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void getCancelationPolicy() {
        progressDialog.show();

        String invoiceUrl = null;
        try {
            invoiceUrl = StaticUrl.cancellationpolicy
                    + "?shopid=" + sharedPreferencesUtils.getShopID()
                    + "&tblName=" + sharedPreferencesUtils.getAdminType();
            Log.d("CancellationUrl",invoiceUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                invoiceUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("invoice", response);
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
//                                 JSONArray jsonArray = jsonObject.getJSONArray("data");

                                String data = jsonObject.getString("data");
                                //wv_invoice.loadData(data, "text/html", "UTF-8");
                                wv_invoice.loadData(data, "text/html; charset=UTF-8", null);

                            } else {
                                Toast.makeText(mContext, "Contact to PICODEL Admin", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(stringRequest);

    }

    public static void getPath(File path, String fileName) {
        //pdfPath = path+fileName;
        pdfPath = fileName;
        Log.d("pathUrl", pdfPath);
    }

    public void openPdf(String pdfPathUrl) {
        progressDialog.show();
        File pdfFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +" DCIM/picodel/"+ pdfPathUrl);
        // Uri pdfFile = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", pdfFile);
        //Uri path = Uri.parse("file:///storage/sdcard0/DCIM/picodel/"+pdfPathUrl); //"file:///storage/sdcard0/DCIM/picodel/PICODEL_1563629715308.pdf"
        String pdfLocalPath = "file:///storage/sdcard0/DCIM/picodel/" + pdfPathUrl;
        //Toast.makeText(mContext,""+pdfLocalPath,Toast.LENGTH_LONG).show();
        Log.e("pdfLocalPath", "" + pdfLocalPath);
        //  try {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Uri path =  getUriForFile(this, "com.podmerchant.fileprovider", pdfFile);
            Log.e("createPdfUriPath", "" + path);
            // Toast.makeText(mContext,""+path,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
            sendNotification(pdfPathUrl,path);
        } else {
            Uri path = Uri.parse("file:///storage/sdcard0/DCIM/picodel/" + pdfPathUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
            sendNotification(pdfPathUrl,path);
        }


        progressDialog.dismiss();
    }

    private void sendNotification(String messageBody, Uri path) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(path, "application/pdf");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.picodel_logo)
                        .setContentTitle("Download complete")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "PICODEL Merchant",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void createWebPrintJob (WebView webView){
        //progressDialog.show();
        String jobName = getString(R.string.app_name) + "Documents";
        PrintAttributes attributes = new PrintAttributes.Builder()
                .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
                .setResolution(new PrintAttributes.Resolution("pdf", "pdf", 600, 600))
                .setMinMargins(PrintAttributes.Margins.NO_MARGINS).build();
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/picodel/");// + "/Documents/"
        Log.d("path", "" + path);
        PdfPrint pdfPrint = new PdfPrint(attributes);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            pdfPrint.print(webView.createPrintDocumentAdapter(jobName), path, "PICODEL_" + System.currentTimeMillis() + ".pdf");
        } else {
            pdfPrint.print(webView.createPrintDocumentAdapter(), path, "PICODEL_" + System.currentTimeMillis() + ".pdf");
        }

        //openPdf(pdfPath);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("TestpdfUrl", ""+pdfPath);
                openPdf(pdfPath);
            }
        }, 2000);

        //"Total Points: "+totalPoints+"</br>"+
        //progressDialog.show();
        // dialog();
    }

    public void dialog(){
        new AlertDialog.Builder(this)
                .setMessage("Your Invoice is downloaded. Please check PICODEL folder from file manager")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();

        //.setNegativeButton("No", null)
    }

    public static boolean hasPermissions (Context context, String...permissions){
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void getShopCancel() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) {

            JSONObject params = new JSONObject();
            try {
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                params.put("admin_type", sharedPreferencesUtils.getAdminType());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.shopcancelstatus, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("ShopCancel_Status_Res:",""+response);

                    if (response.isNull("posts")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts");
                           /* for (int i = 0; i < categoryJsonArray.length(); i++) {

                            }

*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
