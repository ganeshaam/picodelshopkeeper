package com.podmerchant.activites;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.MenuItem;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.BrandListAdapter;
import com.podmerchant.adapter.OrderlistAdapter;
import com.podmerchant.adapter.ReportListAdapter;
import com.podmerchant.adapter.SliderPagerAdapter;
import com.podmerchant.model.BrandModel;
import com.podmerchant.model.OrderlistModel;
import com.podmerchant.model.SliderModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import dmax.dialog.SpotsDialog;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{ //,SwipeRefreshLayout.OnRefreshListener

    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    ArrayList<BrandModel> brandModelArrayList;
    private RecyclerView.LayoutManager layoutManager;
    private BrandListAdapter brandListAdapter;
    RecyclerView rv_home_brandlist;
    TextView tv_completeProfile,tv_completeAddress,tv_completeDeliverStatus,tv_shophere,yourpoints,tv_network_con,tv_nav_username,tv_nav_mobileno,tv_recharge_point,tv_recharge,
            tv_shopname,tv_neworder,tv_accepted,tv_assigned,version,tv_newDelivery,tv_accepted_Delivery,tv_assigned_Delivery;
    CardView cv_assignCard,cv_checknewOrder,cv_brand_list,cv_points,card_check_new_Delivery,card_Accepted_Delivery,card_assigned_Delivery_list;
    CardView card_check_new_order,card_assign_pending_order,card_assigned_orderlist,cv_delivery_points;
    LinearLayout layout_header,ll_rechargeBgcolor,layout_DeliveryRequest;
    AlertDialog progressDialog;
    String strLatitude="",strLongitude="",strDeliveryinkm="",logintype="",flag,appversion,isRegister="0";
    ViewPager vp_homeSlider;
    private static final String HINDI_LOCALE = "hi";
    private static final String ENGLISH_LOCALE = "en_US";
    SliderPagerAdapter  sliderPagerAdapter;
    ArrayList<SliderModel>  sliderModelArrayList;
    private int currentPage = 0;
    Timer timer;

    ArrayList<OrderlistModel> orderArrayList;
    private OrderlistAdapter orderlistAdapter;
    RecyclerView rv_home_orderlist;

    private Button btn_start, btn_cancel;
    private TextView tv_timer;
    String date_time;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    EditText et_hours;
    SharedPreferences mpref;
    SharedPreferences.Editor mEditor;
    //SwipeRefreshLayout srl_refresh_home;
    CardView cv_refer;
    NavigationView check_reg_navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = HomeActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        //Log.d("TestShop",sharedPreferencesUtils.getShopName());

        //srl_refresh_home = findViewById(R.id.srl_refresh_home);
        //srl_refresh_home.setOnRefreshListener(this);
        /*Resources resources = getResources();
        Locale local = new Locale("hi");//sharedPreferencesUtils.getLocal()
        Log.d("local",sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        Locale locale = new Locale(HINDI_LOCALE);
        Locale.setDefault(locale);
        Configuration configuration = mContext.getResources().getConfiguration();
        configuration.setLocale(locale);
        mContext.createConfigurationContext(configuration);

         if(sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)){
            local = new Locale(ENGLISH_LOCALE);
            sharedPreferencesUtils.setLocal(ENGLISH_LOCALE);
        }else {
            local = new Locale(HINDI_LOCALE);
            local = new Locale(HINDI_LOCALE);
            sharedPreferencesUtils.setLocal(HINDI_LOCALE);
        }*/

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        cv_brand_list = findViewById(R.id.cv_brand_list);
        layout_header = findViewById(R.id.layout_header);
        cv_points = findViewById(R.id.cv_points);
        rv_home_brandlist = findViewById(R.id.rv_home_brandlist);
        tv_network_con = findViewById(R.id.tv_network_con);
        tv_recharge_point = findViewById(R.id.tv_recharge_point);
        tv_recharge = findViewById(R.id.tv_recharge);
        card_check_new_order = findViewById(R.id.card_check_new_order);
        card_assign_pending_order= findViewById(R.id.card_assign_pending_order);
        card_assigned_orderlist= findViewById(R.id.card_assigned_orderlist);
        tv_shopname= findViewById(R.id.tv_shopname);
        yourpoints= findViewById(R.id.yourpoints);
        tv_neworder= findViewById(R.id.tv_neworder);
        tv_accepted= findViewById(R.id.tv_accepted);
        tv_assigned= findViewById(R.id.tv_assigned);
        vp_homeSlider = findViewById(R.id.vp_homeSlider);
        tv_shophere = findViewById(R.id.tv_shophere);
        tv_completeProfile = findViewById(R.id.tv_completeProfile);
        tv_completeAddress = findViewById(R.id.tv_completeAddress);
        tv_completeDeliverStatus = findViewById(R.id.tv_completeDeliverStatus);
        ll_rechargeBgcolor = findViewById(R.id.ll_rechargeBgcolor);
        cv_refer= findViewById(R.id.cv_refer);
        layout_DeliveryRequest = findViewById(R.id.layout_DeliveryRequest);//Layout for Delivery Request
        card_check_new_Delivery = findViewById(R.id.card_check_new_Delivery);
        card_assigned_Delivery_list = findViewById(R.id.card_assigned_Delivery_list);
        card_Accepted_Delivery = findViewById(R.id.card_Accepted_Delivery);
        tv_newDelivery = findViewById(R.id.tv_newDelivery);
        tv_accepted_Delivery = findViewById(R.id.tv_accepted_Delivery);
        tv_assigned_Delivery = findViewById(R.id.tv_assigned_Delivery);
        cv_delivery_points = findViewById(R.id.cv_delivery_points);

        rv_home_brandlist.setHasFixedSize(true);
        brandModelArrayList = new ArrayList<>();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        //layoutManager = new LinearLayoutManager(mContext);
        rv_home_brandlist.setLayoutManager(gridLayoutManager);
        rv_home_brandlist.setItemAnimator(new DefaultItemAnimator());
        brandListAdapter = new BrandListAdapter(brandModelArrayList,mContext);
        rv_home_brandlist.setAdapter(brandListAdapter);

        sliderModelArrayList = new ArrayList<>();
        sliderPagerAdapter = new SliderPagerAdapter(mContext,sliderModelArrayList);
        vp_homeSlider.setAdapter(sliderPagerAdapter);

        rv_home_orderlist = findViewById(R.id.rv_home_orderlist);
        orderArrayList = new ArrayList<>();
        rv_home_orderlist.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_home_orderlist.setLayoutManager(layoutManager);
        rv_home_orderlist.setItemAnimator(new DefaultItemAnimator());
        orderlistAdapter = new OrderlistAdapter(orderArrayList,mContext);
        rv_home_orderlist.setAdapter(orderlistAdapter);

       /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               *//* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*//*
                customDialog("Hello");
            }
        });*/

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        check_reg_navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        tv_nav_username = header.findViewById(R.id.tv_nav_username);
        tv_nav_mobileno= header.findViewById(R.id.tv_nav_mobileno);

        String tmpString = sharedPreferencesUtils.getUserName().replace( '+', ' ' );

         tv_nav_username.setText(tmpString);
         tv_nav_mobileno.setText(sharedPreferencesUtils.getPhoneNumber());
        String tmpStringShopName = sharedPreferencesUtils.getShopName().replace( '+', ' ' );
        tv_shopname.setText(tmpStringShopName);

        strLatitude=sharedPreferencesUtils.getLatitude();
        strLongitude=sharedPreferencesUtils.getLongitude();
        strDeliveryinkm=sharedPreferencesUtils.getDistancekm();
        logintype =sharedPreferencesUtils.getAdminType();

        Log.d("logintype",sharedPreferencesUtils.getAdminType() + sharedPreferencesUtils.getShopName());

        if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){//Delivery Boy
                layout_header.setVisibility(View.GONE);
            //hide navigation menu item for delivery boy
            navigationView =  findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_orderhistroy).setVisible(false);
            nav_Menu.findItem(R.id.nav_reports).setVisible(false);
            nav_Menu.findItem(R.id.nav_recharge_histroy).setVisible(false);
            nav_Menu.findItem(R.id.nav_freelancerArea).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile).setVisible(false);
            nav_Menu.findItem(R.id.nav_address).setVisible(false);
            nav_Menu.findItem(R.id.nav_add_manager).setVisible(false);
            nav_Menu.findItem(R.id.nav_add_delivery_boy).setVisible(false);
            nav_Menu.findItem(R.id.nav_myorders).setVisible(false);
            nav_Menu.findItem(R.id.nav_mynotifications).setVisible(false);
            nav_Menu.findItem(R.id.nav_termscondition).setVisible(false);
            nav_Menu.findItem(R.id.nav_partnerkit).setVisible(false);
            nav_Menu.findItem(R.id.nav_help).setVisible(false);
            nav_Menu.findItem(R.id.nav_registration_invoice).setVisible(false);
            nav_Menu.findItem(R.id.nav_picodel_points).setVisible(false);
            nav_Menu.findItem(R.id.nav_preports).setVisible(false);

            flag="assigned_delboy_order";

            tv_shophere.setVisibility(View.GONE);
            rv_home_brandlist.setVisibility(View.GONE);
            layout_header.setVisibility(View.GONE);
            cv_points.setVisibility(View.GONE);
            vp_homeSlider.setVisibility(View.GONE);
        }else if(sharedPreferencesUtils.getAdminType().equals("Seller")){//Delivery Boy

            //hide navigation menu item for delivery boy
            if(sharedPreferencesUtils.getRShopCatType().equalsIgnoreCase("Chemist Shop")||sharedPreferencesUtils.getRShopCatType().equalsIgnoreCase("Pet Shop")||sharedPreferencesUtils.getRShopCatType().equalsIgnoreCase("Sweets Shop")) {//
                //layout_header.setVisibility(View.GONE);
                navigationView = findViewById(R.id.nav_view);
                Menu nav_Menu = navigationView.getMenu();
                nav_Menu.findItem(R.id.nav_orderhistroy).setVisible(false);
                nav_Menu.findItem(R.id.nav_reports).setVisible(false);
                nav_Menu.findItem(R.id.nav_recharge_histroy).setVisible(false);
                nav_Menu.findItem(R.id.nav_freelancerArea).setVisible(false);
                nav_Menu.findItem(R.id.nav_profile).setVisible(false);
                nav_Menu.findItem(R.id.nav_address).setVisible(false);
                nav_Menu.findItem(R.id.nav_add_manager).setVisible(false);
                nav_Menu.findItem(R.id.nav_add_delivery_boy).setVisible(false);
                nav_Menu.findItem(R.id.nav_myorders).setVisible(false);
                nav_Menu.findItem(R.id.nav_mynotifications).setVisible(false);
                nav_Menu.findItem(R.id.nav_termscondition).setVisible(false);
                nav_Menu.findItem(R.id.nav_partnerkit).setVisible(false);
                nav_Menu.findItem(R.id.nav_cancellation).setVisible(false);
                nav_Menu.findItem(R.id.nav_help).setVisible(false);
                nav_Menu.findItem(R.id.nav_registration_invoice).setVisible(false);
                nav_Menu.findItem(R.id.nav_picodel_points).setVisible(false);
                nav_Menu.findItem(R.id.nav_test_order).setVisible(false);
                flag = "new_order";
                cv_points.setVisibility(View.GONE);

            }
        }else if(sharedPreferencesUtils.getAdminType().equals("Freelancer(FL)")){
            layout_header.setVisibility(View.VISIBLE);
            flag="new_order";
            navigationView = findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_freelancerArea).setVisible(true);
        }else if(sharedPreferencesUtils.getAdminType().equals("Manager")){
            layout_header.setVisibility(View.VISIBLE);
            flag="new_order";
            navigationView = findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_reports).setVisible(false);
            nav_Menu.findItem(R.id.nav_recharge_histroy).setVisible(false);
            nav_Menu.findItem(R.id.nav_freelancerArea).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile).setVisible(false);
            nav_Menu.findItem(R.id.nav_address).setVisible(false);
            nav_Menu.findItem(R.id.nav_add_manager).setVisible(false);
            nav_Menu.findItem(R.id.nav_add_delivery_boy).setVisible(false);
            nav_Menu.findItem(R.id.nav_mynotifications).setVisible(false);
            nav_Menu.findItem(R.id.nav_termscondition).setVisible(false);
            nav_Menu.findItem(R.id.nav_partnerkit).setVisible(false);
            nav_Menu.findItem(R.id.nav_myorders).setVisible(false);

            tv_shophere.setVisibility(View.GONE);
            rv_home_brandlist.setVisibility(View.GONE);
            cv_points.setVisibility(View.GONE);

        }else if(sharedPreferencesUtils.getAdminType().equals("Manufacturer")){
            layout_header.setVisibility(View.VISIBLE);
            flag="new_order";
            getSupportActionBar().setTitle("PICODEL Brand Partner");
            navigationView =   findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_freelancerArea).setVisible(false);
            nav_Menu.findItem(R.id.nav_myorders).setVisible(false);
            nav_Menu.findItem(R.id.nav_mynotifications).setVisible(false);
            nav_Menu.findItem(R.id.nav_add_manager).setVisible(false);
            nav_Menu.findItem(R.id.nav_add_delivery_boy).setVisible(false);
            nav_Menu.findItem(R.id.nav_partnerkit).setVisible(false);
            nav_Menu.findItem(R.id.nav_cancellation).setVisible(false);
            nav_Menu.findItem(R.id.nav_registration_invoice).setVisible(false);
            nav_Menu.findItem(R.id.nav_preports).setVisible(false);

            tv_shophere.setVisibility(View.GONE);
            rv_home_brandlist.setVisibility(View.GONE);
        }else if(sharedPreferencesUtils.getAdminType().equals("Distributor")){
            layout_header.setVisibility(View.VISIBLE);
            flag="new_order";
            getSupportActionBar().setTitle("PICODEL Distributor");
            navigationView =   findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_freelancerArea).setVisible(false);
            nav_Menu.findItem(R.id.nav_myorders).setVisible(false);
            nav_Menu.findItem(R.id.nav_mynotifications).setVisible(false);
            nav_Menu.findItem(R.id.nav_preports).setVisible(false);

            tv_shophere.setVisibility(View.GONE);
            rv_home_brandlist.setVisibility(View.GONE);
        } else{
            layout_header.setVisibility(View.VISIBLE);
             flag="new_order";
            navigationView =   findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_freelancerArea).setVisible(false);
        }

        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            navigationView =   findViewById(R.id.nav_view);
            Menu menu = navigationView.getMenu();
            MenuItem nav_version = menu.findItem(R.id.nav_version);
            nav_version.setTitle("Version "+"V"+version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

       Log.e("Test:",""+"value");
        getDeliveryDetails();

        /*TextView navVersion = (TextView) nav_Menu.findItem(R.id.nav_version).getActionView();
        navVersion.setText("Version"+"1.7");*/
        //navVersion.setGravity(View.TEXT_ALIGNMENT_CENTER);

        if(Connectivity.isConnected(mContext)){
            if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                getOrderList();
            }else {
                getRechargePoints();
                getSlider();
                getBrandList();


            }
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        card_check_new_Delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext,RequestListActivity.class);
                intent.putExtra("flag","New_Delivery");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);//
            }
        });

        card_Accepted_Delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,RequestListActivity.class);
                intent.putExtra("flag","Accepted_Delivery");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);//
            }
        });

        card_assigned_Delivery_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,RequestListActivity.class);
                intent.putExtra("flag","Assigned_Delivery");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);//
            }
        });

        //Header card view click
        card_check_new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,OrderListActivity.class);
                intent.putExtra("flag","new_order");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        card_assign_pending_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,OrderListActivity.class);
                intent.putExtra("flag","approved_order");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });
        card_assigned_orderlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,OrderListActivity.class);
                intent.putExtra("flag","assigned_order");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        //send to recharge
        tv_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //00000
                String points = tv_recharge_point.getText().toString();
               /* if(points.equalsIgnoreCase("00000")){
                    sampleDialog();
                }else {
               */     Intent rechargeIntent = new Intent(mContext, RechargeActivity.class);
                      rechargeIntent.putExtra("rechargeFrom", "home");
                      startActivity(rechargeIntent);
                //}
            }
        });

        //send to product list
        rv_home_brandlist.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rv_home_brandlist,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        Intent intent = new Intent(mContext, ProductListActivity.class);
                        intent.putExtra("tag", "next");
                        intent.putExtra("selectedName", brandModelArrayList.get(position).getBrandName());
                        startActivity(intent);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        //Toast.makeText(mContext,"Product List",Toast.LENGTH_SHORT).show();
                    }
                }));

        //order details
        rv_home_orderlist.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_home_orderlist,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        String orderId = orderArrayList.get(position).getOrder_id();
                        String cartUniqueId = orderArrayList.get(position).getCart_unique_id();
                        String orderByUserId = orderArrayList.get(position).getUser_id();
                        String cartUserId = orderArrayList.get(position).getCart_user_id();
                        String shipping_address = orderArrayList.get(position).getShipping_address();
                        String latitude = orderArrayList.get(position).getLatitude();
                        String longitude = orderArrayList.get(position).getLongitude();
                        String total_amount = orderArrayList.get(position).getTotal_amount();
                        String order_date  = orderArrayList.get(position).getOrder_date();
                        String address_id = orderArrayList.get(position).getAddress_id();
                        String assign_delboy_id = orderArrayList.get(position).getAssign_delboy_id();
                        String action_time = orderArrayList.get(position).getActionTime();
                        String address1 = orderArrayList.get(position).getAddress1();
                        String address2 = orderArrayList.get(position).getAddress2();
                        String address3 = orderArrayList.get(position).getAddress3();
                        String address4 = orderArrayList.get(position).getAddress4();
                        String address5 = orderArrayList.get(position).getAddress5();
                        String area = orderArrayList.get(position).getArea();
                        String pincode = orderArrayList.get(position).getPincode();
                        String showcart = orderArrayList.get(position).getShow_cart();
                        String updatedat = orderArrayList.get(position).getUpdated_at();
                        String aam_delivery_charge = orderArrayList.get(position).getAam_delivery_charge();
                        String color_flag = orderArrayList.get(position).getColor_flag();
                        String admin_aprove = orderArrayList.get(position).getAdmin_approve();
                        //chkOrderStatus(orderId,cartUniqueId,orderByUserId,cartUserId,cartUserId,shipping_address,latitude,longitude,total_amount,order_date,address_id,assign_delboy_id,action_time);

                        Intent intent = new Intent(mContext,OrderDetailActivity.class);
                        intent.putExtra("orderId",orderId);
                        intent.putExtra("cartUniqueId",cartUniqueId);
                        intent.putExtra("userId",orderByUserId);
                        intent.putExtra("cartUserId",cartUserId);
                        intent.putExtra("shipping_address",shipping_address);
                        intent.putExtra("latitude",latitude);
                        intent.putExtra("longitude",longitude);
                        intent.putExtra("total_amount",total_amount);
                        intent.putExtra("order_date",order_date);
                        intent.putExtra("address_id",address_id);
                        intent.putExtra("assign_delboy_id",assign_delboy_id);
                        intent.putExtra("action_time",action_time);
                        intent.putExtra("address1",address1);
                        intent.putExtra("address2",address2);
                        intent.putExtra("address3",address3);
                        intent.putExtra("address4",address4);
                        intent.putExtra("address5",address5);
                        intent.putExtra("area",area);
                        intent.putExtra("pincode",pincode);
                        intent.putExtra("showcart",showcart);
                        intent.putExtra("flag",flag);
                        intent.putExtra("updatedat",updatedat);
                        intent.putExtra("aam_delivery_charge",aam_delivery_charge);
                        intent.putExtra("areaName","areaName");
                        intent.putExtra("admin_aprove",admin_aprove);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        //init();
        //listener();
        tv_completeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentShopProfile = new Intent(mContext,ShopProfileActivity.class);
                startActivity(intentShopProfile);
                finish();
            }
        });
        tv_completeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentShopProfile = new Intent(mContext,ChangeAddressActivity.class);
                startActivity(intentShopProfile);
                finish();
            }
        });

        tv_completeDeliverStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(mContext,OrderListActivity.class);
                intent.putExtra("flag","approved_order");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);*/
            }
        });



       /* srl_refresh_home.post(new Runnable() {
            @Override
            public void run() {
              srl_refresh_home.setRefreshing(true);
             *//*   if(Connectivity.isConnected(mContext)){
                    getRechargePoints();
                }else {
                    tv_network_con.setVisibility(View.VISIBLE);
                }*//*
            }
        });*/
        cv_refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Refer your known Shops to register and earn 5000 points ");
                //share.putExtra(Intent.EXTRA_TEXT, "Refer your known Shops to register and earn 5000 points  https://play.google.com/store/apps/details?id=com.podmerchant&referrer=POD"+sharedPreferencesUtils.getShopID());
                share.putExtra(Intent.EXTRA_TEXT, "  यदि आप भी मेरी तरह अपना किराना एवं सब्जी व्यापार बढ़ाना चाहते हैं तो आज ही पीकोडेल पर रजिस्टर करें : https://play.google.com/store/apps/details?id=com.podmerchant&referrer=POD"+sharedPreferencesUtils.getShopID());
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
    }

    //get brand list
    public void getBrandList(){

        progressDialog.show();
        brandModelArrayList.clear();

        String targetUrlBrandlist = StaticUrl.brandlist;

        Log.d("targetUrlBrandlist",targetUrlBrandlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlBrandlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                            progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                                if ((strStatus=true)){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                  for(int i = 0; i < jsonArray.length(); i++){ //jsonArray.length()
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        BrandModel model = new BrandModel();
                                        model.setBrandId(olderlist.getString("bid"));
                                        model.setBrandName(olderlist.getString("brand_name"));
                                        model.setBrandImgUrl(olderlist.getString("brand_logo"));
                                      brandModelArrayList.add(model);
                                    }
                                    brandListAdapter.notifyDataSetChanged();
                                }else if(strStatus=false){
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }
    //get points
    public void getRechargePoints(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();
            //srl_refresh_home.setRefreshing(true);
            String urlRechartGet = StaticUrl.getrecharge
                    + "?shopid="+sharedPreferencesUtils.getShopID()
                    + "&tblName="+sharedPreferencesUtils.getAdminType()
                    + "&latitude=" + strLatitude
                    + "&longitude=" + strLongitude
                    + "&deliveryinkm=" + strDeliveryinkm;

            Log.d("RechargePoints",urlRechartGet);

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlRechartGet,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.dismiss();

                            Log.e("GetRecharge:",""+response);
                            //srl_refresh_home.setRefreshing(false);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){
                                    //get profile status
                                    String profileStatus = jsonObject.getString("profileStatus");
                                    if(profileStatus.equals("0")){
                                        tv_completeProfile.setVisibility(View.VISIBLE);
                                        layout_header.setVisibility(View.GONE);
                                        rv_home_brandlist.setVisibility(View.GONE);
                                        tv_shophere.setVisibility(View.GONE);
                                    }
                                    //get address status
                                    String addressStatus = jsonObject.getString("addressStatus");
                                    if(addressStatus.equals("0")){
                                        tv_completeAddress.setVisibility(View.VISIBLE);
                                        layout_header.setVisibility(View.GONE);
                                        rv_home_brandlist.setVisibility(View.GONE);
                                        tv_shophere.setVisibility(View.GONE);
                                        cv_points.setVisibility(View.GONE);
                                    }
                                    //previous_order_status
                                    String previous_order_status = jsonObject.getString("previous_order_status");
                                    if(previous_order_status.equals("1")){
                                        tv_completeDeliverStatus.setVisibility(View.VISIBLE);
                                    }
                                        //isRegister
                                    String isRegister = jsonObject.getString("isRegister");
                                    if(previous_order_status.equals("1")){
                                        isRegister="1";
                                    }else{
                                        isRegister="0";
                                    }

                                    //set points and message
                                    JSONArray jsonArray = jsonObject.getJSONArray("recharge");
                                    JSONObject jsonObjectData =  jsonArray.getJSONObject(0);
                                    String rechargeAmount = jsonObjectData.getString("recharge_amount");
                                    String rechargePoints = jsonObjectData.getString("recharge_points");
                                    String message = jsonObject.getString("message");
                                    if(rechargePoints.equalsIgnoreCase("0")){
                                        card_check_new_order.setEnabled(false);
                                        card_assign_pending_order.setEnabled(false);
                                        card_assigned_orderlist.setEnabled(false);
                                        yourpoints.setText(getResources().getString(R.string.yourpoints_recharge));
                                        ll_rechargeBgcolor.setBackgroundColor(getResources().getColor(R.color.yellow));
                                        yourpoints.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    }else {
                                        tv_recharge_point.setText(rechargePoints);
                                        if(message.equals("0")){
                                            yourpoints.setText(getResources().getString(R.string.yourpoints_recharge));
                                            ll_rechargeBgcolor.setBackgroundColor(getResources().getColor(R.color.yellow));
                                            yourpoints.setTextColor(getResources().getColor(R.color.colorPrimary));
                                        }else {
                                            yourpoints.setText(message);
                                        }
                                    }
                                    //show count of order list
                                    JSONArray jsonArrayOrderCount = jsonObject.getJSONArray("ordercount");
                                    JSONObject jsonObjectOrder =  jsonArrayOrderCount.getJSONObject(0);
                                    tv_neworder.setText(jsonObjectOrder.getString("Neworder"));
                                    tv_accepted.setText(jsonObjectOrder.getString("Approved"));
                                    tv_assigned.setText(jsonObjectOrder.getString("Assigned"));

                                   /* //slider
                                    JSONArray jsonArraySlider = jsonObject.getJSONArray("slider");
                                    for(int i=0; i<jsonArraySlider.length(); i++) {
                                        JSONObject jsonObjectSlider = jsonArraySlider.getJSONObject(i);
                                        SliderModel model = new SliderModel();
                                        model.setImgUrl(jsonObjectSlider.getString("front_search_banner"));
                                        sliderModelArrayList.add(model);
                                    }
                                sliderPagerAdapter.notifyDataSetChanged();
                                 setupAutoPager();*/

                                    //get app version
                                    appversion = jsonObject.getString("appversion");
                                    if(!StaticUrl.versionName.equals(appversion)){
                                            updateDialog();
                                    }
                                }else if(!statuss) {
                                    tv_recharge_point.setText("00000");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            //srl_refresh_home.setRefreshing(false);
                        }
                    });
            //stringRequestOtp.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    //get slider images
    public void getSlider(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();
             String urlSLiderGet = StaticUrl.getslider+"?tblName="+sharedPreferencesUtils.getAdminType();

            Log.d("urlSLiderGet",urlSLiderGet);

            StringRequest stringRequestSlider = new StringRequest(Request.Method.GET,
                    urlSLiderGet,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.dismiss();
                             try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){
                                    //slider
                                    JSONArray jsonArraySlider = jsonObject.getJSONArray("slider");
                                    for(int i=0; i<jsonArraySlider.length(); i++) {
                                        JSONObject jsonObjectSlider = jsonArraySlider.getJSONObject(i);
                                        SliderModel model = new SliderModel();
                                        model.setImgUrl(jsonObjectSlider.getString("front_search_banner"));
                                        sliderModelArrayList.add(model);
                                    }
                                    sliderPagerAdapter.notifyDataSetChanged();
                                    setupAutoPager();

                                }else if(!statuss) {
                                    tv_recharge_point.setText("00000");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                         }
                    });
            //stringRequestOtp.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestSlider);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    //slider scroll handle
    private void setupAutoPager() {
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run()
            {
                vp_homeSlider.setCurrentItem(currentPage, true);
                if(currentPage == Integer.MAX_VALUE)
                {
                    currentPage = 0;
                }
                else
                {
                    ++currentPage ;
                }
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 3000, 3000);
    }
    //get orders for delivery boy
    public void getOrderList(){

        progressDialog.show();
        orderArrayList.clear();

        String targetUrlOrderlist = StaticUrl.orderlist
                + "?latitude=" + strLatitude
                + "&longitude=" + strLongitude
                + "&deliveryinkm=" + strDeliveryinkm
                + "&shopid=" + sharedPreferencesUtils.getShopID()
                + "&delboyid=" + sharedPreferencesUtils.getDelId()
                + "&areaName=areaName"
                + "&flag=" + flag
                + "&tblName="+ URLEncoder.encode(sharedPreferencesUtils.getShopAdminType());

        Log.d("targetUrlOrderlist",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus=true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    int j=3;
                                    if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                                        j=jsonArray.length();
                                    }else {
                                        j=3;
                                    }
                                    for(int i = 0; i < j; i++){ //jsonArray.length()
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        model.setOrder_id(olderlist.getString("order_id"));
                                        model.setUser_id(olderlist.getString("user_id"));
                                        model.setAssign_shop_id(olderlist.getString("assign_shop_id"));
                                        model.setAssign_delboy_id(olderlist.getString("assign_delboy_id"));
                                        model.setStatus(olderlist.getString("status"));
                                        model.setCart_user_id(olderlist.getString("cart_user_id"));
                                        model.setCart_unique_id(olderlist.getString("cart_unique_id"));
                                        model.setPayment_method(olderlist.getString("payment_method"));
                                        model.setUpdate_payment_method(olderlist.getString("update_payment_method"));
                                        model.setPayment_type(olderlist.getString("payment_type"));
                                        model.setReferrence_transaction_id(olderlist.getString("referrence_transaction_id"));
                                        model.setTotal_amount(olderlist.getString("total_amount"));
                                        model.setUpdated_total_amount(olderlist.getString("updated_total_amount"));
                                        model.setShipping_address(olderlist.getString("shipping_address"));
                                        model.setLatitude(olderlist.getString("latitude"));
                                        model.setLongitude(olderlist.getString("longitude"));
                                        model.setUpdated_shipping_charges(olderlist.getString("updated_shipping_charges"));
                                        model.setOrder_date(olderlist.getString("order_date"));
                                        model.setUser_pending_bill(olderlist.getString("user_pending_bill"));
                                        model.setPending_bill_status(olderlist.getString("pending_bill_status"));
                                        model.setPending_bill_updatedy(olderlist.getString("pending_bill_updatedy"));
                                        model.setPending_bii_date(olderlist.getString("pending_bii_date"));
                                        model.setDistance(olderlist.getString("distance"));
                                        model.setAddress_id(olderlist.getString("address_id"));
                                        model.setActionTime(olderlist.getString("action_time"));
                                        model.setAddress1(olderlist.getString("address1"));
                                        model.setAddress2(olderlist.getString("address2"));
                                        model.setAddress3(olderlist.getString("address3"));
                                        model.setAddress4(olderlist.getString("address4"));
                                        model.setAddress5(olderlist.getString("address5"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setPincode(olderlist.getString("pincode"));
                                        model.setShow_cart(olderlist.getString("show_cart"));
                                        model.setDelivery_date(olderlist.getString("delivery_date"));
                                        model.setDelivery_time(olderlist.getString("delivery_time"));
                                        model.setDelivery_method(olderlist.getString("delivery_method"));
                                        model.setAdmin_approve(olderlist.getString("admin_approve"));
                                        model.setColor_flag(olderlist.getString("color_flag"));//color_flag
                                        orderArrayList.add(model);

                                    }
                                    orderlistAdapter.notifyDataSetChanged();
                                    }else {
                                         tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                   tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }
    @Override
    protected void onResume() {
        super.onResume();
           if(Connectivity.isConnected(mContext)){
                    getRechargePoints();
                }else {
                    tv_network_con.setVisibility(View.VISIBLE);
                }
    }
   /* @Override
    public void onRefresh() {
        if(Connectivity.isConnected(mContext)){
            //getSlider();
            getRechargePoints();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }
    }
   */ //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private HomeActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final HomeActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
    //logout dialog
    public void logoutDialog() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Logout", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sharedPreferencesUtils.logout();
                       Intent intent = new Intent(mContext,LoginActivity.class);
                       startActivity(intent);
                       finish();
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Shopkeeper Profile
            Intent profile = new Intent(mContext, ShopProfileActivity.class);
            //profile.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(profile);
            finish();
        } else if (id == R.id.nav_address) {
            Intent address = new Intent(mContext,ChangeAddressActivity.class);
            startActivity(address);
        } else if (id == R.id.nav_add_delivery_boy) {
            Intent deliveryboyIntent = new Intent(mContext,AddDeliveryBoyActivity.class);
            deliveryboyIntent.putExtra("assignOrder", "noOrder");
            deliveryboyIntent.putExtra("orderid", "0");
            startActivity(deliveryboyIntent);
        }else if (id == R.id.nav_logout) {
            logoutDialog();
        }else if( id == R.id.nav_orderhistroy){
            Intent intent = new Intent(mContext,OrderListActivity.class);
            intent.putExtra("flag","histroy_order");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if( id == R.id.nav_test_order){
            // Call Test Order API
            placeTestAPI();

        }else if(id == R.id.nav_home){
            Intent intent = new Intent(mContext,HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_reports){
            Intent intent = new Intent(mContext,ReportsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);//nav_delivery_request_list
        }else if(id == R.id.nav_delivery_request_list){
            Intent intent = new Intent(mContext,RequestListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);//
        }else if(id == R.id.nav_freelancerArea){
            Intent intent = new Intent(mContext,FreelancerActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_termscondition){
            Intent intent = new Intent(mContext,TermsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_cancellation){ ////nav_cancellation
            Intent intent = new Intent(mContext,ShopCancelActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }//nav_add_manager
        else if(id == R.id.nav_add_manager){
            Intent intent = new Intent(mContext,AddManagerActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        } else if(id == R.id.nav_add_delivery_request){
            Intent intent = new Intent(mContext,DeliveryRequerstActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_myorders){
            Intent intent = new Intent(mContext,MyOrder.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_mynotifications){
            Intent intent = new Intent(mContext,MyNotifications.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_recharge_histroy){
            Intent intent = new Intent(mContext,RechargeHistoryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_registration_invoice){
            getIsRegister();
        }else if(id == R.id.nav_partnerkit){
            Intent intent = new Intent(mContext,PartnerkitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent); //nav_registration_invoice
        } else if(id == R.id.nav_help){
            Intent intent = new Intent(mContext,HelpActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_setting){
            Intent intent = new Intent(mContext,SettingsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_picodel_points){
            Intent intent = new Intent(mContext,PicodelPointsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_preports){
            Intent intent = new Intent(mContext,ProductReportActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_franchisee){
            //getFranchiseeDetails();
            FranchiseeDialog();
        }//nav_franchisee

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    //custom dialog
    private  void  customDialog(String msg){

            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_custom_reg);

            TextView text = dialog.findViewById(R.id.tv_dialog_msg);
           // text.setText(msg);

            Button dialogButton =  dialog.findViewById(R.id.btnreg_proceed);
            Button dialogButton2 =  dialog.findViewById(R.id.btn_regedit);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
    }
   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.getItem(0);
        if(sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)){
            menuItem.setTitle("Language");
        }else if(sharedPreferencesUtils.getLocal().equals(ENGLISH_LOCALE)){
            menuItem.setTitle("Language");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Locale local;
        //noinspection SimplifiableIfStatement
        //handle local language Hindi and English
        if (id == R.id.action_settings) {

            Resources resources = getResources();
            if(sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)){
                local = new Locale(ENGLISH_LOCALE);
                sharedPreferencesUtils.setLocal(ENGLISH_LOCALE);
            }else {
                local = new Locale(HINDI_LOCALE);
                sharedPreferencesUtils.setLocal(HINDI_LOCALE);
            }
            Configuration configuration =  resources.getConfiguration();
            configuration.setLocale(local);
            getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
            recreate();
            //return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to close PICODEL?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }
    //Check order is already approved by some one
    public void chkOrderStatus(final String orderId,final String cartUniqueId, final String orderByUserId, final String cartUserId, final String userId, final String shipping_address, final String latitude, final String longitude, final String total_amount, final String order_date, final String address_id, final String assign_delboy_id, final String admin_approve){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.chkorderstatus+"?order_id="+orderId;

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String approvedStatus = jsonObjectData.getString("approveBy");

                                    if(approvedStatus.equals("New Order")){

                                        Intent intent = new Intent(mContext,OrderDetailActivity.class);
                                        intent.putExtra("orderId",orderId);
                                        intent.putExtra("cartUniqueId",cartUniqueId);
                                        intent.putExtra("userId",orderByUserId);
                                        intent.putExtra("cartUserId",cartUserId);
                                        intent.putExtra("shipping_address",shipping_address);
                                        intent.putExtra("latitude",latitude);
                                        intent.putExtra("longitude",longitude);
                                        intent.putExtra("total_amount",total_amount);
                                        intent.putExtra("order_date",order_date);
                                        intent.putExtra("address_id",address_id);
                                        intent.putExtra("assign_delboy_id",assign_delboy_id);
                                        intent.putExtra("admin_aprove",admin_approve);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);

                                    }else if(approvedStatus.equals("Approved")){
                                        String mesg = jsonObject.getString("mesg");
                                        chkApprovedDialog(mesg);
                                    }
                                }else if(!status){
                                    Toast.makeText(mContext,"Sorry. This order you can not accept.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    //check Approved Dialog
    private void chkApprovedDialog(String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_chkapprove);

        TextView text = dialog.findViewById(R.id.tv_dialog_msg);
        text.setText(message);

        Button dg_btn_refreshlist =  dialog.findViewById(R.id.btn_refreshlist);

        dg_btn_refreshlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    public  void updateDialog(){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_app_update);
        dialog.setCancelable(false);
        Button button = dialog.findViewById(R.id.btn_update_playstore);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.podmerchant&hl=en"));
                startActivity(intent);
                dialog.dismiss();
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.podmerchant&hl=en"));
                startActivity(intent);
            }
        },3000);

        dialog.show();

    }
    public void sampleDialog(String message){
        AlertDialog alertDialog = new AlertDialog.Builder(this)

                .setTitle("PICODEL Merchant")
                //set message
                .setMessage(message)
                //set positive button
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //set what would happen when positive button is clicked
                        //finish();
                        dialogInterface.dismiss();
                    }
                })

                .show();
        /* new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to close PICODEL?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();*/
    }

    private void placeTestAPI() {

        //Place Test Order for the Notification and Accept Order
            if(Connectivity.isConnected(mContext)){
                progressDialog.show();
                String urlAcceptOrder = StaticUrl.actionTestOrder+"?shop_id="+sharedPreferencesUtils.getShopID();

                StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                        urlAcceptOrder,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                            Log.e("TestOrderRes:",""+response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    boolean status = jsonObject.getBoolean("status");
                                    String message = jsonObject.getString("message");
                                    if(status){
                                       // JSONArray jsonArray =  jsonObject.getJSONArray("data");
                                        //String approvedStatus = jsonObjectData.getString("approveBy");
                                        /*if(approvedStatus.equals("New Order")){
                                        }else if(approvedStatus.equals("Approved")){
                                        }*/

                                        Intent intent = new Intent(mContext,OrderListActivity.class);
                                        intent.putExtra("flag","testorder");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);

                                    }else if(!status){
                                        Toast.makeText(mContext,"Sorry. Please Contact To PICODEL Admin.",Toast.LENGTH_SHORT).show();
                                        sampleDialog(message);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                            }
                        });
                MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
            }else {
                Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
            }
        }

    private void getIsRegister(){
        //actionIsregister

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();
            //srl_refresh_home.setRefreshing(true);
            String urlRechartGet = StaticUrl.actionIsregister
                    + "?shopid="+sharedPreferencesUtils.getShopID();


            Log.d("isRegister",urlRechartGet);

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlRechartGet,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.dismiss();

                            Log.e("IsRegister:",""+response);
                            //srl_refresh_home.setRefreshing(false);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String MESSAGE_VALUE;
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){

                                    String isRegister = jsonObject.getString("isRegister");
                                    MESSAGE_VALUE = jsonObject.getString("message");
                                    if(isRegister.equals("1")){
                                        IsRegisterDialog(MESSAGE_VALUE);
                                    }else{
                                        Intent intent = new Intent(mContext, RegistrationInvoiceActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent); //nav_registration_invoice
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            //srl_refresh_home.setRefreshing(false);
                        }
                    });
            //stringRequestOtp.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }


    }


    public void IsRegisterDialog(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    //admin_id Method to get Franchisee Details from the admin_id associate for

    public void getFranchiseeDetails(){
        progressDialog.show();
        //areaWise_order  // category
        String targetUrlOrderlist = null;
        try {
            targetUrlOrderlist = StaticUrl.checkadmin
                    + "?admin_id=" + sharedPreferencesUtils.getAdminID();
            //+ "&city=" + sharedPreferencesUtils.getAdminCity();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("Url_checkadmin_",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("checkadmin_Res",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){

                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();

                                        /*model.setShop_contact(olderlist.getString("shop_contact"));
                                        model.setUser_fname(olderlist.getString("user_fname"));
                                        model.setUser_lname(olderlist.getString("user_lname"));
                                        model.setRemark(olderlist.getString("user_remark"));
                                        model.setOrder_count(olderlist.getString("order_count"));*/
                                        //orderArrayList.add(model);
                                    }
                                    //orderlistAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus==false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    //Method to show the OrdeID Details
    public void FranchiseeDialog() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_franchisee_details, null);

        final TextView tv_name = view.findViewById(R.id.tv_name);
        final TextView tv_city = view.findViewById(R.id.tv_city);
        final TextView tv_contact = view.findViewById(R.id.tv_contact);

        Button btn_submitreson = view.findViewById(R.id.btn_search);


        //areaWise_order  // category
        String targetUrlOrderlist = null;
        try {
            targetUrlOrderlist = StaticUrl.checkadmin
                    + "?admin_id=" + sharedPreferencesUtils.getAdminID();
            //+ "&city=" + sharedPreferencesUtils.getAdminCity();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("Url_checkadmin_",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("checkadmin_Res",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){

                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        tv_name.setText("Name:"+olderlist.getString("sellername"));
                                        tv_contact.setText("Contact: "+olderlist.getString("contact"));
                                        tv_city.setText("City: "+olderlist.getString("city"));
                                        /*model.setShop_contact(olderlist.getString("shop_contact"));
                                        model.setUser_fname(olderlist.getString("user_fname"));
                                        model.setUser_lname(olderlist.getString("user_lname"));
                                        model.setRemark(olderlist.getString("user_remark"));
                                        model.setOrder_count(olderlist.getString("order_count"));*/
                                        //orderArrayList.add(model);
                                    }
                                    //orderlistAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                //}
            }
        });


        dialog.setContentView(view);

        dialog.show();
    }

    // Get Delivery Details
    private void getDeliveryDetails() { //TODO Server method here
        ///if (Connectivity.isConnected(HomeActivity.this)) { // Internet connection is not present, Ask user to connect to Internet

            JSONObject params = new JSONObject();
            try {
                params.put("city", sharedPreferencesUtils.getRCity());
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                Log.e("Request_param:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlgetDeliveryRequestDetails, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.e("DeliveryRequestRes:",""+response);
                        //event_array_url.clear();

                         String value_delivery_request =  response.getString("layout_status");
                         String new_count = response.getString("new_count");
                         String accepted_count = response.getString("accepted_count");
                         String delivered_count = response.getString("delivered_count");
                            if(value_delivery_request.equalsIgnoreCase("active")){
                                layout_DeliveryRequest.setVisibility(View.VISIBLE);
                                //cv_delivery_points.setVisibility(View.VISIBLE);
                                tv_accepted_Delivery.setText(accepted_count);
                                tv_assigned_Delivery.setText(delivered_count);
                                tv_newDelivery.setText(new_count);
                            }else{
                                layout_DeliveryRequest.setVisibility(View.GONE);
                                cv_delivery_points.setVisibility(View.GONE);
                            }

                        if (response.isNull("posts")) {

                        } else {
                            JSONArray mainOrderArray = response.getJSONArray("posts");

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    // GateWay gateWay = new GateWay(getActivity());
                    // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    //showAlertDialog("Error",error.toString(),"OK");
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(HomeActivity.this).add(request);
        //}
    }


    /*
    * New System signature
    * 4:48:37 PM: Executing task 'signingReport'...

Executing tasks: [signingReport]


> Configure project :app
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)

> Task :app:signingReport
Variant: debugUnitTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------
Variant: debugAndroidTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------
Variant: releaseUnitTest
Config: none
----------
Variant: debug
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------
Variant: release
Config: none
----------

BUILD SUCCESSFUL in 0s
1 actionable task: 1 executed
4:48:38 PM: Task execution finished 'signingReport'.

    * */
}
