package com.podmerchant.activites;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.PaymentModeAdaptor;
import com.podmerchant.adapter.TimeSlotAdaptor;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class TimeSlotActivity extends Activity {

    RecyclerView rv_timeslot_rb;
    Button btn_time_slots;
    LinearLayoutManager layoutManager;
    TimeSlotAdaptor paymentModeAdaptor;
    Context mContext;
    ArrayList<String> slot_array = new ArrayList<>();
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    String total_amount,orderId,product_brand,shopkpwise_id;
    String admin_aprove,areaName,aam_delivery_charge,updatedat,showcart, pincode, flag, action_time, cartUniqueId, userId, cartUserId, shipping_address, latitude, longitude, order_date, address_id, assign_delboy_id, reason_of_rejection,delivery_time="", address1, address2, address3, address4, address5, area,delivery_date,delivery_time_get,delivery_method,payment_option,user_fname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot);

        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        rv_timeslot_rb = findViewById(R.id.rv_timeslot_rb);
        btn_time_slots= findViewById(R.id.btn_time_slots);

        mContext = TimeSlotActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();



            Intent intent = getIntent();
            slot_array = intent.getStringArrayListExtra("slot_array");
            orderId = intent.getStringExtra("orderId");
            cartUniqueId = intent.getStringExtra("cartUniqueId");
            userId = intent.getStringExtra("userId");
            cartUserId = intent.getStringExtra("cartUserId");
            //shipping_address = intent.getStringExtra("shipping_address");
            latitude = intent.getStringExtra("latitude");
            longitude = intent.getStringExtra("longitude");
            total_amount = intent.getStringExtra("total_amount");
            order_date = intent.getStringExtra("order_date");
            address_id = intent.getStringExtra("address_id");
            assign_delboy_id = intent.getStringExtra("assign_delboy_id");
            action_time = intent.getStringExtra("action_time");
            address1 = intent.getStringExtra("address1");
            address2 = intent.getStringExtra("address2");
            address3 = intent.getStringExtra("address3");
            address4 = intent.getStringExtra("address4");
            address5 = intent.getStringExtra("address5");
            area = intent.getStringExtra("area");
            pincode = intent.getStringExtra("pincode");
            flag = intent.getStringExtra("flag");
            showcart = intent.getStringExtra("showcart");
            updatedat = intent.getStringExtra("updatedat");
            aam_delivery_charge = intent.getStringExtra("aam_delivery_charge");
            areaName = intent.getStringExtra("areaName");
            delivery_date = intent.getStringExtra("delivery_date");
            delivery_time_get = intent.getStringExtra("delivery_time");
            delivery_method = intent.getStringExtra("delivery_method");
            payment_option = intent.getStringExtra("payment_option");
            admin_aprove = intent.getStringExtra("admin_aprove");
            total_amount = intent.getStringExtra("total_amount");
            Log.e("slot_array:", "" + slot_array);


        rv_timeslot_rb.hasFixedSize();
        layoutManager = new LinearLayoutManager(mContext);
        rv_timeslot_rb.setLayoutManager(layoutManager);
        rv_timeslot_rb.setItemAnimator(new DefaultItemAnimator());

        //slot_array

        paymentModeAdaptor = new TimeSlotAdaptor(mContext,slot_array);
        rv_timeslot_rb.setAdapter(paymentModeAdaptor);

        rv_timeslot_rb.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_timeslot_rb,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        // delBoyId =  paymentModeList.get(position).getDelBoyId();

                            String slot = slot_array.get(position);
                            Log.e("Selected Slot value:", "" + slot);
                            acceptOrder(sharedPreferencesUtils.getShopID(), orderId, slot);
                            finish();


                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    //accept order
    public void acceptOrder(String shopID, final String orderId, String delivery_time) {
        if (Connectivity.isConnected(mContext)) {

           // progressDialog.show();

            String urlAcceptOrder = null;
            try {
                urlAcceptOrder = StaticUrl.acceptorder
                        + "?order_id=" + orderId
                        + "&approveBy=" +  sharedPreferencesUtils.getEmailId()
                        //+ "&manager_id=" + sharedPreferencesUtils.getManagerId()
                        + "&delivery_time=" + URLEncoder.encode(delivery_time,"utf-8")
                        + "&assign_shop_id=" + shopID
                        + "&tblName="+sharedPreferencesUtils.getAdminType()
                        + "&totalAmount="+total_amount
                        + "&shopName="+URLEncoder.encode(sharedPreferencesUtils.getShopName(),"utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

             Log.d("urlAcceptOrder",urlAcceptOrder);
            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                         //   progressDialog.dismiss();
                            try {
                                Log.e("acceptorderRes:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status == true) {
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext, "Order Accepted.", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(mContext,OrderDetailActivity.class);
                                    intent.putExtra("flag","approved_order");
                                    intent.putExtra("action_time", action_time);
                                    intent.putExtra("orderId",orderId);
                                    intent.putExtra("cartUniqueId",cartUniqueId);
                                    intent.putExtra("userId",userId);
                                    intent.putExtra("cartUserId",cartUserId);
                                    intent.putExtra("shipping_address",shipping_address);
                                    intent.putExtra("latitude",latitude);
                                    intent.putExtra("longitude",longitude);
                                    intent.putExtra("total_amount",total_amount);
                                    intent.putExtra("order_date",order_date);
                                    intent.putExtra("address_id",address_id);
                                    intent.putExtra("assign_delboy_id",assign_delboy_id);
                                    intent.putExtra("action_time",action_time);
                                    intent.putExtra("address1",address1);
                                    intent.putExtra("address2",address2);
                                    intent.putExtra("address3",address3);
                                    intent.putExtra("address4",address4);
                                    intent.putExtra("address5",address5);
                                    intent.putExtra("area",area);
                                    intent.putExtra("pincode",pincode);
                                    intent.putExtra("showcart",showcart);
                                   // intent.putExtra("flag",flag);
                                    intent.putExtra("updatedat",updatedat);
                                    intent.putExtra("aam_delivery_charge",aam_delivery_charge);
                                    intent.putExtra("areaName",areaName);
                                    intent.putExtra("delivery_date",delivery_date);
                                    intent.putExtra("delivery_time",delivery_time_get);
                                    intent.putExtra("delivery_method",delivery_method);
                                    intent.putExtra("payment_option",payment_option);
                                    intent.putExtra("admin_aprove",admin_aprove);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                } else if (status == false) {
                                    //Toast.makeText(mContext, "Sorry. This order you can not accept.", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                          //  progressDialog.dismiss();
                            Log.e("error:",""+error.toString());
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }




}
