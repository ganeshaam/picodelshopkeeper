package com.podmerchant.activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.podmerchant.R;
import com.podmerchant.local_storage.DBHelper;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.ShowcaseViewBuilder;
import com.podmerchant.util.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;

public class PawAddToCartActivity extends AppCompatActivity {

    public static int size = 0;
    private final String class_name = this.getClass().getSimpleName();
    private RelativeLayout addToCartMainLayout;
    private RelativeLayout addToCartRelativeLayout;
    private int cnt, presentQty, holder2iCount;
    private String shopId;
    private DialogPlus dialog, dialogPlus;
    private String pId;
    private String offerCode;
    private String strContact;
    private String strEmail;
    private String final_price, strMsg;
    private String strType;
    private TextView tvPrice, tvCount, tvTitle, tvMsg;
    private Button btnShopNow;
    private RecyclerView mRecyclerView;
    private CardAdapter cardAdapter;
    private CardAdapter_OutOfStock outofstock_cardAdapter;
    private RelativeLayout relativeLayoutEmptyCart, msgLayout;
    private LinearLayout relativeLayoutPrice;
    private MenuItem item;
    private LinearLayout Main_Layout_NoInternet;
    private List<Cart_Item> innerMovies1;
    private List<Normal_Cart_Item> innerMovies2;
    private Button tvPlaceOrder;
    private LinearLayout totalLay;
    private ShowcaseViewBuilder showcaseViewBuilder;
    private String product_total;
    private String Saving_Amount = null;
    private TextView tvTotalSaving;
    private BroadcastReceiver myReceiver;
    private TextView txtNoConnection;
    String product_maincat="";
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paw_add_to_cart);//activity_add_to_cart

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_add_to_cart);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PawAddToCartActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        init();

        GateWay gateWay = new GateWay(PawAddToCartActivity.this);
        strContact = sharedPreferencesUtils.getPhoneNumber();
        strEmail = sharedPreferencesUtils.getEmailId();

        btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                    Intent intent = new Intent(PawAddToCartActivity.this, PawHomeActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                    gateWay.displaySnackBar(addToCartMainLayout);
                }
            }
        });

        if(Connectivity.isConnected(mContext)){
            Main_Layout_NoInternet.setVisibility(View.GONE);
            addToCartMainLayout.setVisibility(View.VISIBLE);
            txtNoConnection.setText("Back online");
            txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
            slideDown(txtNoConnection);

            //check internet connectivity available or not
            //if internet connection is available then we check there is
            //any product is in out of stock from this method
            fetch_outofstock_item();
        }else {
            if (item != null) {
                item.setVisible(false);
            }
            relativeLayoutEmptyCart.setVisibility(View.GONE);
            addToCartMainLayout.setVisibility(View.GONE);
            Main_Layout_NoInternet.setVisibility(View.VISIBLE);

            txtNoConnection.setText("No connection");
            txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
            slideUp(txtNoConnection);
        }
    }


    @Override
    protected void onPause() {
        //   unregisterReceiver(myReceiver);
        super.onPause();
    }

    private void init() {
        //   myReceiver = new Network_Change_Receiver();

        txtNoConnection = findViewById(R.id.txtNoConnection);
        addToCartRelativeLayout = findViewById(R.id.addToCartRelativeLayout);
        addToCartMainLayout = findViewById(R.id.addToCartMainLayout);
        totalLay = findViewById(R.id.totalLay);
        tvPlaceOrder = findViewById(R.id.btnPlaceOrder);

        relativeLayoutEmptyCart = findViewById(R.id.relativeLayoutEmptyCart);
        relativeLayoutPrice = findViewById(R.id.relativeLayoutPrice);
        msgLayout = findViewById(R.id.msgLayout);
        tvPrice = findViewById(R.id.txtPrice);
        tvTotalSaving = findViewById(R.id.txtTotalSaving);
        tvCount = findViewById(R.id.txtCount);
        tvMsg = findViewById(R.id.txtMsg);
        btnShopNow = findViewById(R.id.btnShopNow);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        tvPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(PawAddToCartActivity.this)) {
                  /*  UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + " clicked on CHECKOUT button", AddToCart.this);
*/

                    /*if (product_maincat.equalsIgnoreCase("Grocery") && Double.parseDouble(final_price) < 500) {
                        Double chkPrice = 500 - Double.parseDouble(final_price);
                        popupLimitbyCat(product_maincat, final_price,chkPrice);
                    } else if (product_maincat.equalsIgnoreCase("Fruits and Veggies") && Double.parseDouble(final_price) < 300) {
                        Double chkPrice =  300 - Double.parseDouble(final_price);
                        popupLimitbyCat(product_maincat, final_price,chkPrice);
                    } else
                    {*/       // StopOrder(); chk order limit
                    try {
                        String localFinalPrice = tvPrice.getText().toString();
                        //Intent intent = new Intent(PawAddToCartActivity.this, Order_Details_StepperActivity.class);
                        if(sharedPreferencesUtils.getPawMode().equals("pawuser")) {
                            Intent intent = new Intent(PawAddToCartActivity.this, PawUserListActivity.class);
                            intent.putExtra("finalPrice", localFinalPrice);
                            startActivity(intent);
                        }else if(sharedPreferencesUtils.getPawMode().equals("paw")) {
                            Intent intent = new Intent(PawAddToCartActivity.this,PawOrderDetailActivity.class);
                            intent.putExtra("userid", "paw");
                            intent.putExtra("fname", "");
                            intent.putExtra("lname", "");
                            intent.putExtra("mobileno", "");
                            intent.putExtra("finalPrice",localFinalPrice);
                            startActivity(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //}
                } else {
                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                    gateWay.displaySnackBar(addToCartMainLayout);
                }
            }
        });
    }


    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                if (item != null) {
                    item.setVisible(false);
                }
                relativeLayoutEmptyCart.setVisibility(View.GONE);
                addToCartMainLayout.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                addToCartMainLayout.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);

                //check internet connectivity available or not
                //if internet connection is available then we check there is
                //any product is in out of stock from this method
                fetch_outofstock_item();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void StopOrder() { //TODO Server method here
        final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", sharedPreferencesUtils.getPhoneNumber());
                params.put("email", sharedPreferencesUtils.getEmailId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlStopOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            if (Connectivity.isConnected(PawAddToCartActivity.this)) {
                                try {
                                    String localFinalPrice = tvPrice.getText().toString();
                                    Intent intent = new Intent(PawAddToCartActivity.this, Order_Details_StepperActivity.class);
                                    intent.putExtra("finalPrice", localFinalPrice);
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        } else {
                            JSONArray array = response.getJSONArray("posts");
                            JSONObject jsonObject = array.getJSONObject(0);

                            String reason = jsonObject.getString("reason");
                            String image = jsonObject.getString("image");

                            dialog = DialogPlus.newDialog(PawAddToCartActivity.this)
                                    .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_stop_order))
                                    .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                    .setGravity(Gravity.CENTER)
                                    .setCancelable(false)
                                    .setPadding(20, 20, 20, 20)
                                    .create();
                            dialog.show();
                            ImageView imgStopOrder = (ImageView) dialog.findViewById(R.id.imgCashBack);
                            RelativeLayout stop_orderLayout = (RelativeLayout) dialog.findViewById(R.id.stop_orderLayout);
                            TextView tvMsg = (TextView) dialog.findViewById(R.id.txtMessage);
                            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                            if (reason.equals("")) {
                                tvMsg.setVisibility(View.GONE);
                            } else {
                                tvMsg.setVisibility(View.VISIBLE);
                                tvMsg.setText(reason);
                            }
                            if (image.equals("")) {
                                stop_orderLayout.setVisibility(View.GONE);
                            } else {
                                stop_orderLayout.setVisibility(View.VISIBLE);

                                //TODO here show app update image setup to glide
                                /*Glide.with(AddToCart.this)
                                        .load(image)
                                        .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(imgStopOrder);*/
                                Picasso.with(PawAddToCartActivity.this)
                                        .load(image)
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(imgStopOrder);
                            }

                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(addToCartMainLayout);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_cart, menu);
        item = menu.findItem(R.id.action_empty_cart);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_empty_cart:
                EmptyCartAlertDialog();
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PawAddToCartActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteAllProductFromCartItem() { //TODO Server method here
        if (Connectivity.isConnected(PawAddToCartActivity.this)) {
            final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (response.isNull("posts")) {
                    } else {
                        item.setVisible(false);

                        DBHelper db = new DBHelper(PawAddToCartActivity.this);
                        db.deleteOnlyCartTable();

                        tvPrice.setText("0");
                        addToCartMainLayout.setVisibility(View.GONE);
                        relativeLayoutEmptyCart.setVisibility(View.VISIBLE);

                     /*   btnShopNow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                    Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                    intent.putExtra("tag", "");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    gateWay.displaySnackBar(addToCartMainLayout);
                                }
                            }
                        });*/
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private void fetch_cart_item() { //TODO Server method here
        if (Connectivity.isConnected(PawAddToCartActivity.this)) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params1.addRule(RelativeLayout.ABOVE, R.id.relativeLayoutPrice);
            mRecyclerView.setLayoutParams(params1);

            innerMovies2 = new ArrayList<>();

            final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
            gateWay.progressDialogStart();


            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", sharedPreferencesUtils.getPhoneNumber());
                params.put("pawmode", sharedPreferencesUtils.getPawMode());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetMyCartPawItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("CartListItems:",""+response.toString());

                        if (response.isNull("posts")) {
                            //tvTitle.setText(R.string.title_activity_add_to_cart);
                            item.setVisible(false);
                            addToCartMainLayout.setVisibility(View.GONE);
                            relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                        } else {
                            //tvTitle.setText(R.string.title_activity_add_to_cart);
                            item.setVisible(true);
                            msgLayout.setVisibility(View.GONE);
                            addToCartMainLayout.setVisibility(View.VISIBLE);

                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");

                            final_price = response.getString("Final_Price");
                            Log.e("FinalAmount:",""+final_price);
                            Saving_Amount = response.getString("productSavingOnMrp");
                            //String product_maincat="";

                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jsonCart = mainClassificationJsonArray.getJSONObject(i);

                                innerMovies2.add(new Normal_Cart_Item(jsonCart.getString("normal_cart_shop_id"),
                                        jsonCart.getString("product_id"), jsonCart.getString("product_name"),
                                        jsonCart.getString("normal_shop_name"), jsonCart.getDouble("product_price"),
                                        jsonCart.getString("product_qty"), jsonCart.getString("product_image"),
                                        jsonCart.getString("type"), jsonCart.getString("available_product_quantity"),
                                        jsonCart.getString("allowed_qty"), jsonCart.getString("hindi_name"),
                                        jsonCart.getString("product_size"), jsonCart.getString("p_name"),
                                        jsonCart.getString("is_supersaver"), jsonCart.getString("supersaver_price"),
                                        jsonCart.getString("supersaver_qty"), jsonCart.getString("proPrice"),
                                        jsonCart.getDouble("product_mrp"), jsonCart.getString("product_discount"),
                                        jsonCart.getString("product_maincat"),
                                        jsonCart.getDouble("product_total")));
                                product_maincat = jsonCart.getString("product_maincat");
                                cardAdapter = new CardAdapter(innerMovies2);
                                mRecyclerView.setAdapter(cardAdapter);

                            }
                           /* if(product_maincat.equalsIgnoreCase("Grocery")&& Double.parseDouble(final_price)<500) {
                                popupLimitbyCat(product_maincat, final_price);
                            }else if(product_maincat.equalsIgnoreCase("Fruits and Veggies")&& Double.parseDouble(final_price)<300){
                                popupLimitbyCat(product_maincat, final_price);
                            }*/
                            tvCount.setText("ITEMS" + " " + "(" + innerMovies2.size() + ")");
                            double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                            //  tvPrice.setText("Total: ₹ " + Double.toString(finalValue));
                            tvPrice.setText( Double.toString(finalValue));
                            if (!Saving_Amount.equals("0")) {
                                tvTotalSaving.setVisibility(View.VISIBLE);

                                SpannableStringBuilder builder = new SpannableStringBuilder();
                                SpannableString str1 = new SpannableString("Your total savings:");
                                str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.textColor)), 0, str1.length(), 0);
                                builder.append(str1);
                                SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                str2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, str2.length(), 0);
                                builder.append(str2);

                                tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                            }
                            displayTuto("1");
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().setPriority(Request.Priority.LOW);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private void displayTuto(String Tag) {
        if (Tag.equals("1")) {
            showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                    .setTargetView(totalLay)
                    .setBackgroundOverlayColor()
                    .singleUse("1")
                    .setRingColor()
                    .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                    .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                    .addCustomView(Gravity.CENTER_HORIZONTAL, "Total Amount & No. of Items !", "Here you can see your ITEM COUNT & TOTAL AMOUNT !", "Next")
                    .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
            showcaseViewBuilder.show();

            showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcaseViewBuilder.hide();
                    displayTuto("2");
                }
            });
        } else {
            showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                    .setTargetView(tvPlaceOrder)
                    .setBackgroundOverlayColor()
                    .singleUse("2")
                    .setRingColor()
                    .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                    .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                    .addCustomView(Gravity.CENTER_HORIZONTAL, "Checkout !", "Tap on CHECKOUT to PLACE the ORDER !", "Got it")
                    .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
            showcaseViewBuilder.show();

            showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcaseViewBuilder.hide();
                }
            });
        }
    }

    private void fetch_outofstock_item() { //TODO Server method here
        if (Connectivity.isConnected(PawAddToCartActivity.this)) {
            RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params1.addRule(RelativeLayout.BELOW, R.id.msgLayout);
            addToCartMainLayout.setLayoutParams(params1);

            innerMovies1 = new ArrayList<>();

            final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
                params.put("pawmode", sharedPreferencesUtils.getPawMode());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetMyCartOutOfStockItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            //check first in cart there is any product is out of stock or not
                            //if there is no product in out of stock then this method call
                            fetch_cart_item();
                        } else {
                            tvTitle.setText("Items unavailable");

                            item.setVisible(false);

                            msgLayout.setVisibility(View.VISIBLE);

                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");
                            strMsg = response.getString("msg");
                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jsonCart = mainClassificationJsonArray.getJSONObject(i);

                                innerMovies1.add(new Cart_Item(jsonCart.getString("normal_cart_shop_id"),
                                        jsonCart.getString("product_id"), jsonCart.getString("product_name"),
                                        jsonCart.getString("normal_shop_name"), jsonCart.getDouble("product_price")
                                        , jsonCart.getString("product_qty"), jsonCart.getString("product_image"),
                                        jsonCart.getString("type"), jsonCart.getString("available_product_quantity")));
                                outofstock_cardAdapter = new CardAdapter_OutOfStock(innerMovies1);
                                mRecyclerView.setAdapter(outofstock_cardAdapter);
                            }
                            tvMsg.setText(strMsg);
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().setPriority(Request.Priority.LOW);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
/*
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(PawAddToCartActivity.this)) {
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, AddToCart.this);

            GateWay gateWay1 = new GateWay(PawAddToCartActivity.this);
            SyncWishListItems UT2 = new SyncWishListItems(SyncWishListItems.context);
            UT2.syncWishListItems(gateWay1.getContact(), gateWay1.getUserEmail(), AddToCart.this);
        }*/


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if (MasterSearch.searchView != null) {
            MasterSearch.searchView.setQuery("", false);
            MasterSearch.searchView.setIconified(false);
        }*/
    }

    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder1> {

        private List<Normal_Cart_Item> mArraylist;

        public CardAdapter(List<Normal_Cart_Item> innerMovies) {
            mArraylist = innerMovies;
        }

        @NonNull
        @Override
        public CardAdapter.ViewHolder1 onCreateViewHolder(@NonNull final ViewGroup viewGroup, int viewType) {
            View v;
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_add_to_cart_new, viewGroup, false);
            relativeLayoutPrice.setVisibility(View.VISIBLE);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                        Normal_Cart_Item movie = mArraylist.get(itemPosition);
                        if (movie.getStrtype().equals("combo")) {
                        } else {
                            if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                                /*UserTracking UT = new UserTracking(UserTracking.context);
                                UT.user_tracking(class_name + ": clicked on view & product is: " + movie.getStr_productName(), AddToCart.this);
*/
                                Intent intent = new Intent(PawAddToCartActivity.this, ProductDetailActivity.class);
                                intent.putExtra("product_name", movie.getP_Name());
                                intent.putExtra("shop_id", movie.getStr_normal_cart_shopId());
                                startActivity(intent);
                            } else {
                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return new CardAdapter.NormalProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull CardAdapter.ViewHolder1 viewHolder, int position) {
            final Normal_Cart_Item cart_item = mArraylist.get(position);

            if (cart_item != null) {
                final CardAdapter.NormalProductViewHolder holder1 = (CardAdapter.NormalProductViewHolder) viewHolder;
                strType = cart_item.getStrtype();

                DBHelper db = new DBHelper(PawAddToCartActivity.this);
                HashMap AddToCartInfo = db.getWishListDetails(cart_item.getStr_product_Id());
                String btnCheckStatus = (String) AddToCartInfo.get("pId");
                if (btnCheckStatus == null) {
                    holder1.layout_action2.setVisibility(View.VISIBLE);
                } else {
                    holder1.layout_action2.setVisibility(View.INVISIBLE);
                }

                if (strType.equals("combo")) {
                    holder1.layout_action2.setVisibility(View.INVISIBLE);

                    //TODO here combo offer products images setup to glide
                    /*Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/
                    Picasso.with(PawAddToCartActivity.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.productImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                                dialogPlus = DialogPlus.newDialog(PawAddToCartActivity.this)
                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .create();

                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                //TODO here when user clicks on combo offer product image setup to glide
                               /* Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                                        .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(alertProductImage1);*/

                                Picasso.with(PawAddToCartActivity.this).load(cart_item.getStr_productImg())
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(alertProductImage1);

                                dialogPlus.show();
                            } else {
                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        }
                    });
                } else {
                    //TODO here products images setup to glide
                    /*Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/

                    Picasso.with(PawAddToCartActivity.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.productImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                                dialogPlus = DialogPlus.newDialog(PawAddToCartActivity.this)
                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .create();

                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                //TODO here when user clicks on product image setup to glide
                               /* Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                                        .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(alertProductImage1);*/

                                Picasso.with(PawAddToCartActivity.this).load(cart_item.getStr_productImg())
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(alertProductImage1);

                                dialogPlus.show();
                            } else {
                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        }
                    });
                }

                double normalPrice;
                double productTotal;

                holder1.tvProductName.setText(cart_item.getStr_productName());
                //holder1.tvNormalSellerName.setText("By: " + cart_item.getStr_shopName());
                String mrp = String.valueOf(cart_item.getProduct_mrp());
                double productMrp = cart_item.getProduct_mrp();
                String discountPrice;

                holder1.tvSuperSaverTitle.setVisibility(View.GONE);
                normalPrice = cart_item.getStr_price();
                productTotal = cart_item.getProduct_total();

                discountPrice = String.valueOf(cart_item.getStr_price());
                String dis = cart_item.getProduct_discount();
                String discount = cart_item.setProduct_discount(dis);
                holder1.tvDiscount.setText(discount + " % OFF");

                if (mrp.equals(discountPrice)) {
                    holder1.tvProductPrice.setVisibility(View.VISIBLE);
                    holder1.tvDiscount.setVisibility(View.GONE);
                    holder1.tvNormalPrice.setVisibility(View.GONE);
                } else {
                    holder1.tvProductPrice.setVisibility(View.VISIBLE);
                    holder1.tvDiscount.setVisibility(View.VISIBLE);
                    holder1.tvNormalPrice.setVisibility(View.VISIBLE);
                    holder1.tvNormalPrice.setBackgroundResource(R.drawable.dash);
                }
                holder1.tvNormalPrice.setText("\u20B9 " + productMrp);
                holder1.tvProductPrice.setText("\u20B9 " + normalPrice);
                holder1.etQuantity.setText(cart_item.getStr_qty());

                if (cart_item.getStrHindi_Name().equals("")) {
                    holder1.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    holder1.tvProductHindiName.setText(" ( " + cart_item.getStrHindi_Name() + " )");
                }
                holder1.tvProductTotal.setText("Net \u20B9: " + productTotal);

                holder1.layout_action2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                            presentQty = 0;

                            DBHelper db = new DBHelper(PawAddToCartActivity.this);

                            CardAdapter.ViewHolder1 holder = (CardAdapter.ViewHolder1) (v.getTag());
                            int pos = holder.getAdapterPosition();
                            //mArraylist.remove(pos);
                            //notifyItemRemoved(pos);

                            pId = cart_item.getStr_product_Id();

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": moved to wishlist & product is: " + cart_item.getStr_productName(), AddToCart.this);
*/
                            String itemsCount = String.valueOf(mArraylist.size());
                            tvCount.setText("ITEMS" + " " + "(" + itemsCount + ")");

                            int iCount = Integer.parseInt(itemsCount);
                            if (iCount == 0) {
                                if (cnt == 0 && holder2iCount == 0) {
                                    tvPrice.setText("0");
                                    item.setVisible(false);
                                    relativeLayoutPrice.setVisibility(View.GONE);
                                    relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                                }
                              /*  btnShopNow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                            Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                            intent.putExtra("tag", "");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            GateWay gateWay = new GateWay(AddToCart.this);
                                            gateWay.displaySnackBar(addToCartMainLayout);
                                        }
                                    }
                                });*/
                            }
                            Toast.makeText(PawAddToCartActivity.this, "Moving product to wishlist.", Toast.LENGTH_SHORT).show();
                            //db.deleteProductItem(pId);  // deleting item from the wishlist
                            MoveToWishlist();
                            holder1.layout_action2.setVisibility(View.INVISIBLE);
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    void MoveToWishlist() { //TODo Server method here
                        shopId = cart_item.getStr_normal_cart_shopId();
                        pId = cart_item.getStr_product_Id();

                        final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                        gateWay.progressDialogStart();

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("versionCode", StaticUrl.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAddCartItemToWishlist, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                if (!response.isNull("posts")) {
                                    try {
                                        final_price = response.getString("Total_Price");
                                        double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                        tvPrice.setText( Double.toString(finalValue));
                                        Saving_Amount = response.getString("productSavingOnMrp");
                                        if (!Saving_Amount.equals("0")) {
                                            tvTotalSaving.setVisibility(View.VISIBLE);
                                            SpannableStringBuilder builder = new SpannableStringBuilder();
                                            SpannableString str1 = new SpannableString("Your total savings:");
                                            str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                            builder.append(str1);
                                            SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                            str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                            builder.append(str2);

                                            tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                        } else {
                                            tvTotalSaving.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                gateWay.progressDialogStop();
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                gateWay.progressDialogStop();

                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                    }
                });

                holder1.imgPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                          /*  UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on plus button & product is: " + cart_item.getStr_productName(), AddToCart.this);
*/
                            int qty = Integer.parseInt(holder1.etQuantity.getText().toString());
                            qty++;
                            checkQuantityCartItemOnPlus(qty);
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    private void checkQuantityCartItemOnPlus(final int qty) { //TODO Server method here
                        shopId = cart_item.getStr_normal_cart_shopId();

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", cart_item.getStr_product_Id());
                            params.put("shop_id", shopId);
                            params.put("size", cart_item.getStr_Size());
                            params.put("qty", qty);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("pawmode", sharedPreferencesUtils.getPawMode());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {
                                } else {
                                    try {
                                        int AllowedQty = Integer.parseInt(cart_item.getStrAllowedQty());
                                        int Available_Qty = Integer.parseInt(cart_item.getStr_AvailableProductQuantity());
                                        if (AllowedQty == 0) {
                                            if (qty > Available_Qty) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PawAddToCartActivity.this);
                                                alertDialogBuilder.setMessage("You cannot add more than " + Available_Qty + " quantities of this product.");
                                                alertDialogBuilder.setPositiveButton("OK",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                dialogInterface.dismiss();
                                                            }
                                                        });
                                                AlertDialog alertDialog = alertDialogBuilder.create();
                                                alertDialog.show();
                                            } else {
                                                updateMethodCall(qty);
                                            }
                                        } else {
                                            if (qty > AllowedQty) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PawAddToCartActivity.this);
                                                alertDialogBuilder.setMessage("You cannot add more than " + AllowedQty + " quantities of this product.");
                                                alertDialogBuilder.setPositiveButton("OK",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                dialogInterface.dismiss();
                                                            }
                                                        });
                                                AlertDialog alertDialog = alertDialogBuilder.create();
                                                alertDialog.show();
                                            } else {
                                                updateMethodCall(qty);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            private void updateMethodCall(int qty) {
                                try {
                                    cart_item.setStr_qty(String.valueOf(qty));
                                    holder1.etQuantity.setText("" + qty);
                                    strType = cart_item.getStrtype();

                                    if (strType.equals("combo")) {
                                        //updateQuantityInComboCartItemOnPlus();
                                    } else {
                                        updateQuantityInNormalCartItemOnPlus();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            private void updateQuantityInNormalCartItemOnPlus() { //TODO Server method here
                                if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                                    shopId = cart_item.getStr_normal_cart_shopId();

                                    final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                    gateWay.progressDialogStart();

                                    JSONObject params = new JSONObject();
                                    try {
                                        params.put("product_id", cart_item.getStr_product_Id());
                                        params.put("shop_id", shopId);
                                        params.put("Qty", holder1.etQuantity.getText().toString());
                                        params.put("contactNo", strContact);
                                        params.put("email", strEmail);
                                        params.put("pawmode", sharedPreferencesUtils.getPawMode());
                                        if (cart_item.is_supersaver.equals("Yes")) {
                                            params.put("tag", "super_saver");
                                        } else {
                                            params.put("tag", "");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {

                                            if (!response.isNull("posts")) {
                                                try {
                                                    final_price = response.getString("Total_Price");
                                                    double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                                    tvPrice.setText( Double.toString(finalValue));
                                                    Saving_Amount = response.getString("productSavingOnMrp");
                                                    if (!Saving_Amount.equals("0")) {
                                                        tvTotalSaving.setVisibility(View.VISIBLE);
                                                        SpannableStringBuilder builder = new SpannableStringBuilder();
                                                        SpannableString str1 = new SpannableString("Your total savings:");
                                                        str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                                        builder.append(str1);
                                                        SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                                        str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                                        builder.append(str2);

                                                        tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                                    }
                                                    product_total = response.getString("product_total");
                                                    double productValue = Math.round(Double.parseDouble(product_total) * 100.0) / 100.0;
                                                    holder1.tvProductTotal.setText("Net \u20B9: " + Double.toString(productValue));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            gateWay.progressDialogStop();
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            error.printStackTrace();

                                            gateWay.progressDialogStop();

                                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                            gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                        }
                                    });
                                    VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                                } else {
                                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                    gateWay.displaySnackBar(addToCartMainLayout);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                    }
                });

                holder1.imgMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet

                         /*   UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on minus button & product is: " + cart_item.getStr_productName(), PawAddToCartActivity.this);
*/
                            int qty = Integer.parseInt(holder1.etQuantity.getText().toString());
                            qty--;
                            if (qty > 0) {
                                cart_item.setStr_qty(String.valueOf(qty));
                                holder1.etQuantity.setText("" + qty);
                                strType = cart_item.getStrtype();

                                if (strType.equals("combo")) {
                                    //updateQuantityInComboCartItemOnPlus();
                                } else {
                                    updateQuantityInNormalCartItemOnPlus();
                                }
                            } else if (qty == 0) {
                                strType = cart_item.getStrtype();
                                if (strType.equals("combo")) {
                                    //deleteComboProductFromCartItem(offerCode);
                                } else {
                                    deleteNormalProductFromCartItem(qty);
                                }
                            }
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    private void updateQuantityInNormalCartItemOnPlus() { //TODO Server method here
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                            shopId = cart_item.getStr_normal_cart_shopId();

                            final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.progressDialogStart();

                            JSONObject params = new JSONObject();
                            try {
                                params.put("product_id", cart_item.getStr_product_Id());
                                params.put("shop_id", shopId);
                                params.put("Qty", holder1.etQuantity.getText().toString());
                                params.put("contactNo", strContact);
                                params.put("email", strEmail);
                                params.put("pawmode", sharedPreferencesUtils.getPawMode());
                                if (cart_item.is_supersaver.equals("Yes")) {
                                    params.put("tag", "super_saver");
                                } else {
                                    params.put("tag", "");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    if (!response.isNull("posts")) {
                                        try {
                                            final_price = response.getString("Total_Price");
                                            double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                            tvPrice.setText(Double.toString(finalValue));
                                            Saving_Amount = response.getString("productSavingOnMrp");
                                            if (!Saving_Amount.equals("0")) {
                                                tvTotalSaving.setVisibility(View.VISIBLE);
                                                SpannableStringBuilder builder = new SpannableStringBuilder();
                                                SpannableString str1 = new SpannableString("Your total savings:");
                                                str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                                builder.append(str1);
                                                SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                                str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                                builder.append(str2);

                                                tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                            }
                                            product_total = response.getString("product_total");
                                            double productValue = Math.round(Double.parseDouble(product_total) * 100.0) / 100.0;
                                            holder1.tvProductTotal.setText("Net \u20B9: " + Double.toString(productValue));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    gateWay.progressDialogStop();
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();

                                    gateWay.progressDialogStop();

                                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                }
                            });
                            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    private void deleteNormalProductFromCartItem(int qty) { //TODO Server method here
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                            final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.progressDialogStart();

                            final DBHelper db = new DBHelper(PawAddToCartActivity.this);

                            JSONObject params = new JSONObject();
                            try {
                                params.put("product_id", cart_item.getStr_product_Id());
                                params.put("contactNo", strContact);
                                params.put("email", strEmail);
                                params.put("qty", qty);
                                params.put("tag", "delete_one_item");
                                params.put("pawmode", sharedPreferencesUtils.getPawMode());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    if (response.isNull("posts")) {
                                    } else {
                                        db.deleteProductItem(cart_item.getStr_product_Id());
                                        try {
                                            int pos = holder1.getAdapterPosition();
                                            mArraylist.remove(pos);
                                            notifyItemRemoved(pos);

                                            //SearchProducts.addToCartArrayList.remove(cart_item.getStr_product_Id());

                                            Saving_Amount = response.getString("productSavingOnMrp");
                                            if (Saving_Amount.equals("0")) {
                                                tvTotalSaving.setVisibility(View.GONE);
                                            }
                                            int itemsCount = mArraylist.size();
                                            if (itemsCount == 0) {
                                                if (cnt == 0 && holder2iCount == 0) {
                                                    tvPrice.setText("0");
                                                    item.setVisible(false);
                                                    relativeLayoutPrice.setVisibility(View.GONE);
                                                    relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                                                }
                                               /* btnShopNow.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                                            Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                                            intent.putExtra("tag", "");
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                            finish();
                                                        } else {
                                                            GateWay gateWay = new GateWay(AddToCart.this);
                                                            gateWay.displaySnackBar(addToCartMainLayout);
                                                        }
                                                    }
                                                });*/
                                            } else {
                                                tvCount.setText("ITEMS" + " " + "(" + itemsCount + ")");

                                                final_price = response.getString("Total_Price");
                                                double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                                tvPrice.setText(Double.toString(finalValue));

                                                Saving_Amount = response.getString("productSavingOnMrp");
                                                if (!Saving_Amount.equals("0")) {
                                                    tvTotalSaving.setVisibility(View.VISIBLE);
                                                    SpannableStringBuilder builder = new SpannableStringBuilder();
                                                    SpannableString str1 = new SpannableString("Your total savings:");
                                                    str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                                    builder.append(str1);
                                                    SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                                    str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                                    builder.append(str2);

                                                    tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    gateWay.progressDialogStop();
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    gateWay.progressDialogStop();

                                    error.printStackTrace();

                                    GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                }
                            });
                            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mArraylist.size();
        }

        class ViewHolder1 extends RecyclerView.ViewHolder {
            ViewHolder1(View itemView) {
                super(itemView);
            }
        }

        public class NormalProductViewHolder extends CardAdapter.ViewHolder1 {

            final TextView tvProductName;
            final TextView tvNormalSellerName;
            final TextView tvNormalPrice;
            final TextView tvProductPrice;
            final TextView tvDiscount;
            final TextView tvProductTotal;
            final TextView tvProductHindiName;
            final TextView tvMovetoWishlist;
            final TextView removeItem;
            final TextView tvSuperSaverTitle;
            final ImageView productImg;
            final ImageView imgMinus;
            final ImageView imgPlus;
            final TextView etQuantity;
            final LinearLayout layout_action1;
            final LinearLayout layout_action2;

            NormalProductViewHolder(View v) {
                super(v);
                tvProductName = v.findViewById(R.id.txtProductName);
                tvProductHindiName = v.findViewById(R.id.txtProductHindiName);
                tvNormalSellerName = v.findViewById(R.id.txtNormalSellerName);
                tvNormalPrice = v.findViewById(R.id.txtNormalPrice);
                tvProductPrice = v.findViewById(R.id.txtProductPrice);
                tvDiscount = v.findViewById(R.id.txtDiscount);
                removeItem = v.findViewById(R.id.removeItem);
                etQuantity = v.findViewById(R.id.editQuantity);
                tvSuperSaverTitle = v.findViewById(R.id.txtSuperSaverTitle);
                productImg = v.findViewById(R.id.productImage);
                imgMinus = v.findViewById(R.id.imgMinus);
                imgPlus = v.findViewById(R.id.imgPlus);
                tvProductTotal = v.findViewById(R.id.txtProductTotal);
                tvMovetoWishlist = v.findViewById(R.id.txtMovetoWishlist);
                layout_action1 = v.findViewById(R.id.layout_action1);
                layout_action2 = v.findViewById(R.id.layout_action2);
                imgMinus.setTag(this);
                imgPlus.setTag(this);
                layout_action1.setTag(this);
                layout_action2.setTag(this);
            }
        }
    }

    private class CardAdapter_OutOfStock extends RecyclerView.Adapter<CardAdapter_OutOfStock.ViewHolder> {

        DialogPlus dialog;
        List<Cart_Item> mArraylist;

        CardAdapter_OutOfStock(List<Cart_Item> innerMovies) {
            mArraylist = innerMovies;
        }

        @NonNull
        @Override
        public CardAdapter_OutOfStock.ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int viewType) {
            View v;
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_add_to_cart, viewGroup, false);
            relativeLayoutPrice.setVisibility(View.GONE);
            return new CardAdapter_OutOfStock.NormalProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
            final Cart_Item cart_item = mArraylist.get(position);

            if (cart_item != null) {
                final CardAdapter_OutOfStock.NormalProductViewHolder holder1 = (CardAdapter_OutOfStock.NormalProductViewHolder) viewHolder;
                strType = cart_item.getStrtype();

                if (strType.equals("combo")) {
                    //TODO here combo offer products images setup to glide, if product is out of stock
                   /* Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/
                    Picasso.with(PawAddToCartActivity.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                } else {
                    //TODO here products images setup to glide, if product is out of stock
                    /*Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/

                    Picasso.with(PawAddToCartActivity.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                }
                holder1.tvProductName.setText(cart_item.getStr_productName());
                // holder1.tvNormalSellerName.setText("By: " + cart_item.getStr_shopName());
                holder1.tvNormalPrice.setText("" + cart_item.getStr_price());
                holder1.etQuantity.setText(String.valueOf(cart_item.getStr_qty()));

                holder1.tvMovetoWishlist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                            presentQty = 0;
                            CardAdapter.ViewHolder1 holder = (CardAdapter.ViewHolder1) (v.getTag());
                            int pos = holder.getAdapterPosition();
                            mArraylist.remove(pos);
                            notifyItemRemoved(pos);

                            pId = cart_item.getStr_product_Id();

                           /* UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": moved product to wishlist & product is: " + cart_item.getStr_productName(), PawAddToCartActivity.this);
*/
                            String itemsCount = String.valueOf(mArraylist.size());
                            tvCount.setText("ITEMS" + " " + "(" + itemsCount + ")");

                            int iCount = Integer.parseInt(itemsCount);
                            if (iCount == 0) {
                                if (cnt == 0 && holder2iCount == 0) {
                                    tvPrice.setText("0");
                                    item.setVisible(false);
                                    relativeLayoutPrice.setVisibility(View.GONE);
                                    relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                                }
                               /* btnShopNow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                            Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                            intent.putExtra("tag", "");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            GateWay gateWay = new GateWay(AddToCart.this);
                                            gateWay.displaySnackBar(addToCartMainLayout);
                                        }
                                    }
                                });*/
                            }
                            Toast.makeText(PawAddToCartActivity.this, "Moving product to wishlist.", Toast.LENGTH_SHORT).show();
                            MoveToWishlist();
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    void MoveToWishlist() { //TODO Server method here
                        final DBHelper db = new DBHelper(PawAddToCartActivity.this);

                        shopId = cart_item.getStr_normal_cart_shopId();
                        pId = cart_item.getStr_product_Id();

                        final GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                        gateWay.progressDialogStart();

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("versionCode", StaticUrl.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAddCartItemToWishlist, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {
                                } else {
                                    db.deleteProductItem(pId);
                                    try {
                                        final_price = response.getString("Total_Price");
                                        double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                        tvPrice.setText( Double.toString(finalValue));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                gateWay.progressDialogStop();
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                gateWay.progressDialogStop();

                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                    }
                });

                holder1.etQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog = DialogPlus.newDialog(PawAddToCartActivity.this)
                                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.quantity_layout))
                                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .setPadding(20, 20, 20, 20)
                                .create();
                        dialog.show();
                        final TextView tvOne = (TextView) dialog.findViewById(R.id.txtOne);
                        final TextView tvTwo = (TextView) dialog.findViewById(R.id.txtTwo);
                        final TextView tvThree = (TextView) dialog.findViewById(R.id.txtThree);
                        final TextView tvFour = (TextView) dialog.findViewById(R.id.txtFour);
                        final TextView tvFive = (TextView) dialog.findViewById(R.id.txtFive);
                        final TextView tvSix = (TextView) dialog.findViewById(R.id.txtSix);
                        final TextView tvSeven = (TextView) dialog.findViewById(R.id.txtSeven);
                        final TextView tvEight = (TextView) dialog.findViewById(R.id.txtEight);
                        final TextView tvNine = (TextView) dialog.findViewById(R.id.txtNine);
                        final TextView tvTen = (TextView) dialog.findViewById(R.id.txtTen);
                        LinearLayout extraQuantityLinearLayout = (LinearLayout) dialog.findViewById(R.id.extraQuantityLinearLayout);
                        if (cart_item.getStr_price() <= 50) {
                            extraQuantityLinearLayout.setVisibility(View.VISIBLE);
                        }
                        tvOne.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String one = tvOne.getText().toString();
                                setQuantityMethod(one);
                            }
                        });
                        tvTwo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String two = tvTwo.getText().toString();
                                setQuantityMethod(two);
                            }
                        });
                        tvThree.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String three = tvThree.getText().toString();
                                setQuantityMethod(three);
                            }
                        });
                        tvFour.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String four = tvFour.getText().toString();
                                setQuantityMethod(four);
                            }
                        });
                        tvFive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String five = tvFive.getText().toString();
                                setQuantityMethod(five);
                            }
                        });
                        tvSix.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String six = tvSix.getText().toString();
                                setQuantityMethod(six);
                            }
                        });
                        tvSeven.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String seven = tvSeven.getText().toString();
                                setQuantityMethod(seven);
                            }
                        });
                        tvEight.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String eight = tvEight.getText().toString();
                                setQuantityMethod(eight);
                            }
                        });
                        tvNine.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String nine = tvNine.getText().toString();
                                setQuantityMethod(nine);
                            }
                        });
                        tvTen.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String ten = tvTen.getText().toString();
                                setQuantityMethod(ten);
                            }
                        });
                    }

                    private void setQuantityMethod(String qty) {
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                            int Available_Qty = Integer.parseInt(cart_item.getStr_AvailableProductQuantity());
                            if (Integer.parseInt(qty) > Available_Qty) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PawAddToCartActivity.this);
                                alertDialogBuilder.setMessage("" + Available_Qty + " quantities available right now.");
                                alertDialogBuilder.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                        dialog.dismiss();
                    }
                });

                try {
                    int qty = Integer.parseInt(cart_item.getStr_qty());
                    double productPrice = cart_item.getStr_price();
                    if (qty > 1) {
                        double total_price = qty * productPrice;
                        double finalValue = Math.round(total_price * 100.0) / 100.0;
                        holder1.tvProductTotal.setText("" + finalValue);
                    } else {
                        holder1.tvProductTotal.setText("" + cart_item.getStr_price());
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                holder1.removeItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Connectivity.isConnected(PawAddToCartActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
                            presentQty = 0;
                            CardAdapter_OutOfStock.ViewHolder holder = (CardAdapter_OutOfStock.ViewHolder) (v.getTag());
                            int pos = holder.getAdapterPosition();
                            mArraylist.remove(pos);
                            notifyItemRemoved(pos);

                            offerCode = cart_item.getStr_product_Id();
                            pId = cart_item.getStr_product_Id();

                            String itemsCount = String.valueOf(mArraylist.size());
                            int iCount = Integer.parseInt(itemsCount);
                            if (iCount == 0) {
                                if (cnt == 0 && holder2iCount == 0) {
                                    msgLayout.setVisibility(View.GONE);

                                    //when user remove product one by one in that case this method call and get fresh data from server
                                    fetch_cart_item();
                                }
                            }
                            strType = cart_item.getStrtype();
                            if (strType.equals("combo")) {
                                //deleteComboProductFromCartItem(offerCode);
                            } else {
                                deleteNormalProductFromCartItem(pId);
                            }
                        } else {
                            GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    public void deleteNormalProductFromCartItem(String p_id) { //TODO Server method here
                        final DBHelper db = new DBHelper(PawAddToCartActivity.this);

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", p_id);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("tag", "delete_one_item");
                            params.put("pawmode", sharedPreferencesUtils.getPawMode());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {
                                } else {
                                    db.deleteProductItem(pId);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                    }

                    public void deleteComboProductFromCartItem(String offer_code) { //TODO Server method here
                        final DBHelper db = new DBHelper(PawAddToCartActivity.this);

                        JSONObject params = new JSONObject();
                        try {
                            params.put("offer_code", offer_code);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("tag", "delete_one_item");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {
                                } else {
                                    db.deleteProductItem(pId);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(PawAddToCartActivity.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mArraylist.size();
        }

        public void add(Cart_Item cart_item) {
            mArraylist.add(cart_item);
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(View itemView) {
                super(itemView);
            }
        }

        class NormalProductViewHolder extends CardAdapter_OutOfStock.ViewHolder {

            final TextView tvProductName;
            final TextView tvNormalSellerName;
            final TextView tvNormalPrice;
            final TextView tvProductTotal;
            final TextView tvMovetoWishlist;
            final ImageView removeItem;
            final ImageView productImg;
            final EditText etQuantity;

            NormalProductViewHolder(View v) {
                super(v);
                tvProductName = v.findViewById(R.id.txtProductName);
                tvNormalSellerName = v.findViewById(R.id.txtNormalSellerName);
                tvNormalPrice = v.findViewById(R.id.txtNormalPrice);
                removeItem = v.findViewById(R.id.removeItem);
                etQuantity = v.findViewById(R.id.editQuantity);
                productImg = v.findViewById(R.id.productImage);
                tvProductTotal = v.findViewById(R.id.txtProductTotal);
                tvMovetoWishlist = v.findViewById(R.id.txtMovetoWishlist);
                removeItem.setTag(this);
                tvMovetoWishlist.setTag(this);
            }
        }
    }

    public static class Normal_Cart_Item {

        final String str_normal_cart_shopId;
        final String str_product_Id;
        final String str_productName;
        final String str_shopName;
        final String str_productImg;
        final String strtype;
        final String str_AvailableProductQuantity;
        final String strHindi_Name;
        final String strAllowedQty;
        final double str_price;
        final String str_Size;
        final String p_Name;
        final String is_supersaver;
        final String supersaver_price;
        final String supersaver_qty;
        final String proPrice;
        final double product_mrp;
        final double product_total;
        final String product_maincat;
        String str_qty;
        String product_discount;


        Normal_Cart_Item(String normal_cart_shopId, String productId, String productName,
                         String shopName, double price, String qty, String productImg,
                         String type, String AvailableProductQuantity, String AllowedQty, String hindiname,
                         String str_Size, String p_Name, String is_supersaver, String supersaver_price, String supersaver_qty,
                         String proPrice, double product_mrp, String product_discount, String product_maincat,double product_total) {
            this.str_normal_cart_shopId = normal_cart_shopId;
            this.str_product_Id = productId;
            this.str_productName = productName;
            this.str_shopName = shopName;
            this.str_price = price;
            this.str_qty = qty;
            this.str_productImg = productImg;
            this.strtype = type;
            this.str_AvailableProductQuantity = AvailableProductQuantity;
            this.strAllowedQty = AllowedQty;
            this.strHindi_Name = hindiname;
            this.str_Size = str_Size;
            this.p_Name = p_Name;
            this.is_supersaver = is_supersaver;
            this.supersaver_price = supersaver_price;
            this.supersaver_qty = supersaver_qty;
            this.proPrice = proPrice;
            this.product_mrp = product_mrp;
            this.product_discount = product_discount;
            this.product_maincat=product_maincat;
            this.product_total = product_total;
        }

        public String getStrHindi_Name() {
            return strHindi_Name;
        }

        public String getP_Name() {
            return p_Name;
        }

        public String getStr_Size() {
            return str_Size;
        }

        public String getStrAllowedQty() {
            return strAllowedQty;
        }

        public String getStr_normal_cart_shopId() {
            return str_normal_cart_shopId;
        }

        public String getStr_product_Id() {
            return str_product_Id;
        }

        public String getStr_productName() {
            return str_productName;
        }

        public String getStr_shopName() {
            return str_shopName;
        }

        public double getStr_price() {
            return str_price;
        }

        public String getStr_qty() {
            return str_qty;
        }

        public void setStr_qty(String str_qty) {
            this.str_qty = str_qty;
        }

        public String getStr_productImg() {
            return str_productImg;
        }

        public String getStrtype() {
            return strtype;
        }

        public String getStr_AvailableProductQuantity() {
            return str_AvailableProductQuantity;
        }

        public double getProduct_mrp() {
            return product_mrp;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
            return product_discount;
        }

        public double getProduct_total() {
            return product_total;
        }

        public String getProduct_maincat() {
            return product_maincat;
        }
    }

    private class Cart_Item {

        final String str_normal_cart_shopId;
        final String str_product_Id;
        final String str_productName;
        final String str_shopName;
        final String str_productImg;
        final String strtype;
        final String str_AvailableProductQuantity;
        final double str_price;
        String str_qty;

        Cart_Item(String normal_cart_shopId, String productId, String productName, String shopName, double price, String qty, String productImg, String type, String AvailableProductQuantity) {
            this.str_normal_cart_shopId = normal_cart_shopId;
            this.str_product_Id = productId;
            this.str_productName = productName;
            this.str_shopName = shopName;
            this.str_price = price;
            this.str_qty = qty;
            this.str_productImg = productImg;
            this.strtype = type;
            this.str_AvailableProductQuantity = AvailableProductQuantity;
        }

        public String getStr_normal_cart_shopId() {
            return str_normal_cart_shopId;
        }

        public String getStr_product_Id() {
            return str_product_Id;
        }

        public String getStr_productName() {
            return str_productName;
        }

        public String getStr_shopName() {
            return str_shopName;
        }

        public double getStr_price() {
            return str_price;
        }

        public String getStr_qty() {
            return str_qty;
        }

        public String getStr_productImg() {
            return str_productImg;
        }

        public String getStrtype() {
            return strtype;
        }

        public String getStr_AvailableProductQuantity() {
            return str_AvailableProductQuantity;
        }
    }

    private void popupLimitbyCat(String mainCat , String final_price, Double chkPrice){

        String strPrice = String.format("%.2f", chkPrice);

        String vegmsg="\n\nFresh Fruits & Vegetables Products\nDelivery charges Rs.30/- below 300/- order";

        String grocery_msg="\n\nGroceries Products \nDelivery charges Rs.30/- below 500/- order";

        String mesg =  "\n\nGreat, you are just INR "+ strPrice +" away from Free Delivery! \n.";//Pls add more items

        //Double finalprice = Double.parseDouble(final_price);

        String MESSAGE="";
        if(mainCat.equals("Grocery")){

            MESSAGE = grocery_msg+mesg;

        }else {
            MESSAGE=vegmsg+mesg;
        }


        String Message = "" +
                MESSAGE+
                "\nExpected Delivery time within 3 hours.";
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PawAddToCartActivity.this);
        builder1.setMessage(Message);
        builder1.setCancelable(true);
        builder1.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                        /*fragment = new DataSegregationLevelThree();
                        Bundle bundle = new Bundle();
                        bundle.putString("product_cat", "Fruits and Veggies");
                        bundle.putString("product_mainCat", "Grocery");
                        fragment.setArguments(bundle);
                        fragment_replace();*/
                try {
                    String localFinalPrice = tvPrice.getText().toString();
                    Intent intent = new Intent(PawAddToCartActivity.this, Order_Details_StepperActivity.class);
                    intent.putExtra("finalPrice", localFinalPrice);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        builder1.setNegativeButton("Add More Items", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                      /*  fragment = new DataSegregationLevelThree();
                        Bundle bundle = new Bundle();
                        bundle.putString("product_cat", "Fruits and Veggies");
                        bundle.putString("product_mainCat", "Grocery");
                        fragment.setArguments(bundle);
                        fragment_replace();*/
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
