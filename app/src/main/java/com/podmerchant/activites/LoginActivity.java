package com.podmerchant.activites;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.model.CityModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

public class LoginActivity extends AppCompatActivity {

    EditText et_shopcontact;
    Button btn_login_submit;
    String MobilePattern = "[0-9]{10}";
    String mRequestBody="",email="",shop_id="",shop_latitude="",shop_longitude="",shop_distanceKm="",
            deliveryboyname="",seller_name="",admin_type,shop_status,notification_id="",deleveryboyID="0",managerID="0",managername="",shopName,shop_category,shopAdminType="shopAdminType";
    ProgressDialog progressDialog;
    Context mContext;
    TextView tv_registration,tv_recharg_login;
    boolean flag = false;
    Spinner sp_login_usertype,sp_city;
    String LoginType = "";
    private ArrayList<String> cityList;
    private ArrayList<CityModel> cityModelArrayList;
    SharedPreferencesUtils sharedPreferencesUtils;
    private static final String ENGLISH_LOCALE = "en_US";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = LoginActivity.this;
        progressDialog = new ProgressDialog(this);

        et_shopcontact = findViewById(R.id.et_shopcontact);
        btn_login_submit = findViewById(R.id.btn_login_submit);
        tv_registration = findViewById(R.id.tv_registration);
        sp_login_usertype = findViewById(R.id.sp_login_usertype);
        tv_recharg_login = findViewById(R.id.tv_recharg_login);
        sp_city = findViewById(R.id.sp_city);

        cityList = new ArrayList<>();
        cityModelArrayList = new ArrayList<>();

        ArrayAdapter<CharSequence> genderAdaptor = ArrayAdapter.createFromResource(this, R.array.admin_Type_login, android.R.layout.simple_spinner_item);
        genderAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_login_usertype.setAdapter(genderAdaptor);

        sp_login_usertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                LoginType = sp_login_usertype.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_shopcontact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 10) {
                    if(LoginType.equalsIgnoreCase("Select Login Type")||LoginType.equalsIgnoreCase("")){
                        Toast.makeText(mContext,"Select Login Type",Toast.LENGTH_LONG).show();
                    }else {
                        if(LoginType.equalsIgnoreCase("Delivery boy(डिलीवरी बॉय)")) {
                            verifyDeliveryBoyMobile();//verifyManagerMobile
                        }else if(LoginType.equals("Manager(दुकान प्रबंधक)")){
                            verifyManagerMobile();
                        }else {
                            verifyMobile();
                        }
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btn_login_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_shopcontact.getText().toString().matches(MobilePattern)){
                    if (flag) {
                        //shop_status
                        checkStatus();
                        //sendOtp();
                    }else {
                        getError("\n\t Something went wron \n \t Please check Internet Connection.\n\t or Mobile Number is valid.");
                    }
                }else {
                    Toast.makeText(LoginActivity.this,"Enter Valid Mobile Number.",Toast.LENGTH_LONG).show();
                }
            }
        });

        tv_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent registerIntent = new Intent(mContext, RegistrationActivity.class);
                startActivity(registerIntent);
                //finish();
            }
        });

        tv_recharg_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent rechargeIntent = new Intent(mContext, RechargeActivity.class);
                rechargeIntent.putExtra("rechargeFrom","login");
                startActivity(rechargeIntent);*/
            }
        });

        mContext = LoginActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        Resources resources = getResources();
        Locale local = new Locale(ENGLISH_LOCALE);
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
        //recreate();
        sharedPreferencesUtils.setLocal(ENGLISH_LOCALE);
        getCityList();
    }

    //get Mobile verify
    public void verifyMobile(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.verifyMobile+"?contact="+et_shopcontact.getText().toString().trim();

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

           /* HurlStack hurlStack = new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(URL url) throws IOException {
                    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                    try {
                        httpsURLConnection.setSSLSocketFactory(getSSLSocketFactory());
                        //httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return httpsURLConnection;
                }
            };
*/
             StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                                Log.d("verifyMobile",response);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){
                                    flag=true;
                                    hideSoftKeyBoard();
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    //String vcode = jsonObjectData.getString("vcode");

                                        email = jsonObjectData.getString("email");
                                        shop_id = jsonObjectData.getString("shop_id");
                                        shop_latitude = jsonObjectData.getString("shop_latitude");
                                        shop_longitude = jsonObjectData.getString("shop_longitude");
                                        shop_distanceKm = jsonObjectData.getString("preferred_delivery_area_km");
                                        seller_name = jsonObjectData.getString("sellername");
                                        admin_type =  jsonObjectData.getString("admin_type");
                                        shop_status =  jsonObjectData.getString("status");
                                        //String temps= jsonObjectData.getString("shop_name");
                                        String area= jsonObjectData.getString("area");
                                        shopName =jsonObjectData.getString("shop_name");
                                        shop_category =jsonObjectData.getString("shop_category");
                                       String admin_id =jsonObjectData.getString("admin_id");
                                        sharedPreferencesUtils.setAdminID(admin_id);
                                        sharedPreferencesUtils.setRShopCatType(shop_category);
                                        sharedPreferencesUtils.setShopArea(area);

                                        //shopName = temp;

                                    Log.e("jsonObjectData",""+jsonObjectData);
                                }else if(!statuss) {
                                    flag=false;
                                    String status = jsonObject.getString("data");
                                    //Toast.makeText(mContext,"Mobile Number is not Register..!!",Toast.LENGTH_LONG).show();
                                    getError(status);
                                    //getError("\n\t Mobile Number is Not Register \n \t with PICoDEL Shopkeeper..!!\n\t Please Register.");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });

            stringRequestOtp.setRetryPolicy(new DefaultRetryPolicy(3000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //get Delevery Boy Mobile Verify
    public void verifyDeliveryBoyMobile(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.verifydeliveryBoyMobile
                    +"?mobile_number="+et_shopcontact.getText().toString().trim();

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){
                                    flag=true;
                                    hideSoftKeyBoard();
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    deleveryboyID = jsonObjectData.getString("ID");
                                    seller_name = jsonObjectData.getString("name");
                                    shop_id = jsonObjectData.getString("shop_id");
                                    admin_type =  jsonObjectData.getString("user_type");
                                    notification_id =  jsonObjectData.getString("notification_id");
                                    //shop_status =  jsonObjectData.getString("status");
                                    Log.e("jsonObjectData",""+jsonObjectData);
                                    getLatLong(shop_id);
                                }else if(!statuss) {
                                    flag=false;
                                    //Toast.makeText(mContext,"Mobile Number is not Register..!!",Toast.LENGTH_LONG).show();
                                    getError("\n\t Mobile Number is Not Registered \n \t with PICODEL Merchant.!\n\t Please Register.");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //verify managers Mobile
    public void verifyManagerMobile(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.verifymanagerMobile
                    +"?contact="+et_shopcontact.getText().toString().trim();

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){
                                    flag=true;
                                    hideSoftKeyBoard();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    for (int i=0; i<jsonArray.length();i++) {
                                        managerID = jsonArray.getJSONObject(i).getString("shopadmin_id");
                                        managername = jsonArray.getJSONObject(i).getString("sellername");
                                        shop_id = jsonArray.getJSONObject(i).getString("shop_id");
                                        admin_type = jsonArray.getJSONObject(i).getString("admin_type");
                                        email = jsonArray.getJSONObject(i).getString("email");
                                        Log.e("manager_email",""+email);
                                        //notification_id =  jsonObjectData.getString("notification_id");
                                        //shop_status =  jsonObjectData.getString("status");
                                    }
                                    Log.e("jsonArrayData",""+jsonArray);
                                    getLatLong(shop_id);
                                }else if(!statuss) {
                                    flag=false;
                                    //Toast.makeText(mContext,"Mobile Number is not Register..!!",Toast.LENGTH_LONG).show();
                                    getError("\n\t Mobile Number is Not Registered \n \t with PICODEL Merchant.\n\t Please Register.");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //get lat long by shop id
    public void getLatLong(String shopid){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.getlatlong +"?shop_id="+shopid;

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    for (int i=0; i<jsonArray.length();i++) {
                                        shop_latitude = jsonArray.getJSONObject(i).getString("shop_latitude");
                                        shop_longitude = jsonArray.getJSONObject(i).getString("shop_longitude");
                                        shopName = jsonArray.getJSONObject(i).getString("shop_name");
                                        email = jsonArray.getJSONObject(i).getString("email");
                                        shopAdminType = jsonArray.getJSONObject(i).getString("admin_type");
                                        //shop_distanceKm ="10";
                                        //notification_id =  jsonObjectData.getString("notification_id");
                                        //shop_status =  jsonObjectData.getString("status");
                                    }
                                    Log.e("jsonArrayDataLatLong",""+jsonArray);
                                }else if(!statuss) {
                                    //Toast.makeText(mContext,"Mobile Number is not Register..!!",Toast.LENGTH_LONG).show();
                                    //getError("\n\t LatLong Not Found");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //send otp
    public void sendOtp(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.sendotp
                    +"?mobileno="+et_shopcontact.getText().toString().trim()
                    +"&email="+email.trim();

            Log.d("urlOtpsend",urlOtpsend);

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("sendOtp",response);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss)
                                {
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");
                                    Log.e("VCODE:",""+vcode+admin_type);

                                    Intent intent = new Intent(mContext,OtpVerifyActivity.class);
                                    intent.putExtra("vcode",vcode);
                                    intent.putExtra("email",email);
                                    intent.putExtra("contact",contact);
                                    intent.putExtra("vdate",vdate);
                                    intent.putExtra("shop_id",shop_id);
                                    intent.putExtra("shop_latitude",shop_latitude);
                                    intent.putExtra("shop_longitude",shop_longitude);
                                    intent.putExtra("shop_distanceKm",shop_distanceKm);
                                    intent.putExtra("seller_name",seller_name);
                                    intent.putExtra("admin_type",admin_type);
                                    intent.putExtra("shop_status",shop_status);
                                    intent.putExtra("deliveryboyID",deleveryboyID);
                                    intent.putExtra("managerID",managerID);
                                    intent.putExtra("shopName",shopName);
                                    intent.putExtra("shopAdminType",shopAdminType);
                                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                   //finish();
                                }else {
                                    Toast.makeText(mContext,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void getError (String Error){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        alertDialogBuilder.setMessage(Error);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void hideSoftKeyBoard() {
        try {
            // hides the soft keyboard when the drawer opens
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //check deactivate flag
    public void checkStatus(){
        if(Connectivity.isConnected(mContext)){

            String urlCheckStatus = StaticUrl.checkstatus +"?shopid="+shop_id;
            Log.d("urlCheckStatus",urlCheckStatus);

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlCheckStatus,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // progressDialog.dismiss();
                            Log.d("urlCheckStatusR",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("statuss");
                                if(status.equals("true")){
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    //String vcode = jsonObjectData.getString("vcode");
                                   /* Intent loginInent = new Intent(MainActivity.this, HomeActivity.class);
                                    startActivity(loginInent);
                                    finish();*/
                                    sendOtp();
                                    //price_restrict flag set
                                    String value_city = sp_city.getSelectedItem().toString();
                                    for(int i =0; i<cityModelArrayList.size();i++){
                                        if(cityModelArrayList.get(i).getCity().contains(value_city)){
                                            sharedPreferencesUtils.setPriceRestrict(cityModelArrayList.get(i).getPrice_restrict());
                                        }
                                    }
                                    sharedPreferencesUtils.setRCity(sp_city.getSelectedItem().toString());
                                }else if(status.equals("false")){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jsonObjectStatus = jsonArray.getJSONObject(0);
                                    String statusObj = jsonObjectStatus.getString("status");
                                    String message = jsonObject.getString("mesg");
                                    String whatsAppNo = jsonObject.getString("whatsappno");
                                    deactivateDialog(message,whatsAppNo);
                                    String value_city = sp_city.getSelectedItem().toString();
                                    for(int i =0; i<cityModelArrayList.size();i++){
                                        if(cityModelArrayList.get(i).getCity().contains(value_city)){
                                            sharedPreferencesUtils.setPriceRestrict(cityModelArrayList.get(i).getPrice_restrict());
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void deactivateDialog(String message, final String whatsAppNo) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_deactivateshop, null);
        TextView tv_deactivateshop = view.findViewById(R.id.tv_deactivateshop);
        Button btn_whatsapp = view.findViewById(R.id.btn_whatsapp);
        Button btn_close = view.findViewById(R.id.btn_close);
        tv_deactivateshop.setText(message);
        btn_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openWhatsApp(whatsAppNo);
                //send to whatsApp
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    private void getCityList() { //TODO Server method here
        if (Connectivity.isConnected(LoginActivity.this)) {
            cityList.clear();
            cityModelArrayList.clear();
            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlactiveCityForshop, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {

                    Log.e("getCityRes:",""+response);

                    try {



                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            CityModel cityModel = new CityModel();
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            cityList.add(jsonCityData.getString("city"));
                            cityModel.setCity(jsonCityData.getString("city"));
                            cityModel.setPrice_restrict(jsonCityData.getString("price_restrict"));
                            cityModelArrayList.add(cityModel);
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter(LoginActivity.this,android.R.layout.simple_spinner_item,cityList);
                        sp_city.setAdapter(adapter);

                        /*if (value_shop_city != null) {
                            int spinnerPosition = adapter.getPosition(value_shop_city);
                            sp_shop_city.setSelection(spinnerPosition);
                        }*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();

                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(request);
        }
    }

    private void openWhatsApp(String mobileNo) {

      /*   String smsNumber = " ";//"7****"; // E164 format without '+' sign
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "PICODEL Admin.");
        sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
        sendIntent.setPackage("com.whatsapp");
        if (sendIntent.resolveActivity(getPackageManager()) == null) {
           // Toast.makeText(this, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(sendIntent);

       Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
        sendIntent.putExtra("Hi", PhoneNumberUtils.stripSeparators(mobileNo)+"@s.whatsapp.net");
        startActivity(sendIntent);*/

        try {
            String text = "Hi PICODEL Admin";// Replace with your message.

            String toNumber = mobileNo; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+toNumber +"&text="+text));
            startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    /*
    * PM 01:03:19: Executing task 'signingReport'...

Executing tasks: [signingReport]


> Configure project :app
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)

> Task :app:signingReport
Variant: debug
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------
Variant: debugUnitTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------
Variant: release
Config: none
----------
Variant: debugAndroidTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------
Variant: releaseUnitTest
Config: none
----------

BUILD SUCCESSFUL in 0s
1 actionable task: 1 executed
PM 01:03:20: Task execution finished 'signingReport'.

    * */

}
