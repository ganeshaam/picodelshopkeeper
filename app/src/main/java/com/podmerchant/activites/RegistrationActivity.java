package com.podmerchant.activites;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleyMultipartRequest;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import static android.os.Build.*;
import static android.os.Build.VERSION.*;

public class RegistrationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener {

    EditText  et_ownername, et_shopcontact, reg_shop_name, et_shopemail,et_refercode, et_shopcity, et_shoparea, et_gst_registraion, et_address1, et_shopaddress2,
            et_shopaddress3, et_shopaddress4, et_shopaddress5, et_shopaddress6,et_brand_name;
    Context mContext;
    Spinner sp_gendertype, sp_idproof, sp_pamentmode, sp_knowledgetech, sp_payschedule, sp_category, sp_shopadmintype;
    SeekBar seakbar_area_km;
    ImageView iv_refresh_latlong, shop_photo1, shop_photo2, img_licence;
    Button  shop_register_btn, uploadphoto, btn_uploadphoto2, btn_previous, btn_previous_first, btn_next_third;
    LinearLayout profile_layout, shop_layout, layout_shopaddress, layout_manufacture_form,ll_brand;
    ProgressDialog progressDialog;
    Bitmap shopbitmap = null, shopLicenceBitmap = null, idproofBitmap = null;
    TextView txt_stepper_sec, txt_stepper_third, tv_sameAsAbove, tv_destancekm, tv_latlong, tv_termscondi;
    View view_stepper_sec;
    public static final int PICK_IMAGE = 1, PICK_IMAGE_SHOPACT = 9, PICK_IMAGE_DOCUMENT = 4, SELECT_PICTURE = 2, PERMISSION_ACCESS_COARSE_LOCATION = 1;
    String mRequestBody = "", firebaseTokan, latitude, longitude, strCity = "City", shop_id, shop_latitude, shop_longitude, shop_distanceKm, seller_name, admin_type, shop_status, email,
            mnf_turnover = "0", mnf_noproduct = "0", mnf_lowcost = "0", mnf_highcost = "0", mnf_islicenses = "0", mnf_isquility_img = "0";
    private static final String TAG = "RegistrationActivity";
    int seakbar_km = 1;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//"2019-06-3 02:09:47"
    public static final String EMAIL_VERIFICATION = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";
    String MobilePattern = "[0-9]{10}";
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private LocationManager locationManager;
    String valid_mobile = "0", valid_email = "0";
    SharedPreferencesUtils sharedPreferencesUtils;
    private static final String HINDI_LOCALE = "hi";
    private static final String ENGLISH_LOCALE = "en_US";
    Locale local;
    public boolean manufacture_flag = false;
    ArrayList<String> arrayList_shopCategory = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = RegistrationActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
       // sharedPreferencesUtils.setLocal(ENGLISH_LOCALE);

        init();
        //handlePermission();

        shop_layout.setVisibility(View.GONE);
        layout_shopaddress.setVisibility(View.GONE);

      /*  if (firstStageValidation()) {}

        //send previous first layout
        btn_previous_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile_layout.setVisibility(View.VISIBLE);
                shop_layout.setVisibility(View.GONE);
                layout_shopaddress.setVisibility(View.GONE);
                txt_stepper_sec.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_light_new)));
            }
        });
        //send next third layout
        btn_next_third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (secondStageValidatoin()) {
                    profile_layout.setVisibility(View.GONE);
                    shop_layout.setVisibility(View.GONE);
                    layout_shopaddress.setVisibility(View.VISIBLE);
                    view_stepper_sec.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                    txt_stepper_third.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
            }
        });
        //send prev sec layout
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile_layout.setVisibility(View.GONE);
                shop_layout.setVisibility(View.VISIBLE);
                layout_shopaddress.setVisibility(View.GONE);
                view_stepper_sec.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_light_new)));
                txt_stepper_third.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_light_new)));

            }
        });
      */  //submit and send to home screen
        shop_register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(mContext)) {
                    if (firstStageValidation()) {
                        checkSecMobileNo(et_shopcontact.getText().toString());

                    }
                } else {
                    Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        uploadphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_images();
            }
        });
        btn_uploadphoto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_ShopAct();
            }
        });

        seakbar_area_km.setMax(10);
        seakbar_area_km.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seakbar_km = progress;
                tv_destancekm.setText(seakbar_km + " Km");
                Log.d(TAG, "SeakbarKm" + seakbar_km);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Get token
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        //Get new Instance ID token
                        firebaseTokan = task.getResult().getToken();
                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("firebaseTokan", firebaseTokan);
                        //Toast.makeText(RegistrationActivity.this, firebaseTokan, Toast.LENGTH_SHORT).show();
                    }
                });
        // [END retrieve_current_token]

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        checkLocation(); //check whether location service is enable or not in your  phone

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegistrationActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_COARSE_LOCATION);
        }
        //refresh current location for take lat long
        iv_refresh_latlong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLocation == null) {
                    startLocationUpdates();
                    tv_latlong.setText(latitude + "    " + longitude);
                }
            }
        });

        //Check contact number is exists
        et_shopcontact.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (et_shopcontact.getText().toString().length() == 10) {
                    if (!et_shopcontact.getText().toString().matches(MobilePattern)) {
                        //et_shopcontact.requestFocus();
                        et_shopcontact.setError("Please enter valid contact");
                    } else {
                        checkMobileNo(et_shopcontact.getText().toString());
                    }
                    // Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
                    // or
                    //textView.setText("valid email");
                } else {
                    // Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                    //or
                    //textView.setText("invalid email");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // other stuffs
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // other stuffs
            }
        });

      /*  //Check emaild is exists
        et_shopemail.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                    if(!et_shopemail.getText().toString().matches(EMAIL_VERIFICATION)) {
                        //et_shopemail.requestFocus();
                        //et_shopemail.setError("Please enter valid contact");
                    }else {
                        checkEmailid(et_shopemail.getText().toString());
                    }
                    // Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
                    // or
                    //textView.setText("valid email");
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // other stuffs
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // other stuffs
            }
        });*/

        getShopCategory();

        //Binding the dynamic shop Category Adaptor
        ArrayAdapter<String> adapter = new  ArrayAdapter(this,android.R.layout.simple_spinner_item,arrayList_shopCategory);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_category.setAdapter(adapter);

        sp_shopadmintype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (sp_shopadmintype.getSelectedItem().toString().equalsIgnoreCase("Manufacturer(उत्पादक/ वितरक)")) {
                    ll_brand.setVisibility(View.VISIBLE);
                    //manufacture_flag = true;
                } else {
                    ll_brand.setVisibility(View.GONE);
                    //manufacture_flag = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ll_brand.setVisibility(View.GONE);
            }
        });


    }

    private void init() {
        progressDialog = new ProgressDialog(this);
        profile_layout = findViewById(R.id.layout_profile);
        shop_layout = findViewById(R.id.layout_shopdetails);
        layout_shopaddress = findViewById(R.id.layout_shopaddress);
        shop_register_btn = findViewById(R.id.btn_submit2);
        uploadphoto = findViewById(R.id.btn_uploadphoto);
        shop_photo1 = findViewById(R.id.img_selected);
        shop_photo2 = findViewById(R.id.img_selected1);
        btn_previous = findViewById(R.id.btn_previous);
        et_ownername = findViewById(R.id.et_ownername);
        et_shopcontact = findViewById(R.id.et_shopcontact);
        reg_shop_name = findViewById(R.id.reg_shop_name);
        et_shopemail = findViewById(R.id.et_shopemail);
        et_refercode = findViewById(R.id.et_refercode);
        et_shopcity = findViewById(R.id.et_shopcity);
        et_gst_registraion = findViewById(R.id.et_gst_registraion);
        sp_shopadmintype = findViewById(R.id.sp_shopadmintype);
        ll_brand = findViewById(R.id.ll_brand);
        et_brand_name =findViewById(R.id.et_brand_name);
        sp_idproof = findViewById(R.id.sp_idproof);
        //sp_pamentmode = findViewById(R.id.sp_pamentmode);
        seakbar_area_km = findViewById(R.id.bar_area_km);

       // et_no_delivery_boy = findViewById(R.id.et_no_delivery_boy);
        sp_knowledgetech = findViewById(R.id.sp_knowledgetech);
        sp_payschedule = findViewById(R.id.sp_payschedule);
        sp_category = findViewById(R.id.sp_category);
        sp_gendertype = findViewById(R.id.sp_gendertype);
        tv_sameAsAbove = findViewById(R.id.tv_sameAsAbove);

        img_licence = findViewById(R.id.img_licence);

        btn_uploadphoto2 = findViewById(R.id.btn_uploadphoto2);


        btn_previous_first = findViewById(R.id.btn_previous_first);
        btn_next_third = findViewById(R.id.btn_next_third);
        //stepper
        view_stepper_sec = findViewById(R.id.view_stepper_sec);
        txt_stepper_sec = findViewById(R.id.txt_stepper_sec);
        txt_stepper_third = findViewById(R.id.txt_stepper_third);
        tv_destancekm = findViewById(R.id.tv_destancekm);
        //address
        et_address1 = findViewById(R.id.et_address1);
        et_shopaddress2 = findViewById(R.id.et_shopaddress2);
        et_shopaddress3 = findViewById(R.id.et_shopaddress3);
        et_shopaddress4 = findViewById(R.id.et_shopaddress4);
        et_shopaddress5 = findViewById(R.id.et_shopaddress5);
        et_shopaddress6 = findViewById(R.id.et_shopaddress6);
        //refresh lat long current location
        iv_refresh_latlong = findViewById(R.id.iv_refresh_latlong);
        tv_latlong = findViewById(R.id.tv_latlong);

        tv_termscondi = findViewById(R.id.tv_termscondi);

        //Manufacture layout
        layout_manufacture_form = findViewById(R.id.layout_manufacture_form);
    }

    private void select_images() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
    }

    private void select_ShopAct() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_SHOPACT);
    }

    private void select_shopDocument() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_DOCUMENT);
    }


    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if(data!=null) {
        if (requestCode == PICK_IMAGE) {
            //TODO: action
            // Get the url from data
            Uri selectedImageUri = data.getData();
                *//*if (null != selectedImageUri) {
                    // Get the path from the Uri
                    String path = getPathFromURI(selectedImageUri);
                    Log.i(TAG, "Image Path : " + path);
                    // Set the image in ImageView
                    shop_photo1.setImageURI(selectedImageUri);
                }*//*
            //for bit map data
            try {
                   *//* Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                     ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                    shop_photo1.setImageBitmap(thumbnail);
                    shopbitmap=thumbnail;*//*
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                shop_photo1.setImageBitmap(bitmap);
                shopbitmap = bitmap;
                Log.e("shopbitmap", "" + shopbitmap);
            } catch (Exception e) {
                e.getMessage();
                Log.e("BitmapExp", "" + e.getMessage());
            }
        }
        if (requestCode == PICK_IMAGE_SHOPACT) {
            //TODO: action
            // Get the url from data
            Uri selectedImageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                img_licence.setImageBitmap(bitmap);
                shopLicenceBitmap = bitmap;
                Log.e("shopbitmap", "" + shopbitmap);
            } catch (Exception e) {
                e.getMessage();
                Log.e("BitmapExp", "" + e.getMessage());
            }
        }
        if (requestCode == PICK_IMAGE_SHOPACT) {
            //TODO: action
            // Get the url from data
            Uri selectedImageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                img_licence.setImageBitmap(bitmap);
                shopLicenceBitmap = bitmap;
                Log.e("shopbitmap", "" + shopbitmap);
            } catch (Exception e) {
                e.getMessage();
                Log.e("BitmapExp", "" + e.getMessage());
            }

        }
        // }
    }
*/
    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    private void handlePermission() {

        if (SDK_INT < VERSION_CODES.M) {
            return;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, SELECT_PICTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case SELECT_PICTURE:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                        if (showRationale) {
                            //  Show your own message here
                        } else {
                            showSettingsAlert();
                        }
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        openAppSettings(RegistrationActivity.this);
                    }
                });
        alertDialog.show();
    }

    public static void openAppSettings(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

  /*  @Override
    protected void onResume()
    {
        super.onResume();
        if (sharedPreferencesUtils.getRPageno().equals("1"))
        {
            ArrayAdapter<CharSequence> genderAdaptor = ArrayAdapter.createFromResource(this, R.array.Gender_Type, android.R.layout.simple_spinner_item);
            genderAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_gendertype.setAdapter(genderAdaptor);
            if (sharedPreferencesUtils.getROwnerGender() != null) {
                int spinnerPosition0 = genderAdaptor.getPosition(sharedPreferencesUtils.getROwnerGender());
                sp_gendertype.setSelection(spinnerPosition0);
            }
            et_ownername.setText(sharedPreferencesUtils.getROwnerName());
            reg_shop_name.setText(sharedPreferencesUtils.getRShopName());
            et_shopcontact.setText(sharedPreferencesUtils.getRShopContact());
            et_shopemail.setText(sharedPreferencesUtils.getRShopEmail());
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.ShopCategory_Type, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_category.setAdapter(adapter);
            if (sharedPreferencesUtils.getRShopCatType() != null) {
                int spinnerPosition = adapter.getPosition(sharedPreferencesUtils.getRShopCatType());
                sp_category.setSelection(spinnerPosition);
            }
            ArrayAdapter<CharSequence> shopAdminAdoptor = ArrayAdapter.createFromResource(this, R.array.reg_admin_Type, android.R.layout.simple_spinner_item);
            shopAdminAdoptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_shopadmintype.setAdapter(shopAdminAdoptor);
            if (sharedPreferencesUtils.getRShoppAdmintype() != null) {
                int spinnerPosition3 = shopAdminAdoptor.getPosition(sharedPreferencesUtils.getRShoppAdmintype());
                sp_shopadmintype.setSelection(spinnerPosition3);
            }

            if (!sharedPreferencesUtils.getRIdProofBitmap().equalsIgnoreCase("")) {
                byte[] b = Base64.decode(sharedPreferencesUtils.getRIdProofBitmap(), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                idproofBitmap = bitmap;
            }

        } else
        if (sharedPreferencesUtils.getRPageno().equals("2"))
         {
            ArrayAdapter<CharSequence> deliveryBoyAdaptor = ArrayAdapter.createFromResource(this, R.array.DeliveryBoy_Type, android.R.layout.simple_spinner_item);
            deliveryBoyAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            et_no_delivery_boy.setText(sharedPreferencesUtils.getNoDeliveryboy());
            ArrayAdapter<CharSequence> knowledgeAdaptor = ArrayAdapter.createFromResource(this, R.array.Knowledge_Type, android.R.layout.simple_spinner_item);
            knowledgeAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_knowledgetech.setAdapter(knowledgeAdaptor);
            if (sharedPreferencesUtils.getRKnowledgeTech() != null) {
                int spinnerPosition5 = knowledgeAdaptor.getPosition(sharedPreferencesUtils.getRKnowledgeTech());
                sp_knowledgetech.setSelection(spinnerPosition5);
            }
            ArrayAdapter<CharSequence> payscheduleAdaptor = ArrayAdapter.createFromResource(this, R.array.PaySchedule_Type, android.R.layout.simple_spinner_item);
            payscheduleAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_payschedule.setAdapter(payscheduleAdaptor);
            if (sharedPreferencesUtils.getRPaySchedule() != null) {
                int spinnerPosition6 = payscheduleAdaptor.getPosition(sharedPreferencesUtils.getRPaySchedule());
                sp_payschedule.setSelection(spinnerPosition6);
            }
            ArrayAdapter<CharSequence> idproofAdaptor = ArrayAdapter.createFromResource(this, R.array.ID_Proof_Type, android.R.layout.simple_spinner_item);
            idproofAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_idproof.setAdapter(idproofAdaptor);
            if (sharedPreferencesUtils.getRIdProof() != null) {
                int spinnerPosition8 = idproofAdaptor.getPosition(sharedPreferencesUtils.getRIdProof());
                sp_idproof.setSelection(spinnerPosition8);
            }
            if (!sharedPreferencesUtils.getRShopImg().equalsIgnoreCase("")) {
                byte[] b = Base64.decode(sharedPreferencesUtils.getRShopImg(), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                shopLicenceBitmap = bitmap;
            }
        } else if (sharedPreferencesUtils.getRPageno().equals("3")) {
            seakbar_area_km.setProgress(sharedPreferencesUtils.getRSeekBar());
            //tv_destancekm.setText(sharedPreferencesUtils.getRSeekBar());
            et_address1.setText(sharedPreferencesUtils.getRAddress1());
            et_shopaddress2.setText(sharedPreferencesUtils.getRAddress2());
            et_shopaddress3.setText(sharedPreferencesUtils.getRAddress3());
            et_shopaddress4.setText(sharedPreferencesUtils.getRAddress4());
            //et_shopaddress5.setText(sharedPreferencesUtils.getRAddress4());
            et_shopaddress6.setText(sharedPreferencesUtils.getRAddress6());
            et_shopcity.setText(sharedPreferencesUtils.getRCity());
        }
    }
*/
    public boolean firstStageValidation() {

       /* sharedPreferencesUtils.setRPageno("1");
        sharedPreferencesUtils.setROwnerGender(sp_gendertype.getSelectedItem().toString().trim());
        sharedPreferencesUtils.setROwnerName(et_ownername.getText().toString().trim());
        sharedPreferencesUtils.setRShopName(reg_shop_name.getText().toString().trim());
        sharedPreferencesUtils.setRShopContact(et_shopcontact.getText().toString().trim());
        sharedPreferencesUtils.setRShopEmail(et_shopemail.getText().toString().trim());
        sharedPreferencesUtils.setRShopCatType(sp_category.getSelectedItem().toString().trim());
        sharedPreferencesUtils.setRShopAdmintype(sp_shopadmintype.getSelectedItem().toString().trim());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        idproofBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        sharedPreferencesUtils.setRIdProofBitmap(encodedImage);
       */
        if (et_ownername.getText().toString().isEmpty()) {
            et_ownername.requestFocus();
            et_ownername.setError("Enter Owner Name");
            return false;
        } else if (reg_shop_name.getText().toString().isEmpty()) {
            reg_shop_name.requestFocus();
            reg_shop_name.setError("Enter Shop Name");
            return false;
        } else if (et_shopcontact.getText().toString().isEmpty()) {
            et_shopcontact.requestFocus();
            et_shopcontact.setError("Enter contact for to get Order details");
            return false;
        } else if (!et_shopcontact.getText().toString().matches(MobilePattern)) {
            et_shopcontact.requestFocus();
            et_shopcontact.setError("Enter valid contact");
            return false;
        } else if (et_shopemail.getText().toString().isEmpty()) {
            et_shopemail.requestFocus();
            et_shopemail.setError("Enter Email Address");
            return false;
        } else if (!et_shopemail.getText().toString().trim().matches(EMAIL_VERIFICATION)) {
            et_shopemail.requestFocus();
            et_shopemail.setError("Enter valid Email Address");
            return false;
        } else if (sp_category.getSelectedItem().toString().trim().equals("Select Shop Category")) {
            Toast.makeText(RegistrationActivity.this, "Select shop Category", Toast.LENGTH_SHORT).show();
            return false;
        } else  if (sp_shopadmintype.getSelectedItem().toString().trim().equals("Select User Type")) {
            Toast.makeText(RegistrationActivity.this, "Select User Type", Toast.LENGTH_SHORT).show();
            return false;
        }else  if (sp_shopadmintype.getSelectedItem().toString().trim().equals("Manufacturer(उत्पादक/ वितरक)")&& et_brand_name.getText().toString().isEmpty()) {
            Toast.makeText(RegistrationActivity.this, "Please Enter Brand Name", Toast.LENGTH_SHORT).show();
            return false;
        }else if (et_shopaddress6.getText().toString().isEmpty()) {
            et_shopaddress6.requestFocus();
            et_shopaddress6.setError("Enter Area");
            return true;
        }
        return true;
    }

    /*public boolean secondStageValidatoin() {

        sharedPreferencesUtils.setRPageno("2");
        sharedPreferencesUtils.setRNoDeliveryboy(et_no_delivery_boy.getText().toString().trim());
        sharedPreferencesUtils.setRKnowledgeTech(sp_knowledgetech.getSelectedItem().toString());
        sharedPreferencesUtils.setRPaySchedule(sp_payschedule.getSelectedItem().toString());
        sharedPreferencesUtils.setRIdProof(sp_idproof.getSelectedItem().toString());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        shopLicenceBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        sharedPreferencesUtils.setRIdProofImg(encodedImage);
        ByteArrayOutputStream baosShopPhoto = new ByteArrayOutputStream();
        shopbitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] bShopPhoto = baosShopPhoto.toByteArray();
        String encodedImageShopPhoto = Base64.encodeToString(bShopPhoto, Base64.DEFAULT);
        sharedPreferencesUtils.setRShopImg(encodedImageShopPhoto);


         if (sp_knowledgetech.getSelectedItem().toString().trim().equals("Tech Knowledge")) {
            Toast.makeText(RegistrationActivity.this, "Select Tech Knowledge type", Toast.LENGTH_SHORT).show();
            return false;
        } else if (sp_payschedule.getSelectedItem().toString().trim().equals("Pick One")) {
            Toast.makeText(RegistrationActivity.this, "Select pay schedule type", Toast.LENGTH_SHORT).show();
            return false;
        } else if (sp_idproof.getSelectedItem().toString().trim().equals("ID Proof")) {
            Toast.makeText(RegistrationActivity.this, "Select ID Proof type", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }*/

  /*  public boolean thirdStageValidation() {

      *//*  sharedPreferencesUtils.setRPageno("3");
        sharedPreferencesUtils.setRSeekBar(seakbar_km);
        sharedPreferencesUtils.setRAddress1(et_address1.getText().toString().trim());
        sharedPreferencesUtils.setRAddress2(et_shopaddress2.getText().toString().trim());
        sharedPreferencesUtils.setRAddress3(et_shopaddress3.getText().toString().trim());
        sharedPreferencesUtils.setRAddress4(et_shopaddress4.getText().toString().trim());
        //sharedPreferencesUtils.setRAddress5(et_shopaddress5.getText().toString().trim());
        sharedPreferencesUtils.setRAddress6(et_shopaddress6.getText().toString().trim());
        sharedPreferencesUtils.setRCity(et_shopcity.getText().toString().trim());
*//*

        if (et_address1.getText().toString().isEmpty()) {
            et_address1.requestFocus();
            et_address1.setError("Enter Shop Number");
        } else if (et_shopaddress2.getText().toString().isEmpty()) {
            et_shopaddress2.requestFocus();
            et_shopaddress2.setError("Enter Society/Bulding Name");
        } else if (et_shopaddress3.getText().toString().isEmpty()) {
            et_shopaddress3.requestFocus();
            et_shopaddress3.setError("Enter Landmark");
        } else if (et_shopaddress4.getText().toString().isEmpty()) {
            et_shopaddress4.requestFocus();
            et_shopaddress4.setError("Enter Road name");
        } else*//* if (et_shopaddress5.getText().toString().isEmpty()) {
            et_shopaddress5.requestFocus();
            et_shopaddress5.setError("Enter Colony name");
        } else *//*if (et_shopaddress6.getText().toString().isEmpty()) {
            et_shopaddress6.requestFocus();
            et_shopaddress6.setError("Enter Area");
        } else if (et_shopcity.getText().toString().isEmpty()) {
            et_shopcity.requestFocus();
            et_shopcity.setError("Enter City");
            return false;
        }
        return true;
    }
*/
    private boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() < 6;
    }

    //shop keeper registration
    public void uploadImageWithRegistration() {
        if (Connectivity.isConnected(mContext)) {
            final String currentDateandTime = sdf.format(new Date());

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String url = StaticUrl.shopregistration;
            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    try {

                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e(TAG, "resp_shop_reg" + resultResponse.toString());
                        Log.e(TAG, "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                           /* JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                            email = jsonObjectData.getString("email");
                            shop_id = jsonObjectData.getString("shop_id");
                            shop_latitude = jsonObjectData.getString("shop_latitude");
                            shop_longitude = jsonObjectData.getString("shop_longitude");
                            shop_distanceKm = jsonObjectData.getString("preferred_delivery_area_km");
                            seller_name = jsonObjectData.getString("sellername");
                            admin_type = jsonObjectData.getString("admin_type");
                            shop_status = jsonObjectData.getString("status");
*/
                            //sendOtp();
                          //shopAdminRegistration(shop_id);
                            Toast.makeText(mContext,"Your Registration is submitted. Pls Logiin to continue", Toast.LENGTH_SHORT).show();
                         Intent intent = new Intent(mContext,LoginActivity.class);
                         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                         startActivity(intent);
                        } else if (!status) {
                            Log.d(TAG, "status_false" + resultResponse);
                            Toast.makeText(mContext,"Re -Submit", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleYIMAGE_EXP:", "" + e.getMessage());
                        progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;

                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";

                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<>();

                    params.put("gender_type", sp_gendertype.getSelectedItem().toString().trim());
                    params.put("sellername", et_ownername.getText().toString());
                    params.put("shop_name", reg_shop_name.getText().toString());

                    // params.put("owner_contact", "");
                    params.put("contact", et_shopcontact.getText().toString());
                    params.put("email", et_shopemail.getText().toString());
                    params.put("shop_category", sp_category.getSelectedItem().toString().trim());
                    String refercode = et_refercode.getText().toString();
                    if(refercode.isEmpty() || refercode == null){
                        params.put("refercode", "NA");
                    }else {
                        params.put("refercode", et_refercode.getText().toString());
                    }
                    //params.put("shop_type", "");
                    String admin_type_str = "Seller";
                    if(sp_shopadmintype.getSelectedItem().toString().equals("Shop Owner(दुकान मालिक)")){
                        admin_type_str="Seller";
                    }else if(sp_shopadmintype.getSelectedItem().toString().equals("Manufacturer(उत्पादक/ वितरक)")){
                        admin_type_str="Manufacturer";
                    }else if(sp_shopadmintype.getSelectedItem().toString().equals("Freelancer(FL)(मोबाइल मर्चेंट)")){
                        admin_type_str="Freelancer(FL)";
                    }else if(sp_shopadmintype.getSelectedItem().toString().equals("Distributor(वितरक)")){
                        admin_type_str="Distributor";
                    }//वितरक
                    else if(sp_shopadmintype.getSelectedItem().toString().equals("C&amp;F Distributor(वितरक)")){
                        admin_type_str="Manufacturer";
                    }
                    else if(sp_shopadmintype.getSelectedItem().toString().equals("PAW")){
                        admin_type_str="PAW";
                    }
                    /*else{
                        admin_type_str="Manufacturer";
                    }*/
                    //C&amp;F Distributor(वितरक)
                    params.put("admin_type",admin_type_str );

                    if(admin_type_str.equalsIgnoreCase("Manufacturer")){
                        params.put("brand_name",et_brand_name.getText().toString());
                    }else if(admin_type_str.equalsIgnoreCase("Seller")){
                        params.put("brand_name","NA");
                    }else if(admin_type_str.equalsIgnoreCase("Distributor")){
                        params.put("brand_name","NA");
                    }else if(admin_type_str.equalsIgnoreCase("Freelancer(FL)")){
                        params.put("brand_name","NA");
                    }


                    params.put("area", et_shopaddress6.getText().toString());
                    //params.put("city", et_shopcity.getText().toString());
                    params.put("shop_latitude", latitude);
                    params.put("shop_longitude", longitude);
                    params.put("firebase_token", firebaseTokan);
                    //params.put("identity_proof", sp_idproof.getSelectedItem().toString().trim());
                    params.put("reg_completed","0");

                    /*    *//*Manufacture parameters*//*
                    if (manufacture_flag) {
                        params.put("mnf_turnover", "");
                        params.put("mnf_noproduct", "");
                        params.put("mnf_lowcost", "");
                        params.put("mnf_highcost", "");
                            params.put("mnf_islicenses", "1");
                            params.put("mnf_isquility_img", "0");
                    } else {

                        params.put("mnf_turnover", mnf_turnover);
                        params.put("mnf_noproduct", mnf_noproduct);
                        params.put("mnf_lowcost", mnf_noproduct);
                        params.put("mnf_highcost", mnf_noproduct);
                        params.put("mnf_islicenses", mnf_noproduct);
                        params.put("mnf_isquility_img", mnf_noproduct);
                    }
                 */   Log.e("Params", "" + params);
                    return params;
                }

           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = "#concord$" + ":" + "!@#con co@$rd!@#";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }*/

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                /*    params.put("shop_logo", new DataPart(getBytesFromBitmap(shopbitmap)));
                    params.put("identity_proof_img", new DataPart(getBytesFromBitmap(shopLicenceBitmap)));
                    params.put("doc_type_idproof", new DataPart(getBytesFromBitmap(idproofBitmap)));
                    Log.e("ParamsImg", "" + params);
                */    return params;
                }
            };

            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        return stream.toByteArray();
    }

    //send otp
    public void sendOtp() {
        if (Connectivity.isConnected(mContext)) {

            String urlOtpsend = StaticUrl.sendotp
                    + "?mobileno=" + et_shopcontact.getText().toString().trim()
                    + "&email=" + et_shopemail.getText().toString().trim();

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status = true) {
                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");

                                    Intent intent = new Intent(mContext, OtpVerifyActivity.class);
                                    intent.putExtra("vcode", vcode);
                                    intent.putExtra("email", email);
                                    intent.putExtra("contact", contact);
                                    intent.putExtra("vdate", vdate);
                                    intent.putExtra("shop_id", shop_id);
                                    intent.putExtra("shop_latitude", shop_latitude);
                                    intent.putExtra("shop_longitude", shop_longitude);
                                    intent.putExtra("shop_distanceKm", shop_distanceKm);
                                    intent.putExtra("seller_name", seller_name);
                                    intent.putExtra("admin_type", admin_type);
                                    intent.putExtra("shop_status", shop_status);

                                    Log.e("VerifyOtp:", "" + vcode);

                                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });

            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void hideKeyBoard() {
        // Check if no view has focus:
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //get current Location
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            Log.d("Location", mLocation.getLatitude() + " " + mLocation.getLongitude());
            latitude = String.valueOf(mLocation.getLatitude());
            longitude = String.valueOf(mLocation.getLongitude());
            //mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
            tv_latlong.setText(latitude + "  " + longitude);
            //geocoder
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                addresses = geocoder.getFromLocation(mLocation.getLatitude(), mLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                // String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                strCity = addresses.get(0).getLocality();
                et_shopcity.setText(strCity);
                //String state = addresses.get(0).getAdminArea();
                //String country = addresses.get(0).getCountryName();
                //String postalCode = addresses.get(0).getPostalCode();
                //String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //  Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }
/*
    @Override
    public void onLocationChanged(Location location) {

    }*/

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
     /*   mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);*/
        mLocationRequest = LocationRequest.create();//.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onLocationChanged(Location location) {

        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.d("UpdatedLocation", msg);
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        //mLongitudeTextView.setText(String.valueOf(location.getLongitude() ));
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        //LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    //check mobile no exists
    public void checkMobileNo(String mobileno) {
        if (Connectivity.isConnected(mContext)) {

            String urlCheckMobileNo = StaticUrl.verifyMobile + "?contact=" + mobileno;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlCheckMobileNo,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    /*JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");/*/
                                    valid_mobile = "1";
                                    Toast.makeText(mContext, "Mobile Number Already Exists", Toast.LENGTH_SHORT).show();
                                } else if (!status) {
                                    valid_mobile = "0";
                                    // Toast.makeText(mContext,"Invalid obile Number",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });

            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void checkSecMobileNo(String mobileno) {
        if (Connectivity.isConnected(mContext)) {

            String urlCheckMobileNo = StaticUrl.verifyMobile + "?contact=" + mobileno;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlCheckMobileNo,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "CheckSecMobile" + response);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    /*JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");/*/
                                    valid_mobile = "1";

                                    showMobileExstingDialog();

                                    // Toast.makeText(mContext,"Mobile Number Already Exists",Toast.LENGTH_SHORT).show();
                                } else if (!status) {
                                    uploadImageWithRegistration();
                                   /* valid_mobile = "0";
                                    profile_layout.setVisibility(View.GONE);
                                    shop_layout.setVisibility(View.VISIBLE);
                                    layout_shopaddress.setVisibility(View.GONE);
                                    // view_stepper_sec.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                    txt_stepper_sec.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                                   */ //txt_stepper_third.setBackgroundColor(getResources().getColor(R.color.grey_light_new));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });

            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void showMobileExstingDialog() {
        final Dialog dialog = new Dialog(mContext); // Context, this, etc.
        dialog.setContentView(R.layout.moibleexist_dialog);
        //dialog.setTitle(R.string.dialog_title);
        Button btn_login = dialog.findViewById(R.id.btn_reg_login);
        Button btn_cancel = dialog.findViewById(R.id.btn_reg_cancel);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // Get getShopCategory Names
    public void getShopCategory(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.getshopcategory;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.e("ShopCategory_Type",""+response);

                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for(int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        String value = jsonObject1.getString("shop_category");
                                        arrayList_shopCategory.add(value);
                                    }

                                    ArrayAdapter<String> adapter = new  ArrayAdapter(RegistrationActivity.this,android.R.layout.simple_spinner_item,arrayList_shopCategory);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    sp_category.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();

                                }else if(!statuss) {

                                    Toast.makeText(mContext,"No Category Found..!!",Toast.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
            Log.e("GetCategory_Res:",""+stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //check emailid   exists
    public void checkEmailid(String emaild) {
        if (Connectivity.isConnected(mContext)) {

            String urlCheckMobileNo = "https://www.picodel.com/seller/shopapi/checkmono?email=" + emaild;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlCheckMobileNo,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("checkEmailid", response);
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status = true) {
                                    /*JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");/*/
                                    Toast.makeText(mContext, "Email id Already Exists", Toast.LENGTH_SHORT).show();
                                    valid_email = "1";
                                } else if (status = false) {
                                    valid_email = "0";
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });

            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.getItem(0);
        if (sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)) {
            menuItem.setTitle("Language");
        } else {
            menuItem.setTitle("Language");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //handle local language Hindi and English
        if (id == R.id.action_settings) {

            Resources resources = getResources();
            /*if (sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)) {
                local = new Locale(ENGLISH_LOCALE);
                sharedPreferencesUtils.setLocal(ENGLISH_LOCALE);
            } else {
                local = new Locale(HINDI_LOCALE);
                sharedPreferencesUtils.setLocal(HINDI_LOCALE);
            }*/
            if (sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)) {
                Locale local = new Locale(HINDI_LOCALE);
                Configuration configuration = resources.getConfiguration();
                configuration.setLocale(local);
                getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
                recreate();
                sharedPreferencesUtils.setLocal(ENGLISH_LOCALE);
            } else {
                Locale local = new Locale(ENGLISH_LOCALE);
                Configuration configuration = resources.getConfiguration();
                configuration.setLocale(local);
                getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
                recreate();
                sharedPreferencesUtils.setLocal(HINDI_LOCALE);
            }
                //return true;
            }
            return super.onOptionsItemSelected(item);
        }

    @Override
    public void onBackPressed () {
            //super.onBackPressed();
            new android.app.AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to close PICODEL Registration?")//you have any queries/question whats app
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
}


/*
* PM 07:28:21: Executing task 'signingReport'...

Executing tasks: [signingReport]


> Configure project :app
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)

> Task :app:signingReport
Variant: debugUnitTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------
Variant: debugAndroidTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------
Variant: release
Config: none
----------
Variant: releaseUnitTest
Config: none
----------
Variant: debug
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------

BUILD SUCCESSFUL in 0s
1 actionable task: 1 executed
PM 07:28:22: Task execution finished 'signingReport'.


* */