package com.podmerchant.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.PicodelPointsAdapter;
import com.podmerchant.model.PicodelPointsModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class PicodelPointsActivity extends AppCompatActivity {

    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    AlertDialog progressDialog;
    ArrayList<PicodelPointsModel> picodelPointsModelArrayList;
    RecyclerView rv_picodel_points;
    private RecyclerView.LayoutManager layoutManager;
    private PicodelPointsAdapter picodelPointsAdapter;
    TextView tv_network_con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picodel_points);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getText(R.string.nav_picodel_points));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PicodelPointsActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        rv_picodel_points = findViewById(R.id.rv_picodel_points);
        tv_network_con = findViewById(R.id.tv_network_con);

        rv_picodel_points.setHasFixedSize(true);
        picodelPointsModelArrayList = new ArrayList<>();
        layoutManager = new GridLayoutManager(mContext,2);
       // layoutManager = new LinearLayoutManager(mContext);
        rv_picodel_points.setLayoutManager(layoutManager);
        rv_picodel_points.setItemAnimator(new DefaultItemAnimator());
        picodelPointsAdapter = new PicodelPointsAdapter(picodelPointsModelArrayList,mContext);
        rv_picodel_points.setAdapter(picodelPointsAdapter);

        getReferByPicodelPoints();
    }

    //get picodel points from refer to new shop keeper
    public void getReferByPicodelPoints(){

        if(Connectivity.isConnected(mContext)){
            progressDialog.show();

            String targetUrl = StaticUrl.getpicodelpoints+"?shop_id="+sharedPreferencesUtils.getShopID();
            Log.d("referPoints",targetUrl);
            StringRequest requestPicodelPoints = new StringRequest(Request.Method.GET,
                    targetUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if(status.equals("true")){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for(int i=0; i < jsonArray.length(); i++){
                                        JSONObject  objectPoints = jsonArray.getJSONObject(i);
                                        PicodelPointsModel pointsModel = new PicodelPointsModel();
                                        pointsModel.setPoints(objectPoints.getString("points"));
                                        pointsModel.setReferto(objectPoints.getString("referto"));
                                        pointsModel.setStatus(objectPoints.getString("status"));
                                        pointsModel.setCreatedat(objectPoints.getString("createdat"));
                                        picodelPointsModelArrayList.add(pointsModel);
                                    }
                                    picodelPointsAdapter.notifyDataSetChanged();
                                }else if(status.equals("false")){
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(requestPicodelPoints);
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private PicodelPointsActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final PicodelPointsActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
             /*   Intent intent = new Intent(mContext, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);*/
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
