package com.podmerchant.activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.podmerchant.R;
import com.podmerchant.local_storage.DBHelper;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;


public class MyOrderDetails extends AppCompatActivity {

    private final String class_name = this.getClass().getSimpleName();
    private String order_id, result, strContact, strEmail, strMsg;
    private Button btnBookNow;
    private RelativeLayout relativeLayoutOrderNow;
    private CoordinatorLayout myOrderDetailsMainLayout;
    private RecyclerView recyclerView;
    private MyOrderDetailsAdapter myOrderDetailsAdapter;
    private LinearLayout Main_Layout_NoInternet;
    private RelativeLayout msgLayout;
    private RelativeLayout myordersMainLayout;
    private TextView tvMsg;
    private Network_Change_Receiver myReceiver;
    private TextView txtNoConnection;
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onPause() {
       // unregisterReceiver(myReceiver);
        super.onPause();
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                myordersMainLayout.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                myordersMainLayout.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
                getProductDetails();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_details);

        /*Toolbar toolbar = findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_myproduct_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = MyOrderDetails.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            order_id = bundle.getString("order_id");
            //tvTitle.setText("Order Id " + order_id + " Details");
        }

        GateWay gateWay = new GateWay(MyOrderDetails.this);
        /*strContact = gateWay.getContact();
        strEmail = gateWay.getUserEmail();*/

        strContact = sharedPreferencesUtils.getPhoneNumber();
        strEmail = sharedPreferencesUtils.getEmailId();

        msgLayout = findViewById(R.id.msgLayout);
        myordersMainLayout = findViewById(R.id.myordersMainLayout);
        myReceiver = new Network_Change_Receiver();

        txtNoConnection = findViewById(R.id.txtNoConnection);
        tvMsg = findViewById(R.id.txtMsg);
        myOrderDetailsMainLayout = findViewById(R.id.myOrderDetailsMainLayout);
        relativeLayoutOrderNow = findViewById(R.id.relativeLayoutOrderNow);
        btnBookNow = findViewById(R.id.btnBookNow);

        recyclerView = findViewById(R.id.my_orders_details_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
    }

    private void getProductDetails() { //TODO Server method here
        if (Connectivity.isConnected(MyOrderDetails.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(MyOrderDetails.this);
            gateWay.progressDialogStart();

            myOrderDetailsAdapter = new MyOrderDetailsAdapter();

            JSONObject params = new JSONObject();
            try {
                params.put("order_id", order_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlProductDetails, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            relativeLayoutOrderNow.setVisibility(View.GONE);
                        } else {
                            strMsg = response.getString("msg");
                            JSONArray mainOrderArray = response.getJSONArray("posts");

                            Log.e("MyOrderDetailsResp:",""+response.toString());

                            for (int i = 0; i < mainOrderArray.length(); i++) {
                                JSONObject jSonMyOrderData = mainOrderArray.getJSONObject(i);

                                InnerMovie innerMovie = new InnerMovie(jSonMyOrderData.getString("shop_id"), jSonMyOrderData.getString("shop_name"),
                                        jSonMyOrderData.getString("product_id"), jSonMyOrderData.getString("product_name"),
                                        jSonMyOrderData.getString("product_brand"), jSonMyOrderData.getString("product_image"),
                                        jSonMyOrderData.getString("product_price"), jSonMyOrderData.getString("product_quantity"),
                                        jSonMyOrderData.getString("offer_code"), jSonMyOrderData.getString("product_qty"),
                                        jSonMyOrderData.getString("product_status"), jSonMyOrderData.getString("p_name"), jSonMyOrderData.getString("type"));
                                myOrderDetailsAdapter.add(innerMovie);
                            }
                            recyclerView.setAdapter(myOrderDetailsAdapter);
                            tvMsg.setText(strMsg);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(MyOrderDetails.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void bookOrderNow(final String pId) { //TODO Server method here
        if (Connectivity.isConnected(MyOrderDetails.this)) { // Internet connection is not present, Ask user to connect to Internet
            final DBHelper dbHelper = new DBHelper(this);

            final GateWay gateWay = new GateWay(MyOrderDetails.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
                params.put("order_id", order_id);
                params.put("versionCode", StaticUrl.versionName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlUserRepeatOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        result = response.getString("posts");
                        if (result.equals("true")) {
                            dbHelper.insertCount(pId);
                            Intent intent = new Intent(MyOrderDetails.this, AddToCartActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(MyOrderDetails.this, "Product already in my cart.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(MyOrderDetails.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(MyOrderDetails.this);
            gateWay.displaySnackBar(myOrderDetailsMainLayout);
        }
    }

    private class MyOrderDetailsAdapter extends RecyclerView.Adapter<ViewHolder> {

        final ArrayList<InnerMovie> mItems = new ArrayList<>();

        MyOrderDetailsAdapter() {
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_my_order_details, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(MyOrderDetails.this)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = recyclerView.getChildAdapterPosition(v);
                            InnerMovie movie = mItems.get(itemPosition);
                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on view & product is: " + movie.getP_name(), MyOrderDetails.this);*/

                            if (movie.getProduct_status().equals("deactive")) {
                                Toast.makeText(MyOrderDetails.this, "Product Information is not available for " + movie.getP_name() + " because it's deactivated.", Toast.LENGTH_SHORT).show();
                            } else {
                                if (movie.getType().equals("combo")) {

                                } else {
                                    Intent intent = new Intent(MyOrderDetails.this, ProductDetailActivity.class);
                                    intent.putExtra("product_name", movie.getP_name());
                                    intent.putExtra("shop_id", movie.getShop_id());
                                    startActivity(intent);
                                }
                            }
                        } else {
                            GateWay gateWay = new GateWay(MyOrderDetails.this);
                            gateWay.displaySnackBar(myOrderDetailsMainLayout);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            final InnerMovie innerMovie = mItems.get(position);

            if (!innerMovie.getOffer_code().equals("")) {//any order content combo offer that time user can't be give repeat order.
                relativeLayoutOrderNow.setVisibility(View.GONE);

                //TODO in this case combo image should be replace with product image.
               /* Glide.with(MyOrderDetails.this).load("http://s.apneareamein.com/seller/assets/" + innerMovie.getProduct_image())
                        .thumbnail(Glide.with(MyOrderDetails.this).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.tvProductImage);*/
                Picasso.with(MyOrderDetails.this).load("http://s.apneareamein.com/seller/assets/" + innerMovie.getProduct_image())
                        .error(R.drawable.ic_app_transparent)
                        .into(holder.tvProductImage);

            } else {
                //TODO if combo offer consist in user order combo image should be reflect with product image
                /*Glide.with(MyOrderDetails.this)
                        .load("http://simg.picodel.com/" + innerMovie.getProduct_image())
                        .thumbnail(Glide.with(MyOrderDetails.this).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.tvProductImage);*/
                Picasso.with(MyOrderDetails.this)
                        .load("https://simg.picodel.com/" + innerMovie.getProduct_image())
                        .error(R.drawable.ic_app_transparent)
                        .into(holder.tvProductImage);
            }
            holder.tvProductName.setText(innerMovie.getProduct_name());
            holder.tvSellerName.setText("By: " + innerMovie.getShop_name());
            holder.tvProductPrice.setText("₹ " + innerMovie.getProduct_price());
            holder.tvProductQty.setText("Quantity: " + innerMovie.getProduct_quantity());
            String QTY = innerMovie.getProduct_qty();
            if (QTY.equals("0") || QTY.contains("-") || QTY.equals("null")) {
                msgLayout.setVisibility(View.VISIBLE);
                holder.imgOutOfStock.setVisibility(View.VISIBLE);

                //TODO here out of stock GIF image setup to glide
               /* Glide.with(MyOrderDetails.this)
                        .load(R.drawable.outofstock)
                        .error(R.drawable.icerror_outofstock)
                        .into(holder.imgOutOfStock);*/
                Picasso.with(MyOrderDetails.this)
                        .load(R.drawable.outofstock)
                        .error(R.drawable.icerror_outofstock)
                        .into(holder.imgOutOfStock);

            } else {
                holder.imgOutOfStock.setVisibility(View.GONE);
            }

            btnBookNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   /* UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on reorder button & order id is: " + order_id, MyOrderDetails.this);*/

                    DBHelper dbHelper = new DBHelper(MyOrderDetails.this);
                    HashMap AddToCartInfo;
                    String pId = innerMovie.getProduct_id();
                    AddToCartInfo = dbHelper.getCartDetails(pId);
                    String strProductId = (String) AddToCartInfo.get("new_pid");
                    if (strProductId == null) {
                        for (int i = 0; i < mItems.size(); i++) {
                            InnerMovie movie = mItems.get(i);
                        }
                    }
                    bookOrderNow(pId);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(InnerMovie innerMovie) {
            mItems.add(innerMovie);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvProductName;
        final TextView tvSellerName;
        final TextView tvProductPrice;
        final TextView tvProductQty;
        final ImageView tvProductImage;
        final ImageView imgOutOfStock;

        public ViewHolder(View itemView) {
            super(itemView);

            tvProductName = itemView.findViewById(R.id.txtProductName);
            tvProductPrice = itemView.findViewById(R.id.txtProductPrice);
            tvSellerName = itemView.findViewById(R.id.txtSellerName);
            tvProductQty = itemView.findViewById(R.id.txtProductQuantity);
            tvProductImage = itemView.findViewById(R.id.productImage);
            imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
        }
    }

    private class InnerMovie {

        final String shop_id;
        final String shop_name;
        final String product_id;
        final String product_name;
        final String product_brand;
        final String product_image;
        final String product_price;
        final String product_quantity;
        final String offer_code;
        final String product_qty;
        final String product_status;
        final String p_name;
        final String type;

        public InnerMovie(String shop_id, String shop_name, String product_id, String product_name, String product_brand, String product_image,
                          String product_price, String product_quantity, String offer_code, String product_qty,
                          String product_status, String p_name, String type) {
            this.shop_id = shop_id;
            this.shop_name = shop_name;
            this.product_id = product_id;
            this.product_name = product_name;
            this.product_brand = product_brand;
            this.product_image = product_image;
            this.product_price = product_price;
            this.product_quantity = product_quantity;
            this.offer_code = offer_code;
            this.product_qty = product_qty;
            this.product_status = product_status;
            this.p_name = p_name;
            this.type = type;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getProduct_brand() {
            return product_brand;
        }

        public String getProduct_image() {
            return product_image;
        }

        public String getProduct_price() {
            return product_price;
        }

        public String getProduct_quantity() {
            return product_quantity;
        }

        public String getOffer_code() {
            return offer_code;
        }

        public String getShop_id() {
            return shop_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_qty() {
            return product_qty;
        }

        public String getProduct_status() {
            return product_status;
        }

        public String getP_name() {
            return p_name;
        }

        public String getType() {
            return type;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        /*if (Connectivity.isConnected(MyOrderDetails.this)) {
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, MyOrderDetails.this);
        }*/
    }


}
