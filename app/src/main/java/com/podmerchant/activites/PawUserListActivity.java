package com.podmerchant.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.podmerchant.R;
import com.podmerchant.adapter.CustomSpinnerAdapter;
import com.podmerchant.adapter.PawUserListAdapter;
import com.podmerchant.adapter.PaymentModeAdaptor;
import com.podmerchant.model.PawUserModel;
import com.podmerchant.model.PaymentModeModel;
import com.podmerchant.model.StateCityModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class PawUserListActivity extends AppCompatActivity {

    Context mContext;
    AlertDialog progressDialog;
    ArrayList<PawUserModel> pawUserModels;
    PawUserListAdapter pawUserListAdapter;
    RecyclerView rv_pawuserlist;
    LinearLayoutManager layoutManager;
    SharedPreferencesUtils sharedPreferencesUtils;
    FloatingActionButton fbtn_addUser;
    String MobilePattern = "[0-9]{10}",finalPrice="00";
    String str_state,str_city,str_area;
    Spinner sp_state_spinner,sp_city_spinner,sp_area_spinner;
    private final ArrayList<String> stateList = new ArrayList<>();
    private final ArrayList<String> cityList = new ArrayList<>();
    private final ArrayList<String> areaList = new ArrayList<>();
    private ArrayList<StateCityModel> stateCityModelArrayList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paw_user_list);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Select User");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PawUserListActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        rv_pawuserlist = findViewById(R.id.rv_pawuserlist);
        fbtn_addUser = findViewById(R.id.fbtn_addUser);
        rv_pawuserlist.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        pawUserModels = new ArrayList<>();
        rv_pawuserlist.setLayoutManager(layoutManager);
        rv_pawuserlist.setItemAnimator(new DefaultItemAnimator());
        pawUserListAdapter = new PawUserListAdapter(mContext,pawUserModels);
        rv_pawuserlist.setAdapter(pawUserListAdapter);

        paymentModeList();

        fbtn_addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog();
            }
        });

        if(!finalPrice.equals("")) {
            finalPrice = getIntent().getStringExtra("finalPrice");
        }

        rv_pawuserlist.addOnItemTouchListener(new PawUserListActivity.RecyclerTouchListener(mContext, rv_pawuserlist,
                new PawUserListActivity.RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        //Toast.makeText(mContext,staffModelArrayList.get(position).getStaffName(),Toast.LENGTH_SHORT).show();
                        //Intent intent = new Intent(PawUserListActivity.this,Order_Details_StepperActivity.class);
                        Intent intent = new Intent(PawUserListActivity.this,PawOrderDetailActivity.class);
                        intent.putExtra("userid", pawUserModels.get(position).getId());
                        intent.putExtra("fname", pawUserModels.get(position).getFname());
                        intent.putExtra("lname", pawUserModels.get(position).getLname());
                        intent.putExtra("mobileno", pawUserModels.get(position).getContactno());
                        intent.putExtra("finalPrice",finalPrice);
                        startActivity(intent);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

    }

    //add user dialog
    private  void  customDialog(){
        final Dialog dialog = new Dialog(PawUserListActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dailog_custom_add_user);

        final EditText et_firstname = dialog.findViewById(R.id.et_firstname);
        final EditText et_lastname = dialog.findViewById(R.id.et_lastname);
        final EditText et_mobileno = dialog.findViewById(R.id.et_mobileno);
        final EditText et_emailid = dialog.findViewById(R.id.et_emailid);
        final EditText et_areaname = dialog.findViewById(R.id.et_areaname);
        final EditText et_buildingname = dialog.findViewById(R.id.et_buildingname);
        final EditText et_flatno = dialog.findViewById(R.id.et_flatno);
        final EditText et_roadname = dialog.findViewById(R.id.et_roadname);
        final Button btn_add_user = dialog.findViewById(R.id.btn_add_user);
        //  sp_state_spinner = dialog.findViewById(R.id.sp_state_spinner);
          sp_city_spinner = dialog.findViewById(R.id.sp_city_spinner);
          //sp_area_spinner = dialog.findViewById(R.id.sp_area_spinner);

        sp_city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_city = cityList.get(i);
                Log.d("str_city",str_city);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        // text.setText(msg);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        btn_add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(et_firstname.getText().toString().equalsIgnoreCase("")){
                     et_firstname.requestFocus();
                     et_firstname.setError(" Enter First Name");
                }else  if(et_lastname.getText().toString().equalsIgnoreCase("")){
                     et_lastname.requestFocus();
                     et_lastname.setError(" Enter Last Name");
                 }else if(et_mobileno.getText().toString().equalsIgnoreCase("")){
                     et_mobileno.requestFocus();
                     et_mobileno.setError(" Enter Mobile No");
                }else if(!et_mobileno.getText().toString().matches(MobilePattern)) {
                    Toast.makeText(mContext,"Please Enter valid Contact",Toast.LENGTH_LONG).show();
                }else if(et_areaname.getText().toString().equalsIgnoreCase("")){
                     et_areaname.requestFocus();
                     et_areaname.setError(" Enter Area Name");
                }else if(et_buildingname.getText().toString().equalsIgnoreCase("")){
                     et_buildingname.requestFocus();
                     et_buildingname.setError(" Enter Building/Cluster Name");
                 }else if(et_flatno.getText().toString().equalsIgnoreCase("")){
                     et_flatno.requestFocus();
                     et_flatno.setError(" Enter Flat No/House No");
                 }else {
                    addNewPawUser(et_emailid.getText().toString(), et_firstname.getText().toString(),et_lastname.getText().toString(),et_mobileno.getText().toString(),
                            et_areaname.getText().toString(),et_buildingname.getText().toString(),et_flatno.getText().toString(),et_roadname.getText().toString(),str_city);
                    dialog.dismiss();
                }
            }
        });

        //getState();
        getCityList();
        dialog.show();
    }

    //add new user from paw
    public void addNewPawUser(String email, String userName, String userLastName, String mobNumber,String areaname,String buildingname,String flatno,String roadname,String cityName){
        //pawAddNewUser.php
        JSONObject params = new JSONObject();
        try {
            //vasundhara
            params.put("email", email);
            params.put("userName", userName);
            params.put("userLastName", userLastName);
            params.put("mob_number", mobNumber);
            params.put("pwd", "999999");
            //params.put("pwd", pwd);
            params.put("address", buildingname+" "+flatno+" "+roadname);
            params.put("city", cityName);
            params.put("area", "areaname");
            params.put("landmark", "");
            params.put("pinCode", "");
            params.put("knowFrom", "paw");
            params.put("notification_id", "notificationId");
            params.put("refercode", "");
            params.put("device_id", "androidId");
            params.put("pawid", sharedPreferencesUtils.getShopID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.e("userResponse:",""+StaticUrl.urlPawAddNewUser+" "+params);
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                StaticUrl.urlPawAddNewUser,
                params,
                new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("addNewPawUser",""+response);
                     try {
                         String result = response.getString("posts");
//                        String user_id = response.getString("user_id");
                        if (result.equals("already_registered")) {
                            Toast.makeText(PawUserListActivity.this, "This user already registered", Toast.LENGTH_LONG).show();
                        }
                            paymentModeList();
                     }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        VolleySingleton.getInstance(PawUserListActivity.this).addToRequestQueue(request);
    }

    //Get paymentmode Api method
    public void paymentModeList(){

        if(Connectivity.isConnected(mContext)){
            pawUserModels.clear();
            progressDialog.show();

            final String urlAcceptOrder = StaticUrl.urlPawUserList+"?pawid="+sharedPreferencesUtils.getShopID();

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.e("userResponse:",""+urlAcceptOrder+" "+response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                     JSONArray jsonArray =  jsonObject.getJSONArray("posts");
                                    Log.e("paymentModData:",""+jsonArray);

                                    for (int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        PawUserModel pawUserModel = new PawUserModel();
                                        pawUserModel.setId(jsonObject1.getString("ID"));
                                        pawUserModel.setFname(jsonObject1.getString("fname"));
                                        pawUserModel.setLname(jsonObject1.getString("lname"));
                                        pawUserModel.setContactno(jsonObject1.getString("contactno"));
                                        pawUserModels.add(pawUserModel);
                                    }
                                    pawUserListAdapter.notifyDataSetChanged();

                                }else {
                                    Toast.makeText(mContext,"Add New User.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void getState() { //TODO Server method here
        if (Connectivity.isConnected(PawUserListActivity.this)) {
            stateList.clear();

            // final GateWay gateWay = new GateWay(LocationActivity.this);
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlStateList, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    Log.e("State_value:",""+response);
                    try {
                        //JSONArray jsonArray = response.getJSONArray("");
                        //JSONObject jsonObjectTest = jsonArray.getJSONObject(0);
                        // Log.d("posts",response.getString("image"));
                        //TODO here show location GIF image setup to glide
                           /*Glide.with(LocationActivity.this)
                                    .load(response.getString("image"))
                                    .thumbnail(Glide.with(LocationActivity.this).load(R.drawable.ic_location_on_black_24dp))
                                    .error(R.drawable.ic_app_transparent)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imageView);*/
                        //imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_on_black_24dp));

                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            stateList.add(jsonCityData.getString("city"));
                        }
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(PawUserListActivity.this, stateList, null, null, "location");
                        sp_state_spinner.setAdapter(customSpinnerAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(LocationActivity.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(PawUserListActivity.this).addToRequestQueue(request);
        }
    }

    private void getCityList() { //TODO Server method here
        if (Connectivity.isConnected(PawUserListActivity.this)) {
            cityList.clear();

            //final GateWay gateWay = new GateWay(EditAddress.this);
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);

            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCityConfiguration, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    try {

                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            cityList.add(jsonCityData.getString("city"));
                        }
                        /*CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, areaList, null, null, "location");
                        sp_Area_spinner.setAdapter(customSpinnerAdapter);*/

                        ArrayAdapter<String> adapter = new ArrayAdapter(PawUserListActivity.this,android.R.layout.simple_spinner_item,cityList);
                        sp_city_spinner.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
//                    simpleProgressBar.setVisibility(View.INVISIBLE);
//
//                    GateWay gateWay = new GateWay(EditAddress.this);
//                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(PawUserListActivity.this).addToRequestQueue(request);
        }
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  PawUserListActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final PawUserListActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}