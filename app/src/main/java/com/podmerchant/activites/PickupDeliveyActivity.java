package com.podmerchant.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class PickupDeliveyActivity extends AppCompatActivity {

    TextView tv_pickup_location;
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;
    String shopkpwise_id,orderId;
    AlertDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pickup_location);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("View Pickup Location");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PickupDeliveyActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        tv_pickup_location = findViewById(R.id.tv_pickup_location);


          shopkpwise_id = getIntent().getStringExtra("shopkpwise_id");
          orderId = getIntent().getStringExtra("order_id");

            getManufacturerPickupLocation();

    }

    //Get Distributor Pickup Location by Brand or manufacturer
    public void getManufacturerPickupLocation( ) {
        if (Connectivity.isConnected(mContext)) {
            progressDialog.show();
            String urlpickuplocation = null;
            try {
                urlpickuplocation = StaticUrl.Getpickuplocation
                        + "?order_id=" + orderId
                        + "&shopkpwise_id=" + shopkpwise_id
                        + "&distributor_id=" + sharedPreferencesUtils.getShopID()
                        + "&admin_type="+sharedPreferencesUtils.getAdminType();

                //+ "&product_brand="+product_brand;
                //+ "&test_order="+"test_order";
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("url_pickup_location",urlpickuplocation);


            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlpickuplocation,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                Log.e("pickup_location_Res:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status == true) {

                                    //Toast.makeText(mContext, "Order Accepted. Successfully", Toast.LENGTH_SHORT).show();

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject jsonObjectData =  jsonArray.getJSONObject(i);
                                        String pickup_address1 = jsonObjectData.getString("pickup_address1");
                                        String pickup_address2 = jsonObjectData.getString("pickup_address2");
                                        String pickup_address3 = jsonObjectData.getString("pickup_address3");
                                        String pickup_address5 = jsonObjectData.getString("pickup_address5");

                                        tv_pickup_location.setText("Pickup Location is: "+pickup_address1+"\n"+pickup_address2+"\n"+pickup_address3+"\n"+pickup_address5);
                                    }



                                } else if (status == false) {
                                    // Toast.makeText(mContext, "Sorry. This order you can not accept.", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(mContext, jsonObject.getString("Not Approve Yet"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key. The {@link #getOnBackPressedDispatcher() OnBackPressedDispatcher} will be given a
     * chance to handle the back button before the default behavior of
     * {@link Activity#onBackPressed()} is invoked.
     *
     * @see #getOnBackPressedDispatcher()
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
