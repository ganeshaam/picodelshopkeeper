package com.podmerchant.activites;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.CartListAdapter;
import com.podmerchant.adapter.DeliveryRequestAdapter;
import com.podmerchant.adapter.PaymentModeAdaptor;
import com.podmerchant.model.DeliveryModel;
import com.podmerchant.model.PaymentModeModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dmax.dialog.SpotsDialog;

public class RequestListActivity extends AppCompatActivity {

    RecyclerView rv_RequestList;
    Context mContext;
    LinearLayoutManager layoutManager;
    DeliveryRequestAdapter requestAdapter;
    ArrayList<DeliveryModel> arrayList;
    AlertDialog progressDialog;
    TextView tv_network_con;
    SharedPreferencesUtils sharedPreferencesUtils;
    ArrayList<PaymentModeModel> paymentModeList;
    PaymentModeAdaptor paymentModeAdaptor;
    String flag = "New";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_request_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Request Order List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rv_RequestList = findViewById(R.id.rv_RequestList);
        tv_network_con = findViewById(R.id.tv_network_con);

        mContext = RequestListActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        arrayList = new ArrayList<>();
        paymentModeList = new ArrayList<>();

        Intent intent = getIntent();
        flag = intent.getStringExtra("flag");

        rv_RequestList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_RequestList.setLayoutManager(layoutManager);
        rv_RequestList.setItemAnimator(new DefaultItemAnimator());
        requestAdapter = new DeliveryRequestAdapter(arrayList,mContext,"");
        rv_RequestList.setAdapter(requestAdapter);

        getDeliveryRequestList(flag);//Accepted_Delivery

        paymentModeList();

        // click on the recycler item view
        rv_RequestList.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_RequestList,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        //delBoyId =  boyModelArrayList.get(position).getDelBoyId();
                      String  Selected_id= arrayList.get(position).getShop_id();
                      String  D_Status= arrayList.get(position).getD_status();
                       if(D_Status.equalsIgnoreCase("Accepted")){
                           Toast.makeText(mContext,"Order Already Accepted",Toast.LENGTH_LONG).show();
                           DeliveredDialog(Selected_id);
                       }else {
                           AcceptDialog(Selected_id);
                       }
                        Log.e("selected PAYMENT MODE",""+Selected_id);
                        Toast.makeText(mContext,arrayList.get(position).getShop_id(),Toast.LENGTH_SHORT).show();
                        //tv_selectedvalue.setText("Payment Mode:"+Selected_PaymentMode);
                        //paymentModeAdaptor.notifyDataSetChanged();
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

    }

    public void getDeliveryRequestList(String request_type){
        progressDialog.show();
        arrayList.clear();


        String targetUrlShopList = "https://www.picodel.com/seller/franchisee/getdeliverydetails"+"?shop_id="+sharedPreferencesUtils.getShopID()
                +"&flag="+request_type;

        Log.d("targetUrlShopList",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Request_list",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        DeliveryModel model = new DeliveryModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setD_from(olderlist.getString("d_from"));
                                        model.setD_destination(olderlist.getString("d_destination"));
                                        model.setD_height(olderlist.getString("d_height"));
                                        model.setD_weight(olderlist.getString("d_weight"));
                                        model.setD_no_of_items(olderlist.getString("d_no_items"));
                                        model.setD_amount(olderlist.getString("d_amount"));
                                        model.setD_cdate(olderlist.getString("d_cdate"));
                                        model.setD_status(olderlist.getString("d_status"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        arrayList.add(model);
                                    }
                                    requestAdapter = new DeliveryRequestAdapter(arrayList,mContext,"");
                                    rv_RequestList.setAdapter(requestAdapter);
                                    requestAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Dialog to Accept Delivery Order
    public void AcceptDialog(final String id){
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to Accept this Delivery Order?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finishAffinity();
                        acceptRequest(id,"Accepted","");
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    // Dialog to Delivered status of  request Order
    public void DeliveredDialog(final String id){
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to Delivered this Delivery Order?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finishAffinity();
                        //acceptRequest(id,"Delivered");
                        PaymentModeDailog(id);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    public void acceptRequest(String ID , final String flag, final String paymentmode){
        progressDialog.show();
        arrayList.clear();
        String targetUrlShopList="";

        if(flag == "Delivered"){  //update_request_status.php
             targetUrlShopList = "https://www.picodel.com/seller/franchisee/acceptdeliveryrequest" + "?d_id="+ ID +"&flag=Delivered"+ "payment_mode="+paymentmode+"&shop_id=" + sharedPreferencesUtils.getShopID();
             //targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/update_request_status.php" + "?d_id="+"&flag=Delivered"+ ID + "&shop_id=" + sharedPreferencesUtils.getShopID();
        }else {
             targetUrlShopList = "https://www.picodel.com/seller/franchisee/acceptdeliveryrequest" + "?d_id="+ ID + "&flag=Delivered"  + "&shop_id=" + sharedPreferencesUtils.getShopID();
        }


        Log.d("URL_AcceptRequest",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RequestAccept_Res",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                //JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonObject.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    if(flag == "Delivered"){
                                        Toast.makeText(mContext, "Request Delivered Successfully", Toast.LENGTH_LONG).show();
                                    }else {
                                        Toast.makeText(mContext, "Request Accepted Successfully", Toast.LENGTH_LONG).show();
                                    }

                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    // payment List dailog
    public void PaymentModeDailog( final String d_id) {
        //final Dialog dialog = new Dialog(this);
        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.payment_list_dailog, null);

        Button btn_submitreson = view.findViewById(R.id.btn_reason_list);

        final RecyclerView cart_list = (RecyclerView) view.findViewById(R.id.rv_payment_list);
        cart_list.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        cart_list.setLayoutManager(layoutManager);
        cart_list.setVisibility(View.VISIBLE);

        final String[] selected_mode = {""};

        Log.e("paymentModeList Size:",""+paymentModeList.size());


        paymentModeAdaptor = new PaymentModeAdaptor(mContext,paymentModeList);
        cart_list.setAdapter(paymentModeAdaptor);


        cart_list.addOnItemTouchListener(new RecyclerTouchListener(mContext, cart_list,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        Log.e("selected Product",""+paymentModeList.get(position).getPaymentType());
                        Toast.makeText(mContext,paymentModeList.get(position).getPaymentType(),Toast.LENGTH_SHORT).show();
                        selected_mode[0] = paymentModeList.get(position).getPaymentType();
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                     Toast.makeText(mContext,selected_mode[0],Toast.LENGTH_SHORT).show();
                     acceptRequest(d_id,"Delivered",selected_mode[0]);
            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

    //Get paymentmode Api method
    public void paymentModeList(){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            //String urlAcceptOrder = StaticUrl.getPaymentMode;
            String urlAcceptOrder = StaticUrl.getPaymentMode+"?user="+"shop";// +"?order_id="+orderId

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONArray jsonArray =  jsonObject.getJSONArray("data");
                                    Log.e("paymentModData:",""+jsonArray);

                                    for (int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        PaymentModeModel paymentMode_model = new PaymentModeModel();
                                        paymentMode_model.setPaymentType(jsonObject1.getString("payment_type"));
                                        paymentMode_model.setPayment_details(jsonObject1.getString("payment_details"));
                                        //payment_details
                                        paymentModeList.add(paymentMode_model);
                                    }
                                    //paymentModeAdaptor.notifyDataSetChanged();
                                    //     Toast.makeText(mContext,"Payment List Successfully fetched",Toast.LENGTH_SHORT).show();
                                }else if(status=false){
                                    Toast.makeText(mContext,"Sorry. No Payment Mode Found.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
}
