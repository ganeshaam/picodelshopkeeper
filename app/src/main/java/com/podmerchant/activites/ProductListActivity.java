package com.podmerchant.activites;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.podmerchant.R;
import com.podmerchant.adapter.CustomSpinnerAdapter;
import com.podmerchant.fragment.filter_fragments.BrandFilter;
import com.podmerchant.fragment.filter_fragments.DepartmentFilter;
import com.podmerchant.fragment.filter_fragments.OtherFilter;
import com.podmerchant.fragment.filter_fragments.PriceFilter;
import com.podmerchant.fragment.filter_fragments.SellerFilter;
import com.podmerchant.fragment.filter_fragments.SortFilter;
import com.podmerchant.local_storage.DBHelper;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.HidingScrollListenerForFab;
import com.podmerchant.util.PaginationScrollListener;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.ShowcaseViewBuilder;
import com.podmerchant.util.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;

public class ProductListActivity extends AppCompatActivity {

    public static final HashMap mainCat_CatHashMap = new HashMap();
    public static Set<String> uniqueBrandNames;
    public static Set<String> uniqueShopNames;
    public static Set<String> uniqueSizeList;
    public static Set<String> uniqueMainCategory;
    public static ArrayList<String> productMainCategory = new ArrayList<>();
    public static ArrayList<String> catList = new ArrayList<>();
    private static Set<String> productIdSet = new TreeSet<>();
    private final String FIND_TAG = "find", NEXT_TAG = "next", FILTER_TAG = "filter_tag";
    private ArrayList<String> product_id;
    private final ArrayList<String> product_name = new ArrayList<>();
    private final ArrayList<String> shop_name = new ArrayList<>();
    private final ArrayList<String> shopNamesList = new ArrayList<>();
    private final ArrayList<String> categoryList = new ArrayList<>();
    private final ArrayList<String> brandList = new ArrayList<>();
    private final ArrayList<String> sizeList = new ArrayList<>();
    private final ArrayList<String> sizeParameterList = new ArrayList<>();
    private final ArrayList<String> allSizeList = new ArrayList<>();
    private final ArrayList<ProductCodeWiseProduct> similarProductWiseList = new ArrayList<>();
    private ArrayList<productCodeWithProductList> FinalList;
    private final ArrayList<productCodeWithProductList> SimilarFinalList = new ArrayList<>();
    private final LinkedHashMap similarProductListHashMap = new LinkedHashMap(); // will contain all
    private ShowcaseViewBuilder showcaseViewBuilder;
    private FloatingActionButton floatingActionButton;
    private CoordinatorLayout searchProductCoordinateLayout;
    private RelativeLayout searchProductsMainLayout;
    private DialogPlus dialog, dialogPlus;
    private ProgressBar progressBar;
    private GateWay gateWay;
    private String strSelectedName, strCity, strArea, shopId, product_Size, pId, tag, pName, strId, strContact, strEmail, strName;
    private int count, allProductsCount;
    private TextView tvWishList, txtMessage, tvTitle, tvCartNumber;
    private JSONArray categoryArray;
    private JSONArray brandArray;
    private JSONArray priceArray;
    private JSONArray mainCatArray;
    private JSONArray sizeArray;
    private JSONArray discountArray;
    private JSONArray sortArray;
    private JSONArray cakeArray;
    private JSONArray shopNameArray;
    private RecyclerView mRecyclerView, similarProductRecyclerView;
    private CardAdapter cardAdapter;
    private CardAdapter.SimilarProductCardAdapter similarProductCardAdapter;
    private LinearLayoutManager similarLayoutManager;
    private LinearLayout similarProductLinearLayout;
    private ArrayList<String> productCategories = new ArrayList<>();
    private ArrayList<Integer> offset_count;
    private ArrayList<ProductCodeWiseProduct> tempProductWiseList;
    private LinkedHashMap tempProductListHashMap;
    private TextView tvMessage, tvYes, tvNo;
    private AlertDialog alertDialog;
    private FrameLayout frameFABLayout;
    private int WishListCount, CartCount;
    private String strCheckQty;
    private TextView tvFilter;
    private final String class_name = this.getClass().getSimpleName();
    private LinearLayout Main_Layout_NoInternet;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int currentPage;
    private LinearLayoutManager mLayoutManager;
    private BroadcastReceiver myReceiver;
    private TextView txtNoConnection;
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Product List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myReceiver = new Network_Change_Receiver();

        mContext = ProductListActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        //start----- these are all findViewById here
        searchProductCoordinateLayout = findViewById(R.id.searchProductCoordinateLayout);
        searchProductsMainLayout = findViewById(R.id.searchProductsMainLayout);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        // = findViewById(R.id.txtTitle);
        txtMessage = findViewById(R.id.txtMessage);
        tvFilter = findViewById(R.id.txtFilter);
        progressBar = findViewById(R.id.progressBar);
        tvCartNumber = findViewById(R.id.cartNumber);
        floatingActionButton = findViewById(R.id.userCart);
        frameFABLayout = findViewById(R.id.frameFABLayout);
        txtNoConnection = findViewById(R.id.txtNoConnection);
        //end----- these are all findViewById here

       // GateWay gateWay = new GateWay(this);
        strCity = "Maharashtra";//gateWay.getCity();
        strArea = "Pune";//gateWay.getArea();
        strContact = sharedPreferencesUtils.getPhoneNumber();//gateWay.getContact();
        strEmail =  sharedPreferencesUtils.getEmailId();//gateWay.getUserEmail();
        strName = sharedPreferencesUtils.getUserName();//gateWay.getUserName();

        Bundle bundle = getIntent().getExtras();
        tag = bundle.getString("tag");
        strSelectedName = bundle.getString("selectedName");//"Mapro";

        categoryArray = new JSONArray(DepartmentFilter.departmentNameList);
        if (BrandFilter.brandTag.equals("normalTag")) {
            brandArray = new JSONArray(BrandFilter.normalBrandNameList);
        } else {
            if (BrandFilter.dependentBrandNameListLastResult.size() > 0) {
                brandArray = new JSONArray(BrandFilter.dependentBrandNameListLastResult);
            } else {
                brandArray = new JSONArray(BrandFilter.dependentBrandNameList);
            }
        }
        priceArray = new JSONArray(PriceFilter.priceNameList);

        switch (OtherFilter.sizeTag) {
            case "bothCategoryAndBrand":
                sizeArray = new JSONArray(OtherFilter.categoryBrandDependentSizeNameList);
                break;
            case "onlyNormalBrand":
                sizeArray = new JSONArray(OtherFilter.brandDependentSizeNameList);
                break;
            case "onlyCategory":
                sizeArray = new JSONArray(OtherFilter.categoryDependentSizeNameList);
                break;
            case "normal":
                sizeArray = new JSONArray(OtherFilter.directNormalSizeNameList);
                break;
            case "":
                sizeArray = new JSONArray();
                break;
        }

        discountArray = new JSONArray(PriceFilter.discountNameList);
        sortArray = new JSONArray(SortFilter.sortNameList);
        shopNameArray = new JSONArray(SellerFilter.sellerNameList);

        cakeArray = new JSONArray(OtherFilter.cakeTypeList);

        if ((categoryArray.length() == 0) && (brandArray.length() == 0) && (priceArray.length() == 0) && (sizeArray.length() == 0)
                && (discountArray.length() == 0) && (sortArray.length() == 0) && (cakeArray.length() == 0) && (shopNameArray.length() == 0)) {
            switch (tag) {
                case "find":
                    tvFilter.setVisibility(View.INVISIBLE);
                    break;
                case "exciting_offer":
                    tvFilter.setVisibility(View.INVISIBLE);
                    break;
                default:
                    tag = "next";
                    getCategoryForCake();
                    getAllFilterData();
                    break;
            }
        } else {
            currentPage = 0;
            tag = bundle.getString("tag");
            strSelectedName = bundle.getString("selectedName");
        }

        tvFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet

                        //UserTracking UT = new UserTracking(UserTracking.context);
                        //UT.user_tracking(class_name + ": clicked on filter button", mContext);

                        if (shopNamesList.size() > 0 || categoryList.size() > 0 && brandList.size() > 0 &&
                                sizeList.size() > 0 || sizeParameterList.size() > 0 || allSizeList.size() > 0) {
                            try {
                                if (product_name.size() == 0) {
                                    Toast.makeText(mContext, "There is no data to apply filter.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Intent intent = new Intent(mContext, FilterLayout.class);
                                    intent.putExtra("selectedName", strSelectedName);
                                    intent.putExtra("open", 0);
                                    startActivity(intent);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                if (product_name.size() == 0) {
                                    Toast.makeText(mContext, "There is no data to apply filter.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(mContext, "There is no data to apply filter.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        GateWay gateWay = new GateWay(mContext);
                        gateWay.displaySnackBar(searchProductCoordinateLayout);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cardAdapter = new CardAdapter();
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(cardAdapter);

        mRecyclerView.setOnScrollListener(new  HidingScrollListenerForFab() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                   /* DBHelper db = new DBHelper(mContext);
                    count = (int) db.fetchAddToCartCount();*/
                    //count = (int) CartCount;

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on FAB", mContext);
*/

                    int count = SyncData();
                    updateAddToCartActivityCount(count);

                    if (SyncData() == 0) {
                        GateWay gateWay = new GateWay(mContext);
                        gateWay.cartAlertDialog();
                    } else {
                        Intent intent = new Intent(mContext, AddToCartActivity.class);
                        startActivity(intent);
                    }
                } else {
                    GateWay gateWay = new GateWay(mContext);
                    gateWay.displaySnackBar(searchProductCoordinateLayout);
                }
            }
        });

        takeResults(strSelectedName);

        try {
        }catch (Exception e){
            Log.e("Testing Commit ","Hello Testing");
        }

        SyncData();
    }
    //popular product list
    private void takeResults(final String strSelectedName) { //TODO Server method here
        if (Connectivity.isConnected(ProductListActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            product_id = new ArrayList<>();
            FinalList = new ArrayList<>();
            offset_count = new ArrayList<>();
            tempProductListHashMap = new LinkedHashMap();
            tempProductWiseList = new ArrayList<>();

            final GateWay gateWay = new GateWay(ProductListActivity.this);
            if (currentPage == 0) {
               gateWay.progressDialogStart();
            }

            JSONObject params = new JSONObject();
            JSONArray pIdArray;
            if (sortArray.length() == 0) {
                pIdArray = new JSONArray();
            } else {
                pIdArray = new JSONArray(productIdSet);
            }

            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("name", strSelectedName);
                params.put("tag", tag);
                params.put("category", categoryArray);
                params.put("brand", brandArray);
                params.put("price", priceArray);
                params.put("size", sizeArray);
                params.put("discount", discountArray);
                params.put("sort", sortArray);
                params.put("cake", cakeArray);
                params.put("shopName", shopNameArray);
                params.put("sort_product_id", pIdArray);
                params.put("page", currentPage);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("takeResults",""+params);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlNewGetProducts, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("takeResult",""+response);
                    if (response.isNull("posts")) {
                        if (currentPage == 0) {
                            gateWay.progressDialogStop();
                        }
                        loadFirstPage();
                        if (TOTAL_PAGES == 0) {
                            mRecyclerView.setVisibility(View.GONE);
                            txtMessage.setVisibility(View.VISIBLE);
                        }
                    } else {
                        try {
                            if (response.getString("posts").equals("false")) {
                                progressBar.setVisibility(View.GONE);
                            } else {
                                mRecyclerView.setVisibility(View.VISIBLE);
                                JSONArray mainShopJsonArray = response.getJSONArray("posts");
                                for (int i = 0; i < mainShopJsonArray.length(); i++) {
                                    JSONObject jSonShopData = mainShopJsonArray.getJSONObject(i);

                                    shop_name.add(jSonShopData.getString("shop_name"));
                                    product_name.add(jSonShopData.getString("product_name"));
                                    product_id.add(jSonShopData.getString("product_id"));
                                    offset_count.add(jSonShopData.getInt("offset_count"));

                                    tempProductListHashMap.put(jSonShopData.getString("code"), "");

                                    tempProductWiseList.add(new ProductCodeWiseProduct(jSonShopData.getString("code"), jSonShopData.getString("shop_id"),
                                            jSonShopData.getString("shop_name"), jSonShopData.getString("shop_category"),
                                            jSonShopData.getString("product_name"), jSonShopData.getString("product_brand"),
                                            jSonShopData.getString("product_id"), jSonShopData.getString("product_image"),
                                            jSonShopData.getString("product_image1"), jSonShopData.getString("product_image2"),
                                            jSonShopData.getString("product_image3"), jSonShopData.getString("product_image4"),
                                            jSonShopData.getString("product_size"), jSonShopData.getString("product_mrp"),
                                            jSonShopData.getString("product_price"), jSonShopData.getString("product_discount"),
                                            jSonShopData.getString("product_description"), jSonShopData.getString("similar_product_status"),
                                            jSonShopData.getString("type"), jSonShopData.getString("product_qty"),
                                            jSonShopData.getString("hindi_name"),jSonShopData.getString("maincategory")));
                                }
                                allProductsCount = offset_count.get(0);//now this is use for total_pages
                                TOTAL_PAGES = Math.round(allProductsCount / 10);

                                productIdSet = new TreeSet<>();
                                productIdSet.addAll(product_id);

                                switch (tag) {
                                    case FIND_TAG:
                                       // tvTitle.setText(shop_name.get(0));

                                        break;

                                    case NEXT_TAG:
                                        //tvTitle.setText(strSelectedName);
                                        break;

                                    case FILTER_TAG:
                                        //tvTitle.setText(strSelectedName);
                                        break;
                                }

                                if (product_id.size() > 0) {
                                    productCodeWithProductList withProductCode;
                                    try {
                                        for (Object o : tempProductListHashMap.keySet()) {
                                            String key = (String) o;

                                            withProductCode = new productCodeWithProductList();
                                            withProductCode.productCodeWiseProducts = new ArrayList<>();
                                            for (ProductCodeWiseProduct pp : tempProductWiseList) {
                                                if (pp.code.equals(key)) {
                                                    withProductCode.productCodeWiseProducts.add(pp);
                                                }
                                            }
                                            FinalList.add(withProductCode);
                                        }

                                        txtMessage.setVisibility(View.GONE);
                                        loadFirstPage();

                                        mRecyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
                                            @Override
                                            protected void loadMoreItems() {
                                                isLoading = true;
                                                currentPage++;
                                                // mocking network delay for API call
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        takeResults(strSelectedName);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public int getTotalPageCount() {
                                                return TOTAL_PAGES;
                                            }

                                            @Override
                                            public boolean isLastPage() {
                                                return isLastPage;
                                            }

                                            @Override
                                            public boolean isLoading() {
                                                return isLoading;
                                            }
                                        });
                                        displayTuto("1");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                clearAllArrayList();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (currentPage == 0) {
                        gateWay.progressDialogStop();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().setPriority(Request.Priority.LOW);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Cannot proceed with the operation,No network connection! Please check your Internet connection")
                    .setTitle("Connection Offline")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            Intent intent = new Intent(ProductListActivity.this, HomeActivity.class);
                            intent.putExtra("tag", "");
                            startActivity(intent);
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @Override
    protected void onPause() {
//        unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        SyncData();
    }

    private void loadFirstPage() {
        if (currentPage == 0) {
            progressBar.setVisibility(View.GONE);
            cardAdapter.addAll(FinalList);

            if (TOTAL_PAGES == 0 && currentPage == 0) {
                isLastPage = true;
            } else if (currentPage <= TOTAL_PAGES) {
                cardAdapter.addLoadingFooter();
            } else isLastPage = true;
        } else {
            cardAdapter.removeLoadingFooter();
            isLoading = false;

            cardAdapter.addAll(FinalList);

            if (currentPage != TOTAL_PAGES) cardAdapter.addLoadingFooter();
            else isLastPage = true;
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    private void hideViews() {
        frameFABLayout.animate().translationY(frameFABLayout.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    private void showViews() {
        frameFABLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                mRecyclerView.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                searchProductsMainLayout.setVisibility(View.GONE);
                frameFABLayout.setVisibility(View.GONE);

                if (dialog != null) {
                    dialog.dismiss();
                    dialog = null;
                }
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                mRecyclerView.setVisibility(View.VISIBLE);
                Main_Layout_NoInternet.setVisibility(View.GONE);
                searchProductsMainLayout.setVisibility(View.VISIBLE);
                frameFABLayout.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);

                getAllFilterData();
                getCategoryForCake();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void displayTuto(String Tag) {
        if (Tag.equals("1")) {
            showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                    .setTargetView(tvFilter)
                    .setBackgroundOverlayColor()
                    .singleUse("5")
                    .setRingColor()
                    .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                    .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_bottom_arrow2), Gravity.BOTTOM)
                    .addCustomView(Gravity.CENTER_HORIZONTAL, "Filter Products !", "Tap here to filter out products which you want !", "Next")
                    .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
            showcaseViewBuilder.show();

            showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcaseViewBuilder.hide();
                    displayTuto("2");
                }
            });
        } else {
            showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                    .setTargetView(floatingActionButton)
                    .setBackgroundOverlayColor()
                    .singleUse("6")
                    .setRingColor()
                    .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                    .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                    .addCustomView(Gravity.CENTER_HORIZONTAL, "Cart !", "Tap here for go to cart page directly!", "Got it")
                    .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
            showcaseViewBuilder.show();

            showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcaseViewBuilder.hide();
                }
            });
        }
    }

    private void clearAllArrayList() {
        /*--------------department--------------------*/
        DepartmentFilter.departmentNameList.clear();
        DepartmentFilter.mChildCheckStates.clear();
        /*--------------Sort--------------------*/
        SortFilter.sortNameList.clear();
        SortFilter.sortRangeSelectedList.clear();
        /*--------------Brand--------------------*/
        BrandFilter.normalBrandNameList.clear();
        BrandFilter.dependentBrandNameList.clear();
        BrandFilter.dependentBrandNameListLastResult.clear();
        /*--------------Price--------------------*/
        PriceFilter.priceRangeSelectedList.clear();
        PriceFilter.priceNameList.clear();
        PriceFilter.discountRangeSelectedList.clear();
        PriceFilter.discountNameList.clear();
        /*--------------Seller--------------------*/
        SellerFilter.sellerSelectedList.clear();
        SellerFilter.sellerNameList.clear();
        /*-------------Other Cake Type------------------*/
        OtherFilter.cakeTypeList.clear();
        /*--------------Other Size -------------------*/
        OtherFilter.categoryBrandDependentSizeNameList.clear();
        OtherFilter.brandDependentSizeNameList.clear();
        OtherFilter.categoryDependentSizeNameList.clear();
        OtherFilter.directNormalSizeNameList.clear();//to be added
    }

    /**
     * When bakery items search any user that time call this methods
     */
    private void getCategoryForCake() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) {
            catList = new ArrayList<>();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("searchKeyword", strSelectedName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetCategories, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    if (response.isNull("posts")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < categoryJsonArray.length(); i++) {
                                JSONObject jsonCatData = categoryJsonArray.getJSONObject(i);
                                catList.add(jsonCatData.getString("cat"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    /**
     * Here we use filter too get all filter data in this methods
     */

    private void getAllFilterData() { //TODO Server method here
        /*if (Connectivity.isConnected(mContext)) {
            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("name", strSelectedName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        */
        String urlAllFilerData = StaticUrl.urlGetAllFilterData +"?city=Pune&area=Hadapser&name="+strSelectedName;
        // JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, StaticUrl.urlGetAllFilterData, params, new Response.Listener<JSONArray>() {
        JsonArrayRequest  request = new JsonArrayRequest(urlAllFilerData, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        Log.d("getAllFilterData",""+response);
                        JSONObject value = response.getJSONObject(0);
                        JSONObject value2 = response.getJSONObject(1);

                        if (value.isNull("seller")) {

                        } else {
                            JSONArray seller = value.getJSONArray("seller");
                            for (int i = 0; i < seller.length(); i++) {
                                JSONObject jsonSellerObject = (JSONObject) seller.get(i);
                                shopNamesList.add(jsonSellerObject.getString("shop_name")); //add to arraylist
                                uniqueShopNames = new TreeSet<>(shopNamesList);
                            }
                        }
                        if (value2.isNull("brand")) {

                        } else {
                            JSONArray brand = value2.getJSONArray("brand");
                            for (int k = 0; k < brand.length(); k++) {
                                JSONObject jsonBrandObject = (JSONObject) brand.get(k);
                                String tp = jsonBrandObject.getString("product_brand");
                                String tp1 = jsonBrandObject.getString("maincategory");

                                if (tp != null && !(tp.equals(""))) {
                                    brandList.add(jsonBrandObject.getString("product_brand")); //add to arraylist
                                }
                                if (tp1 != null && !(tp1.equals(""))) {
                                    categoryList.add(jsonBrandObject.getString("maincategory")); //add to arraylist
                                }
                                if ((jsonBrandObject.has("size")) && (jsonBrandObject.has("sizeIn"))) {
                                    sizeList.add(jsonBrandObject.getString("size"));
                                    sizeParameterList.add(jsonBrandObject.getString("sizeIn"));
                                }
                                uniqueBrandNames = new TreeSet<>(brandList);
                                uniqueMainCategory = new TreeSet<>(categoryList);
                            }
                            if (sizeList.size() == sizeParameterList.size()) {
                                for (int q = 0; q < sizeList.size(); q++) {
                                    allSizeList.add(sizeList.get(q) + sizeParameterList.get(q));
                                }
                            }
                            allSizeList.removeAll(Collections.singleton(""));
                            uniqueSizeList = new TreeSet<>(allSizeList);
                            mainCatArray = new JSONArray(uniqueMainCategory);
                            getSubCategoryResults(mainCatArray);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //AppController.getInstance().setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    //}

    /**
     * Here we get mainCategory related deta fetch from server
     */

    private void getSubCategoryResults(JSONArray category) { //TODO Server method here
        if (Connectivity.isConnected(mContext)) {
            productMainCategory = new ArrayList<>();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("category", category);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetSubCategories, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("", response.toString());
                        JSONArray responceArray =  response.getJSONArray("");

                        JSONObject value = responceArray.getJSONObject(0);
                        JSONObject value1 = responceArray.getJSONObject(1);

                        JSONArray mainCat = value.getJSONArray("maincategory");
                        for (int i = 0; i < mainCat.length(); i++) {
                            JSONObject jsonMainCatObject = (JSONObject) mainCat.get(i);
                            productMainCategory.add(jsonMainCatObject.getString("product_maincat"));
                        }

                        JSONArray ProductCat = value1.getJSONArray("category");
                        for (int j = 0; j < ProductCat.length(); j++) {
                            JSONObject jsonProductCatObject = (JSONObject) ProductCat.get(j);
                            JSONArray myArray = jsonProductCatObject.getJSONArray(productMainCategory.get(j));
                            productCategories = new ArrayList<>();

                            for (int q = 0; q < myArray.length(); q++) {
                                JSONObject myObject = (JSONObject) myArray.get(q);
                                productCategories.add(myObject.getString("product_cat"));
                            }
                            mainCat_CatHashMap.put(productMainCategory.get(j), productCategories);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
           // AppController.getInstance().setPriority(Request.Priority.LOW);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int ITEM = 0;
        private static final int LOADING = 1;
        private boolean isLoadingAdded = false;
        int selectedPosition;
        String selectCondition = "";
        ArrayList<productCodeWithProductList> allProductItemsList;

        public CardAdapter() {
            allProductItemsList = new ArrayList<>();
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            switch (viewType) {
                case ITEM:
                    viewHolder = getViewHolder(parent, inflater);
                    break;
                case LOADING:
                    View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                    viewHolder = new LoadingVH(v2);
                    break;
            }
            return viewHolder;
        }

        @NonNull
        private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
            RecyclerView.ViewHolder viewHolder;
            View v1 = inflater.inflate(R.layout.card_view_for_search_products, parent, false);
            viewHolder = new MainViewHolder(v1);

            v1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                            productCodeWithProductList movie = allProductItemsList.get(itemPosition);
                            ProductCodeWiseProduct forClickEvent = movie.productCodeWiseProducts.get(movie.getPosition());

                            //UserTracking UT = new UserTracking(UserTracking.context);
                            //UT.user_tracking(class_name + ": clicked on view & product is: " + forClickEvent.getProduct_name() + " " + forClickEvent.getProduct_size(), mContext);

                            Intent intent = new Intent(mContext, ProductDetailActivity.class);
                            intent.putExtra("product_name", forClickEvent.getProduct_name());
                            intent.putExtra("shop_id", forClickEvent.getShop_id());
                            startActivity(intent);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(searchProductCoordinateLayout);
                        }
                    } catch (Exception e) {
                            e.printStackTrace();
                    }
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

            switch (getItemViewType(position)) {
                case ITEM:
                    final MainViewHolder viewHolder = (MainViewHolder) holder;

                    final DBHelper db = new DBHelper(mContext);
                    ArrayList<String> sizesArrayList = new ArrayList<>();
                    ArrayList<String> pricesArrayList = new ArrayList<>();
                    ArrayList<String> DiscountpricesArrayList = new ArrayList<>();
                    try {
                        productCodeWithProductList newProductList = allProductItemsList.get(position);
                        selectedPosition = newProductList.getPosition();
                        for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                            ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                            sizesArrayList.add(tp.getProduct_size());
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    try {
                        productCodeWithProductList newProductList = allProductItemsList.get(position);
                        selectedPosition = newProductList.getPosition();
                        for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                            ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                            DiscountpricesArrayList.add(tp.getProduct_price());
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    try {
                        productCodeWithProductList newProductList = allProductItemsList.get(position);
                        selectedPosition = newProductList.getPosition();
                        for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                            ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                            pricesArrayList.add(tp.getProduct_mrp());

                            if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                                viewHolder.LayoutSpinner.setVisibility(View.VISIBLE);
                                CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), pricesArrayList, DiscountpricesArrayList, sizesArrayList, "");
                                viewHolder.spinner.setAdapter(customSpinnerAdapter);
                            } else {
                                viewHolder.LayoutSpinner.setVisibility(View.GONE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*--------------------------------for default value------------------------------*/
                    try {
                        productCodeWithProductList movie = allProductItemsList.get(position);
                        ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);

                        if (tp.getSimilar_product_status().equals("Show")) {
                            viewHolder.similarProductsImg.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.similarProductsImg.setVisibility(View.GONE);
                        }

                        HashMap AddToCartActivityInfo = db.getCartDetails(tp.getProduct_id());
                        String btnCheckStatus = (String) AddToCartActivityInfo.get("new_pid");
                        if (btnCheckStatus == null) {
                            viewHolder.btnAddToCartActivity.setText("Add To Cart");
                        } else {
                            viewHolder.btnAddToCartActivity.setText("Go To Cart");
                        }

                        HashMap AddToCartActivityInfo1 = db.getWishListDetails(tp.getProduct_id());
                        String btnCheckStatus1 = (String) AddToCartActivityInfo1.get("pId");
                        if (btnCheckStatus1 == null) {
                            viewHolder.imgWishList.setVisibility(View.VISIBLE);
                            viewHolder.imgWishListSelected.setVisibility(View.GONE);
                        } else {
                            viewHolder.imgWishList.setVisibility(View.GONE);
                            viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                        }

                        if (tp.getStrHindiName().equals("")) {
                            viewHolder.tvProductHindiName.setVisibility(View.GONE);
                        } else {
                            viewHolder.tvProductHindiName.setVisibility(View.VISIBLE);
                            viewHolder.tvProductHindiName.setText(tp.getStrHindiName());
                        }

                        switch (tp.getSelect_type()) {
                            case "Size":
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.VISIBLE);
                                break;
                            case "Condition":
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                            default:
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                        }

                        //TODO here product image setup to glide
                       /* Glide.with(mContext).load(ApplicationUrlAndConstants.IMAGE_URL + tp.getProduct_image())
                                .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                                .error(R.drawable.ic_app_transparent)
                                .fitCenter()
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(viewHolder.img);*/
                        Picasso.with(mContext).load(StaticUrl.IMAGE_URL + tp.getProduct_image())
                                .placeholder(R.drawable.loading)
                                .error(R.drawable.ic_app_transparent)
                                .into(viewHolder.img);

                        Log.e("SearchPorductImgUrl:",""+StaticUrl.IMAGE_URL + tp.getProduct_image());
                        viewHolder.tvSellerName.setText("By:" + " " + tp.getShop_name());
                        //viewHolder.tvSellerName.setText("Category:" + " " + tp.getStrProduct_maincat());
                        String size = tp.getProduct_size();
                        viewHolder.tvProductName.setText(tp.getProduct_name() + " " + size);
                        String dis = tp.getProduct_discount();
                        String discount = tp.setProduct_discount(dis);
                        viewHolder.tvDiscount.setText(discount + "% OFF");
                        String productMrp = tp.getProduct_mrp();
                        String discountPrice = tp.getProduct_price();
                        String QTY = tp.getProduct_qty();

                        if (QTY.equals("0")) {
                            viewHolder.btnAddToCartActivity.setVisibility(View.GONE);
                            viewHolder.imgOutOfStock.setVisibility(View.VISIBLE);

                            //TODO here out of stock GIF image setup to glide
                            /*Glide.with(mContext)
                                    .load(R.drawable.outofstock)
                                    .error(R.drawable.icerror_outofstock)
                                    .into(viewHolder.imgOutOfStock);*/
                            Picasso.with(mContext)
                                    .load(R.drawable.outofstock)
                                    .placeholder(R.drawable.loading)
                                    .error(R.drawable.icerror_outofstock)
                                    .into(viewHolder.imgOutOfStock);
                        } else {
                            viewHolder.btnAddToCartActivity.setVisibility(View.VISIBLE);
                            viewHolder.imgOutOfStock.setVisibility(View.GONE);
                        }

                        if (productMrp.equals(discountPrice)) {
                            viewHolder.tvPrice.setVisibility(View.VISIBLE);
                            viewHolder.frameimgMsg.setVisibility(View.GONE);
                            viewHolder.tvMrp.setVisibility(View.GONE);
                        } else {
                            viewHolder.tvPrice.setVisibility(View.VISIBLE);
                            viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                            viewHolder.tvMrp.setVisibility(View.VISIBLE);
                            viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                        }
                        viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                        viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                        viewHolder.tvRipeType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvRipeType.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvRawType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvRawType.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvSmallSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvSmallSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvMediumSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvMediumSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvLargeSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvLargeSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    viewHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {

                            try {
                                if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);

                                    movie.setPosition(pos); //set here position when user any wants to buy multiple size products

                                    if (tp.getSimilar_product_status().equals("Show")) {
                                        viewHolder.similarProductsImg.setVisibility(View.VISIBLE);
                                    } else {
                                        viewHolder.similarProductsImg.setVisibility(View.GONE);
                                    }

                                    switch (tp.getSelect_type()) {
                                        case "Size":
                                            viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                            viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                            viewHolder.linearLayoutSelectSize.setVisibility(View.VISIBLE);
                                            break;
                                        case "Condition":
                                            viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                            viewHolder.linearLayoutSelectType.setVisibility(View.VISIBLE);
                                            viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                            break;
                                        default:
                                            viewHolder.linearLayoutSelectCondition.setVisibility(View.GONE);
                                            viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                            viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                            break;
                                    }

                                    viewHolder.img.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                                dialogPlus = DialogPlus.newDialog(mContext)
                                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                                        .setGravity(Gravity.CENTER)
                                                        .create();

                                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);

                                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                                //TODO here when user clicks on product image setup to glide
                                                /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                                                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                                                        .error(R.drawable.ic_app_transparent)
                                                        .fitCenter()
                                                        .crossFade()
                                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                        .into(alertProductImage1);*/
                                                Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                                        .placeholder(R.drawable.loading)
                                                        .error(R.drawable.ic_app_transparent)
                                                        .into(alertProductImage1);
                                                Log.e("SimgPath","https://simg.picodel.com/"+tp.getProduct_image());
                                                dialogPlus.show();
                                            } else {
                                                GateWay gateWay = new GateWay(mContext);
                                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                                            }
                                        }
                                    });

                                    String QTY = tp.getProduct_qty();
                                    if (QTY.equals("0")) {
                                        viewHolder.btnAddToCartActivity.setVisibility(View.GONE);
                                        viewHolder.imgOutOfStock.setVisibility(View.VISIBLE);

                                        //TODO here out of stock GIF image setup to glide
                                        /*Glide.with(mContext)
                                                .load(R.drawable.outofstock)
                                                .error(R.drawable.icerror_outofstock)
                                                .into(viewHolder.imgOutOfStock);*/
                                        Picasso.with(mContext)
                                                .load(R.drawable.outofstock)
                                                .placeholder(R.drawable.loading)
                                                .error(R.drawable.icerror_outofstock)
                                                .into(viewHolder.imgOutOfStock);

                                    } else {
                                        viewHolder.btnAddToCartActivity.setVisibility(View.VISIBLE);
                                        viewHolder.imgOutOfStock.setVisibility(View.GONE);
                                    }

                                    if (tp.getStrHindiName().equals("")) {
                                        viewHolder.tvProductHindiName.setVisibility(View.GONE);
                                    } else {
                                        viewHolder.tvProductHindiName.setVisibility(View.VISIBLE);
                                        viewHolder.tvProductHindiName.setText(tp.getStrHindiName());
                                    }

                                    //TODO here product image setup to glide when user select different product variant from spinner
                                    /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                                            .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                                            .error(R.drawable.ic_app_transparent)
                                            .fitCenter()
                                            .crossFade()
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(viewHolder.img);*/

                                    Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                            .placeholder(R.drawable.loading)
                                            .error(R.drawable.ic_app_transparent)
                                            .into(viewHolder.img);
                                    Log.e("SimgPath","https://simg.picodel.com/"+tp.getProduct_image());

                                    String size = tp.getProduct_size();
                                    viewHolder.tvProductName.setText(tp.getProduct_name() + " " + size);
                                    String dis = tp.getProduct_discount();
                                    String discount = tp.setProduct_discount(dis);
                                    viewHolder.tvDiscount.setText(discount + "% OFF");
                                    String productMrp = tp.getProduct_mrp();
                                    String discountPrice = tp.getProduct_price();
                                    if (productMrp.equals(discountPrice)) {
                                        viewHolder.tvPrice.setVisibility(View.VISIBLE);
                                        viewHolder.frameimgMsg.setVisibility(View.GONE);
                                        viewHolder.tvMrp.setVisibility(View.GONE);
                                    } else {
                                        viewHolder.tvPrice.setVisibility(View.VISIBLE);
                                        viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                                        viewHolder.tvMrp.setVisibility(View.VISIBLE);
                                        viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                                    }
                                    viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                                    viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                                    HashMap AddToCartActivityInfo = db.getCartDetails(tp.getProduct_id());
                                    String btnCheckStatus = (String) AddToCartActivityInfo.get("new_pid");
                                    if (btnCheckStatus == null) {
                                        tp.setStatus(false);
                                        viewHolder.btnAddToCartActivity.setText("Add To Cart");
                                    } else {
                                        tp.setStatus(true);
                                        if (QTY.equals("0")) {
                                            viewHolder.btnAddToCartActivity.setText("Add To Cart");
                                        } else {
                                            viewHolder.btnAddToCartActivity.setText("Go To Cart");
                                        }
                                    }

                                    HashMap wishListInfo;
                                    String str_PIdForChange = tp.getProduct_id();
                                    wishListInfo = db.getWishListDetails(str_PIdForChange);

                                    List<String> listProductNameAlreadyHave = new ArrayList<>(wishListInfo.values());
                                    boolean val = listProductNameAlreadyHave.contains(tp.getProduct_id());
                                    if (val) {
                                        viewHolder.imgWishList.setVisibility(View.GONE);
                                        viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                                    } else {
                                        viewHolder.imgWishList.setVisibility(View.VISIBLE);
                                        viewHolder.imgWishListSelected.setVisibility(View.GONE);
                                    }

                                    viewHolder.tvRipeType.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            selectCondition = viewHolder.tvRipeType.getText().toString();
                                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                            ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                            viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                        }
                                    });

                                    viewHolder.tvRawType.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            selectCondition = viewHolder.tvRawType.getText().toString();
                                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                            ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                            viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                        }
                                    });

                                    viewHolder.tvSmallSize.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            selectCondition = viewHolder.tvSmallSize.getText().toString();
                                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                            ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                            viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                        }
                                    });

                                    viewHolder.tvMediumSize.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            selectCondition = viewHolder.tvMediumSize.getText().toString();
                                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                            ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                            viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                        }
                                    });

                                    viewHolder.tvLargeSize.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            selectCondition = viewHolder.tvLargeSize.getText().toString();
                                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                            ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                            viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                        }
                                    });
                                } else {
                                    GateWay gateWay = new GateWay(mContext);
                                    gateWay.displaySnackBar(searchProductCoordinateLayout);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    viewHolder.img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                dialogPlus = DialogPlus.newDialog(mContext)
                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .create();

                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);

                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                //TODO here when user clicks on product image setup to glide
                                /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(alertProductImage1);*/

                                Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(alertProductImage1);

                                dialogPlus.show();
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                            }
                        }
                    });

                    viewHolder.imgWishList.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                selectedPosition = movie.getPosition();
                                ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                                //UserTracking UT = new UserTracking(UserTracking.context);
                                //UT.user_tracking(class_name + ": clicked on add wishlist & product is: " + forWishList.getProduct_name() + " " + forWishList.getProduct_size(), mContext);

                                HashMap wishListHashMap;
                                pId = forWishList.getProduct_id();

                                //uniqueWishList.add(pId); //here productId add in the arrayList for img wishList
                                wishListHashMap = db.getWishListDetails(pId);
                                strId = (String) wishListHashMap.get("pId");
                                //WishList.wishlist1.add(pId);

                                if (strId == null) {
                                    db.insertProductIntoWishList(pId);
                                    int cnt = (int) db.fetchWishListCount();
                                    updateWishListCount(cnt);

                                    HashMap infoChangeButtonName;
                                    String pIdForButtonChange = forWishList.getProduct_id();
                                    infoChangeButtonName = db.getWishListDetails(pIdForButtonChange);

                                    List<String> listProductNameAlreadyHave = new ArrayList<>(infoChangeButtonName.values());
                                    boolean val = listProductNameAlreadyHave.contains(pId);

                                    if (val) {
                                        viewHolder.imgWishList.setVisibility(View.GONE);
                                        viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                                    } else {
                                        viewHolder.imgWishList.setVisibility(View.VISIBLE);
                                    }
                                    if (Connectivity.isConnected(mContext)) {
                                        addWishListItemsToServer();
                                    } else {
                                        GateWay gateWay = new GateWay(mContext);
                                        gateWay.displaySnackBar(searchProductCoordinateLayout);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        private void addWishListItemsToServer() { //TODO Server method here
                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                            pId = forWishList.getProduct_id();
                            shopId = forWishList.getShop_id();
                            if (Connectivity.isConnected(mContext)) {
                                JSONObject params = new JSONObject();
                                try {
                                    params.put("product_id", pId);
                                    params.put("shop_id", shopId);
                                    params.put("versionCode", StaticUrl.versionName);
                                    params.put("Qty", 1);
                                    params.put("contactNo", strContact);
                                    params.put("email", strEmail);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAddMyWishListItems, params, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();

                                        GateWay gateWay = new GateWay(mContext);
                                        gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                    }
                                });
                                VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                            }
                        }
                    });

                    viewHolder.imgWishListSelected.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                selectedPosition = movie.getPosition();
                                ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                                //UserTracking UT = new UserTracking(UserTracking.context);
                                //UT.user_tracking(class_name + ": clicked on remove wishlist & product is: " + forWishList.getProduct_name() + " " + forWishList.getProduct_size(), mContext);

                                pId = forWishList.getProduct_id();
                                db.deleteWishListProductItem(pId);
                                int cnt = (int) db.fetchWishListCount();
                                updateWishListCount(cnt);
                                //uniqueWishList.remove(pId);
                                //WishList.wishlist1.remove(pId);
                                viewHolder.imgWishList.setVisibility(View.VISIBLE);
                                viewHolder.imgWishListSelected.setVisibility(View.GONE);
                                deleteoneProductFromWishlist(pId);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        private void deleteoneProductFromWishlist(String pid) { //TODO Server method here
                            if (Connectivity.isConnected(mContext)) {
                                JSONObject params = new JSONObject();
                                try {
                                    params.put("contactNo", strContact);
                                    params.put("email", strEmail);
                                    params.put("product_id", pid);
                                    params.put("tag", "delete_one_item");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteMyWishListItems, params, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (response.isNull("posts")) {
                                        } else {
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();

                                        GateWay gateWay = new GateWay(mContext);
                                        gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                    }
                                });
                                VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                            }
                        }
                    });

                    viewHolder.btnAddToCartActivity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                    selectedPosition = movie.getPosition();
                                    final ProductCodeWiseProduct forAddToCartActivity = movie.productCodeWiseProducts.get(selectedPosition);
                                    final String mainCategroy = movie.productCodeWiseProducts.get(selectedPosition).getStrProduct_maincat();

                                   // UserTracking UT = new UserTracking(UserTracking.context);
                                   // UT.user_tracking(class_name + ": clicked on add to cart & product is: " + forAddToCartActivity.getProduct_name() + " " + forAddToCartActivity.getProduct_size(), mContext);

                                    shopId = forAddToCartActivity.getShop_id();
                                    pId = forAddToCartActivity.getProduct_id();

                                    HashMap AddToCartActivityInfo;
                                    AddToCartActivityInfo = db.getCartDetails(pId);
                                    strId = (String) AddToCartActivityInfo.get("new_pid");

                                    if (strId == null) { //TODO Server method here
                                        forAddToCartActivity.setStatus(false);

                                        pName = viewHolder.tvProductName.getText().toString();
                                        product_Size = forAddToCartActivity.getProduct_size();

                                        JSONObject params = new JSONObject();
                                        try {
                                            params.put("product_id", pId);
                                            params.put("shop_id", shopId);
                                            params.put("size", product_Size);
                                            params.put("qty", 1);
                                            params.put("contactNo", strContact);
                                            params.put("email", strEmail);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                if (response.isNull("posts")) {
                                                } else {
                                                    try {
                                                        strCheckQty = response.getString("posts");
                                                        if (strCheckQty.equals("true")) {
                                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                                                            alertDialogBuilder.setMessage("You cannot add more than 1000 gm of this product.");
                                                            alertDialogBuilder.setPositiveButton("OK",
                                                                    new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                            dialogInterface.dismiss();
                                                                        }
                                                                    });
                                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                                            alertDialog.show();
                                                        } else {
                                                            /*
                                                            SyncData();
                                                            if (cnt >= 0) {

                                                            updateAddToCartActivityCount(cnt);*/
                                                            db.insertCount(pId);
                                                            int cnt = (int) db.fetchAddToCartCount();
                                                            updateAddToCartActivityCount(cnt);

                                                            if (cnt >= 0) {

                                                                forAddToCartActivity.setStatus(true);
                                                                // if (forAddToCartActivity.isStatus(true)) {
                                                                //viewHolder.btnAddToCartActivity.setText("Go To Cart");
                                                                // }
                                                            } else {
                                                                forAddToCartActivity.setStatus(false);
                                                                // if (forAddToCartActivity.isStatus(false)) {
                                                                //viewHolder.btnAddToCartActivity.setText("Add To Cart");
                                                                // }
                                                            }


                                                            addCartItemsToServer(mainCategroy);
                                                            /*//check sessoin main category
                                                            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
                                                            String mainCat = sharedPreferencesUtils.getCategory();
                                                            if(mainCategroy.equalsIgnoreCase(mainCat)) {
                                                                Log.d("SearchProduct","if "+mainCategroy+" "+mainCat+" ");

                                                                //set session for category
                                                                sharedPreferencesUtils.setCategory(mainCategroy);
                                                            }else{
                                                                Log.d("SearchProduct",mainCategroy+" "+mainCat+" ");
                                                            }*/
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                error.printStackTrace();

                                                GateWay gateWay = new GateWay(mContext);
                                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                            }
                                        });
                                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                                    } else {
                                        //UserTracking UT1 = new UserTracking(UserTracking.context);
                                        //UT1.user_tracking(class_name + ": clicked on remove from cart & product is: " + forAddToCartActivity.getProduct_name() + " " + forAddToCartActivity.getProduct_size(), mContext);

                                        viewHolder.btnAddToCartActivity.setText("Go To Cart");
                                        deleteCartItemDialog(pId);
                                    }
                                } else {
                                    GateWay gateWay = new GateWay(mContext);
                                    gateWay.displaySnackBar(searchProductCoordinateLayout);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        private void deleteCartItemDialog(final String pId) {
                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            ProductCodeWiseProduct forAddToCartActivity = movie.productCodeWiseProducts.get(selectedPosition);

                            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                            View review = layoutInflater.inflate(R.layout.same_shop_cart, null);
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                            alertDialogBuilder.setView(review);

                            tvMessage = review.findViewById(R.id.txtMessage);
                            tvYes = review.findViewById(R.id.btnYes);
                            tvNo = review.findViewById(R.id.btnNo);
                            tvMessage.setText("Product is " + forAddToCartActivity.getProduct_name() + ". Do you want remove or Go To Cart.");
                            // set dialog message
                            alertDialogBuilder.setCancelable(true);
                            // create alert dialog
                            alertDialog = alertDialogBuilder.create();
                            // show it
                            alertDialog.show();

                            tvNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    deleteNormalProductFromCartItem(pId);
                                }
                            });

                            tvYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Intent intent = new Intent(mContext, AddToCartActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }

                        private void deleteNormalProductFromCartItem(String p_id) { //TODO Server method here
                            final GateWay gateWay = new GateWay(mContext);
                            gateWay.progressDialogStart();

                            final DBHelper db = new DBHelper(mContext);

                            if (Connectivity.isConnected(mContext)) {
                                JSONObject params = new JSONObject();
                                try {
                                    params.put("product_id", p_id);
                                    params.put("contactNo", strContact);
                                    params.put("email", strEmail);
                                    params.put("tag", "delete_one_item");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (response.isNull("posts")) {
                                        } else {
                                            db.deleteProductItem(pId);
                                            //AddToCartActivityArrayList.remove(pId);
                                            viewHolder.btnAddToCartActivity.setText("Add To Cart");
                                        }
                                        count = SyncData();
                                        //count =(int) db.fetchAddToCartCount();
                                        updateAddToCartActivityCount(count);
                                        gateWay.progressDialogStop();
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        gateWay.progressDialogStop();

                                        error.printStackTrace();

                                        GateWay gateWay = new GateWay(mContext);
                                        gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                    }
                                });
                                VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                            } else {
                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                            }
                        }

                        private void addCartItemsToServer(String maincategroy) { //TODO Server method here
                            JSONObject params = new JSONObject();
                            if (pName.contains("Small")) {
                                selectCondition = "Small";
                            } else if (pName.contains("Large")) {
                                selectCondition = "Large";
                            } else if (pName.contains("Medium")) {
                                selectCondition = "Medium";
                            } else if (pName.contains("Ripe")) {
                                selectCondition = "Ripe";
                            } else if (pName.contains("Raw")) {
                                selectCondition = "Raw";
                            } else {
                                selectCondition = "";
                            }
                            try {
                                params.put("product_id", pId);
                                params.put("shop_id", shopId);
                                params.put("selectedType", selectCondition);
                                params.put("versionCode", StaticUrl.versionName);
                                params.put("Qty", 1);
                                params.put("contactNo", strContact);
                                params.put("email", strEmail);
                                params.put("sessionMainCat", maincategroy);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("addCartItemsToServer"," "+params);
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {

                                    Log.d("addCartItemsR",""+response);

                                    try {
                                        String posts = response.getString("posts");

                                        if (posts.equals("true")){
                                            db.insertCount(pId);
                                            int cnt = SyncData();//(int) db.fetchAddToCartCount();
                                            updateAddToCartActivityCount(cnt);

                                            if (cnt >= 0) {
                                                // forAddToCartActivity.setStatus(true);
                                                //if (forAddToCartActivity.isStatus(true)) {
                                                viewHolder.btnAddToCartActivity.setText("Go To Cart");
                                                // }
                                            } else {
                                                // forAddToCartActivity.setStatus(false);
                                                // if (forAddToCartActivity.isStatus(false)) {
                                                viewHolder.btnAddToCartActivity.setText("Add To Cart");
                                                // }
                                            }

                                            Toast toast = Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_LONG);
                                            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                            toast.show();
                                        }else {
                                            openSessionDialog(posts);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();

                                    GateWay gateWay = new GateWay(mContext);
                                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                }
                            });
                            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                        }
                    });

                    viewHolder.similarProductsImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                selectedPosition = movie.getPosition();
                                ProductCodeWiseProduct forSimilarProduct = movie.productCodeWiseProducts.get(selectedPosition);
                                String pId = forSimilarProduct.getProduct_id();

                                //UserTracking UT = new UserTracking(UserTracking.context);
                                //UT.user_tracking(class_name + ": clicked on similar button & product is: " + forSimilarProduct.getProduct_name() + " " + forSimilarProduct.getProduct_size(), mContext);

                                gateWay = new GateWay(mContext);
                                gateWay.progressDialogStart();

                                dialog = DialogPlus.newDialog(mContext)
                                        .setContentHolder(new ViewHolder(R.layout.activity_similar_products))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.BOTTOM)
                                        .create();
                                similarProductLinearLayout = (LinearLayout) dialog.findViewById(R.id.similarProductLinearLayout);
                                similarProductRecyclerView = (RecyclerView) dialog.findViewById(R.id.similarProductRecyclerView);
                                similarProductRecyclerView.setHasFixedSize(true);
                                similarLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                                similarProductRecyclerView.setLayoutManager(similarLayoutManager);
                                similarProductsGetFromServer(pId);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(v);
                            }
                        }

                        private void similarProductsGetFromServer(String pId) { //TODO Server method here
                            JSONObject params = new JSONObject();
                            try {
                                params.put("city", strCity);
                                params.put("area", strArea);
                                params.put("product_id", pId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlSimilarResult, params, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        if (response.isNull("posts")) {
                                            similarProductLinearLayout.setVisibility(View.GONE);
                                        } else {
                                            similarProductListHashMap.clear();
                                            similarProductWiseList.clear();
                                            SimilarFinalList.clear();

                                            similarProductLinearLayout.setVisibility(View.VISIBLE);
                                            JSONArray mainShopJsonArray = response.getJSONArray("posts");

                                            for (int i = 0; i < mainShopJsonArray.length(); i++) {
                                                JSONObject jSonShopData = mainShopJsonArray.getJSONObject(i);

                                                similarProductListHashMap.put(jSonShopData.getString("code"), "");

                                                similarProductWiseList.add(new ProductCodeWiseProduct(jSonShopData.getString("code"), jSonShopData.getString("shop_id"),
                                                        jSonShopData.getString("shop_name"), jSonShopData.getString("shop_category"),
                                                        jSonShopData.getString("product_name"), jSonShopData.getString("product_brand"),
                                                        jSonShopData.getString("product_id"), jSonShopData.getString("product_image"),
                                                        jSonShopData.getString("product_image1"), jSonShopData.getString("product_image2"),
                                                        jSonShopData.getString("product_image3"), jSonShopData.getString("product_image4"),
                                                        jSonShopData.getString("product_size"), jSonShopData.getString("product_mrp"),
                                                        jSonShopData.getString("product_price"), jSonShopData.getString("product_discount"),
                                                        jSonShopData.getString("product_description"), jSonShopData.getString("similar_product_status"), jSonShopData.getString("type"),
                                                        jSonShopData.getString("product_qty"), jSonShopData.getString("hindi_name"),jSonShopData.getString("maincategory")));
                                            }
                                            dialog.show();
                                            for (Object o : similarProductListHashMap.keySet()) {
                                                String key = (String) o;
                                                productCodeWithProductList withProductCode = new productCodeWithProductList();
                                                withProductCode.code = key;
                                                withProductCode.similarProductCodeWiseProducts = new ArrayList<>();
                                                for (ProductCodeWiseProduct pp : similarProductWiseList) {
                                                    if (pp.code.equals(key)) {
                                                        withProductCode.similarProductCodeWiseProducts.add(pp);
                                                    }
                                                }
                                                SimilarFinalList.add(withProductCode);
                                            }
                                            similarProductCardAdapter = new SimilarProductCardAdapter(SimilarFinalList);
                                            similarProductRecyclerView.setAdapter(similarProductCardAdapter);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    gateWay.progressDialogStop();
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();

                                    gateWay.progressDialogStop();

                                    GateWay gateWay = new GateWay(mContext);
                                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                }
                            });
                            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                        }
                    });

                    break;
                case LOADING:
                    //Do nothing
                    break;
            }
        }

        void addAll(List<productCodeWithProductList> mcList) {
            for (productCodeWithProductList mc : mcList) {
                add(mc);
            }
        }

        public void add(productCodeWithProductList mc) {
            allProductItemsList.add(mc);
            notifyItemInserted(allProductItemsList.size() - 1);
        }

        @Override
        public int getItemCount() {
            return allProductItemsList == null ? 0 : allProductItemsList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return (position == allProductItemsList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }

        void addLoadingFooter() {
            isLoadingAdded = true;
            add(new productCodeWithProductList());
        }

        void removeLoadingFooter() {
            isLoadingAdded = false;

            int position = allProductItemsList.size() - 1;
            productCodeWithProductList item = getItem(position);

            if (item != null) {
                allProductItemsList.remove(position);
                notifyItemRemoved(position);
            }
        }

        public productCodeWithProductList getItem(int position) {
            return allProductItemsList.get(position);
        }

        void clearApplications() {
            int size = this.allProductItemsList.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    allProductItemsList.remove(0);
                }

                this.notifyItemRangeRemoved(0, size);
            }
        }

        class LoadingVH extends RecyclerView.ViewHolder {

            LoadingVH(View itemView) {
                super(itemView);
            }
        }

        class MainViewHolder extends RecyclerView.ViewHolder {

            final ImageView img;
            TextView btnAddToCartActivity;
            final ImageView imgWishList;
            final ImageView imgWishListSelected;
            final ImageView similarProductsImg;
            final ImageView imgOutOfStock;
            final TextView tvProductName;
            final TextView tvSellerName;
            final TextView tvMrp;
            final TextView tvDiscount;
            final TextView tvPrice;
            final TextView tvRipeType;
            final TextView tvRawType;
            final TextView tvSmallSize;
            final TextView tvMediumSize;
            final TextView tvLargeSize;
            final TextView tvProductHindiName;
            final RelativeLayout linearLayoutProduct;
            final CardView cardView;
            final LinearLayout linearLayoutSelectCondition;
            final LinearLayout linearLayoutSelectType;
            final LinearLayout linearLayoutSelectSize;
            final Spinner spinner;
            final RelativeLayout LayoutSpinner;
            final FrameLayout frameimgMsg;

            public MainViewHolder(View itemView) {
                super(itemView);

                cardView = itemView.findViewById(R.id.view);
                img = itemView.findViewById(R.id.imgProduct);
                tvProductName = itemView.findViewById(R.id.txtProductName);
                tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
                tvSellerName = itemView.findViewById(R.id.txtSellerName);
                tvMrp = itemView.findViewById(R.id.txtTotal);
                tvDiscount = itemView.findViewById(R.id.txtDiscount);
                tvPrice = itemView.findViewById(R.id.txtPrice);
                tvRipeType = itemView.findViewById(R.id.txtRipeType);
                tvRawType = itemView.findViewById(R.id.txtRawType);
                tvSmallSize = itemView.findViewById(R.id.txtSmallSize);
                tvMediumSize = itemView.findViewById(R.id.txtMediumSize);
                tvLargeSize = itemView.findViewById(R.id.txtLargeSize);
                btnAddToCartActivity = itemView.findViewById(R.id.btnAddToCart);
                imgWishList = itemView.findViewById(R.id.wishList);
                imgWishListSelected = itemView.findViewById(R.id.wishList1);
                linearLayoutProduct = itemView.findViewById(R.id.linearLayoutProduct);
                similarProductsImg = itemView.findViewById(R.id.similarProductImage);
                linearLayoutSelectCondition = itemView.findViewById(R.id.selectCondition);
                linearLayoutSelectType = itemView.findViewById(R.id.linearLayoutSelectType);
                linearLayoutSelectSize = itemView.findViewById(R.id.linearLayoutSelectSize);
                imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
                spinner = itemView.findViewById(R.id.spinner);
                LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
                frameimgMsg = itemView.findViewById(R.id.frameimgMsg);
                btnAddToCartActivity.setTag(this);
                imgWishList.setTag(this);
                imgWishListSelected.setTag(this);
                linearLayoutProduct.setTag(this);
                similarProductsImg.setTag(this);
            }
        }

        private class SimilarProductCardAdapter extends RecyclerView.Adapter<SimilarProductCardAdapter.SimilarProductsViewHolder> {

            List<productCodeWithProductList> mItems;

            SimilarProductCardAdapter(ArrayList<productCodeWithProductList> finalList) {
                this.mItems = finalList;
            }

            @NonNull
            @Override
            public SimilarProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_for_ultimate_products, parent, false);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                int itemPosition = similarProductRecyclerView.getChildAdapterPosition(v);
                                productCodeWithProductList movie = mItems.get(itemPosition);
                                ProductCodeWiseProduct forClickEvent = movie.similarProductCodeWiseProducts.get(movie.getPosition());

                                //UserTracking UT = new UserTracking(UserTracking.context);
                                //UT.user_tracking(class_name + ": clicked on similar product view & product is: " + forClickEvent.getProduct_name() + " " + forClickEvent.getProduct_size(), mContext);

                                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                                intent.putExtra("product_name", forClickEvent.getProduct_name());
                                intent.putExtra("shop_id", forClickEvent.getShop_id());
                                startActivity(intent);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                return new SimilarProductsViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull final SimilarProductsViewHolder holder, int position) {
                final DBHelper db = new DBHelper(mContext);

                ArrayList<String> sizesArrayList = new ArrayList<>();
                ArrayList<String> pricesArrayList = new ArrayList<>();
                ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

                try {
                    productCodeWithProductList newProductList = mItems.get(position);
                    selectedPosition = newProductList.getPosition();
                    for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                        ProductCodeWiseProduct tp1 = newProductList.similarProductCodeWiseProducts.get(l);
                        sizesArrayList.add(tp1.getProduct_size());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                try {
                    productCodeWithProductList newProductList = mItems.get(position);
                    selectedPosition = newProductList.getPosition();
                    for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                        ProductCodeWiseProduct tp3 = newProductList.similarProductCodeWiseProducts.get(l);
                        DiscountpricesArrayList.add(tp3.getProduct_price());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                try {
                    productCodeWithProductList newProductList = mItems.get(position);
                    selectedPosition = newProductList.getPosition();
                    for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                        ProductCodeWiseProduct tp2 = newProductList.similarProductCodeWiseProducts.get(l);
                        pricesArrayList.add(tp2.getProduct_mrp());

                        if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                            holder.LayoutSpinner.setVisibility(View.VISIBLE);
                            CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(mContext, pricesArrayList, DiscountpricesArrayList, sizesArrayList, "homepage");
                            holder.spinner.setAdapter(customSpinnerAdapter);
                        } else {
                            holder.LayoutSpinner.setVisibility(View.INVISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*--------------------------------for default value------------------------------*/
                try {
                    productCodeWithProductList movie = mItems.get(position);
                    ProductCodeWiseProduct tp = movie.similarProductCodeWiseProducts.get(0);

                    if (tp.getStrHindiName().equals("")) {
                        holder.tvProductHindiName.setVisibility(View.GONE);
                    } else {
                        holder.tvProductHindiName.setVisibility(View.VISIBLE);
                        holder.tvProductHindiName.setText(tp.getStrHindiName());
                    }

                    HashMap AddToCartActivityInfo = db.getCartDetails(tp.getProduct_id());
                    String btnCheckStatus = (String) AddToCartActivityInfo.get("new_pid");
                    if (btnCheckStatus == null) {
                        holder.btnAddToCartActivity.setText("Add To Cart");
                    } else {
                        holder.btnAddToCartActivity.setText("Go To Cart");
                    }

                    //TODO here similar product image setup to glide
                    /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                            .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.imgProduct);*/

                    Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder.imgProduct);
                    Log.e("SimgPath","https://simg.picodel.com/"+tp.getProduct_image());

                    String size = tp.getProduct_size();
                    holder.tvProductName.setText(tp.getProduct_name() + " " + size);
                    String dis = tp.getProduct_discount();
                    String discount = tp.setProduct_discount(dis);
                    holder.tvDiscount.setText(discount + "% OFF");
                    String productMrp = tp.getProduct_mrp();
                    String discountPrice = tp.getProduct_price();
                    String QTY = tp.getProduct_qty();

                    if (QTY.equals("0")) {
                        holder.btnAddToCartActivity.setVisibility(View.GONE);
                        holder.imgOutOfStock.setVisibility(View.VISIBLE);

                        //TODO here out of stock GIF image setup to glide
                        /*Glide.with(mContext)
                                .load(R.drawable.outofstock)
                                .error(R.drawable.icerror_outofstock)
                                .into(holder.imgOutOfStock);*/
                        Picasso.with(mContext)
                                .load(R.drawable.outofstock)
                                .placeholder(R.drawable.loading)
                                .error(R.drawable.icerror_outofstock)
                                .into(holder.imgOutOfStock);
                    } else {
                        holder.btnAddToCartActivity.setVisibility(View.VISIBLE);
                        holder.imgOutOfStock.setVisibility(View.GONE);
                    }

                    if (productMrp.equals(discountPrice)) {
                        holder.tvPrice.setVisibility(View.VISIBLE);
                        holder.tvDiscount.setVisibility(View.INVISIBLE);
                        holder.tvMrp.setVisibility(View.GONE);
                    } else {
                        holder.tvPrice.setVisibility(View.VISIBLE);
                        holder.tvDiscount.setVisibility(View.VISIBLE);
                        holder.tvMrp.setVisibility(View.VISIBLE);
                        holder.tvMrp.setBackgroundResource(R.drawable.dash);
                    }
                    holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                    holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
                    /*--------------------------------for default value------------------------------*/

                    holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                            if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                try {
                                    productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                                    ProductCodeWiseProduct tp = movie.similarProductCodeWiseProducts.get(pos);

                                    movie.setPosition(pos); //set here position when user any wants to buy multiple size products

                                    if (tp.getStrHindiName().equals("")) {
                                        holder.tvProductHindiName.setVisibility(View.GONE);
                                    } else {
                                        holder.tvProductHindiName.setVisibility(View.VISIBLE);
                                        holder.tvProductHindiName.setText(tp.getStrHindiName());
                                    }

                                    //TODO here product image setup to glide when user select different product variant from spinner
                                    /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                                            .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                                            .error(R.drawable.ic_app_transparent)
                                            .fitCenter()
                                            .crossFade()
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(holder.imgProduct);*/

                                    Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                            .placeholder(R.drawable.loading)
                                            .error(R.drawable.ic_app_transparent)
                                            .into(holder.imgProduct);
                                    Log.e("SimgPath","https://simg.picodel.com/"+tp.getProduct_image());
                                    String size = tp.getProduct_size();
                                    holder.tvProductName.setText(tp.getProduct_name() + " " + size);
                                    String dis = tp.getProduct_discount();
                                    String discount = tp.setProduct_discount(dis);
                                    holder.tvDiscount.setText(discount + "% OFF");
                                    String productMrp = tp.getProduct_mrp();
                                    String discountPrice = tp.getProduct_price();
                                    String QTY = tp.getProduct_qty();

                                    if (QTY.equals("0")) {
                                        holder.btnAddToCartActivity.setVisibility(View.GONE);
                                        holder.imgOutOfStock.setVisibility(View.VISIBLE);

                                        //TODO here out of stock GIF image setup to glide
                                        /*Glide.with(mContext)
                                                .load(R.drawable.outofstock)
                                                .error(R.drawable.icerror_outofstock)
                                                .into(holder.imgOutOfStock);*/
                                        Picasso.with(mContext)
                                                .load(R.drawable.outofstock)
                                                .placeholder(R.drawable.loading)
                                                .error(R.drawable.icerror_outofstock)
                                                .into(holder.imgOutOfStock);

                                    } else {
                                        holder.btnAddToCartActivity.setVisibility(View.VISIBLE);
                                        holder.imgOutOfStock.setVisibility(View.GONE);
                                    }

                                    if (productMrp.equals(discountPrice)) {
                                        holder.tvPrice.setVisibility(View.VISIBLE);
                                        holder.tvDiscount.setVisibility(View.INVISIBLE);
                                        holder.tvMrp.setVisibility(View.GONE);
                                    } else {
                                        holder.tvPrice.setVisibility(View.VISIBLE);
                                        holder.tvDiscount.setVisibility(View.VISIBLE);
                                        holder.tvMrp.setVisibility(View.VISIBLE);
                                        holder.tvMrp.setBackgroundResource(R.drawable.dash);
                                    }
                                    holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                                    holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                                    HashMap AddToCartActivityInfo = db.getCartDetails(tp.getProduct_id());
                                    String btnCheckStatus = (String) AddToCartActivityInfo.get("new_pid");
                                    if (btnCheckStatus == null) {
                                        tp.setStatus(false);
                                        holder.btnAddToCartActivity.setText("Add To Cart");
                                    } else {
                                        tp.setStatus(true);
                                        if (QTY.equals("0")) {
                                            holder.btnAddToCartActivity.setText("Add To Cart");
                                        } else {
                                            holder.btnAddToCartActivity.setText("Go To Cart");
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                    holder.btnAddToCartActivity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                                productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                                final ProductCodeWiseProduct forAddToCartActivity = movie.similarProductCodeWiseProducts.get(movie.getPosition());
                                final String mainCategroy = movie.productCodeWiseProducts.get(selectedPosition).getStrProduct_maincat();

                                //UserTracking UT = new UserTracking(UserTracking.context);
                                //UT.user_tracking(class_name + ": clicked on add to cart & product is: " + forAddToCartActivity.getProduct_name() + " " + forAddToCartActivity.getProduct_size(), mContext);

                                shopId = forAddToCartActivity.getShop_id();
                                pId = forAddToCartActivity.getProduct_id();

                                HashMap AddToCartActivityInfo;
                                AddToCartActivityInfo = db.getCartDetails(pId);
                                String strId = (String) AddToCartActivityInfo.get("new_pid");

                                if (strId == null) { //TODO Server method here
                                    forAddToCartActivity.setStatus(false);

                                    pName = holder.tvProductName.getText().toString();
                                    product_Size = forAddToCartActivity.getProduct_size();

                                    JSONObject params = new JSONObject();
                                    try {
                                        params.put("product_id", pId);
                                        params.put("shop_id", shopId);
                                        params.put("size", product_Size);
                                        params.put("qty", 1);
                                        params.put("contactNo", strContact);
                                        params.put("email", strEmail);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            if (response.isNull("posts")) {
                                            } else {
                                                try {
                                                    strCheckQty = response.getString("posts");
                                                    if (strCheckQty.equals("true")) {
                                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                                                        alertDialogBuilder.setMessage("You cannot add more than 1000 gm of this product.");
                                                        alertDialogBuilder.setPositiveButton("OK",
                                                                new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                        dialogInterface.dismiss();
                                                                    }
                                                                });
                                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                                        alertDialog.show();
                                                    } else {


                                                        db.insertCount(pId);
                                                        int cnt = SyncData();//(int) db.fetchAddToCartCount();
                                                        updateAddToCartActivityCount(cnt);

                                                        if (cnt >= 0) {
                                                            forAddToCartActivity.setStatus(true);
                                                            // if (forAddToCartActivity.isStatus(true)) {
                                                            //    holder.btnAddToCartActivity.setText("Go To Cart");
                                                            // }
                                                        } else {
                                                            forAddToCartActivity.setStatus(false);
                                                            // if (forAddToCartActivity.isStatus(false)) {
                                                            //   holder.btnAddToCartActivity.setText("Add To Cart");
                                                            // }
                                                        }
                                                        //Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_SHORT).show();
                                                        addCartItemsToServer1(mainCategroy);
                                                       /* SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
                                                        String mainCat = sharedPreferencesUtils.getCategory();
                                                        if(mainCategroy.equals(mainCat)) {
                                                            Log.d("SearchProduct","if "+mainCategroy+" "+mainCat+" ");
                                                            Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_SHORT).show();

                                                        }else{
                                                            Log.d("SearchProduct",mainCategroy+" "+mainCat+" ");
                                                        }*/
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            error.printStackTrace();

                                            GateWay gateWay = new GateWay(mContext);
                                            gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                        }
                                    });
                                    VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                                } else {
                                   // UserTracking UT1 = new UserTracking(UserTracking.context);
                                    //UT1.user_tracking(class_name + ": clicked on remove from cart & product is: " + forAddToCartActivity.getProduct_name() + " " + forAddToCartActivity.getProduct_size(), mContext);

                                    holder.btnAddToCartActivity.setText("Go To Cart");
                                    deleteCartItemDialog(pId);
                                }
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(searchProductCoordinateLayout);
                            }
                        }

                        private void deleteCartItemDialog(final String pId) {
                            productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            ProductCodeWiseProduct forAddToCartActivity = movie.similarProductCodeWiseProducts.get(selectedPosition);

                            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                            View review = layoutInflater.inflate(R.layout.same_shop_cart, null);
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                            alertDialogBuilder.setView(review);

                            tvMessage = review.findViewById(R.id.txtMessage);
                            tvYes = review.findViewById(R.id.btnYes);
                            tvNo = review.findViewById(R.id.btnNo);
                            tvMessage.setText("Product is " + forAddToCartActivity.getProduct_name() + ". Do you want remove or Go To Cart.");
                            // set dialog message
                            alertDialogBuilder.setCancelable(true);
                            // create alert dialog
                            alertDialog = alertDialogBuilder.create();
                            // show it
                            alertDialog.show();

                            tvNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    deleteNormalProductFromCartItem(pId);
                                }

                                private void deleteNormalProductFromCartItem(final String pId) { //TODO Server method here
                                    final GateWay gateWay = new GateWay(mContext);
                                    gateWay.progressDialogStart();

                                    final DBHelper db = new DBHelper(mContext);

                                    if (Connectivity.isConnected(mContext)) {
                                        JSONObject params = new JSONObject();
                                        try {
                                            params.put("product_id", pId);
                                            params.put("contactNo", strContact);
                                            params.put("email", strEmail);
                                            params.put("tag", "delete_one_item");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                if (response.isNull("posts")) {
                                                } else {
                                                    db.deleteProductItem(pId);
                                                    holder.btnAddToCartActivity.setText("Add To Cart");
                                                }
                                                count = SyncData();
                                                //(int) db.fetchAddToCartCount();
                                                updateAddToCartActivityCount(count);
                                                gateWay.progressDialogStop();
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                gateWay.progressDialogStop();

                                                error.printStackTrace();

                                                GateWay gateWay = new GateWay(mContext);
                                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                            }
                                        });
                                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                                    } else {
                                        gateWay.displaySnackBar(searchProductCoordinateLayout);
                                    }
                                }
                            });

                            tvYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    alertDialog.dismiss();
                                    Intent intent = new Intent(mContext, AddToCartActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }

                        private void addCartItemsToServer1(String maincategroy) { //TODO Server method here
                            JSONObject params = new JSONObject();
                            if (pName.contains("Small")) {
                                selectCondition = "Small";
                            } else if (pName.contains("Large")) {
                                selectCondition = "Large";
                            } else if (pName.contains("Medium")) {
                                selectCondition = "Medium";
                            } else if (pName.contains("Ripe")) {
                                selectCondition = "Ripe";
                            } else if (pName.contains("Raw")) {
                                selectCondition = "Raw";
                            } else {
                                selectCondition = "";
                            }
                            try {
                                params.put("product_id", pId);
                                params.put("shop_id", shopId);
                                params.put("selectedType", "");
                                params.put("versionCode", StaticUrl.versionName);
                                params.put("Qty", 1);
                                params.put("contactNo", strContact);
                                params.put("email", strEmail);
                                params.put("sessionMainCat", maincategroy);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("addCartItemsToServer1"," "+params);
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("addCartPosts",""+response);

                                    try {
                                        String posts = response.getString("posts");

                                        if (posts.equals("true")){
                                            db.insertCount(pId);
                                            int cnt = SyncData(); //(int) db.fetchAddToCartCount();
                                            updateAddToCartActivityCount(cnt);

                                            if (cnt >= 0) {
                                                // forAddToCartActivity.setStatus(true);
                                                //if (forAddToCartActivity.isStatus(true)) {
                                                holder.btnAddToCartActivity.setText("Go To Cart");
                                                // }
                                            } else {
                                                // forAddToCartActivity.setStatus(false);
                                                // if (forAddToCartActivity.isStatus(false)) {
                                                holder.btnAddToCartActivity.setText("Add To Cart");
                                                // }
                                            }
                                            Toast toast = Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_LONG);
                                            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                            toast.show();
                                        }else {
                                            openSessionDialog(posts);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();

                                    GateWay gateWay = new GateWay(mContext);
                                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                }
                            });
                            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public int getItemCount() {
                return mItems.size();
            }

            class SimilarProductsViewHolder extends RecyclerView.ViewHolder {

                final ImageView imgProduct;
                final TextView tvProductName;
                final TextView tvProductHindiName;
                final TextView tvMrp;
                final TextView tvDiscount;
                final TextView tvPrice;
                final TextView btnAddToCartActivity;
                final Spinner spinner;
                final RelativeLayout LayoutSpinner;
                final ImageView imgOutOfStock;

                SimilarProductsViewHolder(View itemView) {
                    super(itemView);

                    imgProduct = itemView.findViewById(R.id.imgProduct);
                    tvProductName = itemView.findViewById(R.id.txtProductName);
                    tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
                    tvMrp = itemView.findViewById(R.id.txtTotal);
                    tvDiscount = itemView.findViewById(R.id.txtDiscount);
                    tvPrice = itemView.findViewById(R.id.txtPrice);
                    btnAddToCartActivity = itemView.findViewById(R.id.btnAddToCart);
                    spinner = itemView.findViewById(R.id.spinner);
                    LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
                    imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
                    btnAddToCartActivity.setTag(this);
                }
            }
        }
    }

    private class ProductCodeWiseProduct {

        final String shop_category;
        final String product_brand;
        final String product_image1;
        final String product_image2;
        final String product_image3;
        final String product_image4;
        final String product_size;
        final String product_mrp;
        final String product_description;
        final String similar_product_status;
        final String select_type;
        final String product_qty;
        final String strHindiName;
        String code;
        String shop_id;
        String shop_name;
        String product_name;
        String product_id;
        String product_image;
        String product_price;
        String product_discount;
        String strProduct_maincat;
        String cart_pstatus;
        boolean status = false;

        ProductCodeWiseProduct(String code, String shop_id, String shop_name, String shop_category,
                               String product_name, String product_brand, String product_id, String product_image,
                               String product_image1, String product_image2, String product_image3,
                               String product_image4, String product_size, String product_mrp, String product_price,
                               String product_discount, String product_description, String similar_product_status,
                               String type, String product_qty, String hindi_name,String product_maincat) {
            this.code = code;
            this.shop_id = shop_id;
            this.shop_name = shop_name;
            this.shop_category = shop_category;
            this.product_name = product_name;
            this.product_brand = product_brand;
            this.product_id = product_id;
            this.product_image = product_image;
            this.product_image1 = product_image1;
            this.product_image2 = product_image2;
            this.product_image3 = product_image3;
            this.product_image4 = product_image4;
            this.product_size = product_size;
            this.product_mrp = product_mrp;
            this.product_price = product_price;
            this.product_discount = product_discount;
            this.product_description = product_description;
            this.similar_product_status = similar_product_status;
            this.select_type = type;
            this.product_qty = product_qty;
            this.strHindiName = hindi_name;
            this.strProduct_maincat = product_maincat;
        }

        public String getProduct_qty() {
            return product_qty;
        }

        public String getStrHindiName() {
            return strHindiName;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getProduct_brand() {
            return product_brand;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getProduct_size() {
            return product_size;
        }

        public String getProduct_mrp() {
            return product_mrp;
        }

        public String getProduct_price() {
            return product_price;
        }

        public void setProduct_price(String product_price) {
            this.product_price = product_price;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
            return product_discount;
        }

        public boolean isStatus(boolean check) {
            return status;
        }

        public boolean setStatus(boolean status) {
            this.status = status;
            return status;
        }

        public String getSimilar_product_status() {
            return similar_product_status;
        }

        public String getSelect_type() {
            return select_type;
        }

        public String getStrProduct_maincat() {
            return strProduct_maincat;
        }

        public void setStrProduct_maincat(String strProduct_maincat) {
            this.strProduct_maincat = strProduct_maincat;
        }

        public String getCart_pstatus() {
            return cart_pstatus;
        }

        public void setCart_pstatus(String cart_pstatus) {
            this.cart_pstatus = cart_pstatus;
        }
    }

    private class productCodeWithProductList {
        String code;
        int position;

        ArrayList<ProductCodeWiseProduct> productCodeWiseProducts;
        ArrayList<ProductCodeWiseProduct> similarProductCodeWiseProducts;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

    //show dialog when If he tries to select something in other category
    private void openSessionDialog(String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sessoin);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        TextView tv_clearcart = dialog.findViewById(R.id.tv_clearcart);
        TextView tv_continue = dialog.findViewById(R.id.tv_continue);
        tv_message.setText(message);
        tv_clearcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(mContext)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            // GateWay gateWay = new GateWay(getActivity());
                            //gateWay.displaySnackBar(homeMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteAllProductFromCartItem() {
        if (Connectivity.isConnected(mContext)) {
            final GateWay gateWay = new GateWay(mContext);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", sharedPreferencesUtils.getPhoneNumber());
                params.put("email", sharedPreferencesUtils.getEmailId());
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(mContext);
                        db.deleteOnlyCartTable();

                        //after remove cart items from local and online db
                        //then sync online cart count again
                        SyncData();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private int SyncData() { //TODO Server method here
        JSONObject params = new JSONObject();
        try {
            params.put("contact", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetCartCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    WishListCount = response.getInt("wishlist_count");
                    CartCount = response.getInt("normal_cart_count");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (CartCount >= 0) {
                        updateAddToCartCount(CartCount);


                    }
                    if (WishListCount >= 0) {
                        updateWishListCount(WishListCount);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                productIdSet.clear();
                productIdSet.addAll(product_id);
                cardAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);

        return CartCount;
    }

    private void updateAddToCartCount(final int count) {
        if (tvCartNumber == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0) {
                    tvCartNumber.setVisibility(View.INVISIBLE);
                    tvCartNumber.setText("" + count);
                } else {
                    tvCartNumber.setVisibility(View.VISIBLE);
                    tvCartNumber.setText("" + count);
                }
            }
        });
    }

    private void updateWishListCount(final int count) {
        if (tvWishList == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0)
                    tvWishList.setVisibility(View.INVISIBLE);
                else {
                    tvWishList.setVisibility(View.VISIBLE);
                    tvWishList.setText(Integer.toString(count));
                }
            }
        });
    }

    private void updateAddToCartActivityCount(final int count) {
        if (tvCartNumber == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0) {
                    tvCartNumber.setVisibility(View.INVISIBLE);
                    tvCartNumber.setText("" + count);
                } else {
                    tvCartNumber.setVisibility(View.VISIBLE);
                    tvCartNumber.setText("" + count);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
