package com.podmerchant.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class PawAddProductActivity extends AppCompatActivity {

    Context mContext;
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    Button btn_submit_product;
    EditText et_product_name,et_product_qty,et_product_size,et_product_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paw_add_product);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Product");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PawAddProductActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        btn_submit_product = findViewById(R.id.btn_submit_product);
        et_product_name = findViewById(R.id.et_product_name);
        et_product_qty = findViewById(R.id.et_product_qty);
        et_product_size = findViewById(R.id.et_product_size);
        et_product_price = findViewById(R.id.et_product_price);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        btn_submit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_product_name.getText().toString().equals("")){
                    Toast.makeText(PawAddProductActivity.this,"Enter Product Name",Toast.LENGTH_SHORT).show();
                }else if(et_product_qty.getText().toString().equals("")){
                    Toast.makeText(PawAddProductActivity.this,"Enter Product Qty",Toast.LENGTH_SHORT).show();
                }else if(et_product_size.getText().toString().equals("")){
                    Toast.makeText(PawAddProductActivity.this,"Enter Product Size",Toast.LENGTH_SHORT).show();
                }else if(et_product_price.getText().toString().equals("")){
                    Toast.makeText(PawAddProductActivity.this,"Enter Product Price",Toast.LENGTH_SHORT).show();
                }else{
                    addProductByPaw(et_product_name.getText().toString(),et_product_qty.getText().toString(),et_product_size.getText().toString(),et_product_price.getText().toString());
                }
            }
        });
    }

    public void addProductByPaw(String pName,String pQty,String pSize,String pPrice){
        if (Connectivity.isConnected(PawAddProductActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            //final GateWay gateWay = new GateWay(PawAddProductActivity.this);
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);
            progressDialog.show();
            JSONObject params = new JSONObject();
            try {
                params.put("product_name", pName);
                params.put("item_price", pPrice);
                params.put("product_size", pSize);
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                Log.e("InsertToCart_parm",""+StaticUrl.urladdPawProduct+" "+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                StaticUrl.urladdPawProduct,
                params,
                new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        boolean status= response.getBoolean("status");
                        if (status) {
                            Toast.makeText(PawAddProductActivity.this, "Product Added Successfully", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                             Toast.makeText(PawAddProductActivity.this, "Product does not Added", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDialog.dismiss();
                    error.printStackTrace();
                    Log.e("InsertToCart_resp",""+error.toString());
                    GateWay gateWay = new GateWay(PawAddProductActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    //headers.put("key", "Value");
                    return headers;
                    //return super.getHeaders();
                }
            };
            VolleySingleton.getInstance(PawAddProductActivity.this).addToRequestQueue(request);
        } else {
            Log.e("Similler_items",""+"error");
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}