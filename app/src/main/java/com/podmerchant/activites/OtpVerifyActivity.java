package com.podmerchant.activites;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class OtpVerifyActivity extends AppCompatActivity {

    Context mContext;
    String firebaseTokan,emailid,moibleno,vcode,vdate,shop_id,shop_latitude,shop_longitude,shop_distanceKm,seller_name,admin_type,shop_status,
            deliveryboyID="0",managerID="0",shopName,shopAdminType,admin_id;
    ProgressDialog progressDialog;
    Button btn_verifyotp;
    TextView tv_resend;
    EditText et_verifyotp;
    SharedPreferencesUtils sharedPreferencesUtils;
    Locale local;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        mContext = OtpVerifyActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        btn_verifyotp = findViewById(R.id.btn_verifyotp);
        tv_resend = findViewById(R.id.tv_resend);
        et_verifyotp = findViewById(R.id.et_verifyotp);
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new ProgressDialog(mContext);

        Resources resources = getResources();
        local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        Intent intent = getIntent();
        if(intent!=null){
            emailid = intent.getStringExtra("email");
            moibleno = intent.getStringExtra("contact");
            vcode = intent.getStringExtra("vcode");
            vdate = intent.getStringExtra("vdate");
            shop_id = intent.getStringExtra("shop_id");
            shop_latitude = intent.getStringExtra("shop_latitude");
            shop_longitude = intent.getStringExtra("shop_longitude");
            shop_distanceKm = intent.getStringExtra("shop_distanceKm");
            seller_name = intent.getStringExtra("seller_name");
            admin_type = intent.getStringExtra("admin_type");
            shop_status = intent.getStringExtra("shop_status");
            deliveryboyID = intent.getStringExtra("deliveryboyID");
            managerID = intent.getStringExtra("managerID");
            shopName = intent.getStringExtra("shopName");
            shopAdminType = intent.getStringExtra("shopAdminType");
            //admin_id = intent.getStringExtra("admin_id");
            Log.e("VerifyOtp:",""+vcode+" "+shopName);
        }

        btn_verifyotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyOtp();
            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reSendOtp();
            }
        });

        // Get token
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("firebaseTokan", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        firebaseTokan = task.getResult().getToken();
                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("firebaseTokan", firebaseTokan);
                        //Toast.makeText(RegistrationActivity.this, firebaseTokan, Toast.LENGTH_SHORT).show();
                    }
                });
        // [END retrieve_current_token]
    }

    public void verifyOtp(){

       /* progressDialog.setMessage("Please wait...");
         progressDialog.setCancelable(false);
         progressDialog.show();*/

        if(et_verifyotp.getText().toString().equals("")) {
          //  progressDialog.dismiss();
            Toast.makeText(mContext,"Please Enter OTP", Toast.LENGTH_SHORT).show();
        }else {
            if(vcode.equals(et_verifyotp.getText().toString().trim())){
               // progressDialog.dismiss();

                if(admin_type.equals("Manager"))
                {
                    updateManagerFirebaseToken();
                }else {
                    updateFirebaseToken();
                }
                //finish();
            }else {
                Toast.makeText(mContext,"Invalid OTP", Toast.LENGTH_SHORT).show();
            }
        }
    }
    //update shop firebase token
    public void updateFirebaseToken(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String urlOtpsend = StaticUrl.updatetoken
                    +"?shop_id="+shop_id
                    +"&firebase_token="+firebaseTokan;

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    //String vcode = jsonObjectData.getString("shop_id");
                                    sharedPreferencesUtils.setEmailId(emailid);
                                    sharedPreferencesUtils.setShopID(shop_id);
                                    sharedPreferencesUtils.setPhoneNumber(moibleno);
                                    sharedPreferencesUtils.setLatitude(shop_latitude);
                                    sharedPreferencesUtils.setLongitude(shop_longitude);
                                    sharedPreferencesUtils.setUserName(seller_name);
                                    sharedPreferencesUtils.setDistancekm(shop_distanceKm);
                                    sharedPreferencesUtils.setAdminType(admin_type);
                                    sharedPreferencesUtils.setShopStatus(shop_status);
                                    sharedPreferencesUtils.setLoginFlag(true);
                                    sharedPreferencesUtils.setDelId(deliveryboyID);
                                    sharedPreferencesUtils.setManagerId(managerID);
                                    sharedPreferencesUtils.setLocal("en_US");
                                    sharedPreferencesUtils.setShopName(shopName);
                                    sharedPreferencesUtils.setShopAdminType(shopAdminType);
                                    //sharedPreferencesUtils.setPawMode("pawuser");
                                    //sharedPreferencesUtils.setReferStatus("yes");
                                    //sharedPreferencesUtils.setReferCode("POD404");
                                    Toast.makeText(mContext,"Thanks, Verification Successfully", Toast.LENGTH_SHORT).show();

                                    checkSugestedRefer();
                                    //Intent intent = new Intent(mContext, HomeActivity.class);
                                    //Intent intent = new Intent(mContext, StaffListReferActivity.class);
                                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    //startActivity(intent);
                                   // finishAffinity();
                                }else if(status=true){
                                    Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    //update manager firebase token
    public void updateManagerFirebaseToken(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String urlManagerFirebaseToken = StaticUrl.updateManagertoken
                    +"?shop_id="+shop_id
                    +"&shopadmin_id="+managerID
                    +"&firebase_token="+firebaseTokan;

        Log.d("urlManagerFirebaseToken",urlManagerFirebaseToken);

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlManagerFirebaseToken,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){

                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    sharedPreferencesUtils.setEmailId(emailid);
                                    sharedPreferencesUtils.setShopID(shop_id);
                                    sharedPreferencesUtils.setPhoneNumber(moibleno);
                                    sharedPreferencesUtils.setLatitude(shop_latitude);
                                    sharedPreferencesUtils.setLongitude(shop_longitude);
                                    sharedPreferencesUtils.setUserName(seller_name);
                                    sharedPreferencesUtils.setDistancekm(shop_distanceKm);
                                    sharedPreferencesUtils.setAdminType(admin_type);
                                    sharedPreferencesUtils.setShopStatus(shop_status);
                                    sharedPreferencesUtils.setLoginFlag(true);
                                    sharedPreferencesUtils.setDelId(deliveryboyID);
                                    sharedPreferencesUtils.setLocal("en_US");
                                    sharedPreferencesUtils.setShopName(shopName);
                                    sharedPreferencesUtils.setManagerId(managerID);
                                    sharedPreferencesUtils.setShopAdminType(shopAdminType);

                                    //if(shop_latitude.isEmpty() || shop_longitude.isEmpty() || shop_latitude.equalsIgnoreCase("null") ||shop_longitude.equalsIgnoreCase("null") || shop_latitude == null || shop_longitude == null){
                                   Log.e("ShopLocation:",""+shop_latitude.length());
                                    if(shop_latitude.length() >=5 || shop_longitude.length() >=5 ){

                                        Toast.makeText(mContext, "Thanks, Verification Successfully", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finishAffinity();

                                    }else {

                                        Intent address = new Intent(mContext,ChangeAddressActivity.class);
                                        startActivity(address);
                                        finishAffinity();
                                    }

                                }else if(status==true){
                                    Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    //send otp
    public void reSendOtp(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String urlOtpsend = StaticUrl.sendotp
                    +"?mobileno="+moibleno
                    +"&email="+emailid;

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");

                                    Intent intent = new Intent(mContext,OtpVerifyActivity.class);
                                    intent.putExtra("vcode",vcode);
                                    intent.putExtra("email",email);
                                    intent.putExtra("contact",contact);
                                    intent.putExtra("vdate",vdate);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    //finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //check signUpSugestedBy
    //check refer by and already refer
    //send to home or staffListrefer activity
    String refercode,referById;

    public void checkSugestedRefer() {
        if(Connectivity.isConnected(mContext)){
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (sharedPreferencesUtils.getReferStatus().equals("yes")) {
            refercode = sharedPreferencesUtils.getReferCode();
            referById = refercode.substring(3, refercode.length());
        } else {
            refercode = "pod78";
            referById = "78";
        }
        String targetUrl = StaticUrl.checksugestedrefer
                + "?shop_id=" + sharedPreferencesUtils.getShopID()
                + "&referStatus=" + sharedPreferencesUtils.getReferStatus()
                + "&refercode=" + refercode
                + "&referById=" + referById
                + "&shop_id=" + sharedPreferencesUtils.getShopID();
        Log.d("checkSugestedRefer",targetUrl);
        StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                targetUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            Log.e("shoplocation:",""+shop_latitude+"len"+shop_latitude.length());
                            if(status.equals("true")){
                                if(sharedPreferencesUtils.getAdminType().equals("PAW")){
                                    Intent intent = new Intent(mContext, PawHomeActivity.class);
                                    startActivity(intent);
                                    finishAffinity();
                                }else {

                                    if (shop_latitude.length() >= 5 || shop_longitude.length() >= 5) {
                                        Intent intent = new Intent(mContext, HomeActivity.class);
                                        startActivity(intent);
                                        finishAffinity();
                                    } else {
                                        Intent address = new Intent(mContext, ChangeAddressActivity.class);
                                        startActivity(address);
                                        finishAffinity();
                                    }
                                }
                            }else if(status.equals("false")){
                                if(sharedPreferencesUtils.getAdminType().equals("PAW")){
                                    Intent intent = new Intent(mContext, PawHomeActivity.class);
                                    startActivity(intent);
                                    finishAffinity();
                                }else {

                                    if (shop_latitude.length() >= 5 || shop_longitude.length() >= 5) {
                                        Intent intent = new Intent(mContext, StaffListReferActivity.class);
                                        startActivity(intent);
                                        //finishAffinity();
                                    } else {
                                        Intent address = new Intent(mContext, ChangeAddressActivity.class);
                                        startActivity(address);
                                        //finishAffinity();
                                    }
                                }
                            }else{
                                if(sharedPreferencesUtils.getAdminType().equals("PAW")){
                                    Intent intent = new Intent(mContext, PawHomeActivity.class);
                                    startActivity(intent);
                                    finishAffinity();
                                }else{
                                    if(shop_latitude.length() >=5 || shop_longitude.length() >=5 ) {
                                    Intent intent = new Intent(mContext, HomeActivity.class);
                                    startActivity(intent);
                                    finishAffinity();
                                     }else {
                                        Intent address = new Intent(mContext, ChangeAddressActivity.class);
                                        startActivity(address);
                                        finishAffinity();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                    }
                                });
                        MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
                    }else {
                        Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
                    }
           }

}
