package com.podmerchant.activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class ReportsActivity extends AppCompatActivity {

    Context mContext;
    public static final String TAG = "ReportsActivity";
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    BarChart orderDeliverChart;
    BarChart orderRejectChart;
    //https://github.com/PhilJay/MPAndroidChart/blob/master/MPChartExample/src/main/java/com/xxmassdeveloper/mpchartexample/BarChartActivity.java
    TextView tv_network_con,tv_oh_delivered,tv_oh_rejected,tv_totalprofit,tv_tab_deliver,tv_tab_rejected;
    ArrayList NoOfEmp = new ArrayList();
    ArrayList year = new ArrayList();
    ArrayList NoOfEmp_r = new ArrayList();
    ArrayList year_r = new ArrayList();
    LinearLayout ll_delboy_barchart,ll_rejorder_barchart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_reports);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = ReportsActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        orderDeliverChart = findViewById(R.id.chart);
        orderRejectChart = findViewById(R.id.rejectchart);
        tv_network_con = findViewById(R.id.tv_network_con);
        tv_oh_delivered = findViewById(R.id.tv_oh_delivered);
        tv_oh_rejected = findViewById(R.id.tv_oh_rejected);
        tv_totalprofit = findViewById(R.id.tv_totalprofit);
        tv_tab_deliver = findViewById(R.id.tv_tab_deliver);
        tv_tab_rejected = findViewById(R.id.tv_tab_rejected);
        ll_delboy_barchart = findViewById(R.id.ll_delboy_barchart);
        ll_rejorder_barchart = findViewById(R.id.ll_rejorder_barchart);

        DeliveryReports();
        ll_delboy_barchart.setVisibility(View.VISIBLE);
        tv_tab_deliver.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        tv_tab_rejected.setBackgroundColor(getResources().getColor(R.color.whiteBtnTextColor));
        ll_rejorder_barchart.setVisibility(View.GONE);
        //RejectReports();
        getOrderHistroyCount();
      /*  ArrayList NoOfEmp = new ArrayList();
        NoOfEmp.add(new BarEntry(2f, 0));
        NoOfEmp.add(new BarEntry(4f, 1));

        ArrayList year = new ArrayList();
        year.add("TestA");
        year.add("TestB");

        BarDataSet bardataset = new BarDataSet(NoOfEmp, "No Of Delivered Order's");
        orderDeliverChart.animateY(5000);
        BarData data = new BarData(year, bardataset);
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        orderDeliverChart.setData(data);
        XAxis xAxis = orderDeliverChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
*/
        tv_tab_deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeliveryReports();
                ll_delboy_barchart.setVisibility(View.VISIBLE);
                ll_rejorder_barchart.setVisibility(View.GONE);
                tv_tab_deliver.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                tv_tab_rejected.setBackgroundColor(getResources().getColor(R.color.whiteBtnTextColor));
            }
        });
        tv_tab_rejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RejectReports();
                ll_delboy_barchart.setVisibility(View.GONE);
                ll_rejorder_barchart.setVisibility(View.VISIBLE);
                tv_tab_deliver.setBackgroundColor(getResources().getColor(R.color.whiteBtnTextColor));
                tv_tab_rejected.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }
        });
    }

    public void DeliveryReports(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.deliveryreports
                    + "?shopid="+sharedPreferencesUtils.getShopID()
                    + "&tblName="+sharedPreferencesUtils.getAdminType();

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                   for(int i =0; i < jsonArray.length(); i++){
                                         JSONObject jsonObjectData =  jsonArray.getJSONObject(i);
                                        NoOfEmp.add(new BarEntry(jsonObjectData.getInt("Delivered"), i));
                                        year.add(jsonObjectData.getString("name"));
                                    }

                                    BarDataSet bardataset = new BarDataSet(NoOfEmp, "Delivery Boy");
                                    bardataset.setBarSpacePercent(35f);
                                    orderDeliverChart.animateY(4000);
                                    BarData data = new BarData(year, bardataset);
                                    bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
                                    orderDeliverChart.setData(data);
                                    XAxis xAxis = orderDeliverChart.getXAxis();
                                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                                    xAxis.setDrawGridLines(false);

                                  }else if(status=false){
                                    Toast.makeText(mContext,"Sorry. This order you can not assign.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    public void RejectReports(){
        if(Connectivity.isConnected(mContext)){
            progressDialog.show();

            String urlAcceptOrder = StaticUrl.rejectionreports+"?shopid="+sharedPreferencesUtils.getShopID();

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for(int i =0; i < jsonArray.length(); i++){
                                        JSONObject jsonObjectData =  jsonArray.getJSONObject(i);
                                        NoOfEmp_r.add(new BarEntry(jsonObjectData.getInt("Reject"), i));
                                        year_r.add(jsonObjectData.getString("reason_of_rejection"));
                                    }

                                    BarDataSet bardataset = new BarDataSet(NoOfEmp_r, "Reson");
                                    bardataset.setBarSpacePercent(35f);
                                    orderRejectChart.animateY(5000);
                                    BarData data = new BarData(year_r, bardataset);
                                    bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
                                    orderRejectChart.setData(data);
                                    XAxis xAxis = orderRejectChart.getXAxis();
                                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                                    xAxis.setDrawGridLines(false);

                                }else if(status=false){
                                    Toast.makeText(mContext,"Sorry. This order you can not assign.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    public void getOrderHistroyCount(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.histroycount
                    + "?shopid="+sharedPreferencesUtils.getShopID()
                    + "&tblName="+sharedPreferencesUtils.getAdminType();

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jsonObjectData =  jsonArray.getJSONObject(0);
                                    tv_oh_delivered.setText(jsonObjectData.getString("Delivered"));
                                    tv_oh_rejected.setText(jsonObjectData.getString("Rejected"));
                                    tv_totalprofit.setText("INR "+jsonObjectData.getString("Profit"));
                                }else if(!status){
                                    Toast.makeText(mContext,"No Order Histroy",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
