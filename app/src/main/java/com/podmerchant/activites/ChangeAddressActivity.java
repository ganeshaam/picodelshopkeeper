package com.podmerchant.activites;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleyMultipartRequest;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;

public class ChangeAddressActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener{

    Context mContext;
    public static final String TAG = "ChangeAddressActivity";
    ProgressDialog progressDialog;
    Button btn_update_Address,btn_location;
    EditText et_address1,et_shopaddress2,et_shopaddress3,et_shopaddress4,et_shopaddress5,et_shopaddress6,et_shopcity;
    SeekBar bar_area_km;
    TextView tv_destancekm,tv_shop_number,tv_location,tv_latitude,tv_longitude;
    String distance_value="",mRequestBody="",latitude="", longitude="";
    LinearLayout ll_km_option,ll_NearbyLocation,ll_currentLocation;
    Spinner sp_shop_city;
    RadioButton rb_nearby,rb_current;
    SharedPreferencesUtils sharedPreferencesUtils;
    private static final String HINDI_LOCALE = "hi";
    private static final String ENGLISH_LOCALE = "en_US";
    Locale local;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    String address_key,version,value_shop_city="";
    private ArrayList<String> cityList;
    RadioGroup rg_location;


    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager locationManager,mLocationManager;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_address);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_shop_address);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = ChangeAddressActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new ProgressDialog(mContext);

        if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Manufacturer")){
            getSupportActionBar().setTitle("Manufacturer Address");
        }

        cityList = new ArrayList<>();

        // for the Hindi containt
        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        initViews();
        getAndroidkey();
        getShopAddressDetails();
        //getCityList();
        btn_update_Address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addressValidation()){
                    UpdateAddressMultipart();
                    value_shop_city = sp_shop_city.getSelectedItem().toString();
                    sharedPreferencesUtils.setRCity(value_shop_city);
                }
            }
        });

        rg_location.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(rb_current.isChecked()){
                    ll_currentLocation.setVisibility(View.VISIBLE);
                    ll_NearbyLocation.setVisibility(View.GONE);
                }else if(rb_nearby.isChecked()){
                    ll_currentLocation.setVisibility(View.GONE);
                    ll_NearbyLocation.setVisibility(View.VISIBLE);
                }
            }
        });

        /*Location Initialization*/

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        checkLocation(); //check whether location service is enable or not in your  phone

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }

        btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mGoogleApiClient = new GoogleApiClient.Builder(ChangeAddressActivity.this)
                        .addConnectionCallbacks(ChangeAddressActivity.this)
                        .addOnConnectionFailedListener(ChangeAddressActivity.this)
                        .addApi(LocationServices.API)
                        .build();

                mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

                checkLocation();

            }
        });

        //bar_area_km.setProgress(0);
        // bar_area_km.incrementProgressBy(0.5f);
        bar_area_km.setMax(6);
        //bar_area_km.setMax(8);
        bar_area_km.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            //int pval = 1;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //pval = progress;
                float pval=(float) progress/2;
                tv_destancekm.setText(pval + "/3");//+ seekBar.getMax()
                distance_value= String.valueOf(pval);
                Log.d("distance_value",distance_value);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Initialize the SDK
        //Places.initialize(getApplicationContext(), getResources().getString(R.string.apiKey));
        /*Places.initialize(getApplicationContext(), address_key);

        // Create a new Places client instance
        PlacesClient placesClient = Places.createClient(this);*/

        //auto complete address
        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setCountry("IN");

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place_et: " + place.getName() + ", " + place.getId() +", "+place.getLatLng());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        final List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        autocompleteFragment.getView().setVisibility(View.GONE);

        // Start the autocomplete intent.
        /*et_shopaddress3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

            }
        });*/

        et_shopaddress3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() == 1){
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(ChangeAddressActivity.this);
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Manufacturer")){
            ll_km_option.setVisibility(View.GONE);
            tv_shop_number.setText("Building Number");
            tv_location.setText("Select Nearest Warehouse Location Address");
        }else {
            ll_km_option.setVisibility(View.VISIBLE);
        }
    }

    private void initViews(){
        //address
        et_address1= findViewById(R.id.et_address1);
        et_shopaddress2= findViewById(R.id.et_shopaddress2);
        et_shopaddress3= findViewById(R.id.et_shopaddress3);
        et_shopaddress4= findViewById(R.id.et_shopaddress4);
        et_shopaddress5= findViewById(R.id.et_shopaddress5);
        et_shopaddress6= findViewById(R.id.et_shopaddress6);
        et_shopcity = findViewById(R.id.et_shopcity);
        btn_update_Address = findViewById(R.id.btn_update_Address);
        bar_area_km = findViewById(R.id.bar_area_km);
        tv_destancekm = findViewById(R.id.tv_destancekm);
        ll_km_option = findViewById(R.id.ll_km_option);
        tv_location = findViewById(R.id.tv_location);
        tv_shop_number = findViewById(R.id.tv_shop_number);
        sp_shop_city = findViewById(R.id.sp_shop_city);
        tv_latitude = findViewById(R.id.tv_latitude);
        tv_longitude = findViewById(R.id.tv_longitude);
        btn_location = findViewById(R.id.btn_location);
        ll_NearbyLocation = findViewById(R.id.ll_NearbyLocation);
        ll_currentLocation = findViewById(R.id.ll_currentLocation);
        rg_location = findViewById(R.id.rg_location);
        rb_nearby = findViewById(R.id.rb_nearby);
        rb_current = findViewById(R.id.rb_current);
        //,;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                // Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                Log.i(TAG, "Place_res: " + place.getName() + ", " + place.getId() + ", " + place.getLatLng());
                if (place != null) {
                    LatLng latLng = place.getLatLng();
                    latitude = String.valueOf(latLng.latitude);
                    longitude = String.valueOf(latLng.longitude);

                    sharedPreferencesUtils.setLatitude(latitude);
                    sharedPreferencesUtils.setLongitude(longitude);
                    Log.d("latlong", latitude);
                    et_shopaddress3.setText(place.getName());
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    public boolean addressValidation() {
        if(et_address1.getText().toString().isEmpty()){
            et_address1.requestFocus();
            et_address1.setError("Please enter Shop Number");
            return  false;
        }else if(et_shopaddress2.getText().toString().isEmpty()){
            et_shopaddress2.requestFocus();
            et_shopaddress2.setError("Please enter Society/Bulding Name");
            return  false;//if(rb_nearby.isChecked()
        }else if(rb_nearby.isChecked()){
            if(et_shopaddress3.getText().toString().equals("")) {
                et_shopaddress3.requestFocus();
                et_shopaddress3.setError(getResources().getString(R.string.reg_address_third));
                return false;
            }else {
                return true;
            }
        }/*else if(et_shopaddress3.getText().toString().equals("")){
            et_shopaddress3.requestFocus();
            et_shopaddress3.setError(getResources().getString(R.string.reg_address_third));
            return  false;
        }*/else if(et_shopaddress4.getText().toString().isEmpty()){
            et_shopaddress4.requestFocus();
            et_shopaddress4.setError("Please enter Road name");
            return  false;
        }else/* if(et_shopaddress5.getText().toString().isEmpty()){
            et_shopaddress5.requestFocus();
            et_shopaddress5.setError("Please enter Colony name");
        }else*/ if(et_shopaddress6.getText().toString().isEmpty()){
            et_shopaddress6.requestFocus();
            et_shopaddress6.setError("Please enter Area");
            return  false;
        }else if(sp_shop_city.getSelectedItem().toString().isEmpty()){
            //et_shopcity.requestFocus();
            //et_shopcity.setError("Please enter Shop City");
            Toast.makeText(mContext,"Please Select City",Toast.LENGTH_LONG).show();
            return  false;
        }/*else if(et_shopcity.getText().toString().isEmpty()){
            et_shopcity.requestFocus();
            et_shopcity.setError("Please enter Shop City");
            return  false;
        }*///sp_shop_city
        return  true;
    }

    private void getShopAddressDetails(){

        //get Shopkeeper Address Details
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.verifyMobile +"?contact="+sharedPreferencesUtils.getPhoneNumber();

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Log.e("ProfileResponse:",""+jsonObjectData.toString());

                                    //shop_id = jsonObjectData.getString("shop_id");
                                    //shop_latitude = jsonObjectData.getString("shop_latitude");
                                    //shop_longitude = jsonObjectData.getString("shop_longitude");
                                    distance_value = jsonObjectData.getString("preferred_delivery_area_km");
                                    et_address1.setText(jsonObjectData.getString("address1"));
                                    et_shopaddress2.setText(jsonObjectData.getString("address2"));
                                    et_shopaddress3.setText(jsonObjectData.getString("address3"));
                                    et_shopaddress4.setText(jsonObjectData.getString("address4"));
                                    //et_shopaddress5.setText(jsonObjectData.getString("address5"));
                                    et_shopaddress6.setText(jsonObjectData.getString("area"));
                                    et_shopcity.setText(jsonObjectData.getString("city"));
                                    value_shop_city= jsonObjectData.getString("city");
                                    getCityList();
                                    //bar_area_km.setProgress(Float.parseFloat(jsonObjectData.getString("preferred_delivery_area_km")));
                                    double double_km = jsonObjectData.getDouble("preferred_delivery_area_km");
                                    double str_km = double_km * 2;
                                    int int_parse = (int)str_km;
                                    Log.d("str_km",""+ int_parse);
                                    bar_area_km.setProgress(int_parse);
                                    tv_destancekm.setText(jsonObjectData.getString("preferred_delivery_area_km") + "/" + "3");
                                    latitude = jsonObjectData.getString("shop_latitude");
                                    longitude = jsonObjectData.getString("shop_longitude");
                                }else if(status==false) {
                                    Toast.makeText(mContext,"Address is Not Register..!!",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }


    }

    //volley json parameters
    private void UpdateAddress ( ){
        try {

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("shop_id", "284");
            jsonBody.put("address1", et_address1.getText().toString().trim());
            jsonBody.put("address2", et_shopaddress2.getText().toString().trim());
            jsonBody.put("address3", et_shopaddress3.getText().toString().trim());
            jsonBody.put("address4", et_shopaddress4.getText().toString().trim());
            //jsonBody.put("address5", et_shopaddress5.getText().toString().trim());
            jsonBody.put("area", et_shopaddress6.getText().toString().trim());
            jsonBody.put("city", et_shopcity.getText().toString().trim());
            jsonBody.put("preferred_delivery_area_km", distance_value.trim());
            jsonBody.put("shop_latitude", latitude);
            jsonBody.put("shop_longitude", longitude);

            mRequestBody = jsonBody.toString();
            Log.e("fdnszfmdsbfdbs", mRequestBody.toString());
        } catch (Exception E) {
            Log.e("getlogindataerror", E.getMessage());
            E.getMessage();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticUrl.shop_profile_address_update, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e("fdnszfmdsbfdbs", response.toString());
                    JSONObject jsonObject = new JSONObject(response);

                    boolean status = jsonObject.getBoolean("status");
                    if(status==true){
                        Toast.makeText(mContext,"Address Updated Successfully..!!",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(mContext,HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);

                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangeAddressActivity.this);
                        alertDialogBuilder.setMessage("Address is not updated...");
                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                } catch (Exception E) {
                    E.getMessage();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



                String errorname = "";
                if (error instanceof NetworkError) {

                    errorname = "Network Connection Error Please Try Again Later";
                    getError(errorname);
                    //   Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    // Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                    errorname = "Server  connection error please try again later";
                    getError(errorname);
                } else if (error instanceof AuthFailureError) {
                    //    Toast.makeText(getApplicationContext(), "Auth Failure Error", Toast.LENGTH_SHORT).show();

                    errorname = "Auth Failure Error please try again later";
                    getError(errorname);
                } else if (error instanceof ParseError) {
                    //   Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();


                    errorname = "Parse Error please try again later";
                    getError(errorname);
                } else if (error instanceof NoConnectionError) {
                    //    Toast.makeText(getApplicationContext(), "No Connection Error", Toast.LENGTH_SHORT).show();

                    errorname = "No Connection Error please try again later";
                    getError(errorname);
                } else if (error instanceof TimeoutError) {
                    //   Toast.makeText(getApplicationContext(), "Time out Error", Toast.LENGTH_SHORT).show();

                    errorname = "Time out Error please try again later";
                    getError(errorname);
                }


            }
        })

        {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }

            }


            //This is for Headers If You Needed
           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = BaseURLsetGet.userid + ":" + BaseURLsetGet.userpass;
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }*/

        };

        Log.e("getURLSET", stringRequest + "");
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                550000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);

    }

    private void getError (String Error){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangeAddressActivity.this);
        alertDialogBuilder.setMessage(Error);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Volley multipart post method to update Address
    public void UpdateAddressMultipart() {
        if(Connectivity.isConnected(mContext)) {
            // final String currentDateandTime = sdf.format(new Date());

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String targetUlr = StaticUrl.shop_address_update;

            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                    targetUlr, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    try {
                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e("CHANGEADD_resp", "resp" + resultResponse.toString());
                        Log.e(TAG, "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status == true)
                        {
                            Toast.makeText(mContext,"Address Update Successfully",Toast.LENGTH_LONG).show();
                            sharedPreferencesUtils.setDistancekm(distance_value);
                            Intent intent = new Intent(mContext,HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                        }else if(status == false){
                            Log.d(TAG,"status_false"+resultResponse);
                            Toast.makeText(mContext,"Something went wrong..!!",Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleYIMAGE_EXP:", "" + e.getMessage());
                        // progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse networkResponse = error.networkResponse;
                    progressDialog.dismiss();
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";

                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.e("Error", ""+errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<>();

                    params.put("shop_id", sharedPreferencesUtils.getShopID());
                    params.put("address1", et_address1.getText().toString().trim());
                    params.put("address2", et_shopaddress2.getText().toString().trim());
                    if(rb_nearby.isChecked()){
                        params.put("address3", et_shopaddress3.getText().toString().trim());
                    }else{
                        params.put("address3", et_shopaddress6.getText().toString().trim());
                    }

                    params.put("address4", et_shopaddress4.getText().toString().trim());
                    params.put("address5", et_shopaddress4.getText().toString().trim());
                    params.put("area", et_shopaddress6.getText().toString().trim());
                    //params.put("city", et_shopcity.getText().toString().trim());
                    params.put("city", sp_shop_city.getSelectedItem().toString().trim());
                    params.put("preferred_delivery_area_km",distance_value);
                    params.put("shop_latitude", latitude);
                    params.put("shop_longitude", longitude);
                    Log.e("UpdateProfileParams", "" + params);

                    sharedPreferencesUtils.setLatitude(latitude);
                    sharedPreferencesUtils.setLongitude(longitude);
                    sharedPreferencesUtils.setRCity(sp_shop_city.getSelectedItem().toString());
                    return params;
                }

           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = "#concord$" + ":" + "!@#con co@$rd!@#";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }*/

               /* @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("shop_logo", new DataPart(getBytesFromBitmap(shopbitmap)));
                    params.put("identity_proof_img", new DataPart(getBytesFromBitmap(shopLicenceBitmap)));
                    params.put("identity_proof", new DataPart(getBytesFromBitmap(idproofBitmap)));
                    return params;
                }*/
            };
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        }
        else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getCityList() { //TODO Server method here
        if (Connectivity.isConnected(ChangeAddressActivity.this)) {
            cityList.clear();

            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlactiveCityForshop, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    try {



                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            cityList.add(jsonCityData.getString("city"));
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter(ChangeAddressActivity.this,android.R.layout.simple_spinner_item,cityList);
                        sp_shop_city.setAdapter(adapter);

                        if (value_shop_city != null) {
                            int spinnerPosition = adapter.getPosition(value_shop_city);
                            sp_shop_city.setSelection(spinnerPosition);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();

                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(ChangeAddressActivity.this).addToRequestQueue(request);
        }
    }

    //get SecreteKey Verify
    public void getAndroidkey(){

        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.androidkey
                    +"?app="+"shopkeeper";

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){

                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    address_key = jsonObjectData.getString("address_key");
                                    version =  jsonObjectData.getString("version");
                                    Log.e("androidkey",""+jsonObjectData);

                                    // Create a new Places client instance
                                    Places.initialize(getApplicationContext(), address_key);
                                    // Create a new Places client instance
                                    PlacesClient placesClient = Places.createClient(ChangeAddressActivity.this);

                                }else if(!statuss) {

                                    //Toast.makeText(mContext,"Mobile Number is not Register..!!",Toast.LENGTH_LONG).show();
                                    getError("\n\t Android Key Not Found Please Check.");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }


    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            Log.d("Location",mLocation.getLatitude()+" "+mLocation.getLongitude());
            latitude = String.valueOf(mLocation.getLatitude());
            longitude = String.valueOf(mLocation.getLongitude());
            Toast.makeText(this, "Location :"+latitude +""+longitude, Toast.LENGTH_SHORT).show();
            tv_latitude.setText(latitude);
            tv_longitude.setText(longitude);

            //mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            //Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            Log.e("Location:",""+location.getLatitude()+""+location.getLongitude());
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            Toast.makeText(this, "Location :"+latitude +""+longitude, Toast.LENGTH_SHORT).show();
            tv_latitude.setText(latitude);
            tv_longitude.setText(longitude);
           /* val MY_READ_EXTERNAL_REQUEST : Int = 1
            if (checkSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_READ_EXTERNAL_REQUEST)
            }*/

        }
    }

    protected void startLocationUpdates() {

        mLocationRequest = LocationRequest.create();//.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }



   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.getItem(0);
        if(sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)){
            menuItem.setTitle("English");
        }else {
            menuItem.setTitle("Hindi");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //handle local language Hindi and English
        if (id == R.id.action_settings) {

            Resources resources = getResources();
            if(sharedPreferencesUtils.getLocal().equals(HINDI_LOCALE)){
                local = new Locale(ENGLISH_LOCALE);
                sharedPreferencesUtils.setLocal(ENGLISH_LOCALE);
            }else {
                local = new Locale(HINDI_LOCALE);
                sharedPreferencesUtils.setLocal(HINDI_LOCALE);
            }
            Configuration configuration =  resources.getConfiguration();
            configuration.setLocale(local);
            getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
            recreate();
            //return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

}
