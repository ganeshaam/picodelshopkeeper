package com.podmerchant.activites;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.podmerchant.R;


public class ContinueShopingActivity extends AppCompatActivity {

    Button btnShopNow;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_continue_shoping);

        btnShopNow = findViewById(R.id.btnShopNow);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_continue_shop);

       /* setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/
        btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContinueShopingActivity.this, HomeActivity.class);
                intent.putExtra("tag", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }


}
