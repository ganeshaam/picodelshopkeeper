package com.podmerchant.activites;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.podmerchant.R;
import com.podmerchant.util.SharedPreferencesUtils;

import java.util.Locale;

import cdflynn.android.library.checkview.CheckView;

public class RechargeDoneActivity extends AppCompatActivity {

    CheckView check;
    String paymentId,rechargeAmount,newPoints,totalPoints,invoiceId,staffName,staffImage,planName,points,amounts,paymentMode,flagPayment;
    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    TextView tv_recharge_point,yourpoints,tv_rechargemsg,tv_rechargemsgsec;
    Button btn_rechInvoice,btn_rechHistroy,btn_rechhome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_done);

        check = findViewById(R.id.check);

        mContext = RechargeDoneActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        tv_recharge_point = findViewById(R.id.tv_recharge_point);
        btn_rechInvoice = findViewById(R.id.btn_rechInvoice);
        btn_rechHistroy = findViewById(R.id.btn_rechHistroy);
        btn_rechhome = findViewById(R.id.btn_rechhome);
        yourpoints = findViewById(R.id.yourpoints);
        tv_rechargemsg = findViewById(R.id.tv_rechargemsg);
        tv_rechargemsgsec = findViewById(R.id.tv_rechargemsgsec);

        Intent intent = getIntent();
        invoiceId = intent.getStringExtra("invoiceId");
        staffName = intent.getStringExtra("staffName");
        staffImage = intent.getStringExtra("staffImage");
        planName = intent.getStringExtra("planName");
        points = intent.getStringExtra("points");
        rechargeAmount = intent.getStringExtra("rechargeAmount");
        paymentMode = intent.getStringExtra("paymentMode");
        totalPoints = intent.getStringExtra("totalPoints");
        flagPayment= intent.getStringExtra("flagPayment");

        if(flagPayment.equals("recharge")){
            tv_recharge_point.setText(points);
        }else if(flagPayment.equals("Registration")){
            //tv_recharge_point.setText(points);
            tv_recharge_point.setText("INR "+rechargeAmount);
        }else if(flagPayment.equals("Partnerkit")) {
            yourpoints.setVisibility(View.GONE);
            tv_recharge_point.setText("INR "+rechargeAmount);
            tv_rechargemsg.setText("Your deposit amount is instantly done as per information provided by you.");
            tv_rechargemsgsec.setVisibility(View.GONE);
        }

        check.check();

        btn_rechInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRechargeDone = new Intent(mContext,InvoiceActivity.class);
                intentRechargeDone.putExtra("totalPoints",totalPoints);
                intentRechargeDone.putExtra("invoiceId",invoiceId);
                intentRechargeDone.putExtra("staffName",staffName);
                intentRechargeDone.putExtra("staffImage",staffImage);
                intentRechargeDone.putExtra("planName",planName);
                intentRechargeDone.putExtra("points",points);
                intentRechargeDone.putExtra("amounts",rechargeAmount);
                intentRechargeDone.putExtra("paymentMode",paymentMode);
                intentRechargeDone.putExtra("flagPayment",flagPayment);
                intentRechargeDone.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intentRechargeDone);
            }
        });
        btn_rechHistroy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentHome = new Intent(mContext,RechargeHistoryActivity.class);
                intentHome.putExtra("paymentMode",flagPayment);
                intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentHome);
            }
        });
        btn_rechhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentHome = new Intent(mContext,HomeActivity.class);
                intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentHome);
            }
        });
    }
}
