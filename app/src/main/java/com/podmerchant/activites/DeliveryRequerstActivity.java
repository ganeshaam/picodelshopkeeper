package com.podmerchant.activites;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.podmerchant.R;
import com.podmerchant.adapter.DeliveryRequestAdapter;
import com.podmerchant.model.DeliveryModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleyMultipartRequest;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import dmax.dialog.SpotsDialog;

public class DeliveryRequerstActivity extends AppCompatActivity {

    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    String mRequestBody="",latitude = "0.0",longitude ="0.0",to_latitude = "0.0",to_longitude ="0.0";
    public static final String TAG = "DeliveryRequerst";
    int AUTOCOMPLETE_REQUEST_FROM_CODE = 1;
    int AUTOCOMPLETE_REQUEST_TO_CODE = 2;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;

    EditText et_from,et_to,et_box_height,et_box_width,et_box_breadth,et_box_weight,et_no_items;
    Button btn_submit_details;
    String address_key,version,value_shop_city="";
    TextView tv_approx_distance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_delivery_request);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_delivery_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = DeliveryRequerstActivity.this;
        intViews();

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }

        getAndroidkey();

        // Initials the value of the Object of the
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setCountry("IN");

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place_et: " + place.getName() + ", " + place.getId() +", "+place.getLatLng());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        final List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        autocompleteFragment.getView().setVisibility(View.GONE);

        // Get the location of From place
        et_from.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 1){
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(DeliveryRequerstActivity.this);
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_FROM_CODE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Get the location from the To place API
        et_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() == 1){
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(DeliveryRequerstActivity.this);
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_TO_CODE);
                    //if(s.length()>3) {
                        getDistanceCalculate("flag");
                    //}
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        btn_submit_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_from.getText().toString().isEmpty()){
                    et_from.setError("Please Enter From Place");
                }else  if(et_to.getText().toString().isEmpty()){
                    et_to.setError("Please Enter destination Place");
                }else  if(et_box_height.getText().toString().isEmpty()){
                    et_box_height.setError("Please Enter Box height");
                }else  if(et_box_width.getText().toString().isEmpty()){
                    et_box_width.setError("Please Enter the Box width");
                }else  if(et_box_weight.getText().toString().isEmpty()){
                    et_box_weight.setError("Please Enter Box weight");
                }else  if(et_no_items.getText().toString().isEmpty()){
                    et_no_items.setError("Please Enter no Items");
                }else {
                    //et_from,et_to,et_box_height,et_box_width,et_box_breadth,et_box_weight,et_no_items;
                    AddDeliveryDetails();
                }
            }
        });

    }

     void intViews(){

         et_from = findViewById(R.id.et_from);
         et_to = findViewById(R.id.et_to);
         et_box_height= findViewById(R.id.et_box_height);
         et_box_width = findViewById(R.id.et_box_width);
         et_box_breadth = findViewById(R.id.et_box_breadth);
         et_box_weight = findViewById(R.id.et_box_weight);
         et_no_items = findViewById(R.id.et_no_items);
         btn_submit_details = findViewById(R.id.btn_submit_details);
         tv_approx_distance = findViewById(R.id.tv_approx_distance);
     }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_FROM_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                // Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                Log.i(TAG, "Place_res: " + place.getName() + ", " + place.getId() + ", " + place.getLatLng());
                if (place != null) {

                    LatLng latLng = place.getLatLng();

                    latitude = String.valueOf(latLng.latitude);
                    longitude = String.valueOf(latLng.longitude);

                    Log.d("latlong", latitude);
                    et_from.setText(place.getName());
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if (requestCode == AUTOCOMPLETE_REQUEST_TO_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                // Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                Log.i(TAG, "Place_res: " + place.getName() + ", " + place.getId() + ", " + place.getLatLng());
                if (place != null) {

                    LatLng latLng = place.getLatLng();

                    to_latitude = String.valueOf(latLng.latitude);
                    to_longitude = String.valueOf(latLng.longitude);

                    Log.d("latlong", latitude);
                    et_to.setText(place.getName());
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

     // API to Store Delivery Details
     public void AddDeliveryDetails() {
        if(Connectivity.isConnected(mContext)) {

            progressDialog.show();

            String url = StaticUrl.urldeliverydetails;

            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    try {

                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e(TAG, "resDelivery_Details" + resultResponse.toString());
                        Log.e(TAG, "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status = true)
                        {
                            JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                            /*shop_id = jsonObjectData.getString("shop_id");
                            seller_name =  jsonObjectData.getString("name");
                            admin_type =  jsonObjectData.getString("admin_type");*/
                            //getDeliveryBoyDetails();

                            Toast.makeText(mContext,"Delivery Record Registered Successfully",Toast.LENGTH_LONG).show();
                            finish();

                        }else if(status = false){
                            Log.d(TAG,"status_false"+resultResponse);
                            Toast.makeText(mContext,"Delivery Details Not Done..!!",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleyDelivery_EXP:", "" + e.getMessage());
                        progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;

                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";

                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {


                    Map<String, String> params = new HashMap<>();
                    params.put("shop_id", sharedPreferencesUtils.getShopID());
                    params.put("d_from", et_from.getText().toString());
                    params.put("d_destination", et_to.getText().toString());
                    params.put("d_no_items", et_no_items.getText().toString());
                    params.put("d_weight", et_box_weight.getText().toString());
                    params.put("d_width", et_box_width.getText().toString());
                    params.put("d_height", et_box_height.getText().toString());
                    params.put("from_latitude", latitude);
                    params.put("from_longitude", longitude);
                    params.put("to_latitude", to_latitude);
                    params.put("to_longitude", to_longitude);
                    //params.put("imageurl", "test_url");


                    Log.e("Params", "" + params);
                    return params;
                }

           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = "#concord$" + ":" + "!@#con co@$rd!@#";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }*/

                /*@Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("imageurl", new DataPart(getBytesFromBitmap(delivery_boybitmap)));

                    Log.e("ParamsImg", "" + params);
                    return params;
                }*/
            };

            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        }
        else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    // API to Get the Delivery Distance calculation
    public void getDistanceCalculate(String request_type){
        progressDialog.show();

        String targetUrlShopList = "https://www.picodel.com/seller/franchisee/getdeliverydistance"+"?shop_id="+sharedPreferencesUtils.getShopID()
                +"&flag="+request_type+"&from_latitude="+latitude+"&from_longitude="+longitude+"&to_latitude="+to_latitude+"&to_longitude="+to_longitude;

        Log.d("targetUrlDistance",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Insta_Distance",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            String Distance_value = jsonObject.getString("d_distance");
                            Log.e("Distance_log",""+Distance_value);
                            tv_approx_distance.setText("Approx Distance:"+Distance_value+"   km");
                             if(strStatus=false){
                                //tv_network_con.setVisibility(View.VISIBLE);
                                //tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    //get SecreteKey Verify
    public void getAndroidkey(){

        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.androidkey
                    +"?app="+"shopkeeper";

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){

                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    address_key = jsonObjectData.getString("address_key");
                                    version =  jsonObjectData.getString("version");
                                    Log.e("androidkey",""+jsonObjectData);

                                    // Create a new Places client instance
                                    Places.initialize(getApplicationContext(), address_key);
                                    // Create a new Places client instance
                                    PlacesClient placesClient = Places.createClient(DeliveryRequerstActivity.this);

                                }else if(!statuss) {

                                    //Toast.makeText(mContext,"Mobile Number is not Register..!!",Toast.LENGTH_LONG).show();
                                    getError("\n\t Android Key Not Found Please Check.");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void getError (String Error){
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(DeliveryRequerstActivity.this);
        alertDialogBuilder.setMessage(Error);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
