package com.podmerchant.activites;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.podmerchant.R;
import com.podmerchant.adapter.CustomSpinnerAdapter;
import com.podmerchant.local_storage.DBHelper;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.GateWay;
import com.podmerchant.util.HorizontalListView;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.ShowcaseViewBuilder;
import com.podmerchant.util.VolleySingleton;
import com.podmerchant.util.ZoomImage;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import static com.podmerchant.util.GateWay.getConnectivityStatusString;
import static com.podmerchant.util.GateWay.slideDown;
import static com.podmerchant.util.GateWay.slideUp;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView tvMessage, tvYes, tvNo;
    private AlertDialog alertDialog;
    private TextView tvAddToCart;
    private TextView tvWishList;
    private DialogPlus dialog;
    private String pId;
    private String pName;
    private String product_Size;
    private String strEmail;
    private String strName;
    private String strReviewFinal;
    private String strRating;
    private String shopId;
    private String Product_Name;
    private String Product_size;
    private String selectCondition;
    private String strContact;
    private String strCity;
    private String strArea;
    private int count;
    private TextView tvProductName;
    private TextView tvBrandName;
    private TextView tvSellerName;
    private TextView tvPrice;
    private TextView tvTotal;
    private TextView tvDiscount;
    private TextView tvDiscount1;
    private TextView tvMrp;
    private TextView tvReviewTitle;
    private TextView tvSpecification;
    private TextView tvDescription;
    private TextView tvRatingValue;
    private TextView tvRatingMessage;
    private TextView tvProductHindiName;
    private ImageView productImage, similarProductImage, imgWishList, imgWishListSelected, img_sentiment;
    private EditText etQuantity, etEmail, etName, etWriteReview;
    private Button btnAddToCart, btnBuyProduct;
    private RecyclerView similarProductRecyclerView;
    private CoordinatorLayout productInformationMainLayout;
    private LinearLayout similarProductLinearLayout, linearLayoutSelectCondition, linearLayoutSelectType, linearLayoutSelectSize, buttons;
    private ListView reviewList;
    private String product_id = null;
    private String strCheckQty;
    private RelativeLayout descriptionLinearLayout;
    private ArrayList<productCodeWithProductList> FinalList = new ArrayList<>();
    private final ArrayList<String> product_ids = new ArrayList<>();
    private ArrayList<productCodeWithProductList> allProductItemsList = new ArrayList<>();
    private int CartCount, WishListCount;
    private int selectedPosition;
    private MyReviewCardAdapter myReviewCardAdapter;
    private SimilarProductCardAdapter similarProductCardAdapter;
    private final LinkedHashMap similarProductListHashMap = new LinkedHashMap();// will contain all
    private final ArrayList<ProductCodeWiseProduct> similarProductWiseList = new ArrayList<>();
    private final ArrayList<productCodeWithProductList> SimilarFinalList = new ArrayList<>();
    private TextView tvRipeType;
    private TextView tvRawType;
    private TextView tvSmallSize;
    private TextView tvMediumSize;
    private TextView tvLargeSize;
    private ShowcaseViewBuilder showcaseViewBuilder;
    private FrameLayout frameimgMsg;
    private HorizontalListView listView, listViewForImage;
    private LinearLayout linListview;
    private View view2;
    private final ArrayList<String> ImagesArrayList = new ArrayList<>();
    private final String class_name = this.getClass().getSimpleName();
    private LinkedHashSet<GateWay.ReviewPOJO> review = new LinkedHashSet<>();
    private TextView txtNoConnection;
    private BroadcastReceiver myReceiver;
    private LinearLayout Main_Layout_NoInternet;
    private NestedScrollView scroll;
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detials_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_product_information);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setTitle(R.string.title_activity_product_information);

        mContext =ProductDetailActivity.this;
        myReceiver = new Network_Change_Receiver();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Product_Name = bundle.getString("product_name");
            shopId = bundle.getString("shop_id");
        }

        final GateWay gateWay = new GateWay(mContext);
        strCity = "Maharashtra";//gateWay.getCity();
        strArea = "Pune";//gateWay.getArea();
        strContact = sharedPreferencesUtils.getPhoneNumber();//gateWay.getContact();
        strEmail = sharedPreferencesUtils.getEmailId();//gateWay.getUserEmail();
        strName = sharedPreferencesUtils.getUserName();//gateWay.getUserName();

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        buttons = findViewById(R.id.buttons);
        scroll = findViewById(R.id.scroll);
        txtNoConnection = findViewById(R.id.txtNoConnection);
        tvProductName = findViewById(R.id.txtProductName);
        tvProductHindiName = findViewById(R.id.txtProductHindiName);
        tvBrandName = findViewById(R.id.txtBrandName);
        tvSellerName = findViewById(R.id.txtSellerName);
        tvPrice = findViewById(R.id.txtPrice);
        tvDiscount = findViewById(R.id.txtDiscount);
        tvDiscount1 = findViewById(R.id.txtDiscount1);
        frameimgMsg = findViewById(R.id.frameimgMsg);
        linListview = findViewById(R.id.linListview);
        view2 = findViewById(R.id.view2);

        listView = findViewById(R.id.listView);
        listViewForImage = findViewById(R.id.listViewForImage);

        tvTotal = findViewById(R.id.txtTotal);
        tvMrp = findViewById(R.id.txtMrp);
        tvReviewTitle = findViewById(R.id.txtReviewTitle);
        tvSpecification = findViewById(R.id.txtSpecification);
        tvDescription = findViewById(R.id.txtDescription);
        imgWishList = findViewById(R.id.wishList);
        imgWishListSelected = findViewById(R.id.wishList1);
        etQuantity = findViewById(R.id.editQuantity);
        etQuantity.setText("1");
        btnAddToCart = findViewById(R.id.btnAddToCart);
        btnBuyProduct = findViewById(R.id.btnBuyProduct);
        TextView tvWriteReview = findViewById(R.id.btnWriteReview);
        descriptionLinearLayout = findViewById(R.id.descriptionRelativeLayout);
        productImage = findViewById(R.id.imgProductImage);
        similarProductImage = findViewById(R.id.similarProductImage);
        productInformationMainLayout = findViewById(R.id.productInformationMainLayout);
        productInformationMainLayout.setVisibility(View.GONE);
        linearLayoutSelectCondition = findViewById(R.id.selectCondition);
        linearLayoutSelectType = findViewById(R.id.linearLayoutSelectType);
        linearLayoutSelectSize = findViewById(R.id.linearLayoutSelectSize);
        tvRipeType = findViewById(R.id.txtRipeType);
        tvRawType = findViewById(R.id.txtRawType);
        tvSmallSize = findViewById(R.id.txtSmallSize);
        tvMediumSize = findViewById(R.id.txtMediumSize);
        tvLargeSize = findViewById(R.id.txtLargeSize);

        reviewList = findViewById(R.id.reviewList);
        reviewList.setFocusable(false);
        reviewList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });


        listViewForImage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
             /*   UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on image", mContext);
*/
                setItemNormal("image");
                setItemSelected(view, "image");
                setImage(ImagesArrayList.get(pos));
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int gridViewPosition, long arg3) {
                setItemNormal("text");
                setItemSelected(view, "text");

                try {
                    for (int i = 0; i < allProductItemsList.size(); i++) {
                        productCodeWithProductList movie = allProductItemsList.get(i);
                        final ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(gridViewPosition);
                        movie.setPosition(gridViewPosition); //set here position when user any wants to buy multiple size products


                        String maincat = movie.productCodeWiseProducts.get(gridViewPosition).product_maincat;
                       /* if(CartCount>0){

                        }else {
                            popupLimitbyCat(maincat);
                        }*/

                        if (tp.getSimilar_product_status().equals("Show")) {
                            similarProductImage.setVisibility(View.VISIBLE);
                        } else {
                            similarProductImage.setVisibility(View.GONE);
                        }

                        switch (tp.getSelect_type()) {
                            case "Size":
                                linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                linearLayoutSelectType.setVisibility(View.GONE);
                                linearLayoutSelectSize.setVisibility(View.VISIBLE);
                                break;
                            case "Condition":
                                linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                linearLayoutSelectType.setVisibility(View.VISIBLE);
                                linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                            default:
                                linearLayoutSelectCondition.setVisibility(View.GONE);
                                linearLayoutSelectType.setVisibility(View.GONE);
                                linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                        }

                        String QTY = tp.getStrAvailable_Qty();
                        if (QTY.equals("0")) {
                            btnAddToCart.setText("OUT OF STOCK");
                            etQuantity.setVisibility(View.GONE);
                            btnAddToCart.setTextColor(getResources().getColor(R.color.red));
                        } else {
                            btnAddToCart.setText("Add to Cart");
                            etQuantity.setVisibility(View.VISIBLE);
                            btnAddToCart.setTextColor(getResources().getColor(R.color.black));
                        }

                        String product_id = tp.getProduct_id();

  /*                      UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": clicked on unit: " + tp.getProduct_size() + " & product is: " + tp.getProduct_name() + " " + tp.getProduct_size(), mContext);
*/
                        requestFromServerOfProductReview(product_id); //this is method for get review from server

                        //TODO here product image setup to glide
                        /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                                .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                                .error(R.drawable.ic_app_transparent)
                                .fitCenter()
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(productImage);*/
                        Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                .placeholder(R.drawable.loading)
                                .error(R.drawable.ic_app_transparent)
                                .into(productImage);

                        ImagesArrayList.clear();
                        if (!tp.getProduct_image().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image());
                        }
                        if (!tp.getProduct_image1().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image1());
                        }
                        if (!tp.getProduct_image2().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image2());
                        }
                        if (!tp.getProduct_image3().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image3());
                        }
                        if (!tp.getProduct_image4().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image4());
                        }
                        if (ImagesArrayList.size() >= 1) {
                            CustomAdapter1 customAdapter1 = new CustomAdapter1(ProductDetailActivity.this, ImagesArrayList);
                            listViewForImage.setAdapter(customAdapter1);
                        }

                        String size = tp.getProduct_size();
                        tvProductName.setText(tp.getProduct_name() + " " + size);

                        final String img1 = tp.getProduct_image();
                        final String img2 = tp.getProduct_image1();
                        final String img3 = tp.getProduct_image2();
                        final String img4 = tp.getProduct_image3();
                        final String img5 = tp.getProduct_image4();

                        productImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(mContext, ZoomImage.class);
                                intent.putExtra("img1", img1);
                                intent.putExtra("img2", img2);
                                intent.putExtra("img3", img3);
                                intent.putExtra("img4", img4);
                                intent.putExtra("img5", img5);
                                startActivity(intent);
                            }
                        });

                        if (tp.getStrHindiName().equals("")) {
                            tvProductHindiName.setVisibility(View.GONE);
                        } else {
                            tvProductHindiName.setText(" ( " + tp.getStrHindiName() + " ) ");
                        }

                        String dis = tp.getProduct_discount();
                        String discount = tp.setProduct_discount(dis);
                        tvDiscount.setText(discount + "% off");
                        tvDiscount1.setText(discount + "% off");
                        String productMrp = tp.getProduct_mrp();
                        String discountPrice = tp.getProduct_price();
                        if (productMrp.equals(discountPrice)) {
                            tvPrice.setVisibility(View.VISIBLE);
                            tvDiscount.setVisibility(View.GONE);
                            frameimgMsg.setVisibility(View.GONE);
                            tvMrp.setVisibility(View.GONE);
                        } else {
                            tvPrice.setVisibility(View.VISIBLE);
                            tvDiscount.setVisibility(View.VISIBLE);
                            frameimgMsg.setVisibility(View.VISIBLE);
                            tvMrp.setVisibility(View.VISIBLE);
                            tvMrp.setBackgroundResource(R.drawable.dash);
                        }
                        tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                        tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
                        tvTotal.setText("Total:" + " " + "\u20B9" + " " + discountPrice + "/-");

                        String check_tag = "check_on_click";
                        changeButtonName(product_id, check_tag); //this is for just check product is addToCart or not

                        changeWishListIcon(product_id); //this is for just check product is wishLIst or not

                        tvRipeType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = tvRipeType.getText().toString();
                                for (int i = 0; i < allProductItemsList.size(); i++) {
                                    productCodeWithProductList movie = allProductItemsList.get(i);
                                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(gridViewPosition);
                                    tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                }
                            }
                        });

                        tvRawType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = tvRawType.getText().toString();
                                for (int i = 0; i < allProductItemsList.size(); i++) {
                                    productCodeWithProductList movie = allProductItemsList.get(i);
                                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(gridViewPosition);
                                    tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                }
                            }
                        });

                        tvSmallSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = tvSmallSize.getText().toString();
                                for (int i = 0; i < allProductItemsList.size(); i++) {
                                    productCodeWithProductList movie = allProductItemsList.get(i);
                                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(gridViewPosition);
                                    tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                }
                            }
                        });

                        tvMediumSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = tvMediumSize.getText().toString();
                                for (int i = 0; i < allProductItemsList.size(); i++) {
                                    productCodeWithProductList movie = allProductItemsList.get(i);
                                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(gridViewPosition);
                                    tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                }
                            }
                        });

                        tvLargeSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = tvLargeSize.getText().toString();
                                for (int i = 0; i < allProductItemsList.size(); i++) {
                                    productCodeWithProductList movie = allProductItemsList.get(i);
                                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(gridViewPosition);
                                    tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                                }
                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        etQuantity.setOnClickListener(this);
        btnAddToCart.setOnClickListener(this);
        tvWriteReview.setOnClickListener(this);
        btnBuyProduct.setOnClickListener(this);
        imgWishList.setOnClickListener(this);
        imgWishListSelected.setOnClickListener(this);
        similarProductImage.setOnClickListener(this);

        if(Connectivity.isConnected(mContext)){
            Main_Layout_NoInternet.setVisibility(View.GONE);
            buttons.setVisibility(View.VISIBLE);
            scroll.setVisibility(View.VISIBLE);

            txtNoConnection.setText("Back online");
            txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
            slideDown(txtNoConnection);
            getSingleProductInformation();
        }else {

            Main_Layout_NoInternet.setVisibility(View.VISIBLE);
            buttons.setVisibility(View.GONE);
            scroll.setVisibility(View.GONE);
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }

            txtNoConnection.setText("No connection");
            txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
            slideUp(txtNoConnection);

        }
    }


    @Override
    protected void onPause() {
/*
        unregisterReceiver(myReceiver);
        hideKeyboard(ProductDetailActivity.this);
*/
        super.onPause();
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.GONE);
                scroll.setVisibility(View.GONE);
                if (dialog != null) {
                    dialog.dismiss();
                    dialog = null;
                }

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                buttons.setVisibility(View.VISIBLE);
                scroll.setVisibility(View.VISIBLE);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
                getSingleProductInformation();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void setItemSelected(View view, String tag) {
        if (tag.equals("text")) {
            TextView tv = view.findViewById(R.id.txtItem);
            tv.setTextColor(getResources().getColor(R.color.colorPrimary));
            tv.setBackgroundResource(R.drawable.image_outlate);
        } else {
            ImageView imageView = view.findViewById(R.id.imgProductImage);
            imageView.setBackgroundResource(R.drawable.image_outlate);
        }
    }

    private void setItemNormal(String tag) {
        if (tag.equals("text")) {
            for (int i = 0; i < listView.getChildCount(); i++) {
                View v = listView.getChildAt(i);
                TextView txtview = v.findViewById(R.id.txtItem);
                txtview.setTextColor(Color.BLACK);
                txtview.setBackgroundResource(R.drawable.login_stroke);
            }
        } else {
            for (int i = 0; i < listViewForImage.getChildCount(); i++) {
                View v = listViewForImage.getChildAt(i);
                ImageView imageView = v.findViewById(R.id.imgProductImage);
                imageView.setBackgroundResource(R.drawable.login_stroke);
            }
        }
    }

    private void requestFromServerOfProductReview(String product_id) { //TODO Server method here
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            myReviewCardAdapter = new MyReviewCardAdapter(mContext);

            JSONObject params = new JSONObject();
            try {
                params.put("product_id", product_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetProductReviewResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            reviewList.setVisibility(View.GONE);
                            tvReviewTitle.setVisibility(View.VISIBLE);
                            tvReviewTitle.setVisibility(View.VISIBLE);
                        } else {
                            reviewList.setVisibility(View.VISIBLE);
                            tvReviewTitle.setVisibility(View.VISIBLE);
                            tvReviewTitle.setVisibility(View.VISIBLE);

                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");

                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = mainClassificationJsonArray.getJSONObject(i);

                                ProductRating productrating = new ProductRating(jSonClassificationData.getString("name"), jSonClassificationData.getString("review"),
                                        jSonClassificationData.getString("rating"), jSonClassificationData.getString("product_id"), jSonClassificationData.getString("rdate"));
                                myReviewCardAdapter.add(productrating);
                            }
                            reviewList.setAdapter(myReviewCardAdapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(mContext);
            gateWay.displaySnackBar(productInformationMainLayout);
        }
    }

    private void getSingleProductInformation() { //TODO Server method here
        final LinkedHashMap productListHashMap = new LinkedHashMap();
        final ArrayList<ProductCodeWiseProduct> productWiseList = new ArrayList<>();
        FinalList = new ArrayList<>();

        if (Connectivity.isConnected(mContext)) {
            JSONObject params = new JSONObject();
            try {
                params.put("product_name", Product_Name);
                params.put("shop_id", shopId);
                params.put("city", strCity);
                params.put("area", strArea);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetSingleProductInformation, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("single_product_info:",""+response.toString());
                    productInformationMainLayout.setVisibility(View.VISIBLE);
                    try {
                        JSONArray mainJsonArray = response.getJSONArray("posts");

                        for (int i = 0; i < mainJsonArray.length(); i++) {
                            JSONObject jsonProductData = mainJsonArray.getJSONObject(i);

                            productListHashMap.put(jsonProductData.getString("code"), "");
                            product_ids.add(jsonProductData.getString("product_id"));

                            productWiseList.add(new ProductCodeWiseProduct(jsonProductData.getString("code"),
                                    jsonProductData.getString("shop_id"), jsonProductData.getString("shop_name"),
                                    jsonProductData.getString("shop_category"), jsonProductData.getString("product_name"),
                                    jsonProductData.getString("product_brand"), jsonProductData.getString("product_id"),
                                    jsonProductData.getString("product_image"), jsonProductData.getString("product_image1"),
                                    jsonProductData.getString("product_image2"), jsonProductData.getString("product_image3"),
                                    jsonProductData.getString("product_image4"), jsonProductData.getString("product_size"),
                                    jsonProductData.getString("product_mrp"), jsonProductData.getString("product_price"),
                                    jsonProductData.getString("product_discount"), jsonProductData.getString("product_description"),
                                    jsonProductData.getString("similar_product_status"), jsonProductData.getString("type"),
                                    jsonProductData.getString("product_qty"), jsonProductData.getString("product_maincat"),jsonProductData.getString("hindi_name")));
                        }
                        for (Object o : productListHashMap.keySet()) {
                            String key = (String) o;
                            productCodeWithProductList withProductCode = new productCodeWithProductList();
                            withProductCode.code = key;
                            withProductCode.productCodeWiseProducts = new ArrayList<>();
                            for (ProductCodeWiseProduct pp : productWiseList) {
                                if (pp.code.equals(key)) {
                                    withProductCode.productCodeWiseProducts.add(pp);
                                }
                            }
                            FinalList.add(withProductCode);
                            allProductItemsList = FinalList;
                        }
                        setAllDataToActivity();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    private void setAllDataToActivity() {
        ArrayList<String> sizesArrayList = new ArrayList<>();
        ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

        try {
            /*--------------------------------Start for sizes------------------------------*/
            try {
                for (int i = 0; i < allProductItemsList.size(); i++) {
                    productCodeWithProductList newProductList = allProductItemsList.get(i);
                    for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                        ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                        if (!tp.getProduct_size().equals("")) {
                            sizesArrayList.add(tp.getProduct_size());
                        }

                        if (sizesArrayList.size() > 0) {
                            linListview.setVisibility(View.VISIBLE);
                            view2.setVisibility(View.VISIBLE);
                            CustomAdapter customAdapter = new CustomAdapter(ProductDetailActivity.this, sizesArrayList);
                            listView.setAdapter(customAdapter);
                        } else {
                            linListview.setVisibility(View.GONE);
                            view2.setVisibility(View.GONE);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                for (int i = 0; i < allProductItemsList.size(); i++) {
                    productCodeWithProductList newProductList = allProductItemsList.get(i);
                    for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                        ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                        DiscountpricesArrayList.add(tp.getProduct_price());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*--------------------------------End for sizes------------------------------*/

            /*--------------------------------Start for default------------------------------*/
            try {
                for (int i = 0; i < allProductItemsList.size(); i++) {
                    productCodeWithProductList movie = allProductItemsList.get(i);
                    ProductCodeWiseProduct tp;
                    if (movie.productCodeWiseProducts.size() == 1) {
                        tp = movie.productCodeWiseProducts.get(0);
                    } else {
                        tp = movie.productCodeWiseProducts.get(selectedPosition);
                    }

                    ImagesArrayList.clear();
                    try {
                        if (!tp.getProduct_image().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image());
                        }
                        if (!tp.getProduct_image1().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image1());
                        }
                        if (!tp.getProduct_image2().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image2());
                        }
                        if (!tp.getProduct_image3().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image3());
                        }
                        if (!tp.getProduct_image4().equals("")) {
                            ImagesArrayList.add(tp.getProduct_image4());
                        }

                        if (ImagesArrayList.size() >= 1) {
                            CustomAdapter1 customAdapter1 = new CustomAdapter1(ProductDetailActivity.this, ImagesArrayList);
                            listViewForImage.setAdapter(customAdapter1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String product_id = tp.getProduct_id();

                    if (tp.getSimilar_product_status().equals("Show")) {
                        similarProductImage.setVisibility(View.VISIBLE);
                    } else {
                        similarProductImage.setVisibility(View.GONE);
                    }

                    switch (tp.getSelect_type()) {
                        case "Size":
                            linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                            linearLayoutSelectType.setVisibility(View.GONE);
                            linearLayoutSelectSize.setVisibility(View.VISIBLE);
                            break;
                        case "Condition":
                            linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                            linearLayoutSelectType.setVisibility(View.VISIBLE);
                            linearLayoutSelectSize.setVisibility(View.GONE);
                            break;
                        default:
                            linearLayoutSelectCondition.setVisibility(View.GONE);
                            linearLayoutSelectType.setVisibility(View.GONE);
                            linearLayoutSelectSize.setVisibility(View.GONE);
                            break;
                    }

                    //TODO here product image setup to glide
                   /* Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                            .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(productImage);*/
                    Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(productImage);

                    /*--------------------------------Start for selection of background drawable------------------------------*/
                    final String img1 = tp.getProduct_image();
                    final String img2 = tp.getProduct_image1();
                    final String img3 = tp.getProduct_image2();
                    final String img4 = tp.getProduct_image3();
                    final String img5 = tp.getProduct_image4();

                    productImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, ZoomImage.class);
                            intent.putExtra("img1", img1);
                            intent.putExtra("img2", img2);
                            intent.putExtra("img3", img3);
                            intent.putExtra("img4", img4);
                            intent.putExtra("img5", img5);
                            startActivity(intent);
                        }
                    });
                    /*--------------------------------End for selection of background drawable------------------------------*/

                    String size = tp.getProduct_size();
                    tvProductName.setText(tp.getProduct_name() + " " + size);

                    if (tp.getProduct_brand().equals("")) {
                        tvBrandName.setText("Brand:" + " " + "NA");
                    } else {
                        tvBrandName.setText("Brand:" + " " + tp.getProduct_brand());
                    }
                    if (tp.getProduct_description().equals("")) {
                        descriptionLinearLayout.setVisibility(View.GONE);
                    } else {
                        descriptionLinearLayout.setVisibility(View.VISIBLE);
                        tvDescription.setVisibility(View.VISIBLE);
                        tvSpecification.setVisibility(View.VISIBLE);
                        tvSpecification.setText(tp.getProduct_description());
                    }

                    if (tp.getStrHindiName().equals("")) {
                        tvProductHindiName.setVisibility(View.GONE);
                    } else {
                        tvProductHindiName.setText(" ( " + tp.getStrHindiName() + " ) ");
                    }
                    tvSellerName.setText("By:" + " " + tp.getShop_name());
                    String dis = tp.getProduct_discount();
                    String discount = tp.setProduct_discount(dis);
                    tvDiscount.setText(discount+ "% off");//String.format("%.2f", discount)
                    tvDiscount1.setText(discount+ "% off");
                    String productMrp = tp.getProduct_mrp();
                    String discountPrice = tp.getProduct_price();
                    if (productMrp.equals(discountPrice)) {
                        tvPrice.setVisibility(View.VISIBLE);
                        tvDiscount.setVisibility(View.GONE);
                        frameimgMsg.setVisibility(View.GONE);
                        tvMrp.setVisibility(View.GONE);
                    } else {
                        tvPrice.setVisibility(View.VISIBLE);
                        tvDiscount.setVisibility(View.VISIBLE);
                        frameimgMsg.setVisibility(View.VISIBLE);
                        tvMrp.setVisibility(View.VISIBLE);
                        tvMrp.setBackgroundResource(R.drawable.dash);
                    }
                    tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                    tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
                    tvTotal.setText("Total:" + " " + "\u20B9" + " " + discountPrice + "/-");

                    String check_tag = "check_on_click";
                    changeButtonName(product_id, check_tag); //this is for just check product is addToCart or not

                    changeWishListIcon(product_id); //this is for just check product is wishLIst or not
                }

                tvRipeType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectCondition = tvRipeType.getText().toString();
                        for (int i = 0; i < allProductItemsList.size(); i++) {
                            productCodeWithProductList movie = allProductItemsList.get(i);
                            ProductCodeWiseProduct tp;
                            if (movie.productCodeWiseProducts.size() == 1) {
                                tp = movie.productCodeWiseProducts.get(0);
                            } else {
                                tp = movie.productCodeWiseProducts.get(selectedPosition);
                            }
                            tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                        }
                    }
                });

                tvRawType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectCondition = tvRawType.getText().toString();
                        for (int i = 0; i < allProductItemsList.size(); i++) {
                            productCodeWithProductList movie = allProductItemsList.get(i);
                            ProductCodeWiseProduct tp;
                            if (movie.productCodeWiseProducts.size() == 1) {
                                tp = movie.productCodeWiseProducts.get(0);
                            } else {
                                tp = movie.productCodeWiseProducts.get(selectedPosition);
                            }
                            tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                        }
                    }
                });

                tvSmallSize.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectCondition = tvSmallSize.getText().toString();
                        for (int i = 0; i < allProductItemsList.size(); i++) {
                            productCodeWithProductList movie = allProductItemsList.get(i);
                            ProductCodeWiseProduct tp;
                            if (movie.productCodeWiseProducts.size() == 1) {
                                tp = movie.productCodeWiseProducts.get(0);
                            } else {
                                tp = movie.productCodeWiseProducts.get(selectedPosition);
                            }
                            tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                        }
                    }
                });

                tvMediumSize.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectCondition = tvMediumSize.getText().toString();
                        for (int i = 0; i < allProductItemsList.size(); i++) {
                            productCodeWithProductList movie = allProductItemsList.get(i);
                            ProductCodeWiseProduct tp;
                            if (movie.productCodeWiseProducts.size() == 1) {
                                tp = movie.productCodeWiseProducts.get(0);
                            } else {
                                tp = movie.productCodeWiseProducts.get(selectedPosition);
                            }
                            tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                        }
                    }
                });

                tvLargeSize.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectCondition = tvLargeSize.getText().toString();
                        for (int i = 0; i < allProductItemsList.size(); i++) {
                            productCodeWithProductList movie = allProductItemsList.get(i);
                            ProductCodeWiseProduct tp;
                            if (movie.productCodeWiseProducts.size() == 1) {
                                tp = movie.productCodeWiseProducts.get(0);
                            } else {
                                tp = movie.productCodeWiseProducts.get(selectedPosition);
                            }
                            tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*--------------------------------End for default------------------------------*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (similarProductImage.getVisibility() == View.VISIBLE) {
            displayTuto("1", "show");
        } else {
            displayTuto("1", "gone");
        }

    }

    private void displayTuto(String Tag, String similar) {
        if (similar.equals("show")) {
            switch (Tag) {
                case "1":
                    showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                            .setTargetView(similarProductImage)
                            .setBackgroundOverlayColor()
                            .singleUse("7")
                            .setRingColor()
                            .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                            .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                            .addCustomView(Gravity.TOP, "Similar Products !", "Here you can see similar products !", "Next")
                            .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
                    showcaseViewBuilder.show();

                    showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showcaseViewBuilder.hide();
                            displayTuto("3", "show");
                        }
                    });

                    break;
                default:
                    showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                            .setTargetView(btnBuyProduct)
                            .setBackgroundOverlayColor()
                            .singleUse("9")
                            .setRingColor()
                            .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                            .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                            .addCustomView(Gravity.CENTER_HORIZONTAL, "Buy Now !", "You can directly place your order from here !!", "Got it")
                            .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
                    showcaseViewBuilder.show();

                    showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showcaseViewBuilder.hide();
                        }
                    });
                    break;
            }
        } else {
            showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                    .setTargetView(btnBuyProduct)
                    .setBackgroundOverlayColor()
                    .singleUse("11")
                    .setRingColor()
                    .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                    .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                    .addCustomView(Gravity.CENTER_HORIZONTAL, "Buy Now !", "You can directly place your order from here !!", "Got it")
                    .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
            showcaseViewBuilder.show();

            showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcaseViewBuilder.hide();
                }
            });
        }
    }

    public class ProductCodeWiseProduct {

        String code;
        String shop_id;
        final String shop_name;
        final String shop_category;
        final String product_name;
        final String product_id;
        final String product_brand;
        final String product_image;
        final String product_image1;
        final String product_image2;
        final String product_image3;
        final String product_image4;
        final String product_size;
        final String product_mrp;
        final String product_price;
        String product_discount;
        final String product_description;
        final String similar_product_status;
        final String select_type;
        final String strAvailable_Qty;
        final String strHindiName;
        final String product_maincat;

        boolean status = false;
        private String product_qty;

        public String getStrAvailable_Qty() {
            return strAvailable_Qty;
        }

        public String getStrHindiName() {
            return strHindiName;
        }

        ProductCodeWiseProduct(String code, String shop_id, String shop_name, String shop_category,
                               String product_name, String product_brand, String product_id, String product_image,
                               String product_image1, String product_image2, String product_image3,
                               String product_image4, String product_size, String product_mrp, String product_price,
                               String product_discount, String product_description, String similar_product_status,
                               String type, String available_qty, String product_maincat,String hindi_name) {
            this.code = code;
            this.shop_id = shop_id;
            this.shop_name = shop_name;
            this.shop_category = shop_category;
            this.product_name = product_name;
            this.product_brand = product_brand;
            this.product_id = product_id;
            this.product_image = product_image;
            this.product_image1 = product_image1;
            this.product_image2 = product_image2;
            this.product_image3 = product_image3;
            this.product_image4 = product_image4;
            this.product_size = product_size;
            this.product_mrp = product_mrp;
            this.product_price = product_price;
            this.product_discount = product_discount;
            this.product_description = product_description;
            this.similar_product_status = similar_product_status;
            this.select_type = type;
            this.strAvailable_Qty = available_qty;
            this.product_maincat = product_maincat;
            this.strHindiName = hindi_name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }


        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getProduct_brand() {
            return product_brand;
        }


        public String getProduct_id() {
            return product_id;
        }


        public String getProduct_image() {
            return product_image;
        }

        public String getProduct_image1() {
            return product_image1;
        }

        public String getProduct_image2() {
            return product_image2;
        }

        public String getProduct_image3() {
            return product_image3;
        }

        public String getProduct_image4() {
            return product_image4;
        }

        public String getProduct_size() {
            return product_size;
        }

        public String getProduct_mrp() {
            return product_mrp;
        }

        public String getProduct_price() {
            return product_price;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
            return product_discount;
        }

        String getProduct_description() {
            return product_description;
        }

        public String getSimilar_product_status() {
            return similar_product_status;
        }

        public String getSelect_type() {
            return select_type;
        }

        public boolean isStatus(boolean check) {
            return status;
        }

        public boolean setStatus(boolean status) {
            this.status = status;
            return status;
        }

        public String getProduct_qty() {
            return product_qty;
        }

        public String getProduct_maincat() {
            return product_maincat;
        }
    }

    public class productCodeWithProductList {
        String code;
        int position;

        public ArrayList<ProductCodeWiseProduct> productCodeWiseProducts;
        public ArrayList<ProductCodeWiseProduct> similarProductCodeWiseProducts;

        public int getPosition() {
            return position;
        }


        public void setPosition(int position) {
            this.position = position;
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        final DBHelper db = new DBHelper(getApplicationContext());
        getMenuInflater().inflate(R.menu.menu_cart_wishlist, menu);

        MenuItem item = menu.findItem(R.id.action_login);
        MenuItemCompat.setActionView(item, R.layout.add_to_cart_notifitcation_icon);
        RelativeLayout notifyCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        tvAddToCart = notifyCount.findViewById(R.id.cartNumber);

        int cntBeforeClick = (int) db.fetchAddToCartCount();

        if (cntBeforeClick == 0) {
            tvAddToCart.setVisibility(View.INVISIBLE);
        } else {
            tvAddToCart.setVisibility(View.VISIBLE);
            tvAddToCart.setText("" + cntBeforeClick);
        }
        notifyCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on cart icon on toolbar", mContext);
*//*
                int cnt = (int) db.fetchAddToCartCount();
                if (cnt == 0) {
                    GateWay gateWay = new GateWay(mContext);
                    gateWay.cartAlertDialog();
                } else {
                    Intent intent = new Intent(mContext, AddToCartActivity.class);
                    startActivity(intent);
                }
            }
        });

        MenuItem item1 = menu.findItem(R.id.action_wish_list);
        MenuItemCompat.setActionView(item1, R.layout.wish_list_notification_icon);
        RelativeLayout notifyCount1 = (RelativeLayout) MenuItemCompat.getActionView(item1);

        tvWishList = notifyCount1.findViewById(R.id.cartNumber);

        int wishListCount = (int) db.fetchWishListCount();
        if (wishListCount == 0) {
            tvWishList.setVisibility(View.INVISIBLE);
        } else {
            tvWishList.setVisibility(View.VISIBLE);
            tvWishList.setText("" + wishListCount);
        }

        notifyCount1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               *//* UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on wishList icon on toolbar", mContext);
*//*
                int cnt = (int) db.fetchWishListCount();
                if (cnt == 0) {
                    GateWay gateWay = new GateWay(mContext);
                    gateWay.wishListAlertDialog();
                } else {
                    Intent intent = new Intent(mContext, WishList.class);
                    startActivity(intent);
                }
            }
        });
        notifyCount1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast toast = Toast.makeText(mContext, " Favourite", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.RIGHT, 0, 110);
                toast.show();
                return true;
            }
        });
        return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateAddToCartCount(final int count) {
        if (tvAddToCart == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0) {
                    tvAddToCart.setVisibility(View.INVISIBLE);
                } else {
                    tvAddToCart.setVisibility(View.VISIBLE);
                    tvAddToCart.setText("" + count);
                }
            }
        });
    }

    private void updateWishListCount(final int count) {
        if (tvWishList == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0) {
                    tvWishList.setVisibility(View.INVISIBLE);
                } else {
                    tvWishList.setVisibility(View.VISIBLE);
                    tvWishList.setText("" + count);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editQuantity:
                for (int i = 0; i < allProductItemsList.size(); i++) {
                    productCodeWithProductList movie = allProductItemsList.get(i);
                    ProductCodeWiseProduct tp;
                    if (movie.productCodeWiseProducts.size() == 1) {
                        tp = movie.productCodeWiseProducts.get(0);
                    } else {
                        tp = movie.productCodeWiseProducts.get(selectedPosition);
                    }

      /*              UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on quantity & product is: " + tp.getProduct_name() + " " + tp.getProduct_size(), mContext);
*/
                    double discountPrice = Double.parseDouble(tp.getProduct_price());
                    dialog = DialogPlus.newDialog(mContext)
                            .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.quantity_layout))
                            .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                            .setGravity(Gravity.CENTER)
                            .setPadding(20, 20, 20, 20)
                            .create();
                    dialog.show();
                    final TextView tvOne = (TextView) dialog.findViewById(R.id.txtOne);
                    final TextView tvTwo = (TextView) dialog.findViewById(R.id.txtTwo);
                    final TextView tvThree = (TextView) dialog.findViewById(R.id.txtThree);
                    final TextView tvFour = (TextView) dialog.findViewById(R.id.txtFour);
                    final TextView tvFive = (TextView) dialog.findViewById(R.id.txtFive);
                    final TextView tvSix = (TextView) dialog.findViewById(R.id.txtSix);
                    final TextView tvSeven = (TextView) dialog.findViewById(R.id.txtSeven);
                    final TextView tvEight = (TextView) dialog.findViewById(R.id.txtEight);
                    final TextView tvNine = (TextView) dialog.findViewById(R.id.txtNine);
                    final TextView tvTen = (TextView) dialog.findViewById(R.id.txtTen);
                    LinearLayout extraQuantityLinearLayout = (LinearLayout) dialog.findViewById(R.id.extraQuantityLinearLayout);
                    if (discountPrice <= 50) {
                        extraQuantityLinearLayout.setVisibility(View.VISIBLE);
                    }
                    tvOne.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String one = tvOne.getText().toString();
                            setQuantityMethod(one);
                        }
                    });
                    tvTwo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String two = tvTwo.getText().toString();
                            setQuantityMethod(two);
                        }
                    });
                    tvThree.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String three = tvThree.getText().toString();
                            setQuantityMethod(three);
                        }
                    });
                    tvFour.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String four = tvFour.getText().toString();
                            setQuantityMethod(four);
                        }
                    });
                    tvFive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String five = tvFive.getText().toString();
                            setQuantityMethod(five);
                        }
                    });
                    tvSix.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String six = tvSix.getText().toString();
                            setQuantityMethod(six);
                        }
                    });
                    tvSeven.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String seven = tvSeven.getText().toString();
                            setQuantityMethod(seven);
                        }
                    });
                    tvEight.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String eight = tvEight.getText().toString();
                            setQuantityMethod(eight);
                        }
                    });
                    tvNine.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String nine = tvNine.getText().toString();
                            setQuantityMethod(nine);
                        }
                    });
                    tvTen.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String ten = tvTen.getText().toString();
                            setQuantityMethod(ten);
                        }
                    });
                }
                break;

            case R.id.btnAddToCart:

                checkQuantityCartItemOnPlus("addCart");
                break;

            case R.id.wishList:
                addToWishList();
                break;

            case R.id.wishList1:
                removeFromWishList();
                break;

            case R.id.btnBuyProduct:
                checkQuantityCartItemOnPlus("buyNow");
                break;

            case R.id.similarProductImage:
                getSimilarProducts();
                break;

            case R.id.btnWriteReview:
                reviewAlertDialog();
                break;
        }
    }

    private void setQuantityMethod(String qty) {

        for (int i = 0; i < allProductItemsList.size(); i++) {
            productCodeWithProductList movie = allProductItemsList.get(i);
            ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(selectedPosition);


            int Available_Qty = Integer.parseInt(tp.getStrAvailable_Qty());

            if (Integer.parseInt(qty) > Available_Qty) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                alertDialogBuilder.setMessage("You cannot add more than " + Available_Qty + " quantities of this product.");
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            } else {
                etQuantity.setText(qty);
                String strTotalAmt = tp.getProduct_price();
                double totalAmt = Double.parseDouble(strTotalAmt);
                count = Integer.parseInt(qty);
                if (count > 0) {
                    calculateTotal(totalAmt, count);
                }
            }
        }
        dialog.dismiss();
    }

    private void setImage(String product_image) {
        try {
            if (!product_image.equals("")) {
                //TODO here product image setup to glide
               /* Glide.with(mContext).load("http://simg.picodel.com/" + product_image)
                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(productImage);*/
                Picasso.with(mContext).load("https://simg.picodel.com/" + product_image)
                        .placeholder(R.drawable.loading)
                        .into(productImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reviewAlertDialog() {
        GateWay gateWay = new GateWay(mContext);
        review = gateWay.fetchReviewData();

      /*  UserTracking UT = new UserTracking(UserTracking.context);
        UT.user_tracking(class_name + ": clicked on rate & review button", mContext);
*/
        dialog = DialogPlus.newDialog(mContext)
                .setContentHolder(new ViewHolder(R.layout.dialog_rating_review))
                .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                .setGravity(Gravity.BOTTOM)
                .setPadding(20, 20, 20, 20)
                .create();
        etEmail = (EditText) dialog.findViewById(R.id.editEmail);
        etName = (EditText) dialog.findViewById(R.id.editName);
        etEmail.setText(sharedPreferencesUtils.getEmailId());
        etName.setText(sharedPreferencesUtils.getUserName());
        etWriteReview = (EditText) dialog.findViewById(R.id.editWriteReview);
        RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        tvRatingValue = (TextView) dialog.findViewById(R.id.txtRatingValue);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        TextView tvSubmit = (TextView) dialog.findViewById(R.id.txtSubmit);
        tvRatingMessage = (TextView) dialog.findViewById(R.id.txtRatingMessage);
        img_sentiment = (ImageView) dialog.findViewById(R.id.sentiment);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                tvRatingValue.setText(String.valueOf(rating));
                for (GateWay.ReviewPOJO reviewPOJO : review) {
                    if (reviewPOJO.getRating().equals(String.valueOf(rating))) {
                        if (reviewPOJO.getReason().equals("")) {
                            tvRatingMessage.setVisibility(View.INVISIBLE);
                            img_sentiment.setVisibility(View.INVISIBLE);
                            tvRatingMessage.setText("");
                        } else {
                            tvRatingMessage.setVisibility(View.VISIBLE);
                            tvRatingMessage.setText(reviewPOJO.getReason());
                            img_sentiment.setVisibility(View.VISIBLE);
                            img_sentiment.setImageResource(reviewPOJO.getDrawableImage());
                            tvRatingMessage.setTextColor(ContextCompat.getColor(mContext, reviewPOJO.getColor()));
                        }
                    }
                }
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });
        dialog.show();
    }

    private void validation() {
        strEmail = etEmail.getText().toString();
        strName = etName.getText().toString();
        strReviewFinal = etWriteReview.getText().toString();
        strRating = tvRatingValue.getText().toString();

        if (strReviewFinal.matches("")) {
            etWriteReview.setError("Please enter review");
        } else {
            sendReviewDataToServer();
            dialog.dismiss();
        }
    }

    private void sendReviewDataToServer() { //TODO Server method here
        String pId = null;
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(mContext);
            gateWay.progressDialogStart();

            for (int i = 0; i < allProductItemsList.size(); i++) {
                productCodeWithProductList movie = allProductItemsList.get(i);
                int selectedPosition = movie.getPosition();
                ProductCodeWiseProduct forSimilarProduct = movie.productCodeWiseProducts.get(selectedPosition);
                pId = forSimilarProduct.getProduct_id();
            }

            JSONObject params = new JSONObject();
            try {
                params.put("product_id", pId);
                params.put("email", strEmail);
                params.put("fName", strName);
                params.put("review", strReviewFinal);
                params.put("rating", strRating);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlReviewResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String result = response.getString("posts");
                        if (result.equals("true")) {
                            Toast.makeText(mContext, "Thanks for giving rating & review.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(mContext);
            gateWay.displaySnackBar(productInformationMainLayout);
        }
    }

    private void calculateTotal(double totalAmt, int count) {
        double amt;
        amt = (totalAmt * count);
        long finalValue = Math.round(amt * 100) / 100;
        tvTotal.setText("Total:" + " " + "\u20B9" + " " + finalValue + "/-");
    }

    private void getSimilarProducts() {
        String pid = null;
        String strPName = null;
        String strPSize = null;
        for (int i = 0; i < allProductItemsList.size(); i++) {
            productCodeWithProductList movie = allProductItemsList.get(i);
            int selectedPosition = movie.getPosition();
            ProductCodeWiseProduct forSimilarProduct = movie.productCodeWiseProducts.get(selectedPosition);
            pid = forSimilarProduct.getProduct_id();
            strPName = forSimilarProduct.getProduct_name();
            strPSize = forSimilarProduct.getProduct_size();
        }
      /*  UserTracking UT = new UserTracking(UserTracking.context);
        UT.user_tracking(class_name + ": clicked on similar product button & product is: " + strPName + " " + strPSize, mContext);
*/
        dialog = DialogPlus.newDialog(mContext)
                .setContentHolder(new ViewHolder(R.layout.activity_similar_products))
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.BOTTOM)
                .create();

        similarProductLinearLayout = (LinearLayout) dialog.findViewById(R.id.similarProductLinearLayout);
        similarProductRecyclerView = (RecyclerView) dialog.findViewById(R.id.similarProductRecyclerView);
        similarProductRecyclerView.setHasFixedSize(true);
        LinearLayoutManager similarLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        similarProductRecyclerView.setLayoutManager(similarLayoutManager);
        similarProductsGetFromServer(pid);
    }

    private void similarProductsGetFromServer(String pId) { //TODO Server method here
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(mContext);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("product_id", pId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlSimilarResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            Toast.makeText(mContext, "There are no similar products", Toast.LENGTH_LONG).show();
                            similarProductLinearLayout.setVisibility(View.GONE);
                        } else {
                            similarProductListHashMap.clear();
                            similarProductWiseList.clear();
                            SimilarFinalList.clear();

                            similarProductLinearLayout.setVisibility(View.VISIBLE);
                            JSONArray mainShopJsonArray = response.getJSONArray("posts");

                            for (int i = 0; i < mainShopJsonArray.length(); i++) {
                                JSONObject jSonShopData = mainShopJsonArray.getJSONObject(i);

                                similarProductListHashMap.put(jSonShopData.getString("code"), "");

                                similarProductWiseList.add(new ProductCodeWiseProduct(jSonShopData.getString("code"), jSonShopData.getString("shop_id"),
                                        jSonShopData.getString("shop_name"), jSonShopData.getString("shop_category"),
                                        jSonShopData.getString("product_name"), jSonShopData.getString("product_brand"),
                                        jSonShopData.getString("product_id"), jSonShopData.getString("product_image"),
                                        jSonShopData.getString("product_image1"), jSonShopData.getString("product_image2"),
                                        jSonShopData.getString("product_image3"), jSonShopData.getString("product_image4"),
                                        jSonShopData.getString("product_size"), jSonShopData.getString("product_mrp"),
                                        jSonShopData.getString("product_price"), jSonShopData.getString("product_discount"),
                                        jSonShopData.getString("product_description"), jSonShopData.getString("similar_product_status"),
                                        jSonShopData.getString("type"), jSonShopData.getString("product_qty"),jSonShopData.getString("product_maincat"), jSonShopData.getString("hindi_name")));
                            }
                            dialog.show();
                            for (Object o : similarProductListHashMap.keySet()) {
                                String key = (String) o;
                                productCodeWithProductList withProductCode = new productCodeWithProductList();
                                withProductCode.code = key;
                                withProductCode.similarProductCodeWiseProducts = new ArrayList<>();
                                for (ProductCodeWiseProduct pp : similarProductWiseList) {
                                    if (pp.code.equals(key)) {
                                        withProductCode.similarProductCodeWiseProducts.add(pp);
                                    }
                                }
                                SimilarFinalList.add(withProductCode);
                            }
                            similarProductCardAdapter = new SimilarProductCardAdapter(SimilarFinalList);
                            similarProductRecyclerView.setAdapter(similarProductCardAdapter);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(mContext);
            gateWay.displaySnackBar(productInformationMainLayout);
        }
    }

    private void checkQuantityCartItemOnPlus(final String tag) { //TODO Server method here
        String strPName = null;
        String strPSize = null;

        try {
            for (int i = 0; i < allProductItemsList.size(); i++) {
                productCodeWithProductList movie = allProductItemsList.get(i);
                int selectedPosition = movie.getPosition();
                ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                String mainCat = movie.productCodeWiseProducts.get(selectedPosition).product_maincat;
               /* if(CartCount>0){

                }else {
                    popupLimitbyCat(mainCat);
                }
*/
                product_id = forWishList.getProduct_id();
                shopId = forWishList.getShop_id();
                Product_size = forWishList.getProduct_size();
                strPName = forWishList.getProduct_name();
                strPSize = forWishList.getProduct_size();
            }
            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name + ": clicked on " + tag + " button & product is: " + strPName + " " + strPSize, mContext);*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject params = new JSONObject();
        try {
            params.put("product_id", product_id);
            params.put("shop_id", shopId);
            params.put("size", Product_size);
            params.put("qty", etQuantity.getText().toString());
            params.put("contactNo", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (!response.isNull("posts")) {
                    try {
                        strCheckQty = response.getString("posts");
                        if (strCheckQty.equals("true")) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                            alertDialogBuilder.setMessage("You cannot add more than 1000 gm of this product.");
                            alertDialogBuilder.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int arg1) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        } else {
                            if (tag.equals("buyNow")) {
                                buyNowProduct();
                            } else {
                                addToCart();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                GateWay gateWay = new GateWay(mContext);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    String mainCategory;
    private void buyNowProduct() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet

            try {
                for (int i = 0; i < allProductItemsList.size(); i++) {
                    productCodeWithProductList movie = allProductItemsList.get(i);
                    int selectedPosition = movie.getPosition();
                    ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);
                    mainCategory = movie.productCodeWiseProducts.get(selectedPosition).getProduct_maincat();
                    product_id = forWishList.getProduct_id();
                    shopId = forWishList.getShop_id();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            JSONObject params = new JSONObject();
            try {
                params.put("product_id", product_id);
                params.put("shop_id", shopId);
                params.put("selectedType", selectCondition);
                params.put("contactNo", strContact);
                params.put("email", strEmail);
                params.put("versionCode", StaticUrl.versionName);
                params.put("Qty", etQuantity.getText().toString());
                params.put("sessionMainCat", mainCategory);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                  /*  if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(mContext);
                        HashMap AddToCartInfo;
                        AddToCartInfo = db.getCartDetails(product_id);
                        String strCartId = (String) AddToCartInfo.get("new_pid");

                        if (strCartId == null) {
                            db.insertCount(product_id);

                            int cnt = (int) db.fetchAddToCartCount();

                            updateAddToCartCount(cnt);

                            String check_tag = "";
                            changeButtonName(product_id, check_tag); //this is for just check product is addToCart or not

                        }
                        Intent intent = new Intent(mContext, AddToCart.class);
                        startActivity(intent);
                    }*/
                    try {
                        String posts = response.getString("posts");

                        if (posts.equals("true")){
                            DBHelper db = new DBHelper(mContext);
                            HashMap AddToCartInfo;
                            AddToCartInfo = db.getCartDetails(product_id);
                            String strCartId = (String) AddToCartInfo.get("new_pid");

                            if (strCartId == null) {
                                db.insertCount(product_id);

                                int cnt = (int) db.fetchAddToCartCount();

                                updateAddToCartCount(cnt);

                                String check_tag = "";
                                changeButtonName(product_id, check_tag); //this is for just check product is addToCart or not
                            }
                            Log.d("addCartItemsToServer1",""+posts);
                            Toast toast = Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }else {
                            openSessionDialog(posts);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(mContext);
            gateWay.displaySnackBar(productInformationMainLayout);
        }
    }

    String strmainCategory;
    private void addToCart() {
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            DBHelper db = new DBHelper(mContext);
            String product_id = null;
            String btn_text = btnAddToCart.getText().toString();
            try {
                for (int i = 0; i < allProductItemsList.size(); i++) {
                    productCodeWithProductList movie = allProductItemsList.get(i);
                    int selectedPosition = movie.getPosition();
                    ProductCodeWiseProduct forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);
                    strmainCategory =  movie.productCodeWiseProducts.get(selectedPosition).getProduct_maincat();
                    product_id = forAddToCart.getProduct_id();
                    shopId = forAddToCart.getShop_id();
                }

                HashMap AddToCartInfo;
                AddToCartInfo = db.getCartDetails(product_id);
                String strCartId = (String) AddToCartInfo.get("new_pid");

                if (btn_text.equals("Add to Cart")) {
                    if (strCartId == null) {
                        db.insertCount(product_id);

                        addCartToServerSide(product_id, shopId,strmainCategory);

                        int cnt = (int) db.fetchAddToCartCount();
                        updateAddToCartCount(cnt);

                        String check_tag = "";
                        changeButtonName(product_id, check_tag); //this is for just check product is addToCart or not
                    } else {
                        Intent intent = new Intent(mContext, AddToCartActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
                if (btn_text.equals("Go to Cart")) {
                    Intent intent = new Intent(mContext, AddToCartActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                if (btn_text.equals("OUT OF STOCK")) {
                    Toast.makeText(mContext, "Currently,This product is not available", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            GateWay gateWay = new GateWay(mContext);
            gateWay.displaySnackBar(productInformationMainLayout);
        }
    }

    private void addCartToServerSide(final String product_id, String shopId,String mainCategory) { //TODO Server method here
        JSONObject params = new JSONObject();
        try {
            params.put("product_id", product_id);
            params.put("shop_id", shopId);
            params.put("selectedType", selectCondition);
            params.put("versionCode", StaticUrl.versionName);
            params.put("Qty", etQuantity.getText().toString());
            params.put("contactNo", strContact);
            params.put("email", strEmail);
            params.put("sessionMainCat", mainCategory);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String posts = response.getString("posts");

                    if (posts.equals("true")){
                        DBHelper db = new DBHelper(mContext);
                        db.insertCount(pId);
                        int cnt = (int) db.fetchAddToCartCount();
                        updateAddToCartCount(cnt);

                        if (cnt >= 0) {
                            // forAddToCart.setStatus(true);
                            //  if (forAddToCart.isStatus(true)) {
                            // view.btnAddToCart.setText("Go To Cart");
                            // }
                        } else {
                            //forAddToCart.setStatus(false);
                            // if (forAddToCart.isStatus(false)) {
                            //holder.btnAddToCart.setText("Add To Cart");
                            // }
                        }
                        Log.d("addCartItemsToServer1",""+posts);
                        Toast toast = Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }else {
                        openSessionDialog(posts);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                GateWay gateWay = new GateWay(mContext);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void changeButtonName(String str_product_id, String check_tag) {
        DBHelper dbHelper = new DBHelper(mContext);
        HashMap infoChangeButtonName = dbHelper.getCartDetails(str_product_id);
        String addToCartQty = (String) infoChangeButtonName.get("new_pid");

        List<String> listProductNameAlreadyHave = new ArrayList<>(infoChangeButtonName.values());
        boolean addToCartResult = listProductNameAlreadyHave.contains(addToCartQty);

        productCodeWithProductList movie = allProductItemsList.get(0);
        int selectedPosition = movie.getPosition();
        ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(selectedPosition);
        String QTY = tp.getStrAvailable_Qty();

        if (addToCartResult) {
            btnAddToCart.setText("Go to Cart");
            etQuantity.setVisibility(View.INVISIBLE);
            if (check_tag.equals("check_on_click")) {
            } else {
                // Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (QTY.equals("0")) {
                btnAddToCart.setText("OUT OF STOCK");
                etQuantity.setVisibility(View.GONE);
                btnBuyProduct.setVisibility(View.GONE);
                btnAddToCart.setTextColor(getResources().getColor(R.color.red));
            } else {
                count = 1;
                etQuantity.setText("" + count);
                btnAddToCart.setText("Add to Cart");
                etQuantity.setVisibility(View.VISIBLE);
                btnBuyProduct.setVisibility(View.GONE);
                btnAddToCart.setTextColor(getResources().getColor(R.color.black));
            }
        }
    }

    private void addToWishList() {
        String strPName = null;
        DBHelper db = new DBHelper(mContext);

        try {
            for (int i = 0; i < allProductItemsList.size(); i++) {
                productCodeWithProductList movie = allProductItemsList.get(i);
                int selectedPosition = movie.getPosition();
                ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);
                product_id = forWishList.getProduct_id();
                shopId = forWishList.getShop_id();
                strPName = forWishList.getProduct_name();
            }

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name + ": clicked on add  Favourite button & product is: " + strPName, mContext);
*/
            HashMap wishListInfo;
            wishListInfo = db.getWishListDetails(product_id);
            String strCartId = (String) wishListInfo.get("pId");
            if (strCartId == null) {
                db.insertProductIntoWishList(product_id);

                int cnt = (int) db.fetchWishListCount();
                updateWishListCount(cnt);

                changeWishListIcon(product_id); //this is for just check product is wishLIst or not
            }
            if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                addWishListItemsToServer();
            } else {
                GateWay gateWay = new GateWay(mContext);
                gateWay.displaySnackBar(productInformationMainLayout);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addWishListItemsToServer() { //TODO Server method here
        GateWay gateWay = new GateWay(mContext);

        for (int i = 0; i < allProductItemsList.size(); i++) {
            productCodeWithProductList movie = allProductItemsList.get(i);
            selectedPosition = movie.getPosition();
            ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);
            shopId = forWishList.getShop_id();
            product_id = forWishList.getProduct_id();
        }

        JSONObject params = new JSONObject();
        try {
            params.put("product_id", product_id);
            params.put("shop_id", shopId);
            params.put("versionCode", StaticUrl.versionName);
            params.put("Qty", 1);
            params.put("contactNo", sharedPreferencesUtils.getPhoneNumber());
            params.put("email", sharedPreferencesUtils.getEmailId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAddMyWishListItems, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (!response.isNull("posts")) {
                    changeWishListIcon(product_id); //this is for just check product is wishLIst or not
                    Toast.makeText(mContext, "Successfully inserted this item to Favourite.", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                GateWay gateWay = new GateWay(mContext);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void changeWishListIcon(String product_id) {
        DBHelper db = new DBHelper(mContext);
        HashMap AddToCartInfo1 = db.getWishListDetails(product_id);
        String btnCheckStatus1 = (String) AddToCartInfo1.get("pId");
        if (btnCheckStatus1 == null) {
            imgWishList.setVisibility(View.VISIBLE);
            imgWishListSelected.setVisibility(View.GONE);
        } else {
            imgWishList.setVisibility(View.GONE);
            imgWishListSelected.setVisibility(View.VISIBLE);
        }
    }

    private void removeFromWishList() {
        String strPName = null;
        String strPSize = null;
        String product_id = null;

        DBHelper db = new DBHelper(mContext);
        try {
            for (int i = 0; i < allProductItemsList.size(); i++) {
                productCodeWithProductList movie = allProductItemsList.get(i);
                selectedPosition = movie.getPosition();
                ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);
                strPName = forWishList.getProduct_name();
                strPSize = forWishList.getProduct_size();
                product_id = forWishList.getProduct_id();
            }

           /* UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name + ": clicked on remove Favourite button & product is: " + strPName + " " + strPSize, mContext);*/

            db.deleteWishListProductItem(product_id);

            int cnt = (int) db.fetchWishListCount();
            updateWishListCount(cnt);

            imgWishList.setVisibility(View.VISIBLE);
            imgWishListSelected.setVisibility(View.GONE);

            deleteOneProductFromWishList(product_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteOneProductFromWishList(final String pid) { //TODO Server method here
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
                params.put("product_id", pid);
                params.put("tag", "delete_one_item");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteMyWishListItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        changeWishListIcon(pid); //this is for just check product is wishLIst or not
                        Toast.makeText(mContext, "Successfully remove this item from  Favourite.", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(mContext);
            gateWay.displaySnackBar(productInformationMainLayout);
        }
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();/*
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        hideKeyboard(mContext);

        if (Connectivity.isConnected(mContext)) {
            GateWay gateWay = new GateWay(mContext);
            SyncNormalCartItems UT1 = new SyncNormalCartItems(SyncNormalCartItems.context);
            UT1.syncNormalCartItems(gateWay.getContact(), gateWay.getUserEmail(), mContext);

            GateWay gateWay1 = new GateWay(mContext);
            SyncWishListItems UT2 = new SyncWishListItems(SyncWishListItems.context);
            UT2.syncWishListItems(gateWay1.getContact(), gateWay1.getUserEmail(), mContext);

            SyncData();

            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, mContext);
        }*/
    }

    private void SyncData() { //TODO Server method here
        GateWay gateWay = new GateWay(mContext);
        String strContact = sharedPreferencesUtils.getPhoneNumber();
        String strEmail = sharedPreferencesUtils.getEmailId();

        JSONObject params = new JSONObject();
        try {
            params.put("contact", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlGetCartCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    WishListCount = response.getInt("wishlist_count");
                    CartCount = response.getInt("normal_cart_count");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (CartCount >= 0) {
                        updateAddToCartCount(CartCount);
                    }
                    if (WishListCount >= 0) {
                        updateWishListCount(WishListCount);
                    }
                    for (int i = 0; i < allProductItemsList.size(); i++) {
                        productCodeWithProductList movie = allProductItemsList.get(i);
                        int selectedPosition = movie.getPosition();
                        ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(selectedPosition);
                        movie.setPosition(selectedPosition); //set here position when user any wants to buy multiple size products

                        changeButtonName(tp.getProduct_id(), "check_on_click"); //this is for just check product is addToCart or not
                        changeWishListIcon(tp.getProduct_id()); //this is for just check product is wishLIst or not
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    class ProductRating {

        final String userName;
        final String userReview;
        final String userRating;
        String date;
        final String product_Id;

        ProductRating(String userName, String userReview, String userRating, String product_Id, String date) {
            this.userName = userName;
            this.userReview = userReview;
            this.userRating = userRating;
            this.date = date;
            this.product_Id = product_Id;
        }

        public String getUserName() {
            return userName;
        }

        public String getUserReview() {
            return userReview;
        }

        public String getUserRating() {
            return userRating;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

    private class ProductReviewViewHolder {
        TextView tvUserName, tvUserDate, tvUserReview;
        RatingBar reviewUserRating;
    }

    private class MyReviewCardAdapter extends ArrayAdapter<ProductRating> {

        final List<ProductRating> cardList = new ArrayList<>();
        ProductRating card;
        ProductReviewViewHolder productReviewViewHolder;

        MyReviewCardAdapter(Context context) {
            super(context, R.layout.card_view_for_product_review);
        }

        public void clear() {
            cardList.clear();
        }

        @Override
        public void add(ProductRating object) {
            cardList.add(object);
            super.add(object);
        }

        @Override
        public int getCount() {
            return this.cardList.size();
        }

        @Override
        public ProductRating getItem(int position) {
            return this.cardList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(R.layout.card_view_for_product_review, parent, false);
                productReviewViewHolder = new ProductReviewViewHolder();

                productReviewViewHolder.tvUserName = convertView.findViewById(R.id.txtUserName);
                productReviewViewHolder.tvUserDate = convertView.findViewById(R.id.txtUserDate);
                productReviewViewHolder.tvUserReview = convertView.findViewById(R.id.txtUserReview);
                productReviewViewHolder.reviewUserRating = convertView.findViewById(R.id.reviewUserRating);

                convertView.setTag(productReviewViewHolder);
            } else {
                productReviewViewHolder = (ProductReviewViewHolder) convertView.getTag();
            }

            card = getItem(position);

            productReviewViewHolder.tvUserName.setText("by " + (card != null ? card.getUserName() : null));
            productReviewViewHolder.tvUserDate.setText(card.getDate());
            productReviewViewHolder.tvUserReview.setText(card.getUserReview());
            String t1 = card.getUserRating();
            float f1 = Float.parseFloat(t1);
            productReviewViewHolder.reviewUserRating.setRating(f1);
            return convertView;
        }
    }

    private class SimilarProductCardAdapter extends RecyclerView.Adapter<SimilarProductCardAdapter.SimilarProductsViewHolder> {

        List<productCodeWithProductList> mItems;

        SimilarProductCardAdapter(ArrayList<productCodeWithProductList> finalList) {
            this.mItems = finalList;
        }

        @NonNull
        @Override
        public SimilarProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_for_ultimate_products, parent, false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = similarProductRecyclerView.getChildAdapterPosition(v);
                            productCodeWithProductList movie = mItems.get(itemPosition);
                            ProductCodeWiseProduct forClickEvent = movie.similarProductCodeWiseProducts.get(movie.getPosition());

             /*               UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on view & product is: " + forClickEvent.getProduct_name() + " " + forClickEvent.getProduct_size(), mContext);
*/
                            Intent intent = new Intent(mContext, ProductDetailActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("product_name", forClickEvent.getProduct_name());
                            intent.putExtra("shop_id", forClickEvent.getShop_id());
                            startActivity(intent);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(productInformationMainLayout);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return new SimilarProductsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final SimilarProductsViewHolder holder, int position) {
            final DBHelper db = new DBHelper(mContext);

            ArrayList<String> sizesArrayList = new ArrayList<>();
            ArrayList<String> pricesArrayList = new ArrayList<>();
            ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp1 = newProductList.similarProductCodeWiseProducts.get(l);
                    sizesArrayList.add(tp1.getProduct_size());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp3 = newProductList.similarProductCodeWiseProducts.get(l);
                    DiscountpricesArrayList.add(tp3.getProduct_price());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                for (int l = 0; l < newProductList.similarProductCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp2 = newProductList.similarProductCodeWiseProducts.get(l);
                    pricesArrayList.add(tp2.getProduct_mrp());

                    if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                        holder.LayoutSpinner.setVisibility(View.VISIBLE);
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(mContext, pricesArrayList, DiscountpricesArrayList, sizesArrayList, "homepage");
                        holder.spinner.setAdapter(customSpinnerAdapter);
                    } else {
                        holder.LayoutSpinner.setVisibility(View.INVISIBLE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*--------------------------------for default value------------------------------*/
            try {
                productCodeWithProductList movie = mItems.get(position);
                ProductCodeWiseProduct tp = movie.similarProductCodeWiseProducts.get(0);

                if (tp.getStrHindiName().equals("")) {
                    holder.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    holder.tvProductHindiName.setVisibility(View.VISIBLE);
                    holder.tvProductHindiName.setText(tp.getStrHindiName());
                }

                HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                String btnCheckStatus = (String) AddToCartInfo.get("new_pid");
                if (btnCheckStatus == null) {
                    holder.btnAddToCart.setText("Add To Cart");
                } else {
                    holder.btnAddToCart.setText("Go To Cart");
                }

                //TODO here product image setup to glide
                /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.imgProduct);*/
                Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.ic_app_transparent)
                        .into(holder.imgProduct);

                String size = tp.getProduct_size();
                holder.tvProductName.setText(tp.getProduct_name() + " " + size);
                String dis = tp.getProduct_discount();
                String discount = tp.setProduct_discount(dis);
                holder.tvDiscount.setText(discount + "% OFF");
                String productMrp = tp.getProduct_mrp();
                String discountPrice = tp.getProduct_price();

                if (productMrp.equals(discountPrice)) {
                    holder.tvPrice.setVisibility(View.VISIBLE);
                    holder.tvDiscount.setVisibility(View.INVISIBLE);
                    holder.tvMrp.setVisibility(View.GONE);
                } else {
                    holder.tvPrice.setVisibility(View.VISIBLE);
                    holder.tvDiscount.setVisibility(View.VISIBLE);
                    holder.tvMrp.setVisibility(View.VISIBLE);
                    holder.tvMrp.setBackgroundResource(R.drawable.dash);
                }
                holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
                /*--------------------------------for default value------------------------------*/

                String QTY = tp.getStrAvailable_Qty();
                if (QTY.equals("0")) {
                    holder.btnAddToCart.setVisibility(View.GONE);
                    holder.imgOutOfStock.setVisibility(View.VISIBLE);
                    holder.btnAddToCart.setText("Add To Cart");

                    //TODO here out of stock GIF image setup to glide
                    Picasso.with(mContext)
                            .load(R.drawable.outofstock)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.icerror_outofstock)
                            .into(holder.imgOutOfStock);
                } else {
                    holder.btnAddToCart.setVisibility(View.VISIBLE);
                    holder.imgOutOfStock.setVisibility(View.GONE);
                }

                holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                            try {
                                productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.similarProductCodeWiseProducts.get(pos);

                                movie.setPosition(pos); //set here position when user any wants to buy multiple size products

                                if (tp.getStrHindiName().equals("")) {
                                    holder.tvProductHindiName.setVisibility(View.GONE);
                                } else {
                                    holder.tvProductHindiName.setVisibility(View.VISIBLE);
                                    holder.tvProductHindiName.setText(tp.getStrHindiName());
                                }

                                //TODO here product image setup to glide when user select different product variant from spinner
                                /*Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(holder.imgProduct);*/
                                Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(holder.imgProduct);

                                String size = tp.getProduct_size();
                                holder.tvProductName.setText(tp.getProduct_name() + " " + size);
                                String dis = tp.getProduct_discount();
                                String discount = tp.setProduct_discount(dis);
                                holder.tvDiscount.setText(discount + "% OFF");
                                String productMrp = tp.getProduct_mrp();
                                String discountPrice = tp.getProduct_price();
                                String QTY = tp.getProduct_qty();

                                if (productMrp.equals(discountPrice)) {
                                    holder.tvPrice.setVisibility(View.VISIBLE);
                                    holder.tvDiscount.setVisibility(View.INVISIBLE);
                                    holder.tvMrp.setVisibility(View.GONE);
                                } else {
                                    holder.tvPrice.setVisibility(View.VISIBLE);
                                    holder.tvDiscount.setVisibility(View.VISIBLE);
                                    holder.tvMrp.setVisibility(View.VISIBLE);
                                    holder.tvMrp.setBackgroundResource(R.drawable.dash);
                                }
                                holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                                holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                                HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                                String btnCheckStatus = (String) AddToCartInfo.get("new_pid");
                                if (btnCheckStatus == null) {
                                    tp.setStatus(false);
                                    holder.btnAddToCart.setText("Add To Cart");
                                } else {
                                    tp.setStatus(true);
                                    if (QTY.equals("0")) {
                                        holder.btnAddToCart.setText("Add To Cart");
                                    } else {
                                        holder.btnAddToCart.setText("Go To Cart");
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(productInformationMainLayout);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });

                holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                            productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                            final ProductCodeWiseProduct forAddToCart = movie.similarProductCodeWiseProducts.get(movie.getPosition());
                            final String maincategory = movie.similarProductCodeWiseProducts.get(movie.getPosition()).getProduct_maincat();
                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on add to cart & product is: " + forAddToCart.getProduct_name() + " " + forAddToCart.getProduct_size(), mContext);
*/
                            shopId = forAddToCart.getShop_id();
                            pId = forAddToCart.getProduct_id();

                            HashMap AddToCartInfo;
                            AddToCartInfo = db.getCartDetails(pId);
                            String strId = (String) AddToCartInfo.get("new_pid");

                            if (strId == null) { //TODO Server method here
                                forAddToCart.setStatus(false);

                                pName = holder.tvProductName.getText().toString();
                                product_Size = forAddToCart.getProduct_size();

                                JSONObject params = new JSONObject();
                                try {
                                    params.put("product_id", pId);
                                    params.put("shop_id", shopId);
                                    params.put("size", product_Size);
                                    params.put("qty", 1);
                                    params.put("contactNo", strContact);
                                    params.put("email", strEmail);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (response.isNull("posts")) {
                                        } else {
                                            try {
                                                strCheckQty = response.getString("posts");
                                                if (strCheckQty.equals("true")) {
                                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                                                    alertDialogBuilder.setMessage("You cannot add more than 1000 gm of this product.");
                                                    alertDialogBuilder.setPositiveButton("OK",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                    dialogInterface.dismiss();
                                                                }
                                                            });
                                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                                    alertDialog.show();
                                                } else {
                                                    db.insertCount(pId);
                                                    int cnt = (int) db.fetchAddToCartCount();
                                                    //updateAddToCartCount(cnt);

                                                    if (cnt >= 0) {
                                                        forAddToCart.setStatus(true);
                                                        if (forAddToCart.isStatus(true)) {
                                                            //  holder.btnAddToCart.setText("Go To Cart");
                                                        }
                                                    } else {
                                                        forAddToCart.setStatus(false);
                                                        if (forAddToCart.isStatus(false)) {
                                                            //  holder.btnAddToCart.setText("Add To Cart");
                                                        }
                                                    }
                                                    addCartItemsToServer(maincategory);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();

                                        GateWay gateWay = new GateWay(mContext);
                                        gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                    }
                                });
                                VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                            } else {
                           /*     UserTracking UT1 = new UserTracking(UserTracking.context);
                                UT1.user_tracking(class_name + ": clicked on remove from cart & product is: " + forAddToCart.getProduct_name() + " " + forAddToCart.getProduct_size(), mContext);
*/
                                holder.btnAddToCart.setText("Go To Cart");
                                deleteCartItemDialog(pId);
                            }
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(productInformationMainLayout);
                        }
                    }

                    private void deleteCartItemDialog(final String pId) {
                        productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                        selectedPosition = movie.getPosition();
                        ProductCodeWiseProduct forAddToCart = movie.similarProductCodeWiseProducts.get(selectedPosition);

                        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                        View review = layoutInflater.inflate(R.layout.same_shop_cart, null);
                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setView(review);

                        tvMessage = review.findViewById(R.id.txtMessage);
                        tvYes = review.findViewById(R.id.btnYes);
                        tvNo = review.findViewById(R.id.btnNo);
                        tvMessage.setText("Product is " + forAddToCart.getProduct_name() + ". Do you want remove or Go To Cart.");
                        // set dialog message
                        alertDialogBuilder.setCancelable(false);
                        // create alert dialog
                        alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();

                        tvNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                deleteNormalProductFromCartItem(pId);
                            }

                            private void deleteNormalProductFromCartItem(final String pId) { //TODO Server method here
                                final GateWay gateWay = new GateWay(mContext);
                                gateWay.progressDialogStart();

                                final DBHelper db = new DBHelper(mContext);

                                if (Connectivity.isConnected(mContext)) {
                                    JSONObject params = new JSONObject();
                                    try {
                                        params.put("product_id", pId);
                                        params.put("contactNo", strContact);
                                        params.put("email", strEmail);
                                        params.put("tag", "delete_one_item");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {
                                            if (response.isNull("posts")) {
                                            } else {
                                                db.deleteProductItem(pId);
                                                holder.btnAddToCart.setText("Add To Cart");
                                            }
                                            count = (int) db.fetchAddToCartCount();
                                            updateAddToCartCount(count);
                                            gateWay.progressDialogStop();
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            gateWay.progressDialogStop();

                                            error.printStackTrace();

                                            GateWay gateWay = new GateWay(mContext);
                                            gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                        }
                                    });
                                    VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                                } else {
                                    gateWay.displaySnackBar(productInformationMainLayout);
                                }
                            }
                        });

                        tvYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                alertDialog.dismiss();
                                Intent intent = new Intent(mContext, AddToCartActivity.class);
                                startActivity(intent);
                            }
                        });
                    }

                    private void addCartItemsToServer(String maincategory) { //TODO Server method here
                        JSONObject params = new JSONObject();
                        if (pName.contains("Small")) {
                            selectCondition = "Small";
                        } else if (pName.contains("Large")) {
                            selectCondition = "Large";
                        } else if (pName.contains("Medium")) {
                            selectCondition = "Medium";
                        } else if (pName.contains("Ripe")) {
                            selectCondition = "Ripe";
                        } else if (pName.contains("Raw")) {
                            selectCondition = "Raw";
                        } else {
                            selectCondition = "";
                        }
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("selectedType", "");
                            params.put("versionCode", StaticUrl.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("sessionMainCat", maincategory);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    String posts = response.getString("posts");

                                    if (posts.equals("true")){
                                        db.insertCount(pId);
                                        int cnt = (int) db.fetchAddToCartCount();
                                        updateAddToCartCount(cnt);

                                        if (cnt >= 0) {
                                            // forAddToCart.setStatus(true);
                                            //  if (forAddToCart.isStatus(true)) {
                                            holder.btnAddToCart.setText("Go To Cart");
                                            // }
                                        } else {
                                            //forAddToCart.setStatus(false);
                                            // if (forAddToCart.isStatus(false)) {
                                            holder.btnAddToCart.setText("Add To Cart");
                                            // }
                                        }
                                        Log.d("addCartItemsToServer1",""+posts);
                                        Toast toast = Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                        toast.show();
                                    }else {
                                        openSessionDialog(posts);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(mContext);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class SimilarProductsViewHolder extends RecyclerView.ViewHolder {

            final ImageView imgProduct;
            final TextView tvProductName;
            final TextView tvProductHindiName;
            final TextView tvMrp;
            final TextView tvDiscount;
            final TextView tvPrice;
            final TextView btnAddToCart;
            final Spinner spinner;
            final RelativeLayout LayoutSpinner;
            final ImageView imgOutOfStock;

            SimilarProductsViewHolder(View itemView) {
                super(itemView);
                imgProduct = itemView.findViewById(R.id.imgProduct);
                tvProductName = itemView.findViewById(R.id.txtProductName);
                tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
                tvMrp = itemView.findViewById(R.id.txtTotal);
                tvDiscount = itemView.findViewById(R.id.txtDiscount);
                tvPrice = itemView.findViewById(R.id.txtPrice);
                btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
                spinner = itemView.findViewById(R.id.spinner);
                LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
                imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
                btnAddToCart.setTag(this);
            }
        }
    }

    private class CustomAdapter extends ArrayAdapter {

        final ArrayList<String> dataList;
        final Context context;

        CustomAdapter(ProductDetailActivity productDetailActivity, ArrayList<String> dataList) {
            super(productDetailActivity, R.layout.variant_list_items, dataList);
            context = productDetailActivity;
            this.dataList = dataList;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            convertView = null;
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.variant_list_items, null);
            TextView textView = convertView.findViewById(R.id.txtItem);
            textView.setVisibility(View.VISIBLE);
            textView.setText(dataList.get(position));

            if (position == 0) {
                setItemSelected(convertView, "text");
            }
            return convertView;
        }
    }

    private class CustomAdapter1 extends ArrayAdapter {

        final ArrayList<String> dataList;
        final Context context;

        CustomAdapter1(ProductDetailActivity productDetailActivity, ArrayList<String> dataList) {
            super(productDetailActivity, R.layout.variant_list_items, dataList);
            context = productDetailActivity;
            this.dataList = dataList;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            convertView = null;
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.variant_list_items, null);
            ImageView imageView = convertView.findViewById(R.id.imgProductImage);
            imageView.setVisibility(View.VISIBLE);

            //TODO when user select particular product image in that case it will be selected. product image setup to glide
           /* Glide.with(mContext).load("http://simg.picodel.com/" + dataList.get(position))
                    .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);*/
            Picasso.with(mContext).load("https://simg.picodel.com/" + dataList.get(position))
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(imageView);

            if (position == 0) {
                setItemSelected(convertView, "image");
            }
            return convertView;
        }
    }


    private void popupLimitbyCat(String mainCat){
        String vegmsg="\n Fresh Vegetables & Fruits \n(Free Delivery above INR 300)\n\n ";
        String grocery_msg="\n\tGroceries\n(Free Delivery above INR 500)";

        String MESSAGE="";
        if(mainCat.equals("Grocery")){
            MESSAGE=grocery_msg;
        }else {
            MESSAGE=vegmsg;
        }

        String Message = "\n What would you like to order today?" +
                MESSAGE+
                "\nExpected Delivery time within 3 hours.";
        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setMessage(Message);
        builder1.setCancelable(true);
        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                        /*fragment = new DataSegregationLevelThree();
                        Bundle bundle = new Bundle();
                        bundle.putString("product_cat", "Fruits and Veggies");
                        bundle.putString("product_mainCat", "Grocery");
                        fragment.setArguments(bundle);
                        fragment_replace();*/

            }
        });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                        /*fragment = new DataSegregationLevelThree();
                        Bundle bundle = new Bundle();
                        bundle.putString("product_cat", "Fruits and Veggies");
                        bundle.putString("product_mainCat", "Grocery");
                        fragment.setArguments(bundle);
                        fragment_replace();*/
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void openSessionDialog(String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sessoin);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        TextView tv_clearcart = dialog.findViewById(R.id.tv_clearcart);
        TextView tv_continue = dialog.findViewById(R.id.tv_continue);
        tv_message.setText(message);
        tv_clearcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(mContext)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            // GateWay gateWay = new GateWay(getActivity());
                            //gateWay.displaySnackBar(homeMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void deleteAllProductFromCartItem() {
        if (Connectivity.isConnected(ProductDetailActivity.this)) {
            final GateWay gateWay = new GateWay(ProductDetailActivity.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", sharedPreferencesUtils.getPhoneNumber());
                params.put("email", sharedPreferencesUtils.getEmailId());
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(ProductDetailActivity.this);
                        db.deleteOnlyCartTable();

                        //after remove cart items from local and online db
                        //then sync online cart count again
                        SyncData();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(ProductDetailActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if (MasterSearch.searchView != null) {
            MasterSearch.searchView.setQuery("", false);
            MasterSearch.searchView.setIconified(false);
            Log.e("testcommit",""+"hello");
        }*/
    }


}
