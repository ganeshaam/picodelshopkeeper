package com.podmerchant.activites;

import android.app.AlertDialog;
import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.StaffListAdaptor;
import com.podmerchant.model.StaffModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class StafflistActivity extends AppCompatActivity {

    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;
    public static final String TAG = "StafflistActivity";
    AlertDialog progressDialog;
    ArrayList<StaffModel> staffModelArrayList,staffModelArrayList2;
    RecyclerView recycler_view_staff;
    private RecyclerView.LayoutManager layoutManager;
    private StaffListAdaptor staffListAdaptor;
    EditText et_search_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stafflist);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.staff_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = StafflistActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        recycler_view_staff = findViewById(R.id.recycler_view_staff);
        et_search_contact = findViewById(R.id.et_search_contact);

        staffModelArrayList = new ArrayList<>();
        staffModelArrayList2 = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        recycler_view_staff.setLayoutManager(layoutManager);
        staffListAdaptor = new StaffListAdaptor(mContext,staffModelArrayList);
        recycler_view_staff.setAdapter(staffListAdaptor);
        //staffListAdaptor.notifyDataSetChanged();

        getStaffList();

        et_search_contact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //if(s.length()>=2) {
                    staffModelArrayList2.clear();
                    for (int i = 0; i < staffModelArrayList.size(); i++) {
                        if(staffModelArrayList.get(i).getMobile().contains(s.toString())){
                        //if(staffModelArrayList.get(i).getStaffName().contains(s.toString())) {
                            staffModelArrayList2.add(staffModelArrayList.get(i));
                        }
                    }

                    staffListAdaptor = new StaffListAdaptor(mContext, staffModelArrayList2);
                    recycler_view_staff.setAdapter(staffListAdaptor);
                    staffListAdaptor.notifyDataSetChanged();
                //}
            }

            @Override
            public void afterTextChanged(Editable s) {



            }
        });

        //staff list
       /* recycler_view_staff.addOnItemTouchListener(new StafflistActivity.RecyclerTouchListener(mContext, recycler_view_staff,
                new StafflistActivity.RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        Toast.makeText(mContext,staffModelArrayList.get(position).getStaffName(),Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent();
                        intent.putExtra("staffName", staffModelArrayList.get(position).getStaffName());
                        intent.putExtra("staffAge", staffModelArrayList.get(position).getAge());
                        intent.putExtra("staffImage", staffModelArrayList.get(position).getImageUrl());
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));*/

    }

    private void getStaffList(){
        //get Mobile verify
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.stafflist
             +"?city="+sharedPreferencesUtils.getRCity();
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                staffModelArrayList.clear();
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    Log.e("ProfileResponse:",""+jsonArray.toString());

                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        StaffModel staffModel = new StaffModel();
                                        staffModel.setId(jsonObject1.getString("id"));
                                        staffModel.setStaffName(jsonObject1.getString("staffname"));
                                        staffModel.setMobile(jsonObject1.getString("contact"));
                                        //staffModel.setAge(jsonObject1.getString("age"));
                                        staffModel.setImageUrl(jsonObject1.getString("image"));
                                        staffModelArrayList.add(staffModel);
                                    }
                                    Log.e("arraylist size", String.valueOf(staffModelArrayList.size()));
                                    staffListAdaptor.notifyDataSetChanged();
                                }else if(status==false) {
                                    Toast.makeText(mContext,"No Records Found",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  StafflistActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final StafflistActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
