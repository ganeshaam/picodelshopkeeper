package com.podmerchant.activites;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.OrderlistAdapter;
import com.podmerchant.model.OrderlistModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

public class OrderListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    ArrayList<OrderlistModel> orderArrayList,orderArrayList2,orderArrayListMerge;
    private RecyclerView.LayoutManager layoutManager;
    private OrderlistAdapter orderlistAdapter;
    RecyclerView rv_home_orderlist;
    TextView tv_network_con,tv_oh_delivered,tv_oh_accepted,tv_oh_rejected,tv_histroy_delivered_list;
    AlertDialog progressDialog;
    String strLatitude="",strLongitude="",strDeliveryinkm="",flag="new_order",areaName="areaName";
    Button btn_next,btn_prev;
    int offset=0;
    CardView cv_histroydetails;
    LinearLayout ll_histroy_accepted;
    SwipeRefreshLayout srl_refreshorderlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getText(R.string.orderlist));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = OrderListActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        rv_home_orderlist = findViewById(R.id.rv_home_orderlist);
        tv_network_con = findViewById(R.id.tv_network_con);
        cv_histroydetails = findViewById(R.id.cv_histroydetails);
        tv_oh_delivered = findViewById(R.id.tv_oh_delivered);
        tv_oh_accepted = findViewById(R.id.tv_oh_accepted);
        tv_oh_rejected = findViewById(R.id.tv_oh_rejected);
        ll_histroy_accepted = findViewById(R.id.ll_histroy_accepted);
        srl_refreshorderlist = findViewById(R.id.srl_refreshorderlist);
        tv_histroy_delivered_list = findViewById(R.id.tv_histroy_delivered_list);
        //btn_next = findViewById(R.id.btn_next);
        //btn_prev = findViewById(R.id.btn_prev);
        srl_refreshorderlist.setOnRefreshListener(this);

        rv_home_orderlist.setHasFixedSize(true);
        orderArrayList = new ArrayList<>();
        orderArrayList2 = new ArrayList<>();
        orderArrayListMerge = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        rv_home_orderlist.setLayoutManager(layoutManager);
        rv_home_orderlist.setItemAnimator(new DefaultItemAnimator());
        orderlistAdapter = new OrderlistAdapter(orderArrayList,mContext);
        rv_home_orderlist.setAdapter(orderlistAdapter);

        strLatitude=sharedPreferencesUtils.getLatitude();
        strLongitude=sharedPreferencesUtils.getLongitude();
        strDeliveryinkm=sharedPreferencesUtils.getDistancekm();

        Intent intent = getIntent();
        flag = intent.getStringExtra("flag");

        if(Connectivity.isConnected(mContext)){
            if(flag.equals("new_order")) {
                getSupportActionBar().setTitle(getResources().getText(R.string.newOrderList));
                areaName = sharedPreferencesUtils.getShopArea();
                getOrderList("new_order",areaName);
            }else  if(flag.equals("approved_order")){
                getSupportActionBar().setTitle(getResources().getText(R.string.newAcceptedOrderList));
                getOrderList("approved_order","areaName");
            }else if(flag.equals("assigned_order")){
                getSupportActionBar().setTitle(R.string.yourOrderList);
                getOrderList("assigned_order","areaName");
            }else if(flag.equals("histroy_order")){
                getSupportActionBar().setTitle(getResources().getText(R.string.title_order_histroy));
                getOrderList("histroy_order","areaName");
                cv_histroydetails.setVisibility(View.VISIBLE);
                tv_histroy_delivered_list.setVisibility(View.VISIBLE);
                getOrderHistroyCount();
            }else if(flag.equals("areaWise_order")){
                getSupportActionBar().setTitle(R.string.areaWiseOrderList);
                areaName = intent.getStringExtra("areaName");
                getOrderList("areaWise_order",areaName);
            }else if(flag.equals("testorder")){
                getSupportActionBar().setTitle("Test Order");
                areaName = intent.getStringExtra("testorder");
                getOrderList("testorder",areaName);
            }else   {
                getSupportActionBar().setTitle(R.string.orderlist);
                getOrderList("new_order","areaName");
            }
            //getOrderList(offset);
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        //order histroy send to accepted order list
        ll_histroy_accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,OrderListActivity.class);
                intent.putExtra("flag","approved_order");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });
      /*  btn_prev.setVisibility(View.GONE);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(activityModelArrayList.size()>0)
                {
                    offset+=10;
                    transction(offset);
                    btn_prev.setVisibility(View.VISIBLE);
                }else {
                    Toast.makeText(mContext,"No More Transctions",Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offset-=10;
                transction(offset);
            }
        }); */
        //order details
        rv_home_orderlist.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_home_orderlist,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        String orderId = orderArrayList.get(position).getOrder_id();
                        String cartUniqueId = orderArrayList.get(position).getCart_unique_id();
                        String orderByUserId = orderArrayList.get(position).getUser_id();
                        String cartUserId = orderArrayList.get(position).getCart_user_id();
                        String shipping_address = orderArrayList.get(position).getShipping_address();
                        String latitude = orderArrayList.get(position).getLatitude();
                        String longitude = orderArrayList.get(position).getLongitude();
                        String total_amount = orderArrayList.get(position).getTotal_amount();
                        String order_date  = orderArrayList.get(position).getOrder_date();
                        String address_id = orderArrayList.get(position).getAddress_id();
                        String assign_delboy_id = orderArrayList.get(position).getAssign_delboy_id();
                        String action_time = orderArrayList.get(position).getActionTime();
                        String address1 = orderArrayList.get(position).getAddress1();
                        String address2 = orderArrayList.get(position).getAddress2();
                        String address3 = orderArrayList.get(position).getAddress3();
                        String address4 = orderArrayList.get(position).getAddress4();
                        String address5 = orderArrayList.get(position).getAddress5();
                        String area = orderArrayList.get(position).getArea();
                        String pincode = orderArrayList.get(position).getPincode();
                        String showcart = orderArrayList.get(position).getShow_cart();
                        String updatedat = orderArrayList.get(position).getUpdated_at();
                        String aam_delivery_charge = orderArrayList.get(position).getAam_delivery_charge();
                        String delivery_date = orderArrayList.get(position).getDelivery_date();
                        String delivery_time = orderArrayList.get(position).getDelivery_time();
                        String delivery_method = orderArrayList.get(position).getDelivery_method();
                        String payment_option = orderArrayList.get(position).getPayment_option();
                        String color_flag = orderArrayList.get(position).getColor_flag();
                        String admin_aprove = orderArrayList.get(position).getAdmin_approve();
                        //chkOrderStatus(orderId,cartUniqueId,orderByUserId,cartUserId,cartUserId,shipping_address,latitude,longitude,total_amount,order_date,address_id,assign_delboy_id,action_time);


                             Intent intent = new Intent(mContext, OrderDetailActivity.class);
                             intent.putExtra("orderId", orderId);
                             intent.putExtra("cartUniqueId", cartUniqueId);
                             intent.putExtra("userId", orderByUserId);
                             intent.putExtra("cartUserId", cartUserId);
                             intent.putExtra("shipping_address", shipping_address);
                             intent.putExtra("latitude", latitude);
                             intent.putExtra("longitude", longitude);
                             intent.putExtra("total_amount", total_amount);
                             intent.putExtra("order_date", order_date);
                             intent.putExtra("address_id", address_id);
                             intent.putExtra("assign_delboy_id", assign_delboy_id);
                             intent.putExtra("action_time", action_time);
                             intent.putExtra("address1", address1);
                             intent.putExtra("address2", address2);
                             intent.putExtra("address3", address3);
                             intent.putExtra("address4", address4);
                             intent.putExtra("address5", address5);
                             intent.putExtra("area", area);
                             intent.putExtra("pincode", pincode);
                             intent.putExtra("showcart", showcart);
                             intent.putExtra("flag", flag);
                             intent.putExtra("updatedat", updatedat);
                             intent.putExtra("aam_delivery_charge", aam_delivery_charge);
                             intent.putExtra("areaName", areaName);
                             intent.putExtra("delivery_date", delivery_date);
                             intent.putExtra("delivery_time", delivery_time);
                             intent.putExtra("delivery_method", delivery_method);
                             intent.putExtra("payment_option", payment_option);
                             intent.putExtra("admin_aprove", admin_aprove);
                             intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Distributor")){
                             if(admin_aprove.equalsIgnoreCase("Not Approved")){

                             }else {
                                 startActivity(intent);
                             }
                        }else {
                            insertViewDetails(orderId,admin_aprove); // Method to check  Order view by shopkeeper
                            startActivity(intent);

                        }

                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        /*
        srl_refreshorderlist.post(new Runnable() {
            @Override
            public void run() {
                srl_refreshorderlist.setRefreshing(true);
                if(Connectivity.isConnected(mContext)){
                    if(flag.equals("new_order")) {
                        getOrderList("new_order");
                    }else  if(flag.equals("approved_order")){
                        getOrderList("approved_order");
                    }else if(flag.equals("assigned_order")){
                        getOrderList("assigned_order");
                    }else if(flag.equals("histroy_order")){
                        getOrderList("histroy_order");
                        getOrderHistroyCount();
                    }else {
                        //getOrderList("new_order");
                    }
                }else {
                    tv_network_con.setVisibility(View.VISIBLE);
                }
            }
        });*/


    }

    //Check order is already approved by some one , final String cartUniqueId, final String orderByUserId, final String cartUserId, final String userId, final String shipping_address, final String latitude, final String longitude, final String total_amount, final String order_date, final String address_id, final String assign_delboy_id, final String action_time
    public void chkOrderStatus(final String orderId){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.chkorderstatus+"?order_id="+orderId;

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String approvedStatus = jsonObjectData.getString("Not Approved");

                                    /*if(approvedStatus.equals("New Order")){
                                        Intent intent = new Intent(mContext,OrderDetailActivity.class);
                                        intent.putExtra("orderId",orderId);
                                        intent.putExtra("cartUniqueId",cartUniqueId);
                                        intent.putExtra("userId",orderByUserId);
                                        intent.putExtra("cartUserId",cartUserId);
                                        intent.putExtra("shipping_address",shipping_address);
                                        intent.putExtra("latitude",latitude);
                                        intent.putExtra("longitude",longitude);
                                        intent.putExtra("total_amount",total_amount);
                                        intent.putExtra("order_date",order_date);
                                        intent.putExtra("address_id",address_id);
                                        intent.putExtra("assign_delboy_id",assign_delboy_id);
                                        intent.putExtra("action_time",action_time);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                    }else if(approvedStatus.equals("Approved")){
                                        String mesg = jsonObject.getString("mesg");
                                        chkApprovedDialog(mesg);
                                    }*/
                                }else if(!status){
                                    Toast.makeText(mContext,"Order accepted by another shop keeper.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void chkApprovedDialog(String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_chkapprove);

        TextView text = dialog.findViewById(R.id.tv_dialog_msg);
        text.setText(message);

        Button dg_btn_refreshlist =  dialog.findViewById(R.id.btn_refreshlist);

        dg_btn_refreshlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getOrderList(flag,"areaName");
            }
        });

        dialog.show();
    }
    //order list
    public void getOrderList(String flag,String areaName) {

        progressDialog.show();
        orderArrayList.clear();
        orderArrayList2.clear();
        srl_refreshorderlist.setRefreshing(true);

        String targetUrlOrderlist = StaticUrl.orderlist
                + "?latitude=" + strLatitude
                + "&longitude=" + strLongitude
                + "&deliveryinkm=" + strDeliveryinkm
                + "&shopid=" + sharedPreferencesUtils.getShopID()
                + "&areaName=" + "areaName"
                + "&flag=" + flag
                + "&tblName="+sharedPreferencesUtils.getAdminType();


        Log.d("targetUrlOrderlist",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlistRes",response);
                        progressDialog.dismiss();
                        srl_refreshorderlist.setRefreshing(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            boolean AreaWisestatus = jsonObject.getBoolean("AreaWisestatus");

                            if ((strStatus)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                //Log.e("JSONS_jsonArray_area:",""+jsonArray_area);
                                if(jsonArray.length()>0){
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        model.setOrder_id(olderlist.getString("order_id"));
                                        model.setUser_id(olderlist.getString("user_id"));
                                        model.setAssign_shop_id(olderlist.getString("assign_shop_id"));
                                        model.setAssign_delboy_id(olderlist.getString("assign_delboy_id"));
                                        model.setStatus(olderlist.getString("status"));
                                        model.setCart_user_id(olderlist.getString("cart_user_id"));
                                        model.setCart_unique_id(olderlist.getString("cart_unique_id"));
                                        model.setPayment_method(olderlist.getString("payment_method"));
                                        model.setUpdate_payment_method(olderlist.getString("update_payment_method"));
                                        model.setPayment_type(olderlist.getString("payment_type"));
                                        model.setReferrence_transaction_id(olderlist.getString("referrence_transaction_id"));
                                        model.setTotal_amount(olderlist.getString("total_amount"));
                                        model.setUpdated_total_amount(olderlist.getString("updated_total_amount"));
                                        model.setShipping_address(olderlist.getString("shipping_address"));
                                        model.setLatitude(olderlist.getString("latitude"));
                                        model.setLongitude(olderlist.getString("longitude"));
                                        model.setUpdated_shipping_charges(olderlist.getString("updated_shipping_charges"));
                                        model.setOrder_date(olderlist.getString("order_date"));
                                        model.setUser_pending_bill(olderlist.getString("user_pending_bill"));
                                        model.setPending_bill_status(olderlist.getString("pending_bill_status"));
                                        model.setPending_bill_updatedy(olderlist.getString("pending_bill_updatedy"));
                                        model.setPending_bii_date(olderlist.getString("pending_bii_date"));
                                        model.setDistance(olderlist.getString("distance"));
                                        model.setAddress_id(olderlist.getString("address_id"));
                                        model.setActionTime(olderlist.getString("action_time"));
                                        model.setAddress1(olderlist.getString("address1"));
                                        model.setAddress2(olderlist.getString("address2"));
                                        model.setAddress3(olderlist.getString("address3"));
                                        model.setAddress4(olderlist.getString("address4"));
                                        model.setAddress5(olderlist.getString("address5"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setPincode(olderlist.getString("pincode"));
                                        model.setShow_cart(olderlist.getString("show_cart"));
                                        model.setAction_time(olderlist.getString("action_time"));
                                        model.setUpdated_at(olderlist.getString("updated_at"));
                                        model.setAam_delivery_charge(olderlist.getString("aam_delivery_charge"));
                                        model.setDelivery_date(olderlist.getString("delivery_date"));
                                        model.setDelivery_time(olderlist.getString("delivery_time"));
                                        model.setDelivery_method(olderlist.getString("delivery_method"));
                                        model.setPayment_option(olderlist.getString("payment_option"));
                                        model.setAdmin_approve(olderlist.getString("admin_approve"));
                                        model.setColor_flag(olderlist.getString("color_flag"));//color_flag

                                        /*if(olderlist.getString("order_id").equalsIgnoreCase("null")||olderlist.getString("order_id")==null||olderlist.getString("order_id").isEmpty()){

                                        }else {*/

                                            orderArrayList.add(model);

                                        //}
                                    }

                                    Log.e("orderlistSize:",""+orderArrayList.size());
                                    orderlistAdapter.notifyDataSetChanged();
                                    //Log.e("orderlistMergeSize:",""+orderArrayListMerge.size());
                                    //getOrderList_AreaWise_NotLatLog("new_order","areaName");
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }




                            }else if(!strStatus){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                            // If the areaWise List has the more than 0 size

                            if ((AreaWisestatus)){
                                JSONArray jsonArray_area = jsonObject.getJSONArray("areaOrderArray");
                                if (jsonArray_area.length() > 0) {
                                    for (int i = 0; i < jsonArray_area.length(); i++) {
                                        JSONObject olderlist = jsonArray_area.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        model.setOrder_id(olderlist.getString("order_id"));
                                        model.setUser_id(olderlist.getString("user_id"));
                                        model.setAssign_shop_id(olderlist.getString("assign_shop_id"));
                                        model.setAssign_delboy_id(olderlist.getString("assign_delboy_id"));
                                        model.setStatus(olderlist.getString("status"));
                                        model.setCart_user_id(olderlist.getString("cart_user_id"));
                                        model.setCart_unique_id(olderlist.getString("cart_unique_id"));
                                        model.setPayment_method(olderlist.getString("payment_method"));
                                        model.setUpdate_payment_method(olderlist.getString("update_payment_method"));
                                        model.setPayment_type(olderlist.getString("payment_type"));
                                        model.setReferrence_transaction_id(olderlist.getString("referrence_transaction_id"));
                                        model.setTotal_amount(olderlist.getString("total_amount"));
                                        model.setUpdated_total_amount(olderlist.getString("updated_total_amount"));
                                        model.setShipping_address(olderlist.getString("shipping_address"));
                                        model.setLatitude(olderlist.getString("latitude"));
                                        model.setLongitude(olderlist.getString("longitude"));
                                        model.setUpdated_shipping_charges(olderlist.getString("updated_shipping_charges"));
                                        model.setOrder_date(olderlist.getString("order_date"));
                                        model.setUser_pending_bill(olderlist.getString("user_pending_bill"));
                                        model.setPending_bill_status(olderlist.getString("pending_bill_status"));
                                        model.setPending_bill_updatedy(olderlist.getString("pending_bill_updatedy"));
                                        model.setPending_bii_date(olderlist.getString("pending_bii_date"));
                                        model.setDistance(olderlist.getString("distance"));
                                        model.setAddress_id(olderlist.getString("address_id"));
                                        model.setActionTime(olderlist.getString("action_time"));
                                        model.setAddress1(olderlist.getString("address1"));
                                        model.setAddress2(olderlist.getString("address2"));
                                        model.setAddress3(olderlist.getString("address3"));
                                        model.setAddress4(olderlist.getString("address4"));
                                        model.setAddress5(olderlist.getString("address5"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setPincode(olderlist.getString("pincode"));
                                        model.setShow_cart(olderlist.getString("show_cart"));
                                        model.setAction_time(olderlist.getString("action_time"));
                                        model.setUpdated_at(olderlist.getString("updated_at"));
                                        model.setAam_delivery_charge(olderlist.getString("aam_delivery_charge"));
                                        model.setDelivery_date(olderlist.getString("delivery_date"));
                                        model.setDelivery_time(olderlist.getString("delivery_time"));
                                        model.setDelivery_method(olderlist.getString("delivery_method"));
                                        model.setPayment_option(olderlist.getString("payment_option"));
                                        model.setAdmin_approve(olderlist.getString("admin_approve"));
                                        model.setColor_flag(olderlist.getString("color_flag"));//color_flag

                                        /*if(olderlist.getString("order_id").equalsIgnoreCase("null")||olderlist.getString("order_id")==null||olderlist.getString("order_id").isEmpty()){

                                        }else {*/

                                        orderArrayList2.add(model);

                                        //}
                                    }

                                    orderArrayList.addAll(orderArrayList2);
                                    sorArrayList();

                                    Log.e("orderlistSize:", "" + orderArrayList.size());
                                    orderlistAdapter.notifyDataSetChanged();
                                    //Log.e("orderlistMergeSize:",""+orderArrayListMerge.size());
                                    //getOrderList_AreaWise_NotLatLog("new_order","areaName");
                                }

                            }else {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        srl_refreshorderlist.setRefreshing(false);
                        Log.e("exp_order_list",""+error.getMessage());
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }
    //  accepted, delivered,rejected order count
    public void getOrderHistroyCount(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.histroycount
                    + "?shopid="+sharedPreferencesUtils.getShopID()
                    + "&tblName="+sharedPreferencesUtils.getAdminType();

            Log.d("histroycount",urlAcceptOrder);

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jsonObjectData =  jsonArray.getJSONObject(0);
                                    tv_oh_delivered.setText(jsonObjectData.getString("Delivered"));
                                    tv_oh_accepted.setText(jsonObjectData.getString("Approved"));
                                    tv_oh_rejected.setText(jsonObjectData.getString("Rejected"));
                                }else if(!status){
                                    Toast.makeText(mContext,"No Order Histroy",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }

}

    public void getTimeSlotDialog(){ }

    //swip to refresh order list
    @Override
    public void onRefresh() {
        if(Connectivity.isConnected(mContext)){
            if(flag.equals("new_order")) {
                getOrderList("new_order","areaName");
            }else  if(flag.equals("approved_order")){
                getOrderList("approved_order","areaName");
            }else if(flag.equals("assigned_order")){
                getOrderList("assigned_order","areaName");
            }else if(flag.equals("histroy_order")){
                getOrderList("histroy_order","areaName");
                getOrderHistroyCount();
            }else if(flag.equals("areaWise_order")) {
                getOrderList("areaWise_order", areaName);
            }else {
                getOrderList("new_order", "areaName");
            }
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private OrderListActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final OrderListActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Intent intent = new Intent(mContext, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String CalculateOrderTime(String OrderDate_time){

        String newTime="";
        try {

            String myTime = OrderDate_time;
            //String myTime = "2019-09-18 19:14:00";
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date d = df.parse(myTime);
            Calendar cal = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.MINUTE, 95);
            newTime = df.format(cal.getTime());
            String current_Time = df.format(cal2.getTime());
            //long diff = cal2.getTime()-cal.getTime();
            Log.e("fL_time:",""+newTime+"C_TIME"+current_Time);

        }catch (Exception E){
            E.getMessage();
            Log.e("fl_exp:",""+E.getMessage());
        }
        return  newTime;

    }

    public boolean DiffCalculate(String action_time) {

        boolean result = false;

        try {
            Date userDob = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(action_time);
            Date today = new Date();
            long diff = today.getTime() - userDob.getTime();
            int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
            int hours = (int) (diff / (1000 * 60 * 60));
            int minutes = (int) (diff / (1000 * 60));
            int seconds = (int) (diff / (1000));

            Log.e("Diffrence is:", "" + numOfDays + "/" + hours + "/" + minutes + "/" + seconds);

            if (numOfDays >=1) {

                result =  true;

            } else {
                if (hours >=1) {
                    result =  true;
                } else {
                    if (minutes >=15) {
                        result =  true;
                    } else if (minutes < 15) {
                        result =  false;
                    }
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return result;
    }

    //Api to check order view by shopkeeper

    public void insertViewDetails(String order_id,String admin_aprove) {

        //progressDialog.show();
        orderArrayList.clear();
        srl_refreshorderlist.setRefreshing(true);

        String targetUrlviewbyShopkeeper = null;
        try {
            targetUrlviewbyShopkeeper = StaticUrl.insertviewbyshop
                    + "?shop_id=" + sharedPreferencesUtils.getShopID()
                    + "&order_id=" + order_id
                    + "&admin_aprove_status=" + URLEncoder.encode( admin_aprove,"utf-8")
                    + "&admin_type="+sharedPreferencesUtils.getAdminType();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.d("ViewByShopkeeper",targetUrlviewbyShopkeeper);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlviewbyShopkeeper,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ViewByshopRes",response);
                       // progressDialog.dismiss();
                        srl_refreshorderlist.setRefreshing(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            /*if ((strStatus)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                       // OrderlistModel model = new OrderlistModel();
                                       // model.setOrder_id(olderlist.getString("order_id"));
                                           //}
                                    }

                                }else {

                                }
                            }else if(!strStatus){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("Something went Wrong Please check...");
                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        srl_refreshorderlist.setRefreshing(false);
                        Log.e("exp_view_by_shop",""+error.getMessage());
                      //  progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    //order list without Lat long
    public void getOrderList_AreaWise_NotLatLog(String flag,String areaName) {

        progressDialog.show();
        orderArrayList2.clear();
        srl_refreshorderlist.setRefreshing(true);

        String targetUrlOrderlist = StaticUrl.orderlistAreaWise
                + "?latitude=" + strLatitude
                + "&longitude=" + strLongitude
                + "&deliveryinkm=" + strDeliveryinkm
                + "&shopid=" + sharedPreferencesUtils.getShopID()
                + "&areaName=" + areaName
                + "&flag=" + flag
                + "&tblName="+sharedPreferencesUtils.getAdminType();


        Log.d("targetUrlOrderlist2",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                        progressDialog.dismiss();
                        srl_refreshorderlist.setRefreshing(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        model.setOrder_id(olderlist.getString("order_id"));
                                        model.setUser_id(olderlist.getString("user_id"));
                                        model.setAssign_shop_id(olderlist.getString("assign_shop_id"));
                                        model.setAssign_delboy_id(olderlist.getString("assign_delboy_id"));
                                        model.setStatus(olderlist.getString("status"));
                                        model.setCart_user_id(olderlist.getString("cart_user_id"));
                                        model.setCart_unique_id(olderlist.getString("cart_unique_id"));
                                        model.setPayment_method(olderlist.getString("payment_method"));
                                        model.setUpdate_payment_method(olderlist.getString("update_payment_method"));
                                        model.setPayment_type(olderlist.getString("payment_type"));
                                        model.setReferrence_transaction_id(olderlist.getString("referrence_transaction_id"));
                                        model.setTotal_amount(olderlist.getString("total_amount"));
                                        model.setUpdated_total_amount(olderlist.getString("updated_total_amount"));
                                        model.setShipping_address(olderlist.getString("shipping_address"));
                                        model.setLatitude(olderlist.getString("latitude"));
                                        model.setLongitude(olderlist.getString("longitude"));
                                        model.setUpdated_shipping_charges(olderlist.getString("updated_shipping_charges"));
                                        model.setOrder_date(olderlist.getString("order_date"));
                                        model.setUser_pending_bill(olderlist.getString("user_pending_bill"));
                                        model.setPending_bill_status(olderlist.getString("pending_bill_status"));
                                        model.setPending_bill_updatedy(olderlist.getString("pending_bill_updatedy"));
                                        model.setPending_bii_date(olderlist.getString("pending_bii_date"));
                                        model.setDistance(olderlist.getString("distance"));
                                        model.setAddress_id(olderlist.getString("address_id"));
                                        model.setActionTime(olderlist.getString("action_time"));
                                        model.setAddress1(olderlist.getString("address1"));
                                        model.setAddress2(olderlist.getString("address2"));
                                        model.setAddress3(olderlist.getString("address3"));
                                        model.setAddress4(olderlist.getString("address4"));
                                        model.setAddress5(olderlist.getString("address5"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setPincode(olderlist.getString("pincode"));
                                        model.setShow_cart(olderlist.getString("show_cart"));
                                        model.setAction_time(olderlist.getString("action_time"));
                                        model.setUpdated_at(olderlist.getString("updated_at"));
                                        model.setAam_delivery_charge(olderlist.getString("aam_delivery_charge"));
                                        model.setDelivery_date(olderlist.getString("delivery_date"));
                                        model.setDelivery_time(olderlist.getString("delivery_time"));
                                        model.setDelivery_method(olderlist.getString("delivery_method"));
                                        model.setPayment_option(olderlist.getString("payment_option"));
                                        model.setAdmin_approve(olderlist.getString("admin_approve"));
                                        model.setColor_flag(olderlist.getString("color_flag"));//color_flag

                                        /*if(olderlist.getString("order_id").equalsIgnoreCase("null")||olderlist.getString("order_id")==null||olderlist.getString("order_id").isEmpty()){

                                        }else {*/

                                        orderArrayList2.add(model);

                                        //}
                                    }
                                    //orderlistAdapter.notifyDataSetChanged();
                                    //orderArrayListMerge.addAll(orderArrayList);
                                    Log.e("orderlistAWiseSize:",""+orderArrayList2.size());
                                    orderArrayList.addAll(orderArrayList2);
                                    /********Method to check the Array List Sorting *********/
                                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        orderArrayList.sort(new AreaSorter());
                                    }*/
                                    sorArrayList();

                                    Log.e("orderlistMergeSize:",""+orderArrayList.size());
                                    orderlistAdapter.notifyDataSetChanged();
                                    //Log.e("orderlistMergeSize:",""+orderArrayListMerge.size());
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(!strStatus){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        srl_refreshorderlist.setRefreshing(false);
                        Log.e("exp_order_list2",""+error.getMessage());
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }


    private void sorArrayList(){
        Collections.sort(orderArrayList, new Comparator<OrderlistModel>() {
            @Override
            public int compare(OrderlistModel o1, OrderlistModel o2) {
                //return 01;
                return o2.getOrder_date().compareToIgnoreCase(o1.getOrder_date());
            }
        });
        orderlistAdapter.notifyDataSetChanged();
    }


}
