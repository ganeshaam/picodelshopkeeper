package com.podmerchant.activites;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleyMultipartRequest;
import com.podmerchant.util.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ShopProfileActivity extends AppCompatActivity {

    EditText et_reg_shopgstno,et_managercontact,et_managername,reg_other_document,et_ownercontact,et_no_delivery_boy,et_ownername,et_shopcontact,reg_shop_name,et_shopemail,et_shopcity,et_shoparea,
            et_gst_registraion,et_address1,et_shopaddress2,et_shopaddress3,et_shopaddress4,et_shopaddress5,et_shopaddress6,et_how_may_products,et_lowest_cost,et_bank_type,et_AC_no,et_ifsc_code,et_highest_cost,
            et_paytm,et_googlepay,et_phonepe;
    Context mContext;
    CheckBox cb_required_licence,cb_quility_product_img;
    Spinner sp_gendertype,sp_idproof,sp_deliveryboy,sp_knowledgetech,sp_payschedule,sp_shoptype,sp_category,sp_shopadmintype,sp_document,sp_turnover;
    SeekBar seakbar_area_km;
    ImageView shop_photo1,shop_photo2,img_licence,img_document,img_document2;
    Button btn_next_soc,shop_register_btn,uploadphoto,btn_uploadphoto2,btn_uploaddocument2,btn_previous,btn_previous_first,btn_next_third;
    Button btn_update_profile;
    LinearLayout profile_layout,shop_layout,layout_shopaddress,layout_manufacture_form,ll_delivery_options;
    ProgressDialog progressDialog;
    public static final int PICK_IMAGE = 1,PICK_IMAGE_SHOPACT = 9, IMG_DOCUMENT_UPLOAD = 5, SELECT_PICTURE = 2,PERMISSION_ACCESS_COARSE_LOCATION = 1,CAPTURE_DOCUMENT=10;
    TextView txt_stepper_sec,txt_stepper_third,tv_sameAsAbove,tv_destancekm,tv_shop_cat;
    View view_stepper_sec;
    SharedPreferencesUtils sharedPreferencesUtils;
    String sp_turnover_value="",shopcontact="",shop_id="",email="",shop_latitude="",shop_longitude="",owner_contact="",shop_name="",shop_type="",
             admin_type="",knowledge_of_tech="",pay_schedule="",delivery="",gender_type= "",shopCategory="",shop_licence="",delivery_areas="",idproofType="";
    int seakbar_km=1;
    private static final String TAG = "ShopProfileActivity";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//"2019-06-3 02:09:47"
     private String latitude,longitude,strCity="City";
    Bitmap shopbitmap=null, shopLicenceBitmap=null, idproofBitmap=null;
    public static final String EMAIL_VERIFICATION = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";
    String MobilePattern = "[0-9]{10}";
    private static int CAPTURE_IMG_DOCUMENT=1,CAPTURE_IMG_SHOPACT=2,CAPTURE_IMG_SHOPPHOTO=3;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;
    ArrayList<String> shop_category_list = new ArrayList<>();
    RadioGroup rg_delivery_provide;
    RadioButton rb_i_have,rb_i_required,rb_both;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_profile2);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_shop_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = ShopProfileActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        progressDialog =  new ProgressDialog(mContext);
        initViews();
        if (!hasPermissions(ShopProfileActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(ShopProfileActivity.this, PERMISSIONS, PERMISSION_ALL);
        }
        getShopCategory();
        getProfileDetails();


        if(sharedPreferencesUtils.getAdminType().equalsIgnoreCase("Manufacturer")){
            getSupportActionBar().setTitle("Manufacturer Profile");
            tv_shop_cat.setText("Manufacturer Category");
        }

        btn_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //RadioButton selectedRadioButton = findViewById(genderRadioGroup.getCheckedRadioButtonId());
                //                gender = selectedRadioButton == null ? "" : selectedRadioButton.getText().toString().trim();
                if(shopUpdateValidation()){
                    uploadImageWithProfileUpdate();
                }
            }
        });
        uploadphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_images();
            }
        });
        btn_uploadphoto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_ShopAct();
            }
        });
        btn_uploaddocument2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_shopDocument();
            }
        });

        sp_document.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (sp_document.getSelectedItem().equals("Other Document")) {
                    reg_other_document.setVisibility(View.VISIBLE);
                } else {
                    reg_other_document.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        //document capture
        img_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_DOCUMENT);
            }
        });
        img_licence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_IMG_SHOPACT);
            }
        });
        shop_photo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_IMG_SHOPPHOTO);
            }
        });

        rg_delivery_provide.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });
    }

    private  void initViews(){

        profile_layout = findViewById(R.id.layout_profile);
        shop_layout =  findViewById(R.id.layout_shopdetails);
        layout_shopaddress=  findViewById(R.id.layout_shopaddress);
        shop_register_btn =  findViewById(R.id.btn_submit2);
        uploadphoto =  findViewById(R.id.btn_uploadphoto);
        shop_photo1 =  findViewById(R.id.img_selected);
        shop_photo2 =  findViewById(R.id.img_selected1);
        btn_previous = findViewById(R.id.btn_previous);
        et_ownername = findViewById(R.id.et_ownername);
        et_shopcontact = findViewById(R.id.et_shopcontact);
        reg_shop_name = findViewById(R.id.reg_shop_name);
        et_shopemail = findViewById(R.id.et_shopemail);
        et_shopcity =  findViewById(R.id.et_shopcity);
        et_gst_registraion =  findViewById(R.id.et_gst_registraion);
        sp_shopadmintype=  findViewById(R.id.sp_shopadmintype);
        sp_idproof = findViewById(R.id.sp_idproof);


        seakbar_area_km = findViewById(R.id.bar_area_km);

        sp_deliveryboy = findViewById(R.id.sp_deliveryboy);
        et_no_delivery_boy = findViewById(R.id.et_no_delivery_boy);
        sp_knowledgetech = findViewById(R.id.sp_knowledgetech);
        sp_payschedule = findViewById(R.id.sp_payschedule);
        sp_shoptype = findViewById(R.id.sp_shoptype);
        sp_category = findViewById(R.id.sp_category);
        sp_document = findViewById(R.id.sp_document);
        reg_other_document = findViewById(R.id.reg_other_document);
        sp_gendertype = findViewById(R.id.sp_gendertype);
        et_managername = findViewById(R.id.et_managername);
        et_managercontact = findViewById(R.id.et_managercontact);
        tv_sameAsAbove = findViewById(R.id.tv_sameAsAbove);
        et_reg_shopgstno = findViewById(R.id.et_reg_shopgstno);
        et_ownercontact= findViewById(R.id.et_ownercontact);
        tv_shop_cat = findViewById(R.id.tv_shop_cat);

        img_licence = findViewById(R.id.img_licence);
        img_document = findViewById(R.id.img_document);
        img_document2 = findViewById(R.id.img_document2);

        btn_uploadphoto2 = findViewById(R.id.btn_uploadphoto2);
        btn_uploaddocument2 = findViewById(R.id.btn_uploaddocument2);

        btn_next_soc=  findViewById(R.id.btn_submit1);
        btn_previous_first = findViewById(R.id.btn_previous_first);
        btn_next_third = findViewById(R.id.btn_next_third);
        //stepper
        view_stepper_sec = findViewById(R.id.view_stepper_sec);
        txt_stepper_sec = findViewById(R.id.txt_stepper_sec);
        txt_stepper_third = findViewById(R.id.txt_stepper_third);
        tv_destancekm = findViewById(R.id.tv_destancekm);
        //address
        et_address1= findViewById(R.id.et_address1);
        et_shopaddress2= findViewById(R.id.et_shopaddress2);
        et_shopaddress3= findViewById(R.id.et_shopaddress3);
        et_shopaddress4= findViewById(R.id.et_shopaddress4);
        et_shopaddress5= findViewById(R.id.et_shopaddress5);
        et_shopaddress6= findViewById(R.id.et_shopaddress6);
        btn_update_profile = findViewById(R.id.btn_update_profile);
        et_shopcontact.setEnabled(false);
        // MANUFACTURES FORMS FIELDS
        layout_manufacture_form = findViewById(R.id.layout_manufacture_form);
        sp_turnover = findViewById(R.id.sp_turnover);
        et_how_may_products = findViewById(R.id.et_how_may_products);
        et_lowest_cost = findViewById(R.id.et_lowest_cost);
        et_highest_cost = findViewById(R.id.et_highest_cost);
        et_bank_type = findViewById(R.id.et_bank_type);
        et_ifsc_code = findViewById(R.id.et_ifsc_code);
        et_AC_no = findViewById(R.id.et_AC_no);

        ll_delivery_options = findViewById(R.id.ll_delivery_options);
        et_paytm = findViewById(R.id.et_paytm);
        et_googlepay = findViewById(R.id.et_googlepay);
        et_phonepe = findViewById(R.id.et_phonepe);


        cb_required_licence = findViewById(R.id.cb_required_licence);
        cb_quility_product_img = findViewById(R.id.cb_quility_product_img);
        rg_delivery_provide = findViewById(R.id.rg_delivery_provide);
        rb_i_have = findViewById(R.id.rb_i_have);
        rb_i_required = findViewById(R.id.rb_i_required);
        rb_both = findViewById(R.id.rb_both);

    }

    private void getProfileDetails(){

        //get Mobile verify
            if(Connectivity.isConnected(mContext)){

                String urlShopProfile = StaticUrl.verifyMobile+"?contact="+sharedPreferencesUtils.getPhoneNumber();

                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                        urlShopProfile,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("shopProfile",response);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    boolean status = jsonObject.getBoolean("status");
                                    if(status==true){
                                        JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                        Log.e("ProfileResponse:",""+jsonObjectData.toString());
                                        gender_type = jsonObjectData.getString("gender_type");
                                        et_ownername.setText(jsonObjectData.getString("sellername"));
                                        et_ownercontact.setText(jsonObjectData.getString("owner_contact"));
                                        reg_shop_name.setText(jsonObjectData.getString("shop_name"));
                                       // Log.e("shopName_value:",""+jsonObjectData.getString("sellername")+":"+jsonObjectData.getString("owner_contact")+":"+jsonObjectData.getString("shop_name"));
                                        et_shopcontact.setText(jsonObjectData.getString("contact"));
                                        et_shopemail.setText(jsonObjectData.getString("email"));

                                        shopCategory = jsonObjectData.getString("shop_category");
                                        shop_type = jsonObjectData.getString("partner_type");
                                        //Log.e("shop_type_get:",""+shop_type);
                                        admin_type = jsonObjectData.getString("admin_type");
                                        //Log.e("admin_type_get:",""+admin_type);
                                        if(admin_type.equalsIgnoreCase("Manufacturer")){
                                            // Manufacture visibility visible
                                            ll_delivery_options.setVisibility(View.GONE);
                                            layout_manufacture_form.setVisibility(View.VISIBLE);
                                            et_bank_type.setText(jsonObjectData.getString("bank_type"));
                                            et_AC_no.setText(jsonObjectData.getString("account_no"));
                                            et_ifsc_code.setText(jsonObjectData.getString("ifsc_code"));
                                            et_paytm.setText(jsonObjectData.getString("paytm"));
                                            et_phonepe.setText(jsonObjectData.getString("phonepe"));
                                            //et_googlepay.setText(jsonObjectData.getString("googlepay"));


                                        }else {
                                            // Manufactures visibility gone
                                            layout_manufacture_form.setVisibility(View.GONE);
                                            ll_delivery_options.setVisibility(View.GONE);
                                            //ll_delivery_options.setVisibility(View.VISIBLE);
                                        }
                                        sp_turnover_value = jsonObjectData.getString("mnf_turnover");
                                        et_highest_cost.setText(jsonObjectData.getString("mnf_highcost"));
                                        et_lowest_cost.setText(jsonObjectData.getString("mnf_lowcost"));
                                        et_how_may_products.setText(jsonObjectData.getString("mnf_noproduct"));

                                        et_managername.setText(jsonObjectData.getString("manager_name"));
                                        et_managercontact.setText(jsonObjectData.getString("manager_contact"));
                                        shop_licence = jsonObjectData.getString("shop_licence");
                                        reg_other_document.setText(jsonObjectData.getString("other_doc"));
                                        et_reg_shopgstno.setText(jsonObjectData.getString("gst_no"));
                                        idproofType = jsonObjectData.getString("identity_proof");
                                        knowledge_of_tech = jsonObjectData.getString("knowledge_of_tech");

                                        pay_schedule = jsonObjectData.getString("pay_schedule");
                                        delivery = jsonObjectData.getString("delivery");
                                        delivery_areas = jsonObjectData.getString("delivery_areas");
                                       // seakbar_km = Integer.parseInt(jsonObjectData.getString("preferred_delivery_area_km"));
                                        //seakbar_area_km.setProgress(seakbar_km);
                                        //Log.e("preferred_area_km:",""+seakbar_area_km);
                                        //params.put("delivery_areas", String.valueOf(preferred_delivery_area_km));
                                        et_no_delivery_boy.setText(jsonObjectData.getString("avilable_delivery_boy"));

                                        shop_id = jsonObjectData.getString("shop_id");
                                        //shop_latitude = jsonObjectData.getString("shop_latitude");
                                        //shop_longitude = jsonObjectData.getString("shop_longitude");

                                        et_shopaddress6.setText(jsonObjectData.getString("area"));
                                        et_shopcity.setText(jsonObjectData.getString("city"));

                                        String doc_type_idproof = jsonObjectData.getString("doc_type_idproof");
                                         if(doc_type_idproof.equals("")){

                                         }else {
                                             Picasso.with(mContext)
                                                     .load(jsonObjectData.getString("doc_type_idproof"))
                                                     .placeholder(R.drawable.loading)
                                                     .error(R.drawable.ic_account_box_black_24dp)
                                                     .into(img_document);
                                         }
                                        //identity_proof_img
                                        Log.e("docurl:",""+jsonObjectData.getString("doc_type_idproof"));
                                        String identity_proof_img = jsonObjectData.getString("identity_proof_img");
                                        if(identity_proof_img.equals("")){

                                        }else {
                                            Picasso.with(mContext)
                                                    .load(jsonObjectData.getString("identity_proof_img"))
                                                    .placeholder(R.drawable.loading)
                                                    .error(R.drawable.ic_account_box_black_24dp)
                                                    .into(img_licence);
                                        }
                                        //shop_logo
                                        Log.e("licenceUrl:",""+jsonObjectData.getString("identity_proof_img"));
                                        String shop_logo = jsonObjectData.getString("shop_logo");
                                        if(shop_logo.equals("")){

                                        }else {
                                            Picasso.with(mContext)
                                                    .load(jsonObjectData.getString("shop_logo"))
                                                    .placeholder(R.drawable.loading)
                                                    .error(R.drawable.ic_account_box_black_24dp)
                                                    .into(shop_photo1);
                                            Log.e("shop_logo_url:", "" + jsonObjectData.getString("shop_logo"));
                                        }

                                        Log.e("ShopProfile Resp:",""+email+""+shop_id+""+owner_contact+""+shop_name);
                                        setProfileData();

                                    }else if(status==false) {
                                        Toast.makeText(mContext,"Mobile Number is not Register",Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                            }
                        });

                MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
            }else {
                Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
            }


    }

    // Get getShopCategory Names
    public void getShopCategory(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.getshopcategory;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.e("ShopCategory_Type",""+response);

                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for(int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        String value = jsonObject1.getString("shop_category");
                                        shop_category_list.add(value);
                                    }

                                    ArrayAdapter<String> adapter = new ArrayAdapter(ShopProfileActivity.this,android.R.layout.simple_spinner_item,shop_category_list);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    sp_category.setAdapter(adapter);
                                    if (shopCategory != null) {
                                        int spinnerPosition = adapter.getPosition(shopCategory);
                                        sp_category.setSelection(spinnerPosition);
                                    }

                                }else if(!statuss) {

                                    Toast.makeText(mContext,"No Category Found..!!",Toast.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
            Log.e("GetCategory_Res:",""+stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void setProfileData(){

        //sp_gendertype
        String genderValue = gender_type;
        ArrayAdapter<CharSequence> genderAdaptor = ArrayAdapter.createFromResource(this, R.array.Gender_Type, android.R.layout.simple_spinner_item);
        genderAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_gendertype.setAdapter(genderAdaptor);
        if (genderValue != null) {
            int spinnerPosition0 = genderAdaptor.getPosition(genderValue);
            sp_gendertype.setSelection(spinnerPosition0);
        }

        String compareValue = shopCategory;
        /*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.ShopCategory_Type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        ArrayAdapter<String> adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,shop_category_list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_category.setAdapter(adapter);
        if (compareValue != null) {
            int spinnerPosition = adapter.getPosition(compareValue);
            sp_category.setSelection(spinnerPosition);
        }

        //sp_shoptype
        String shoptypevalue = shop_type;
        ArrayAdapter<CharSequence> shoptypeAdaptor = ArrayAdapter.createFromResource(this, R.array.Shop_Type, android.R.layout.simple_spinner_item);
        shoptypeAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_shoptype.setAdapter(shoptypeAdaptor);
        if (shoptypevalue != null) {
            int spinnerPosition2 = shoptypeAdaptor.getPosition(shoptypevalue);
            sp_shoptype.setSelection(spinnerPosition2);
        }

        /*String shopAdmin = admin_type;
        ArrayAdapter<CharSequence> shopAdminAdoptor = ArrayAdapter.createFromResource(this, R.array.admin_Type, android.R.layout.simple_spinner_item);
        shopAdminAdoptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_shopadmintype.setAdapter(shopAdminAdoptor);
        if (shopAdmin != null) {
            int spinnerPosition3 = shopAdminAdoptor.getPosition(shopAdmin);
            sp_shopadmintype.setSelection(spinnerPosition3);
        }*/

        String deliveryboy = delivery;
        ArrayAdapter<CharSequence> deliveryBoyAdaptor = ArrayAdapter.createFromResource(this, R.array.DeliveryBoy_Type, android.R.layout.simple_spinner_item);
        deliveryBoyAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_deliveryboy.setAdapter(deliveryBoyAdaptor);
        if (deliveryboy != null) {
            int spinnerPosition4 = deliveryBoyAdaptor.getPosition(deliveryboy);
            sp_deliveryboy.setSelection(spinnerPosition4);
        }


        String knowledge = knowledge_of_tech;
        ArrayAdapter<CharSequence> knowledgeAdaptor = ArrayAdapter.createFromResource(this, R.array.Knowledge_Type, android.R.layout.simple_spinner_item);
        knowledgeAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_knowledgetech.setAdapter(knowledgeAdaptor);
        if (knowledge != null) {
            int spinnerPosition5 = knowledgeAdaptor.getPosition(knowledge);
            sp_knowledgetech.setSelection(spinnerPosition5);
        }

        //sp_document
        String doctype = shop_licence;
        ArrayAdapter<CharSequence> doctypeAdaptor = ArrayAdapter.createFromResource(this, R.array.Upload_Document, android.R.layout.simple_spinner_item);
        doctypeAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_document.setAdapter(doctypeAdaptor);
        if (doctype != null) {
            int spinnerPosition7 = doctypeAdaptor.getPosition(doctype);
            sp_document.setSelection(spinnerPosition7);
        }

        String payscedule = pay_schedule;
        ArrayAdapter<CharSequence> payscheduleAdaptor = ArrayAdapter.createFromResource(this, R.array.PaySchedule_Type, android.R.layout.simple_spinner_item);
        payscheduleAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_payschedule.setAdapter(payscheduleAdaptor);
        if (payscedule != null) {
            int spinnerPosition6 = payscheduleAdaptor.getPosition(payscedule);
            sp_payschedule.setSelection(spinnerPosition6);
        }
        //sp_idproof
        String idproofTypevalue = idproofType;
        ArrayAdapter<CharSequence> idproofAdaptor = ArrayAdapter.createFromResource(this, R.array.ID_Proof_Type, android.R.layout.simple_spinner_item);
        idproofAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_idproof.setAdapter(idproofAdaptor);
        if (idproofTypevalue != null) {
            int spinnerPosition8 = idproofAdaptor.getPosition(idproofTypevalue);
            sp_idproof.setSelection(spinnerPosition8);
        }

        String turnover_value = sp_turnover_value;
        ArrayAdapter<CharSequence> turnOverAdaptor = ArrayAdapter.createFromResource(this, R.array.Turn_over, android.R.layout.simple_spinner_item);
        turnOverAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_turnover.setAdapter(turnOverAdaptor);
        if (turnover_value != null) {
            int spinnerPosition6 = turnOverAdaptor.getPosition(turnover_value);
            sp_turnover.setSelection(spinnerPosition6);
        }

    }

    public boolean shopUpdateValidation() {

        if(et_ownername.getText().toString().isEmpty()){
            et_ownername.requestFocus();
            et_ownername.setError("Please enter Owner Name");
            return false;
        }else if(et_ownercontact.getText().toString().isEmpty()){
            et_ownercontact.requestFocus();
            et_ownercontact.setError("Enter Owner/Freelancer Contact");
            return false;
        }else if(!et_ownercontact.getText().toString().matches(MobilePattern)){
            et_ownercontact.requestFocus();
            et_ownercontact.setError("Please enter valid contact");
            return false;
        } else if(reg_shop_name.getText().toString().isEmpty()){
            reg_shop_name.requestFocus();
            reg_shop_name.setError("Please enter Shop Name");
            return false;
        }else if(et_shopcontact.getText().toString().isEmpty()){
            et_shopcontact.requestFocus();
            et_shopcontact.setError("Please enter contact");
            return false;
        }else if(!et_shopcontact.getText().toString().matches(MobilePattern)){
            et_shopcontact.requestFocus();
            et_shopcontact.setError("Please enter valid contact");
            return false;
        }else if(et_shopemail.getText().toString().isEmpty()){
            et_shopemail.requestFocus();
            et_shopemail.setError("Please enter valid Email Address");
            return false;
        }else if(!et_shopemail.getText().toString().trim().matches(EMAIL_VERIFICATION)){
            et_shopemail.requestFocus();
            et_shopemail.setError("Please enter valid Email Address");
            return false;
        }else if(sp_category.getSelectedItem().toString().trim().equals("Select Shop Category")){
            Toast.makeText(mContext, "Please select shop Category", Toast.LENGTH_SHORT).show();
            return false;
        }else if(rg_delivery_provide.getCheckedRadioButtonId()==-1){//No Radio Button Is Checked
            Toast.makeText(getApplicationContext(), "Please Select Delivery Option", Toast.LENGTH_LONG).show();
        } /*else if(sp_shoptype.getSelectedItem().toString().trim().equals("Shop Type")){
            Toast.makeText(mContext, "Please select shop type", Toast.LENGTH_SHORT).show();
            return false;
        }*/ /*else if(sp_shopadmintype.getSelectedItem().toString().trim().equals("Select User Type")){
            Toast.makeText(mContext, "Please Select User Type", Toast.LENGTH_SHORT).show();
            return  false;
        }*/ else if(sp_document.getSelectedItem().toString().trim().equals("Select Document Type")){
            Toast.makeText(mContext, "Please select Document type", Toast.LENGTH_SHORT).show();
            return false;
        }else if(et_reg_shopgstno.getText().toString().isEmpty()){
            Toast.makeText(mContext, "Mention Number selected document", Toast.LENGTH_SHORT).show();
            return false;
        }else if(!et_managercontact.getText().toString().isEmpty()){
            if(!et_managercontact.getText().toString().matches(MobilePattern)){
                et_managercontact.requestFocus();
                et_managercontact.setError("Please enter valid contact");
                return false;
            }
        }/*else if(sp_deliveryboy.getSelectedItem().toString().trim().equals("Delivery boy")){
            Toast.makeText(mContext, "Please select Delivery boy", Toast.LENGTH_SHORT).show();
            return false;
        }else if(sp_knowledgetech.getSelectedItem().toString().trim().equals("Tech Knowledge")){
        //}else if(sp_knowledgetech.getSelectedItem().toString().trim().equals("Knowledge_Type")){
            Toast.makeText(mContext, "Please select Tech Knowledge type", Toast.LENGTH_SHORT).show();
            return false;
        }*//*else if(sp_payschedule.getSelectedItem().toString().trim().equals("Pick One")){
            Toast.makeText(mContext, "Please select pay schedule type", Toast.LENGTH_SHORT).show();
            return false;
        }*/else if(sp_idproof.getSelectedItem().toString().trim().equals("ID Proof")){
            Toast.makeText(mContext, "Please ID Proof type", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void select_images(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }
    private void select_ShopAct(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_SHOPACT);
    }
    private void select_shopDocument(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMG_DOCUMENT_UPLOAD);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null) {

            //shop photo
            if (requestCode == PICK_IMAGE) {
                //TODO: action
                // Get the url from data
                Uri selectedImageUri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    shop_photo1.setImageBitmap(bitmap);
                    //shopbitmap = bitmap;
                    shopbitmap = getResizedBitmap( bitmap,500);
                    Log.e("shop_photo1bitmap", "" + shopbitmap);
                } catch (Exception e) {
                    e.getMessage();
                    Log.e("BitmapExp", "" + e.getMessage());
                }
            }
            if(requestCode == CAPTURE_IMG_SHOPPHOTO){
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                thumbnail.compress(Bitmap.CompressFormat.PNG, 45, bytes);
                try {
                    shop_photo1.setImageBitmap(thumbnail);
                    //shopbitmap = thumbnail;
                    shopbitmap = getResizedBitmap( thumbnail,400);
                    Log.e("shopbitmap:",""+shopbitmap.toString());
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            //shopAct lic/gst
            if (requestCode == PICK_IMAGE_SHOPACT) {
                //TODO: action
                // Get the url from data
                Uri selectedImageUri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    img_licence.setImageBitmap(bitmap);
                    //shopLicenceBitmap = bitmap;
                    shopLicenceBitmap = getResizedBitmap( bitmap,500);
                    Log.e("img_licencebitmap", "" + shopLicenceBitmap);
                } catch (Exception e) {
                    e.getMessage();
                    Log.e("BitmapExp", "" + e.getMessage());
                }
            }
            if(requestCode == CAPTURE_IMG_SHOPACT){
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                thumbnail.compress(Bitmap.CompressFormat.PNG, 45, bytes);

                try {
                    img_licence.setImageBitmap(thumbnail);
                    //shopLicenceBitmap = thumbnail;
                    shopLicenceBitmap = getResizedBitmap( thumbnail,400); // in KB
                    Log.e("shopLicenceBitmap:",""+shopLicenceBitmap.toString());
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            //select document image
            if (requestCode == IMG_DOCUMENT_UPLOAD) {
                //TODO: action
                //Get the url from data
                Uri selectedImageUri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    img_document.setImageBitmap(bitmap);
                    //idproofBitmap = bitmap;
                    idproofBitmap = getResizedBitmap( bitmap,500);
                    Log.e("idproofBitmap", "" + idproofBitmap);
                } catch (Exception e) {
                    e.getMessage();
                    Log.e("BitmapExp", "" + e.getMessage());
                }
            }
            if(requestCode == CAPTURE_DOCUMENT){
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                thumbnail.compress(Bitmap.CompressFormat.PNG, 45, bytes);
                try {
                    img_document.setImageBitmap(thumbnail);
                    //idproofBitmap = thumbnail;
                    idproofBitmap = getResizedBitmap( thumbnail,400);
                    Log.e("idproofBitmap:",""+idproofBitmap.toString());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        }else {
            //Toast.makeText(mContext,"Please Select Image",Toast.LENGTH_LONG).show();
        }
    }

    public void uploadImageWithProfileUpdate() {
        if(Connectivity.isConnected(mContext)) {
            final String currentDateandTime = sdf.format(new Date());

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            //String urlProfileUpdate = StaticUrl.shop_profile_address_update;
            String urlProfileUpdate = StaticUrl.shop_profile_address_update;
            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                    urlProfileUpdate, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    try {

                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e(TAG, "resp" + resultResponse.toString());
                        Log.e(TAG, "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status == true)
                        {
                            //getProfileDetails();
                            Toast.makeText(mContext,"Profile Update Successfully",Toast.LENGTH_LONG).show();
                            sharedPreferencesUtils.setShopName(reg_shop_name.getText().toString());
                            sharedPreferencesUtils.setUserName(et_ownername.getText().toString());
                            sharedPreferencesUtils.setEmailId(et_shopemail.getText().toString());
                            Intent intent = new Intent(mContext,HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);

                        }else if(status == false){
                            Log.d(TAG,"status_false"+resultResponse);
                            Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleYIMAGE_EXP:", "" + e.getMessage());
                        // progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse networkResponse = error.networkResponse;

                    progressDialog.dismiss();
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.e("Error", ""+errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<>();

                    params.put("shop_id", sharedPreferencesUtils.getShopID());
                   // params.put("gender_type", sp_gendertype.getSelectedItem().toString().trim());
                    try {
                        params.put("sellername", URLEncoder.encode(et_ownername.getText().toString(),"utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    /*try {
                        params.put("sellername", URLEncoder.encode(et_ownername.getText().toString(),"UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }*/

                   /* params.put("owner_contact", et_ownercontact.getText().toString());
                    params.put("shop_name", reg_shop_name.getText().toString());
                    //params.put("contact", et_shopcontact.getText().toString());
                    params.put("email", et_shopemail.getText().toString());
                    params.put("shop_category", sp_category.getSelectedItem().toString().trim());
                    params.put("shop_type", sp_shoptype.getSelectedItem().toString().trim());
                   // params.put("admin_type", sp_shopadmintype.getSelectedItem().toString().trim());
                    params.put("manager_name", et_managername.getText().toString());
                    params.put("manager_contact", et_managercontact.getText().toString());
                    params.put("shop_licence", sp_document.getSelectedItem().toString().trim());//document select
                    params.put("other_doc", reg_other_document.getText().toString());//other document type
                    params.put("gst_no", et_reg_shopgstno.getText().toString());//mention number of selected document
                    params.put("delivery", sp_deliveryboy.getSelectedItem().toString().trim());
                    params.put("knowledge_of_tech", sp_knowledgetech.getSelectedItem().toString().trim());
                    params.put("pay_schedule", sp_payschedule.getSelectedItem().toString().trim());
                    params.put("avilable_delivery_boy", et_no_delivery_boy.getText().toString());*///no of delivery boy availble


                   params.put("shop_name",reg_shop_name.getText().toString());


                    params.put("owner_contact", et_ownercontact.getText().toString());
                    params.put("email", et_shopemail.getText().toString());
                    RadioButton selectedRadioButton = findViewById(rg_delivery_provide.getCheckedRadioButtonId());
                    String delivery_type1 = selectedRadioButton.getText().toString().trim();
                    params.put("delivery_type1", delivery_type1);
                    params.put("shop_category", sp_category.getSelectedItem().toString().trim());
                    params.put("shop_type", sp_shoptype.getSelectedItem().toString().trim());
                   // params.put("manager_name", et_managername.getText().toString());
                   // params.put("manager_contact", et_managercontact.getText().toString());
                    params.put("shop_licence", sp_document.getSelectedItem().toString().trim());//document select
                    params.put("other_doc", reg_other_document.getText().toString());//other document type
                    params.put("gst_no", et_reg_shopgstno.getText().toString());//mention number of selected document

                    if(admin_type.equalsIgnoreCase("Manufacturer")){
                        params.put("delivery", "PICODEL");
                        params.put("avilable_delivery_boy", "1");//no of delivery boy availble*/
                    }else {
                        params.put("delivery","PICODEL" ); //sp_deliveryboy.getSelectedItem().toString().trim()
                        params.put("avilable_delivery_boy", "2");//no of delivery boy availble*/
                        //params.put("avilable_delivery_boy", et_no_delivery_boy.getText().toString());//no of delivery boy availble*/
                    }


                    //params.put("knowledge_of_tech", sp_knowledgetech.getSelectedItem().toString().trim());
                    params.put("knowledge_of_tech", "Good");
                    params.put("pay_schedule", sp_payschedule.getSelectedItem().toString().trim());
                    params.put("delivery_areas", String.valueOf(seakbar_km));

                    params.put("identity_proof", sp_idproof.getSelectedItem().toString().trim());


                    /*params.put("shop_latitude", latitude);
                    params.put("shop_longitude", longitude);*/
                     //Manufacturer values
                    if(admin_type.equalsIgnoreCase("Manufacturer")) {
                        params.put("mnf_turnover", sp_turnover_value);
                        params.put("mnf_highcost", et_highest_cost.getText().toString());
                        params.put("mnf_lowcost", et_lowest_cost.getText().toString());
                        params.put("mnf_noproduct", et_how_may_products.getText().toString());

                        params.put("bank_type", et_bank_type.getText().toString());
                        params.put("account_no", et_AC_no.getText().toString());
                        params.put("ifsc_code", et_ifsc_code.getText().toString());
                        params.put("paytm", et_paytm.getText().toString());
                        params.put("phonepe", et_phonepe.getText().toString());
                       // params.put("googlepay", et_googlepay.getText().toString());

                        //et_phonepe


                        if(cb_required_licence.isChecked()){
                            params.put("mnf_islicenses", "1");
                        }else {
                            params.put("mnf_islicenses", "0");
                        }

                        if(cb_quility_product_img.isChecked()){
                            params.put("mnf_isquility_img", "1");
                        }else {
                            params.put("mnf_isquility_img", "0");
                        }

                    }else {
                        params.put("mnf_turnover", "0");
                        params.put("mnf_highcost", "0");
                        params.put("mnf_lowcost", "0");
                        params.put("mnf_noproduct", "0");
                        params.put("mnf_islicenses", "0");
                        params.put("mnf_isquility_img", "0");
                    }

                    Log.e("UpdateProfileParams", "" + params);
                    return params;
                }


                @Override
                protected Map<String, DataPart> getByteData() {

                    Map<String, DataPart> params = new HashMap<>();
                    try {
                        Bitmap shopbitmapbyphoto = imageView2Bitmap(shop_photo1);

                        params.put("shop_logo", new DataPart(getBytesFromBitmap(shopbitmapbyphoto)));
                        Bitmap licencebitmapbyphoto = imageView2Bitmap(img_licence);
                        params.put("identity_proof_img", new DataPart(getBytesFromBitmap(licencebitmapbyphoto)));
                        Bitmap idbitmapbyphoto = imageView2Bitmap(img_document);
                        params.put("doc_type_idproof", new DataPart(getBytesFromBitmap(idbitmapbyphoto)));
                       /* params.put("shop_logo", new DataPart(getBytesFromBitmap(shopbitmap)));
                        params.put("identity_proof_img", new DataPart(getBytesFromBitmap(shopLicenceBitmap)));
                        params.put("doc_type_idproof", new DataPart(getBytesFromBitmap(idproofBitmap)));*/
                    }catch (Exception e){
                        e.getMessage();
                    }
                    return params;
                }

            };

            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        }
        else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 45, stream);
        return stream.toByteArray();
    }

    private Bitmap imageView2Bitmap(ImageView view){
        Bitmap bitmap = ((BitmapDrawable)view.getDrawable()).getBitmap();
        return bitmap;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
