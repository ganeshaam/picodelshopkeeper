package com.podmerchant.activites;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.podmerchant.R;
import com.podmerchant.adapter.PaymentModeAdaptor;
import com.podmerchant.model.PaymentModeModel;
import com.podmerchant.staticurl.StaticUrl;
import com.podmerchant.util.Connectivity;
import com.podmerchant.util.MySingleton;
import com.podmerchant.util.SharedPreferencesUtils;
import com.podmerchant.util.VolleyMultipartRequest;
import com.podmerchant.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class DistributorDeliverActivity extends AppCompatActivity {

    AlertDialog progressDialog;
    Context mContext;
    ArrayList<PaymentModeModel> paymentModeList;
    RecyclerView rv_paymentMode;
    Button btn_deliverd_status,btn_upload;
    String orderId="",shopID="",product_brand="",Selected_PaymentMode="",payID="0",vcode,contact,shopkpwise_id,cart_unique_id;
    PaymentModeAdaptor paymentModeAdaptor;
    LinearLayoutManager layoutManager;
    LinearLayout layout_upload;
    TextView tv_selectedvalue;
    SharedPreferencesUtils sharedPreferencesUtils;
    ImageView img_licence,img_licence2;
    public static final int PICK_IMAGE = 1,PICK_IMAGE_SHOPACT = 9, IMG_DOCUMENT_UPLOAD = 5, SELECT_PICTURE = 2,PERMISSION_ACCESS_COARSE_LOCATION = 1,CAPTURE_DOCUMENT=10;
    private static int CAPTURE_IMG_DOCUMENT=1,CAPTURE_IMG_SHOPACT=2,CAPTURE_IMG_SHOPPHOTO=3;
    ArrayList<String> paymentOptionList;
    Bitmap paymentBitmap=null;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_order_deliverd);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_select_paymentmode);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = DistributorDeliverActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

       /* Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
*/
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        rv_paymentMode = findViewById(R.id.rv_paymentMode);
        btn_deliverd_status = findViewById(R.id.btn_deliverd_status);
        tv_selectedvalue = findViewById(R.id.tv_selectedvalue);
        img_licence = findViewById(R.id.img_licence);
        img_licence2 = findViewById(R.id.img_licence2);
        btn_upload = findViewById(R.id.btn_upload);
        layout_upload = findViewById(R.id.layout_upload);
        layout_upload.setVisibility(View.VISIBLE);


        rv_paymentMode.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        paymentModeList = new ArrayList<>();

        rv_paymentMode.setLayoutManager(layoutManager);
        rv_paymentMode.setItemAnimator(new DefaultItemAnimator());
        paymentModeAdaptor = new PaymentModeAdaptor(mContext,paymentModeList);
        rv_paymentMode.setAdapter(paymentModeAdaptor);


        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_images();
            }
        });

        //paymentModeList();
        Intent intent = getIntent();
        orderId= intent.getStringExtra("order_id");
        product_brand= intent.getStringExtra("product_brand");
        contact= intent.getStringExtra("contact");
        shopkpwise_id= intent.getStringExtra("shopkpwise_id");
        cart_unique_id= intent.getStringExtra("cart_unique_id");
        Log.e("Product_brand_mob",""+product_brand+":"+orderId+":"+contact+":"+shopkpwise_id+":"+cart_unique_id);

        if (!hasPermissions(DistributorDeliverActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(DistributorDeliverActivity.this, PERMISSIONS, PERMISSION_ALL);
        }

        getAccountDetails(product_brand);


        img_licence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_IMG_SHOPACT);
            }
        });

        rv_paymentMode.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_paymentMode,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        //  delBoyId =  paymentModeList.get(position).getDelBoyId();
                        Selected_PaymentMode= paymentModeList.get(position).getPaymentType();
                        payID= paymentModeList.get(position).getID();
                        Toast.makeText(mContext,paymentModeList.get(position).getPaymentType(),Toast.LENGTH_SHORT).show();
                        tv_selectedvalue.setText("Payment Mode:"+Selected_PaymentMode);
                        //paymentModeAdaptor.notifyDataSetChanged();
                        if(Selected_PaymentMode.equalsIgnoreCase("cash")){
                            layout_upload.setVisibility(View.GONE);

                        }else {
                            layout_upload.setVisibility(View.VISIBLE);
                        }
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        btn_deliverd_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Selected_PaymentMode.equalsIgnoreCase("cash")){
                    sendOtp();
                }else {

                    if (Selected_PaymentMode.equals("")) {
                        Toast.makeText(mContext, "Please select Payment Mode", Toast.LENGTH_LONG).show();
                    } else if (paymentBitmap == null) {
                        Toast.makeText(mContext, "Please select Payment Image", Toast.LENGTH_LONG).show();
                    } else {
                        uploadImageWithPamentUpdate();
                    }
                }
            }
        });


    }

    private void select_images(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null) {

            //shop photo
            if (requestCode == PICK_IMAGE) {
                //TODO: action
                // Get the url from data
                Uri selectedImageUri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    img_licence2.setImageBitmap(bitmap);
                    paymentBitmap = bitmap;
                    Log.e("shop_paymentBitmap", "" + paymentBitmap);
                } catch (Exception e) {
                    e.getMessage();
                    Log.e("BitmapExp", "" + e.getMessage());
                }
            }


            if(requestCode == CAPTURE_IMG_SHOPACT){
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);

                try {
                    img_licence2.setImageBitmap(thumbnail);
                    paymentBitmap = thumbnail;
                    Log.e("shopLicenceBitmap:",""+paymentBitmap.toString());
                } catch (Exception e) {
                    e.getMessage();
                }
            }



        }else {
            Toast.makeText(mContext,"Please Select Image",Toast.LENGTH_LONG).show();
        }
    }

    //Get paymentmode Api method
    public void paymentModeList(){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();
            paymentModeList.clear();
            String urlAcceptOrder = null;
            try {
                urlAcceptOrder = StaticUrl.paymentdetails+"?product_brand="+ URLEncoder.encode(product_brand,"utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.dismiss();
                            try {
                                Log.e("payment_respont",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i=0; i<jsonArray.length();i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    PaymentModeModel paymentMode_model = new PaymentModeModel();
                                     paymentMode_model.setPaymentType(jsonObject1.getString("p_type"));
                                     Log.e("value_type",""+jsonObject1.getString("p_type"));
                                     paymentMode_model.setPayment_details(jsonObject1.getString("details"));
                                     Log.e("value_details",""+jsonObject1.getString("details"));
                                     paymentModeList.add(paymentMode_model);
                                   }

                                    paymentModeAdaptor.notifyDataSetChanged();

                                    //  Toast.makeText(mContext,"Payment List Successfully fetched",Toast.LENGTH_SHORT).show();
                                }else if(status=false){
                                    Toast.makeText(mContext,"Sorry. No PaymentMode Found.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    // Get Account Details of Manufacturer
    private String getAccountDetails(final String product_brand) { //TODO Server method here

        final String[] result_string = {""};
        paymentModeList.clear();

        if (Connectivity.isConnected(mContext)) {
            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
            JSONObject params = new JSONObject();
            try {

                params.put("product_brand", product_brand);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlgetAccountDetails, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Account_Res:",""+response);
                    JSONObject jsonObject = null;
                    try {
                            JSONArray jsonObject1 = response.getJSONArray("posts");
                        boolean status = jsonObject1.getJSONObject(0).getBoolean("status");
                        if(status==true) {
                            JSONArray jsonArray = jsonObject1.getJSONObject(0).getJSONArray("data");
                            for (int i=0; i<jsonArray.length();i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                PaymentModeModel paymentMode_model = new PaymentModeModel();
                                paymentMode_model.setPaymentType(jsonObject2.getString("p_type"));
                                Log.e("value_type",""+jsonObject2.getString("p_type"));
                                paymentMode_model.setPayment_details(jsonObject2.getString("details"));
                                Log.e("value_details",""+jsonObject2.getString("details"));
                                paymentModeList.add(paymentMode_model);
                            }

                            paymentModeAdaptor.notifyDataSetChanged();

                            //  Toast.makeText(mContext,"Payment List Successfully fetched",Toast.LENGTH_SHORT).show();
                        }else if(status==false){
                            Toast.makeText(mContext,"Sorry. No PaymentMode Found.",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }

        return result_string[0];
    }



    //Set Deliver status Api method
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    private Bitmap imageView2Bitmap(ImageView view){
        Bitmap bitmap = ((BitmapDrawable)view.getDrawable()).getBitmap();
        return bitmap;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void uploadImageWithPamentUpdate() {
        if(Connectivity.isConnected(mContext)) {

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            //String urlProfileUpdate = StaticUrl.shop_profile_address_update;
            String urlProfileUpdate = StaticUrl.paymentdeliverypdate;
            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                    urlProfileUpdate, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    try {

                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e("DistributorPayment", "resp" + resultResponse.toString());
                        //Log.e("DistributorPayment", "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status == true)
                        {
                            //getProfileDetails();
                            generateReceiptMail(product_brand,cart_unique_id,shopkpwise_id);
                            Toast.makeText(mContext,"Payment Updated Successfully",Toast.LENGTH_LONG).show();
                            finish();
                        }else if(status == false){

                            Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleYIMAGE_EXP:", "" + e.getMessage());
                        // progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse networkResponse = error.networkResponse;

                    progressDialog.dismiss();
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.e("Error", ""+errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<>();

                    //params.put("shop_id", sharedPreferencesUtils.getShopID());
                    params.put("payment_type", Selected_PaymentMode);
                    params.put("order_id", orderId);
                    params.put("product_brand", product_brand);


                    Log.e("UpdateProfileParams", "" + params);
                    return params;
                }


                @Override
                protected Map<String, DataPart> getByteData() {

                    Map<String, DataPart> params = new HashMap<>();
                    try {

                        Bitmap idbitmapbyphoto = imageView2Bitmap(img_licence2);
                        params.put("doc_type_payment", new DataPart(getBytesFromBitmap(idbitmapbyphoto)));

                    }catch (Exception e){
                        e.getMessage();
                    }
                    return params;
                }

            };

            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        }
        else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //send otp
    public void sendOtp(){
        if(Connectivity.isConnected(mContext)){


            String urlOtpsend = StaticUrl.sendotp
                    +"?mobileno="+contact.trim()//"7276201058"//
                    +"&email="+sharedPreferencesUtils.getEmailId();

            Log.d("urlOtpsend",urlOtpsend);

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("sendOtp",response);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss)
                                {
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    vcode = jsonObjectData.getString("vcode");
                                    String contact = jsonObjectData.getString("contact");
                                    //String vdate = jsonObjectData.getString("vdate");
                                    Log.e("VCODE:",""+vcode);
                                    VerifyOtpDailog();

                                }else {
                                    Toast.makeText(mContext,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void VerifyOtpDailog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_custom_otp_verify, null);

        final EditText OTP_VALUE = (EditText) view.findViewById(R.id.et_recharge_otp);
        final Button btn_submit = (Button) view.findViewById(R.id.btn_submit_otp);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(vcode.equals(OTP_VALUE.getText().toString().trim())){
                    Toast.makeText(DistributorDeliverActivity.this,"OTP Verify Successfully",Toast.LENGTH_LONG).show();
                    uploadImageWithPamentUpdate();
                    dialog.dismiss();

                }else {
                    Toast.makeText(DistributorDeliverActivity.this,"Please enter Valid OTP",Toast.LENGTH_LONG).show();
                    //dialog.dismiss();
                }

            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

    // Receipt Generate while Payment Collected of the Order By the Distributor
    private void generateReceiptMail(String product_brand, String cartUniqueId ,String shopkpwise_id) { //TODO Server method here



        if (Connectivity.isConnected(mContext)) {
            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
            JSONObject params = new JSONObject();
            try {
                params.put("cart_unique_id", cartUniqueId);
                params.put("shop_id", sharedPreferencesUtils.getShopID());
                params.put("order_id", orderId);
                params.put("product_brand", product_brand);
                params.put("admin_type", sharedPreferencesUtils.getAdminType());
                params.put("shopkpwise_id", shopkpwise_id);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.Receipt_Distributor, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("ReceiptEmail_Res:",""+response);

                    if (response.isNull("posts")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts");
                            /*for (int i = 0; i < categoryJsonArray.length(); i++) {
                                JSONObject jsonCatData = categoryJsonArray.getJSONObject(i);
                                String value = jsonCatData.getString("accept");
                                result_string[0] = value;
                                if(value.equalsIgnoreCase("successfull")){
                                    //DialogApproved("Order Accept Successfully");
                                    Toast.makeText(mContext,"Email Invoice Generated Successfully",Toast.LENGTH_LONG).show();
                                }else {
                                    //DialogApproved("Order Not Accept, Something Went Wrong ");
                                }
                            }*/


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }


    }
}
